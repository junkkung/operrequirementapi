﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;

namespace Utilities
{
    public class Http
    {
        public static int GetIdentifier(ClaimsPrincipal user)
        {
            return int.Parse(user.Claims.First(c => c.Type == "identifier").Value);
        }

        public static string GetRole(ClaimsPrincipal user)
        {
            return user.Claims.First(c => c.Type == ClaimTypes.Role).Value;
        }

        public static string GetToken(HttpContext httpContext)
        {
            return httpContext.Request.Headers["Authorization"].First().Substring("Bearer ".Length).Trim();
        }

        public static Dictionary<string, string> Data (string message)
        {
            return new Dictionary<string, string>
            {
                { "data", message }
            };
        }
    }
}
