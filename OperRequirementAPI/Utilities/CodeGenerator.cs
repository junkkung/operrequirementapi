﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Utilities
{
    public class CodeGenerator
    {
        const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz";

        public static string[] Generate(string prefix, int codeRange, int total)
        {

            // code range need to more than 2
            Dictionary<string, string> codeGens = new Dictionary<string, string>();
            Random random = new Random();
            List<char> charsList = chars.ToCharArray().ToList();
            while (codeGens.Count < total)
            {
                int timeLoop = (int)Math.Floor(chars.Length / (double)codeRange);

                for (int i = 0; i < timeLoop; i++)
                {
                    string randomChar = string.Join("", charsList.OrderBy(item => random.Next()).ToList());
                    int pos = 0;
                    for (int j = 0; j < codeRange; j++)
                    {
                        string code = prefix + randomChar.Substring(pos, codeRange);
                        if (code.Length == (codeRange + prefix.Length) && !codeGens.ContainsKey(code))
                        {
                            codeGens.Add(code, code);
                        }

                        pos += codeRange;
                    }
                }
            }

            List<string> codes = codeGens.Values.ToList();
            codes = codes.OrderBy(item => random.Next()).ToList();
            codes.RemoveRange(total, codeGens.Count - total);
            return codes.ToArray();
        }
    }
}
