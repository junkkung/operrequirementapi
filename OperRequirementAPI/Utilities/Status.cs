﻿using Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Utilities
{
    public class Status
    {
        public class Active
        {
            public const int Id = 1;
            public const string Name = "active";
        }

        public class Inactive
        {
            public const int Id = 2;
            public const string Name = "inactive";
        }

        public static Dictionary<string, int> statusData = new Dictionary<string, int>()
        {
            {Active.Name, Active.Id },
            {Inactive.Name, Inactive.Id }
        };

        public static int ToId(string name)
        {
            try
            {
                return statusData[name];
            }
            catch (Exception ex)
            {
                //Logger.Log(Logger.error, ex.ToString());
                return 0;
            }

        }

        public static string ToString(int id)
        {
            try
            {
                return statusData.FirstOrDefault(x => x.Value == id).Key;
            }
            catch (Exception ex)
            {
                //Logger.Log(Logger.error, ex.ToString());
                return "";
            }
        }

        public class Is
        {
            public static bool Active(int statusId) => statusId == Status.Active.Id;
            public static bool InActive(int statusId) => statusId == Status.Inactive.Id;
        }

    }
}
