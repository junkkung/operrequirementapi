﻿using Database;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Utilities;

namespace Utilities
{
    public interface IToken
    {
        bool CanProcess(int requestId, ClaimsPrincipal user);
        bool IsInStructorRequest(ClaimsPrincipal user);
        bool IsStudentRequest(int requestId, ClaimsPrincipal user);
    }

    public class Token : IToken
    {
        private readonly IConfiguration appSetting;
        public Token(IConfiguration appSetting)
        {
            this.appSetting = appSetting;
        }

        public string Identifier { get; set; }
        public string Role { get; set; }
        public string JWTKey { get; set; }
        public string JWTIssuer { get; set; }
        public string JWTAudience { get; set; }
        public int ExpireInMinutes { get; set; }

        public string BuildAccess()
        {
            var claims = new[]
            {
                new Claim("identifier", this.Identifier),
                new Claim(ClaimTypes.Role, this.Role),
                new Claim("time", DateTime.Now.ToString("yyyy:MM:dd HH:mm:ss.fffffffK"))
            };

            return this.Create(claims, DateTime.Now.AddMinutes(this.ExpireInMinutes), this.JWTKey);
        }

        public string BuildRefresh()
        {
            var expireDate = DateTime.Now.AddMinutes(int.Parse(appSetting["Jwt:Refresh:ExpireInMinutes"]));
            var claims = new[]
            {
                new Claim("identifier", this.Identifier),
                new Claim(ClaimTypes.Role, TokenRole.Refresh),
                new Claim("time", DateTime.Now.ToString("yyyy:MM:dd HH:mm:ss.fffffffK"))
            };
            var refreshToken = this.Create(claims, expireDate, appSetting["Jwt:Refresh:Key"]);                       
            return refreshToken;
        }

        public bool CanProcess(int requestId, ClaimsPrincipal user)
        {
            return this.IsInStructorRequest(user) || this.IsStudentRequest(requestId, user);
        }

        public bool IsStudentRequest(int requestId, ClaimsPrincipal user)
        {
            var role = Http.GetRole(user);
            var id = Http.GetIdentifier(user);

            if (TokenRole.IsStudent(role) && requestId == id) return true;
            return false;
        }

        public bool IsInStructorRequest(ClaimsPrincipal user)
        {
            var role = Http.GetRole(user);
            return TokenRole.IsInstructor(role);
        }

        private string Create(Claim[] claims, DateTime expireDate, string key)
        {
            var encryptedKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(key));
            var creds = new SigningCredentials(encryptedKey, SecurityAlgorithms.HmacSha256);
            var token = new JwtSecurityToken(
                this.JWTIssuer,
                this.JWTAudience,
                claims,
                expires: expireDate,
                signingCredentials: creds
              );

            return new JwtSecurityTokenHandler().WriteToken(token);
        }
    }
}
