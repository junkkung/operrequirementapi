﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;

namespace Utilities
{
    public class TokenRole
    {
        public const string Refresh = "Refresh";
        public const string Instructor = "Instructor";
        public const string Student = "Student";

        public static bool IsRefresh(string role) => role == Refresh;
        public static bool IsInstructor(string role) => role == Instructor;
        public static bool IsStudent(string role) => role == Student;
    }
}
