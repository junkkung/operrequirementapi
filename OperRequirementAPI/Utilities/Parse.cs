﻿using System;
using System.Collections.Generic;
using System.Text;
using EN = Database.Entities;

namespace Utilities
{
    public class Parse
    {
        public static string ToSignature(string firstname, string lastname)
        {
            firstname = string.IsNullOrEmpty(firstname) ? "" : $"{firstname} ";
            lastname = string.IsNullOrEmpty(lastname) ? "" : $"{lastname.Substring(0, 1)}."; 
            return $"{firstname}{lastname}";
        }
    }
}
