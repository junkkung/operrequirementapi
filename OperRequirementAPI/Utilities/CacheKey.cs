﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EN = Database.Entities;

namespace Utilities
{
    public class CacheKey
    {
        private const string Prefix = "App";

        public class Config
        {
            private const string Key = Prefix + ".Config";
            public static string All() => $"{Key}.All";
        }

        public class Criteria
        {
            private const string Key = Prefix + ".Criteria";
            public static string Id(int id) => $"{Key}.Id.{id}.";
        }

        public class Critical
        {
            private const string Key = Prefix + ".Critical";
            public static string Id(int id) => $"{Key}.Id.{id}.";
        }

        public class Course
        {
            private const string Key = Prefix + ".Course";
            public static string Id(int id) => $"{Key}.Id.{id}";

            public static string All() => $"{Key}.All";
        }

        public class Experience
        {
            private const string Key = Prefix + ".Experience";
            public static string Id(int id) => $"{Key}.Id.{id}";

            public static string All() => $"{Key}.All";
        }

        public class Instructor
        {
            private const string Key = Prefix + ".Instructor";
            public static string Id(int id) => $"{Key}.Id.{id}.";
        }

        public class Laboratory
        {
            private const string Key = Prefix + ".Laboratory";
            public static string Id(int id) => $"{Key}.Id.{id}.";

            public static string Course(int courseId) => $"{Key}.Course.Id.{courseId}";
            public static string Student(int id, int studentId) => $"{Key}.Id.{id}.Student.Id.{studentId}";
            public static string Detail(int id) => $"{Key}.Id.{id}.Detail";
            public static string AllImage() => $"{Key}.AllImage";
        }

        public class MinusPoint
        {
            private const string Key = Prefix + ".MinusPoint";
            public static string Id(int id) => $"{Key}.Id.{id}";
        }

        public class Student
        {
            private const string Key = Prefix + ".Student";
            public static string Id(int id) => $"{Key}.Id.{id}.";

            public static string Course(int courseId) => $"{Key}.Course.{courseId}";
            public static string LaboratoryDetail(int id, int laboratoryId) => $"{Key}.Id.{id}.Laboratory.Id.{laboratoryId}";
            public static string Experience(int id) => $"{Key}.Id.{id}.Experience";
            public static string IsDoneAll(int id) => $"{Key}.Id.{id}.IsDoneAll";
            public static string LaboratoryImage(int id, int laboratoryId) => $"{Key}.Id.{id}.Laboratory.Id.{laboratoryId}.Images";
        }

        public class SubTopic
        {
            private const string Key = Prefix + ".SubTopic";
            public static string Id(int id) => $"{Key}.Id.{id}.";
        }

        public class Topic
        {
            private const string Key = Prefix + ".Topic";
            public static string Id(int id) => $"{Key}.Id.{id}.";
        }
    }
}
