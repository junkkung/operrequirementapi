﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Utilities
{
    public class Crypto
    {
        public static string EncodeBase64(string plainText)
        {
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
            return System.Convert.ToBase64String(plainTextBytes);
        }

        public static string DecodeBase64(string base64EncodedData)
        {
            var base64EncodedBytes = System.Convert.FromBase64String(base64EncodedData);
            return System.Text.Encoding.UTF8.GetString(base64EncodedBytes);
        }

        public static string EncodeSha256(string input)
        {
            System.Security.Cryptography.SHA256Managed crypt = new System.Security.Cryptography.SHA256Managed();
            System.Text.StringBuilder hash = new System.Text.StringBuilder();
            byte[] crypto = crypt.ComputeHash(Encoding.UTF8.GetBytes(input), 0, Encoding.UTF8.GetByteCount(input));
            foreach (byte theByte in crypto)
            {
                hash.Append(theByte.ToString("x2"));
            }

            return hash.ToString();
        }

        public static string EncryptPassword(IConfiguration appSetting, string username, string password)
        {
            return EncodeSha256($"{username}{appSetting["passwordSalt"]}{password}");
        }

        public static bool IsPasswordAcceptable(string password)
        {
            if (string.IsNullOrEmpty(password)) return false;
            if (password.Any(char.IsUpper) && password.Any(c => "!@#$%^&*()_+-<>{}[]`=?/\\:;|".Contains(c)) && password.Any(char.IsDigit) && password.Length >= 8)
                return true;
            else return false;
        }
    }
}
