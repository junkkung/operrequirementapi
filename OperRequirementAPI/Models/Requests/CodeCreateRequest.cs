﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Models.Requests
{
    public class CodeCreateRequest
    {
        [Required(AllowEmptyStrings = false)]
        public string Prefix { get; set; }
        [Required]
        [Range(4, 10)]
        public int Range { get; set; }
        [Required]
        [Range(1, 100000)]
        public int Total { get; set; }    }
}
