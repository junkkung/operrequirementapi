﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Models.Requests
{
    public class ProfileImageUploadRequest
    {
        [Required]
        [Range(1, int.MaxValue)]
        public int StudentId { get; set; }
        [Required(AllowEmptyStrings = false)]
        public string Base64 { get; set; }
        [Required(AllowEmptyStrings = false)]
        public string Name { get; set; }
        public int Size { get; set; }
    }
}
