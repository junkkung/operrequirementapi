﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Models.Requests
{
    public class InstuctorToothConfirmRequest
    {
        [Required(AllowEmptyStrings = false)]
        public string TeethNo { get; set; }
        [Required]
        [Range(1, int.MaxValue)]
        public int LaboratoryId { get; set; }
        public bool CanUse { get; set; }
        public bool CanNotUse { get; set; }
        public bool UseInLab { get; set; }
        public bool UseInControl { get; set; }
        public string Comment { get; set; }
        public int? ConfirmId { get; set; }
    }
}
