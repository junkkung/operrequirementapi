﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Models.Requests
{
    public class SubmitScoreRequest
    {
        [Required]
        [Range(1, int.MaxValue)]
        public int LaboratoryId { get; set; }
        [Required]
        [Range(1, int.MaxValue)]
        public int SubTopicId { get; set; }
        [Required]
        [Range(1, int.MaxValue)]
        public int CriteriaId { get; set; }
        public string Remark { get; set; }
    }
}
