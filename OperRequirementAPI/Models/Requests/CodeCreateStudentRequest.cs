﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Models.Requests
{
    public class CodeCreateStudentRequest : CodeCreateRequest
    {
        [Required]
        [Range(1, int.MaxValue)]
        public int CourseId { get; set; }
    }
}
