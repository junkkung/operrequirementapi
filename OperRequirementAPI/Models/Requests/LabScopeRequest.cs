﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Models.Requests
{
    public class LabScopeRequest
    {
        [Required]
        [Range(1, int.MaxValue)]
        public int LaboratoryId { get; set; }
        public string Tooth { get; set; }
        public string KindOfWork { get; set; }
        public string Material { get; set; }
    }
}
