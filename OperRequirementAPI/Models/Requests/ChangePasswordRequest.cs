﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Models.Requests
{
    public class ChangePasswordRequest
    {
        [Required(AllowEmptyStrings = false)]
        public string Username { get; set; }
        [Required(AllowEmptyStrings = false)]
        public string NewPassword { get; set; }
        [Required(AllowEmptyStrings = false)]
        public string ConfirmCode { get; set; }
    }
}
