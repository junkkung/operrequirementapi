﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Models.Requests
{
    public class MinusPointAddRequest
    {
        [Required]
        [Range(1, int.MaxValue)]
        public int MinusPointId { get; set; }
        [Required]
        [Range(1, int.MaxValue)]
        public int LaboratoryId { get; set; }
        [Required]
        public int Score { get; set; }
        public string Remark { get; set; }
    }
}
