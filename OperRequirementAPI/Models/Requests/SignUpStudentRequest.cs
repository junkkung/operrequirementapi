﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Models.Requests
{
    public class SignUpStudentRequest
    {
        [Required(AllowEmptyStrings = false)]
        public string Firstname { get; set; }
        [Required(AllowEmptyStrings = false)]
        public string Lastname { get; set; }
        [Required(AllowEmptyStrings = false)]
        public string Username { get; set; }
        [Required(AllowEmptyStrings = false)]
        public string Password { get; set; }
        //[Required(AllowEmptyStrings = false)]
        //public string Image { get; set; }
        [Required(AllowEmptyStrings = false)]
        public string Email { get; set; }
        public string Telephone { get; set; }
        [Required(AllowEmptyStrings = false)]
        public string UniversityIdNumber { get; set; }
        [Required]
        [Range(1, int.MaxValue)]
        public int Number { get; set; }
        [Required]
        [Range(1, int.MaxValue)]
        public int StudentGroupId { get; set; }
        [Required(AllowEmptyStrings = false)]
        public string ConfirmCode { get; set; }

        public T Map<T>()
        {
            var configuration = new MapperConfiguration(cfg => cfg.CreateMap<SignUpStudentRequest, T>());
            var mapper = configuration.CreateMapper();
            return mapper.Map<SignUpStudentRequest, T>(this);
        }
    }
}
