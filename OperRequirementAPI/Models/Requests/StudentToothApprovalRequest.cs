﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Models.Requests
{
    public class StudentToothApprovalRequest
    {
        public bool? IsApproved { get; set; }
        public string Comment { get; set; }
        public int ApprovalId { get; set; }
        [Required]
        [Range(1, int.MaxValue)]
        public int LaboratoryId { get; set; }
    }
}
