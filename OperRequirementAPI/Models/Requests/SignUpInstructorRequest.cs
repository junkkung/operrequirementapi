﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Models.Requests
{
    public class SignUpInstructorRequest
    {
        [Required(AllowEmptyStrings = false)]
        public string Firstname { get; set; }
        [Required(AllowEmptyStrings = false)]
        public string Lastname { get; set; }
        [Required(AllowEmptyStrings = false)]
        public string Username { get; set; }
        [Required(AllowEmptyStrings = false)]
        public string Password { get; set; }
        public string Image { get; set; }
        public string Email { get; set; }
        public string Telephone { get; set; }
        [Required(AllowEmptyStrings = false)]
        public string ConfirmCode { get; set; }

        public T Map<T>()
        {
            var configuration = new MapperConfiguration(cfg => cfg.CreateMap<SignUpInstructorRequest, T>());
            var mapper = configuration.CreateMapper();
            return mapper.Map<SignUpInstructorRequest, T>(this);
        }
    }
}
