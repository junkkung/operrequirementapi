﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Models.Requests
{
    public class StudentToothConfirmRemoveRequest
    {
        [Required]
        [Range(1, int.MaxValue)]
        public int LaboratoryId { get; set; }
        [Required]
        [Range(1, int.MaxValue)]
        public int ConfirmId { get; set; }
    }
}
