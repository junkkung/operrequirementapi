﻿using Database;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utilities;
using EN = Database.Entities;

namespace Models.Functions
{
    public interface ICourse
    {
        Task<EN.Course> Get(int id);
        Task<EN.Course> GetActive();
        Task<List<EN.Course>> Get();
        Task<bool> IsExpired(int id);
        bool IsExpired(EN.Course course);
    }

    public class Course : ICourse
    {
        private readonly OperRequirementContext context;

        public Course(OperRequirementContext context)
        {
            this.context = context;
        }

        public async Task<EN.Course> Get(int id)
        {
            return await context.Courses.Where(c => c.Id == id).AsNoTracking().FirstOrDefaultAsync(); ;
        }

        public async Task<List<EN.Course>> Get()
        {
            var courses = CacheManager.Get<List<EN.Course>>(CacheKey.Course.All());
            if (courses != null) return courses;
            courses = await context.Courses.Select(c => c)
                                            .AsNoTracking()
                                            .OrderByDescending(c => c.SemesterYear)
                                            .ToListAsync();
 
            
            CacheManager.Set(CacheKey.Course.All(), courses);            
            return courses;
        }

        public async Task<EN.Course> GetActive()
        {
            //var course = CacheManager.Get<EN.Course>(CacheKey.Course.Active());
            //if (course != null) return course;
            var courses = await this.Get();
            var course = courses.Where(c => c.StatusId == Status.Active.Id &&
                                            c.StartDate <= DateTime.Now &&
                                            c.EndDate >= DateTime.Now
                                        )
                                        .FirstOrDefault();

            return course;
        }

        public async Task<bool> IsExpired(int id)
        {
            var course = await Get(id);
            if (course is null) return false;
            return DateTime.Now > course.EndDate;
        }

        public bool IsExpired(EN.Course course)
        {
            return DateTime.Now > course.EndDate;
        }
    }
}
