﻿using Database;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utilities;
using EN = Database.Entities;

namespace Models.Functions
{
    public interface IExperience
    {
        Task<List<EN.Experience>> GetAll();
        bool IsAchieve(List<EN.SubTopic> subTopics, List<EN.StudentSubTopic> studentSubTopics);
    }

    public class Experience : IExperience
    {
        private readonly OperRequirementContext context;

        public Experience(OperRequirementContext context)
        {
            this.context = context;
        }

        public async Task<List<EN.Experience>> GetAll()
        {
            var cacheKey = CacheKey.Experience.All();
            var experiences = CacheManager.Get<List<EN.Experience>>(cacheKey);
            if (experiences != null) return experiences;

            experiences = await context.Experiences.Where(e => e.StatusId == Status.Active.Id)
                                                    .Include(e => e.Topics)
                                                        .ThenInclude(t => t.Laboratory)
                                                    .Include(e => e.Topics)
                                                        .ThenInclude(t => t.SubTopics)
                                                    .OrderBy(e => e.Sequence)
                                                    .AsNoTracking()
                                                    .ToListAsync();

            if (experiences is null || experiences?.Count == 0) return new List<EN.Experience>();
            CacheManager.Set(cacheKey, experiences);
            return experiences;
        }

        public bool IsAchieve(List<EN.SubTopic> subTopics, List<EN.StudentSubTopic> studentSubTopics)
        {
            if (subTopics is null || subTopics?.Count == 0) return false;
            if (studentSubTopics is null || studentSubTopics?.Count == 0) return false;

            var subTopicIds = subTopics.Select(s => s.Id).ToList();
            var concernStudentSubTopics = studentSubTopics.Where(ss => subTopicIds.Contains(ss.SubTopicId) &&
                                                ss.StatusId == Status.Active.Id                                                
                                            ).ToList();

            if (concernStudentSubTopics is null || concernStudentSubTopics?.Count == 0) return false;


            var studentScores = 0;
            foreach (var studentSubTopic in concernStudentSubTopics)
            {
                var studentScoreList = studentSubTopic.StudentScores?.Where(ss => Status.Is.Active(ss.StatusId))
                                                    .ToList();
                if (studentScoreList != null && studentScoreList.Count > 0) 
                    studentScores++;
            }
            
            return subTopics.Count == studentScores;
        }
    }
}
