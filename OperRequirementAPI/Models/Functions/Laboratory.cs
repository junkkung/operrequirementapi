﻿using Database;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UT = Utilities;
using EN = Database.Entities;
using Utilities;

namespace Models.Functions
{
    public interface ILaboratory
    {
        Task<EN.Laboratory> Get(int id);
        Task<EN.StudentLaboratory> GetStudentLab(int id, int studentId);
        Task<List<EN.Laboratory>> GetByCourseId(int courseId);
        Task<int> GetTotalScore(int laboratoryId);
        Task<EN.Laboratory> GetDetail(int id);
        Task<List<EN.LaboratoryImage>> GetAllLabImage();
        Task<bool> IsExpired(int id);
        bool IsExpired(EN.Laboratory laboratory);
    }

    public class Laboratory : ILaboratory
    {
        private readonly OperRequirementContext context;
        private readonly ITopic _topic;
        private readonly ICriteria _criteria;

        public Laboratory(OperRequirementContext context, ITopic topic, ICriteria criteria)
        {
            this.context = context;
            this._topic = topic;
            this._criteria = criteria;
        }

        public async Task<EN.Laboratory> Get(int id)
        {
            return await context.Laboratories.Where(c => c.Id == id).AsNoTracking().FirstOrDefaultAsync(); ;
        }

        public async Task<EN.StudentLaboratory> GetStudentLab(int id, int studentId)
        {
            var cacheKey = CacheKey.Laboratory.Student(id, studentId);
            var labStudent = CacheManager.Get<EN.StudentLaboratory>(cacheKey);
            labStudent = await context.StudentLaboratories.Where(sl => sl.StudentId == studentId &&
                                                        sl.LaboratoryId == id &&
                                                        sl.StatusId == Status.Active.Id
                                                    )
                                                    .Include(sl => sl.StudentSubTopics)
                                                        .ThenInclude(sst => sst.StudentScores)
                                                            .ThenInclude(ss => ss.Instructor)
                                                    .Include(sl => sl.StudentSubTopics)
                                                        .ThenInclude(sst => sst.SubTopic)
                                                            .ThenInclude(s => s.Topic)
                                                    .Include(sl => sl.Laboratory)
                                                    .Include(sl => sl.StudentMinusPoints)
                                                        .ThenInclude(sm => sm.Instructor)
                                                    .Include(sl => sl.StudentMinusPoints)
                                                        .ThenInclude(sm => sm.MinusPoint)
                                                    .Include(sl => sl.Instructor)
                                                    .AsNoTracking()
                                                    .FirstOrDefaultAsync();

            CacheManager.Set(cacheKey, labStudent);
            return labStudent;
        }

        public async Task<List<EN.Laboratory>> GetByCourseId(int courseId)
        {
            var cacheKey = CacheKey.Laboratory.Course(courseId);
            var laboratories = CacheManager.Get<List<EN.Laboratory>>(cacheKey);
            if (laboratories != null) return laboratories;
            laboratories = await context.Laboratories.Where(cl => cl.CourseId == courseId)
                                                        .Include(l => l.LaboratoryImages)
                                                        .AsNoTracking()
                                                        .OrderBy(l => l.Sequence)
                                                        .ToListAsync();

            CacheManager.Set(cacheKey, laboratories);
            return laboratories;
        }

        public async Task<int> GetTotalScore(int id)
        {
            var laboratory = await this.GetDetail(id);

            var topics = laboratory.Topics.Where(t => t.StatusId == Status.Active.Id).ToList();

            var subTopics = new List<EN.SubTopic>();
            foreach (var topic in topics)
                subTopics.AddRange(topic.SubTopics.Select(st => st).ToList());

            subTopics = subTopics.Where(s => s.StatusId == Status.Active.Id).ToList();

            var criterias = new List<EN.Criteria>();
            foreach (var subTopic in subTopics)
                criterias.AddRange(subTopic.Criterias.Select(c => c).ToList());

            criterias = criterias.Where(c => c.StatusId == Status.Active.Id).ToList();

            var totalScore = criterias.Sum(c => c.Score);
            return (int)totalScore;
        }

        public async Task<EN.Laboratory> GetDetail(int id)
        {
            var cacheKey = CacheKey.Laboratory.Detail(id);
            var laboratory = CacheManager.Get<EN.Laboratory>(cacheKey);
            if (laboratory != null) return laboratory;

            laboratory = await context.Laboratories.Where(l => l.Id == id && l.StatusId == UT.Status.Active.Id)
                                                .Include(l => l.Topics)
                                                    .ThenInclude(t => t.SubTopics)
                                                        .ThenInclude(s => s.Criterias)
                                                .Include(l => l.Topics)
                                                    .ThenInclude(t => t.SubTopics)
                                                        .ThenInclude(s => s.Critical)
                                                .Include(l => l.LaboratoryImages)
                                                .AsNoTracking()
                                                .FirstOrDefaultAsync();

            if (laboratory is null) return null;
            CacheManager.Set(cacheKey, laboratory);
            return laboratory;
        }

        public async Task<List<EN.LaboratoryImage>> GetAllLabImage()
        {
            var cacheKey = CacheKey.Laboratory.AllImage();
            var allLabImages = CacheManager.Get<List<EN.LaboratoryImage>>(cacheKey);

            if (allLabImages != null) return allLabImages;
            allLabImages = await context.LaboratoryImages.Select(l => l)
                                    .AsNoTracking()
                                    .ToListAsync();

            CacheManager.Set(cacheKey, allLabImages);
            return allLabImages;
        }

        public async Task<bool> IsExpired(int id)
        {
            var laboratory = await Get(id);
            return DateTime.Now > laboratory.EndDate;
        }

        public bool IsExpired(EN.Laboratory laboratory)
        {
            return DateTime.Now > laboratory.EndDate;
        }
    }
}
