﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Models.Functions
{   
    public interface IImage
    {
        string Save(string base64, string location, string name);
        bool IsFileFormatAcceptable(string name);
    }

    public class Image : IImage
    {
        private readonly IConfiguration appSetting;

        public Image(IConfiguration appSetting)
        {
            this.appSetting = appSetting;
        }

        public static List<string> Types = new List<string> { 
            "jpg", "jpeg" , "png", "tiff", "gif", "bmp", "heif"      
        };

        public string Save(string base64, string location, string name)
        {
            var path = $"{appSetting["imageLocation"]}\\{location}";
            if (!Directory.Exists(path))
                Directory.CreateDirectory(path);

            var imageByteArray = Convert.FromBase64String(base64);
            var formattedName = FormatName(name);
            var fullPath = $"{appSetting["imageLocation"]}\\{location}\\{formattedName}";
            File.WriteAllBytes(fullPath, imageByteArray);

            return ConvertToHtmlPath(fullPath);
        }

        public bool IsFileFormatAcceptable(string name)
        {
            if (name.Length < 5) return false;
            var length = name.Length > 5 ? 5 : 4;
            var focus = name.Substring(name.Length - length, length);
            var type = focus.Split('.');

            if (type.Length != 2) return false;
            if (Types.Contains(type[1].ToLower())) return true;
            return false;
        }

        private string ConvertToHtmlPath(string location)
        {            
            var url = location.Replace("C:\\inetpub\\wwwroot\\", appSetting["ProjectRootURL"]);
            url = url.Replace("\\", "/");
            return url;
        }

        private string FormatName(string name)
        {
            return $"{DateTime.Now.ToString("yyyy-MM-dd_HH-mm")}_{name}";
        }
    }
}
