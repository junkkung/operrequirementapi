﻿using Database;
using System;
using System.Collections.Generic;
using System.Text;
using UT = Utilities;
using EN = Database.Entities;
using Utilities;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace Models.Functions
{
    public interface ISubTopic
    {
        Task<EN.SubTopic> Get(int id);
    }

    public class SubTopic: ISubTopic
    {
        private readonly OperRequirementContext context;

        public SubTopic(OperRequirementContext context)
        {
            this.context = context;
        }

        public async Task<EN.SubTopic> Get(int id)
        {
            return await context.SubTopics.Where(c => c.Id == id).AsNoTracking().FirstOrDefaultAsync(); ;
        }
    }
}
