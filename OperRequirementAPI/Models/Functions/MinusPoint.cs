﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UT = Utilities;
using EN = Database.Entities;
using Utilities;
using Database;
using Microsoft.EntityFrameworkCore;

namespace Models.Functions
{
    public interface IMinusPoint
    {
        Task<EN.MinusPoint> Get(int id);
        Task<List<EN.MinusPoint>> Get();
        Task<int> GetMaxMinusPointPerTime();
        Task<int> Calculate(int score);
    }

    public class MinusPoint : IMinusPoint
    {
        private readonly OperRequirementContext context;
        private readonly IConfig _config;

        public MinusPoint(OperRequirementContext context, IConfig config)
        {
            this.context = context;
            this._config = config;
        }

        public async Task<List<EN.MinusPoint>> Get()
        {
            return await context.MinusPoints.Select(m => m).AsNoTracking().ToListAsync();
        }

        public async Task<EN.MinusPoint> Get(int id)
        {
            return await context.MinusPoints.Where(c => c.Id == id).AsNoTracking().FirstOrDefaultAsync(); ;
        }

        public async Task<int> GetMaxMinusPointPerTime()
        {
            return await _config.Get<int>(Config.MaxMinusPointPerTime);
        }

        public async Task<int> Calculate(int score)
        {
            return (await this.GetMaxMinusPointPerTime()) - score;
        } 

    }
}
