﻿using Database;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UT = Utilities;
using EN = Database.Entities;
using Utilities;
using System.Threading.Tasks;

namespace Models.Functions
{
    public interface ICriteria
    {
        Task<EN.Criteria> Get(int id);
    }

    public class Criteria : ICriteria
    {
        private readonly OperRequirementContext context;

        public Criteria(OperRequirementContext context)
        {
            this.context = context;
        }

        public class Type
        {
            public class Good
            {
                public const int Id = 1;
                public const string name = "Good";
            }

            public class FairOrUnAcceptable
            {
                public const int Id = 2;
                public const string name = "FairOrUnAcceptable";
            }

            public static Dictionary<string, int> CriteriaTypeData = new Dictionary<string, int>
            {
                {Good.name, Good.Id },
                {FairOrUnAcceptable.name, FairOrUnAcceptable.Id },
            };
        }

        public async Task<EN.Criteria> Get(int id)
        {
            return await context.Criterias.Where(c => c.Id == id).AsNoTracking().FirstOrDefaultAsync();
        }
    }
}
