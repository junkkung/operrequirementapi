﻿using Database;
using Database.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;
using Microsoft.Extensions.Configuration;
using Models.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Utilities;
using UT = Utilities;
using EN = Database.Entities;
using Models.Requests;

namespace Models.Functions
{
    public interface IAccount
    {
        string Username { get; set; }
        string Password { get; set; }
        int Identifier { get; set; }
        string RefreshToken { get; set; }
        Task<HttpCode<LoginResponse>> LoginInstructor();
        Task<HttpCode<LoginResponse>> RefreshInstructor();
        Task<HttpCode<string>> ChangePasswordInstructor(ChangePasswordRequest request);
        Task<HttpCode<LoginResponse>> LoginStudent();
        Task<HttpCode<LoginResponse>> RefreshStudent();
        Task<HttpCode<string>> ChangePasswordStudent(ChangePasswordRequest request);
    }

    public class Account : IAccount
    {
        private readonly OperRequirementContext context;
        private readonly IConfiguration appSetting;
        private readonly IInstructor _instructor;
        private readonly IStudent _student;

        public Account (OperRequirementContext context, IConfiguration appSetting, IInstructor instructor, IStudent student) 
        {
            this.context = context;
            this.appSetting = appSetting;
            this._instructor = instructor;
            this._student = student;
        }

        public string Username { get; set; }
        public string Password { get; set; }
        public int Identifier { get; set; }
        public string RefreshToken { get; set; }

        public async Task<HttpCode<LoginResponse>> LoginInstructor()
        {
            var username = this.Username.ToLower().Trim();
            var instructor = await context.Instructors.Where(i => (i.Username.ToLower() == username &&
                                                    i.StatusId == UT.Status.Active.Id)
                                                )
                                                .AsNoTracking()
                                                .FirstOrDefaultAsync();
            if (instructor is null) return new HttpCode<LoginResponse> { StatusCode = HttpStatusCode.NotFound };
            if (instructor.Password != Crypto.EncryptPassword(this.appSetting, username, this.Password))
                return new HttpCode<LoginResponse> { StatusCode = HttpStatusCode.BadRequest };

            return await BuildToken(instructor);
        }

        public async Task<HttpCode<LoginResponse>> RefreshInstructor()
        {
            var instructor = await _instructor.Get(this.Identifier);
            if (instructor is null || instructor.StatusId != UT.Status.Active.Id || instructor.RefreshToken != RefreshToken)
            {
                return new HttpCode<LoginResponse> { StatusCode = HttpStatusCode.BadRequest };
            }

            return await BuildToken(instructor);
        }

        public async Task<HttpCode<string>> ChangePasswordInstructor(ChangePasswordRequest request)
        {
            var username = request.Username.ToLower().Trim();
            var newPassword = request.NewPassword.Trim();
            if (!(await _instructor.IsUsernameExisting(username)))
                return new HttpCode<string> { StatusCode = HttpStatusCode.NotFound, Object = "User not found" };

            var confirmCode = await _instructor.GetConfirmCode(request.ConfirmCode.Trim());
            if (confirmCode is null) 
                return new HttpCode<string> { StatusCode = HttpStatusCode.NotFound, Object = "Confirm code is not available" };
            if (!Crypto.IsPasswordAcceptable(newPassword))
                return new HttpCode<string> { StatusCode = HttpStatusCode.BadRequest, Object = "Password is wrong format." };

            var instructor = await context.Instructors.Where(i => i.Username.ToLower() == username)
                                            .AsNoTracking()
                                            .FirstOrDefaultAsync();
            instructor.Password = Crypto.EncryptPassword(appSetting, username, newPassword);
            context.Update(instructor);
            context.Remove(confirmCode);
            await context.SaveChangesAsync();
            context.Entry(instructor).State = EntityState.Detached;

            return new HttpCode<string> { StatusCode = HttpStatusCode.OK, Object = "Password is already updated." };

        }

        public async Task<HttpCode<LoginResponse>> LoginStudent()
        {
            var username = this.Username.ToLower().Trim();
            var student = await context.Students.Where(i => (i.Username.ToLower() == username &&
                                                    i.StatusId == UT.Status.Active.Id)
                                                )
                                                .AsNoTracking()
                                                .FirstOrDefaultAsync();
            if (student is null) return new HttpCode<LoginResponse> { StatusCode = HttpStatusCode.NotFound };
            if (student.Password != Crypto.EncryptPassword(this.appSetting, username, this.Password))
                return new HttpCode<LoginResponse> { StatusCode = HttpStatusCode.BadRequest };

            return await BuildToken(student);
        }

        public async Task<HttpCode<LoginResponse>> RefreshStudent()
        {
            var student = await _student.Get(this.Identifier);
            if (student is null || student.StatusId != UT.Status.Active.Id || student.RefreshToken != RefreshToken)
            {
                return new HttpCode<LoginResponse> { StatusCode = HttpStatusCode.BadRequest };
            }

            return await BuildToken(student);
        }

        public async Task<HttpCode<string>> ChangePasswordStudent(ChangePasswordRequest request)
        {
            var username = request.Username.ToLower().Trim();
            var newPassword = request.NewPassword.Trim();
            if (!(await _student.IsUsernameExisting(username)))
                return new HttpCode<string> { StatusCode = HttpStatusCode.NotFound, Object = "User not found" };

            var confirmCode = await _student.GetConfirmCode(request.ConfirmCode.Trim());
            if (confirmCode is null)
                return new HttpCode<string> { StatusCode = HttpStatusCode.NotFound, Object = "Confirm code is not available" };
            if (!Crypto.IsPasswordAcceptable(newPassword))
                return new HttpCode<string> { StatusCode = HttpStatusCode.BadRequest, Object = "Password is wrong format." };

            var student = await context.Students.Where(i => i.Username.ToLower() == username)
                                            .AsNoTracking()
                                            .FirstOrDefaultAsync();
            student.Password = Crypto.EncryptPassword(appSetting, username, newPassword);
            context.Update(student);
            context.Remove(confirmCode);
            await context.SaveChangesAsync();
            context.Entry(student).State = EntityState.Detached;

            return new HttpCode<string> { StatusCode = HttpStatusCode.OK, Object = "Password is already updated." };

        }

        private async Task<HttpCode<LoginResponse>> BuildToken(EN.Instructor instructor)
        {
            var token = new Token(appSetting)
            {
                Identifier = instructor.Id.ToString(),
                JWTIssuer = appSetting["Jwt:Issuer"],
                JWTAudience = TokenRole.Instructor,
                JWTKey = appSetting[$"Jwt:Instructor:Key"],
                ExpireInMinutes = int.Parse(appSetting[$"Jwt:ExpireInMinutes"]),
                Role = appSetting[$"Jwt:Instructor:Audience"],
            };

            var accessToken = token.BuildAccess();
            var refreshToken = token.BuildRefresh();

            instructor.RefreshToken = refreshToken;
            context.Update(instructor);
            await context.SaveChangesAsync();
            context.Entry(instructor).State = EntityState.Detached;

            return new HttpCode<LoginResponse>
            {
                StatusCode = HttpStatusCode.OK,
                Object = new LoginResponse
                {
                    AccessToken = accessToken,
                    RefreshToken = refreshToken,
                }
            };
        }

        private async Task<HttpCode<LoginResponse>> BuildToken(EN.Student student)
        {
            var token = new Token(appSetting)
            {
                Identifier = student.Id.ToString(),
                JWTIssuer = appSetting["Jwt:Issuer"],
                JWTAudience = TokenRole.Student,
                JWTKey = appSetting[$"Jwt:Student:Key"],
                ExpireInMinutes = int.Parse(appSetting[$"Jwt:ExpireInMinutes"]),
                Role = appSetting[$"Jwt:Student:Audience"],
            };

            var accessToken = token.BuildAccess();
            var refreshToken = token.BuildRefresh();

            student.RefreshToken = refreshToken;
            context.Update(student);
            await context.SaveChangesAsync();
            context.Entry(student).State = EntityState.Detached;

            return new HttpCode<LoginResponse>
            {
                StatusCode = HttpStatusCode.OK,
                Object = new LoginResponse
                {
                    AccessToken = accessToken,
                    RefreshToken = refreshToken,
                    StudentId = student.Id
                }
            };
        }
    }
}
