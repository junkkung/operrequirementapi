﻿using Database;
using System;
using System.Collections.Generic;
using System.Text;
using UT = Utilities;
using EN = Database.Entities;
using Utilities;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace Models.Functions
{
    public interface ITopic
    {
        Task<EN.Topic> Get(int id);
        Task<bool> IsExpired(int id);
        bool IsExpired(EN.Topic topic);
    }

    public class Topic : ITopic
    {
        private readonly OperRequirementContext context;

        public Topic(OperRequirementContext context)
        {
            this.context = context;
        }

        public async Task<EN.Topic> Get(int id)
        {
            return await context.Topics.Where(c => c.Id == id).AsNoTracking().FirstOrDefaultAsync(); ;
        }

        public async Task<bool> IsExpired(int id)
        {
            var topic = await Get(id);
            if (topic is null) return false;
            return DateTime.Now > topic.EndDate;
        }

        public bool IsExpired(EN.Topic topic)
        {
            return DateTime.Now > topic.EndDate;
        }
    }
}
