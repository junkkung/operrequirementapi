﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using UT = Utilities;
using EN = Database.Entities;
using Utilities;
using Database;
using Microsoft.EntityFrameworkCore;

namespace Models.Functions
{
    public interface IConfig
    {
        Task<List<EN.Config>> Get();
        Task<T> Get<T>(int id);
    }

    public class Config : IConfig
    {
        private readonly OperRequirementContext context;

        public Config (OperRequirementContext context)
        {
            this.context = context;
        }

        public const int InitialMinusPoint = 1;
        public const int MaxMinusPointPerTime = 2;

        public async Task<List<EN.Config>> Get()
        {
            var cacheKey = CacheKey.Config.All();
            var configs = CacheManager.Get <List<EN.Config>>(cacheKey);
            if (configs != null) return configs;

            configs =  await context.Configs.AsNoTracking().ToListAsync();
            CacheManager.Set(cacheKey, configs);
            return configs;
        }

        public async Task<T> Get<T>(int id)
        {
            var configs = await this.Get();
            if (configs is null || configs?.Count == 0) return default;
            var value = configs.Where(c => c.Id == id)
                                .Select(c => c.Value)                                         
                                .FirstOrDefault();
            return (T)Convert.ChangeType(value, typeof(T));
        }
    }
}
