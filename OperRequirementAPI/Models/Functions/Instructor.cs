﻿using Database;
using EN = Database.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using Utilities;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Models.Requests;
using Models.Responses;
using System.Net;
using Microsoft.Extensions.Configuration;

namespace Models.Functions
{
    public interface IInstructor
    {
        Task<EN.Instructor> Get(int id);
        Task<HttpCode<string>> SignUp(SignUpInstructorRequest request);
        Task<bool> IsUsernameExisting(string username);
        Task<EN.InstructorConfirmCode> GetConfirmCode(string confirmCodeRequest);
        Task<bool> IsAdmin(int id);
    }

    public class Instructor : IInstructor
    {
        private readonly OperRequirementContext context;
        private readonly IConfiguration appSetting;
        
        public Instructor (OperRequirementContext context, IConfiguration appSetting)
        {
            this.context = context;
            this.appSetting = appSetting;
        }

        public async Task<EN.Instructor> Get(int id)
        {
            return await context.Instructors.Where(c => c.Id == id).AsNoTracking().FirstOrDefaultAsync(); ;
        }

        public async Task<HttpCode<string>> SignUp(SignUpInstructorRequest request)
        {
            var username = request.Username.ToLower().Trim();
            var password = request.Password.Trim();
            if (await IsUsernameExisting(username)) 
                return new HttpCode<string> { StatusCode = HttpStatusCode.Conflict, Object = "Username is already used" };
            if (!Crypto.IsPasswordAcceptable(password))
                return new HttpCode<string> { StatusCode = HttpStatusCode.BadRequest, Object = "Password is wrong format." };

            var confirmCode = await GetConfirmCode(request.ConfirmCode.Trim());
            if (confirmCode is null) 
                return new HttpCode<string> { StatusCode = HttpStatusCode.NotFound, Object = "Confirmation code is not available"};

            var instructor = request.Map<EN.Instructor>();
            instructor.Username = username;
            instructor.StatusId = Status.Active.Id;
            instructor.Password = Crypto.EncryptPassword(appSetting, username, password);
            await context.AddAsync(instructor);
            context.Remove(confirmCode);
            await context.SaveChangesAsync();
            return new HttpCode<string> { StatusCode = HttpStatusCode.Created, Object = "" };
        }

        public async Task<bool> IsUsernameExisting(string username)
        {
            var instructor = await context.Instructors.Where(i => i.Username.ToLower() == username.ToLower())
                                                    .AsNoTracking()
                                                    .FirstOrDefaultAsync();
            return instructor != null;
        }

        public async Task<EN.InstructorConfirmCode> GetConfirmCode(string confirmCodeRequest)
        {
            var confirmCode = await context.InstructorConfirmCodes.Where(ic => ic.Code == confirmCodeRequest)
                                                    .AsNoTracking()
                                                    .FirstOrDefaultAsync();
            return confirmCode;
        }
        
        public async Task<bool> IsAdmin(int id)
        {
            var instructor = await Get(id);
            return instructor.IsAdmin ?? false;
        }
    }
}
