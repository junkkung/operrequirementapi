﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Database;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Models.Requests;
using Models.Responses;
using Utilities;
using EN = Database.Entities;

namespace Models.Functions
{
    public interface IStudent
    {
        Task<EN.Student> Get(int id);
        Task<List<EN.StudentScore>> GetScore(int id, int laboratoryId);
        Task<HttpCode<string>> SignUp(SignUpStudentRequest request);
        Task<bool> IsUsernameExisting(string username);
        Task<EN.StudentConfrimCode> GetConfirmCode(string confirmCodeRequest);
        Task<List<EN.Student>> GetByCourseId(int courseId);
        List<GroupStudentResponse> GroupByStudentGroup(List<EN.Student> students);
        Task<HttpCode<StudentScoreSummaryResponse>> SummaryScore(int studentId);
        int LabScoreSummary(List<EN.StudentScore> scores, EN.Laboratory laboratory);
        Task<List<EN.StudentMinusPoint>> GetMinusPoint(int id);
        Task<List<EN.StudentMinusPoint>> GetMinusPoint(int id, int laboratoryId);
        Task<HttpCode<StudentLaboratoryDetailResponse>> GetLabDetail(int id, int laboratoryId);
        Task<HttpCode<List<StudentExperienceResponse>>> GetExperience(int id);
        bool IsOnlyNotActiveTopic(List<EN.Topic> topics);
        Task<bool> IsDoneAll(int id);
        Task<HttpCode<string>> AddMinusPoint(int id, int instructorId, MinusPointAddRequest request);
        Task<HttpCode<string>> RemoveMinusPoint(int id, int instructorId, int studentMinusPointId);
        Task<HttpCode<string>> CreateOrUpdateSelfAssessment(int id, SelfAssessmentRequest request);
        Task<HttpCode<string>> CreateOrUpdateLabScope(int id, LabScopeRequest request);
        Task<HttpCode<string>> SubmitScore(int id, int instructorId, SubmitScoreRequest request);
        Task<HttpCode<string>> SubmitCritical(int id, int instructorId, SubmitCriticalRequest request);
        Task<HttpCode<string>> CancelCritical(int id, SubmitCriticalRequest request);
        Task<HttpCode<List<LaboratoryImageResponse>>> GetLabImage(int id, int laboratoryId);
        Task SaveLabScopeImage(int id, int laboratoryId, string url);
        Task SaveLabImage(int id, int laboratoryId, int labImageId, string url);
        Task<HttpCode<string>> ConfirmLabImage(int instructorId, int laboratoryId, int studentLabImageId);
        Task<HttpCode<string>> ConfirmLabScopeImage(int id, int instructorId, int laboratoryId);
        Task<HttpCode<string>> StudentUpdateToothConfirm(int id, StudentToothConfirmRequest request);
        Task<HttpCode<string>> InstructorUpdateToothConfirm(int id, InstuctorToothConfirmRequest request, int instructorId);
        Task<HttpCode<string>> RemoveToothConfirm(int id, StudentToothConfirmRemoveRequest request);
        Task<HttpCode<List<StudentToothConfirmResponse>>> GetToothConfirm(int id, int laboratoryId);
        Task<HttpCode<string>> CreateToothConfirm(int id, int laboratoryId);
        Task<HttpCode<StudentToothApprovalResponse>> GetToothApproval(int id, int laboratoryId);
        Task<HttpCode<string>> CreateOrUpdateToothApproval(int id, int instructorId, StudentToothApprovalRequest request);
    }

    public class Student : IStudent
    {
        private readonly OperRequirementContext context;
        private readonly IConfiguration appSetting;
        private readonly ICourse _course;
        private readonly ILaboratory _laboratory;
        private readonly IConfig _config;
        private readonly IExperience _experience;
        private readonly ISubTopic _subTopic;
        private readonly IMinusPoint _minusPoint;

        public Student(OperRequirementContext context, IConfiguration appSetting, ICourse course,
                        ILaboratory laboratory, IConfig config, IExperience experience,
                        ISubTopic subTopic, IMinusPoint minusPoint)
        {
            this.context = context;
            this.appSetting = appSetting;
            this._course = course;
            this._laboratory = laboratory;
            this._config = config;
            this._experience = experience;
            this._subTopic = subTopic;
            this._minusPoint = minusPoint;
        }

        public async Task<EN.Student> Get(int id)
        {
            return await context.Students.Where(c => c.Id == id).AsNoTracking().FirstOrDefaultAsync(); ;
        }

        public async Task<List<EN.StudentScore>> GetScore(int id, int laboratoryId)
        {
            var labStudent = await _laboratory.GetStudentLab(laboratoryId, id);
            if (labStudent is null) return null;
            var studentScoresList = labStudent.StudentSubTopics.Where(sst => sst.StatusId == Status.Active.Id)
                                                    .Select(sst => sst.StudentScores)
                                                    .ToList();
            var studentScores = new List<EN.StudentScore>();
            foreach (var list in studentScoresList)
                studentScores.AddRange(list.Where(ss => ss.StatusId == Status.Active.Id).ToList());

            return studentScores;
        }

        public async Task<List<EN.Student>> GetByCourseId(int courseId)
        {
            var cacheKey = CacheKey.Student.Course(courseId);
            var students = CacheManager.Get<List<EN.Student>>(cacheKey);
            if (students != null) return students;
            students = await context.Students.Where(s => s.CourseId == courseId)
                                            .AsNoTracking()
                                            .ToListAsync();

            CacheManager.Set(cacheKey, students);
            return students;
        }

        public List<GroupStudentResponse> GroupByStudentGroup(List<EN.Student> students)
        {
            if (students is null || students?.Count == 0) return null;
            var responses = new List<GroupStudentResponse>();
            var groups = students.OrderBy(s => s.Group).GroupBy(s => s.Group);
            foreach (var group in groups)
            {
                var studentResponses = group.AsEnumerable().Select(s => new StudentResponse(s)).ToList();
                var response = new GroupStudentResponse
                {
                    StudentGroupId = group.Key,
                    Students = studentResponses
                };
                responses.Add(response);
            }

            return responses;
        }

        public async Task<HttpCode<string>> SignUp(SignUpStudentRequest request)
        {
            var username = request.Username.ToLower().Trim();
            var password = request.Password.Trim();
            if (await IsUsernameExisting(username))
                return new HttpCode<string> { StatusCode = HttpStatusCode.Conflict, Object = "Username is already used" };
            if (!Crypto.IsPasswordAcceptable(password))
                return new HttpCode<string> { StatusCode = HttpStatusCode.BadRequest, Object = "Password is wrong format." };

            var confirmCode = await GetConfirmCode(request.ConfirmCode.Trim());
            if (confirmCode is null)
                return new HttpCode<string> { StatusCode = HttpStatusCode.NotFound, Object = "Confirmation code is not available" };

            var activeCourse = await _course.GetActive();
            if (activeCourse is null)
                return new HttpCode<string> { StatusCode = HttpStatusCode.NotFound, Object = "Course does not open for sign up." };

            var student = request.Map<EN.Student>();
            student.Username = username;
            student.CourseId = activeCourse.Id;
            student.Group = request.StudentGroupId;
            student.StatusId = Status.Active.Id;
            student.Password = Crypto.EncryptPassword(appSetting, username, password);
            await context.AddAsync(student);
            context.Remove(confirmCode);
            await context.SaveChangesAsync();
            CacheManager.Remove(CacheKey.Course.Id(activeCourse.Id));
            return new HttpCode<string> { StatusCode = HttpStatusCode.Created, Object = "" };
        }

        public async Task<bool> IsUsernameExisting(string username)
        {
            var student = await context.Students.Where(i => i.Username.ToLower() == username.ToLower())
                                                    .AsNoTracking()
                                                    .FirstOrDefaultAsync();
            return student != null;
        }

        public async Task<EN.StudentConfrimCode> GetConfirmCode(string confirmCodeRequest)
        {
            var course = await _course.GetActive();
            if (course is null) return null;
            var confirmCode = await context.StudentConfrimCodes.Where(sc => sc.Code == confirmCodeRequest &&
                                                        sc.CourseId == course.Id
                                                    )
                                                    .AsNoTracking()
                                                    .FirstOrDefaultAsync();  

            return confirmCode;
        }

        public async Task<HttpCode<StudentScoreSummaryResponse>> SummaryScore(int id)
        {
            var student = await Get(id);
            if (student is null || Status.Is.InActive(student.StatusId))
                return new HttpCode<StudentScoreSummaryResponse> { StatusCode = HttpStatusCode.NotFound, Object = null };

            var laboratories = await _laboratory.GetByCourseId(student.CourseId);

            var response = new StudentScoreSummaryResponse
            {
                LaboratoriesSummary = new List<StudentScoreSummaryResponse.LabResponse>()
            };

            foreach (var laboratory in laboratories)
            {
                var scores = await this.GetScore(id, laboratory.Id);
                var summary = new StudentScoreSummaryResponse.LabResponse
                {
                    LaboratoryResponse = new LaboratoryResponse(laboratory),
                    Score = this.LabScoreSummary(scores, laboratory),
                    IsLabDone = true
                };

                summary.LaboratoryResponse.TotalScore = await _laboratory.GetTotalScore(laboratory.Id);
                summary.LaboratoryResponse.HasLabImage = await HasLabImage(id, laboratory);

                var labDetail = await this.GetLabDetail(id, laboratory.Id);
                foreach (var topic in labDetail?.Object?.Topics)
                {
                    summary.IsLabDone &= topic.AllScored;
                }

                response.TotalScore += summary.LaboratoryResponse.TotalScore;
                response.SummaryScore += summary.Score;
                response.LaboratoriesSummary.Add(summary);

                var studentMinusPoints = await this.GetMinusPoint(id, laboratory.Id);
                for (var i = 0; i < studentMinusPoints.Count; i++)
                {
                    studentMinusPoints[i].Score = await _minusPoint.Calculate(studentMinusPoints[i].Score);
                }

                response.TotalMinusPoint += studentMinusPoints.Sum(m => m.Score);

            }

            response.TotalMinusPoint -= (await _config.Get<int>(Config.InitialMinusPoint));
            response.NetScore = response.SummaryScore + response.TotalMinusPoint;

            return new HttpCode<StudentScoreSummaryResponse> { StatusCode = HttpStatusCode.OK, Object = response };
        }

        public int LabScoreSummary(List<EN.StudentScore> scores, EN.Laboratory laboratory)
        {
            if (scores is null || scores?.Count == 0 ||
               (scores?.Any(s => DateTime.SpecifyKind(s.CreateDate.Date, DateTimeKind.Utc) >
               laboratory.EndDate) ?? false))
                return 0;

            var groupByTopic = scores.GroupBy(s => s.StudentSubTopic.SubTopic.Topic.Id);
            var totalScore = 0;
            foreach (var topicGroup in groupByTopic)
            {
                var topicScores = topicGroup.ToList();
                if (topicScores is null || topicScores?.Count == 0 ||
                   (topicScores?.Any(ts => DateTime.SpecifyKind(ts.CreateDate.Date, DateTimeKind.Utc) >
                    ts.StudentSubTopic.SubTopic.Topic.EndDate) ?? false))
                    continue;

                var criticalSubTopicIds = topicScores.Where(ts => ts.IsCritical)
                                                .Select(ts => ts.StudentSubTopic.SubTopicId)
                                                .ToList();

                var countableScore = topicScores.Where(ts => !criticalSubTopicIds.Contains(ts.StudentSubTopic.SubTopicId))
                                                .ToList();

                totalScore += countableScore.Sum(s => s.Score);
            }

            return totalScore;
        }

        public async Task<List<EN.StudentMinusPoint>> GetMinusPoint(int id)
        {
            var student = await Get(id);
            if (student is null || Status.Is.InActive(student.StatusId)) return null;

            var laboratories = await _laboratory.GetByCourseId(student.CourseId);
            var studentMinusPoints = new List<EN.StudentMinusPoint>();
            foreach (var laboratory in laboratories)
            {
                var minusPoints = await this.GetMinusPoint(id, laboratory.Id);
                if (minusPoints != null) studentMinusPoints.AddRange(minusPoints);
            }

            return studentMinusPoints.OrderByDescending(m => m.CreateDate).ToList();
        }

        public async Task<List<EN.StudentMinusPoint>> GetMinusPoint(int id, int laboratoryId)
        {
            var studentLab = await _laboratory.GetStudentLab(laboratoryId, id);
            if (studentLab is null) return new List<EN.StudentMinusPoint>();
            var studentMinusPoints = studentLab.StudentMinusPoints.Where(sm => sm.StatusId == Status.Active.Id)
                                                                .ToList();

            return studentMinusPoints;
        }

        public async Task<HttpCode<StudentLaboratoryDetailResponse>> GetLabDetail(int id, int laboratoryId)
        {
            var student = await Get(id);
            if (student is null || Status.Is.InActive(student.StatusId))
                return new HttpCode<StudentLaboratoryDetailResponse> { StatusCode = HttpStatusCode.NotFound, Object = null };

            var cacheKey = CacheKey.Student.LaboratoryDetail(id, laboratoryId);
            var response = CacheManager.Get<StudentLaboratoryDetailResponse>(cacheKey);
            if (response != null) return new HttpCode<StudentLaboratoryDetailResponse>
            {
                StatusCode = HttpStatusCode.OK,
                Object = response
            };

            var laboratory = await _laboratory.GetDetail(laboratoryId);

            if (laboratory is null)
                return new HttpCode<StudentLaboratoryDetailResponse> { StatusCode = HttpStatusCode.NotFound, Object = null };

            var studentLaboratory = await _laboratory.GetStudentLab(laboratoryId, id);
            response = new StudentLaboratoryDetailResponse(studentLaboratory);
            if (response is null) response = new StudentLaboratoryDetailResponse();
            response.Laboratory = new LaboratoryResponse(laboratory);
            response.Topics = new List<StudentLaboratoryDetailResponse.TopicResponse>();
            response.InstructorName = studentLaboratory?.InstructorConfirmId is null ? null :
                                            $"{studentLaboratory.Instructor.Firstname} {studentLaboratory.Instructor.Lastname}";

            var topics = laboratory.Topics.OrderBy(t => t.Sequence);
            foreach (var topic in topics)
            {
                if (!Status.Is.Active(topic.StatusId)) continue;
                var topicResponse = new StudentLaboratoryDetailResponse.TopicResponse(topic)
                {
                    SubTopics = new List<StudentLaboratoryDetailResponse.SubTopicResponse>()
                };

                var score = 0;
                var totalScore = 0;
                var scored = 0;
                var subTopics = topic.SubTopics.OrderBy(s => s.Sequence);
                foreach (var subTopic in subTopics)
                {
                    var isScored = false;
                    if (!Status.Is.Active(subTopic.StatusId)) continue;
                    //var subTopicResponse = StudentLaboratoryDetailResponse.SubTopicResponse.Map(subTopic);
                    var subTopicResponse = new StudentLaboratoryDetailResponse.SubTopicResponse(subTopic)
                    {
                        Critical = new StudentLaboratoryDetailResponse.CriticalResponse(subTopic.Critical)
                    };

                    var studentSubtopic = studentLaboratory?.StudentSubTopics.Where(sst => sst?.SubTopicId == subTopic.Id).FirstOrDefault();
                    var studentScores = new List<EN.StudentScore>();
                    if (studentSubtopic != null)
                    {
                        subTopicResponse.SelfAssessment = studentSubtopic.SelfAssessment;

                        studentScores = studentSubtopic.StudentScores.Where(ss => ss.StatusId == Status.Active.Id).ToList();
                        if (subTopicResponse.Critical != null && studentScores.Any(ss => ss.IsCritical))
                        {
                            var studentScore = studentScores.Where(ss => ss.IsCritical).FirstOrDefault();
                            subTopicResponse.Critical.InstructorName = Parse.ToSignature(studentScore?.Instructor?.Firstname,
                                                                                studentScore?.Instructor?.Lastname);
                            subTopicResponse.Critical.Remark = studentScore?.Remark;
                            subTopicResponse.Critical.UpdateDate = studentScore.UpdateDate;
                            isScored = true;
                        }
                    }

                    if (subTopic.Criterias != null)
                    {
                        var criterias = subTopic.Criterias.ToList();
                        subTopicResponse.Criterias = new List<StudentLaboratoryDetailResponse.CriteriaResponse>();
                        for (var i = 0; i < criterias.Count; i++)
                        {
                            if (!Status.Is.Active(criterias[i].StatusId)) continue;
                            //var criteriaResponse = StudentLaboratoryDetailResponse.CriteriaResponse.Map(criterias[i]);

                            var criteriaResponse = new StudentLaboratoryDetailResponse.CriteriaResponse(criterias[i]);

                            var criteriaScore = studentScores.Where(ss => ss.CriteriaId == criterias[i].Id &&
                                                                        Status.Is.Active(ss.StatusId)
                                                                    ).FirstOrDefault();

                            var criticalScore = studentScores.Where(ss => ss.IsCritical).FirstOrDefault();
                            if (criteriaScore != null)
                            {
                                criteriaResponse.InstructorName = Parse.ToSignature(criteriaScore.Instructor.Firstname,
                                                                                criteriaScore.Instructor.Lastname);
                                criteriaResponse.UpdateDate = criteriaScore.UpdateDate;
                                isScored = true;
                            }
                                

                            if (criteriaScore != null && criticalScore == null)
                                score += criteriaScore.Score;
                            

                            if (criterias[i].CriteriaTypeId == Criteria.Type.Good.Id)
                            {
                                totalScore += criterias[i]?.Score ?? 0;
                            }

                            subTopicResponse.Criterias.Add(criteriaResponse);
                        }

                        //totalScore += criterias.Where(c => c.CriteriaTypeId == Criteria.Type.Good.Id)
                        //                        .FirstOrDefault()?.Score ?? 0;

                        //_logger.LogInformation("done get total score " + subTopic.Name);
                    }

                    if (subTopic.IsLabel) isScored = true;

                    scored += isScored ? 1 : 0;
                    topicResponse.SubTopics.Add(subTopicResponse);
                }

                topicResponse.Score = score;
                topicResponse.TotalScore = totalScore;
                topicResponse.AllScored = scored == topic.SubTopics.Where(s => Status.Is.Active(s.StatusId)).ToList().Count;
                response.Topics.Add(topicResponse);
            }

            CacheManager.Set(cacheKey, response);
            return new HttpCode<StudentLaboratoryDetailResponse>
            {
                StatusCode = HttpStatusCode.OK,
                Object = response
            };
        }

        public async Task<HttpCode<List<StudentExperienceResponse>>> GetExperience(int id)
        {
            var student = await Get(id);
            if (student is null || Status.Is.InActive(student.StatusId))
                return new HttpCode<List<StudentExperienceResponse>> { StatusCode = HttpStatusCode.NotFound, Object = null };

            var cacheKey = CacheKey.Student.Experience(id);
            var responses = CacheManager.Get<List<StudentExperienceResponse>>(cacheKey);
            if (responses != null)
                return new HttpCode<List<StudentExperienceResponse>> { StatusCode = HttpStatusCode.OK, Object = responses };

            var experiences = await _experience.GetAll();

            if (experiences is null || experiences?.Count == 0)
                return new HttpCode<List<StudentExperienceResponse>> { StatusCode = HttpStatusCode.OK, Object = new List<StudentExperienceResponse>() };

            var studentLabs = await context.StudentLaboratories.Where(sl => sl.StudentId == id &&
                                                                sl.StatusId == Status.Active.Id
                                                            )
                                                            .Include(sl => sl.StudentSubTopics)
                                                                .ThenInclude(ss => ss.StudentScores)
                                                            .AsNoTracking()
                                                            .ToListAsync();

            var studentSubTopics = new List<EN.StudentSubTopic>();
            foreach (var studentLab in studentLabs)
                studentSubTopics.AddRange(studentLab.StudentSubTopics?.Where(ss => ss.StatusId == Status.Active.Id));

            responses = new List<StudentExperienceResponse>();
            foreach (var experience in experiences)
            {
                var response = new StudentExperienceResponse(experience);
                var topics = experience.Topics.ToList();
                if (topics != null && topics.Count > 0 &&
                    !IsOnlyNotActiveTopic(topics))
                {
                    response.IsAchieve = true;

                    foreach (var topic in topics)
                    {
                        if (!Status.Is.Active(topic.StatusId) || topic?.Laboratory?.CourseId != student.CourseId) 
                            continue;

                        var subTopics = topic.SubTopics.Where(s => s.StatusId == Status.Active.Id &&
                                                        !s.IsLabel
                                                    ).ToList();

                        response.IsAchieve &= _experience.IsAchieve(subTopics, studentSubTopics);
                    }
                }

                responses.Add(response);
            }

            CacheManager.Set(cacheKey, responses);
            return new HttpCode<List<StudentExperienceResponse>> { StatusCode = HttpStatusCode.OK, Object = responses };
        }

        public bool IsOnlyNotActiveTopic(List<EN.Topic> topics)
        {
            return topics.Where(t => t.StatusId != Status.Active.Id).ToList().Count == topics.Count;
        }

        public async Task<bool> IsDoneAll(int id)
        {
            var cacheKey = CacheKey.Student.IsDoneAll(id);
            var isDoneAll = CacheManager.Get<bool?>(cacheKey);
            if (isDoneAll != null) return (bool)isDoneAll;

            var subTopics = await context.SubTopics.Where(s => s.StatusId == Status.Active.Id)
                                                    .AsNoTracking()
                                                    .OrderBy(stId => stId)
                                                    .ToListAsync();

            var studentLabs = await context.StudentLaboratories.Where(sl => sl.StatusId == Status.Active.Id &&
                                                                sl.StudentId == id
                                                            )
                                                            .Include(sl => sl.StudentSubTopics)
                                                                .ThenInclude(sst => sst.StudentScores)
                                                            .AsNoTracking()
                                                            .ToListAsync();

            var studentSubtopics = studentLabs.SelectMany(sl => sl.StudentSubTopics).ToList();

            isDoneAll = _experience.IsAchieve(subTopics, studentSubtopics);
            CacheManager.Set(cacheKey, isDoneAll);
            return (bool)isDoneAll;
        }

        public async Task<HttpCode<string>> AddMinusPoint(int id, int instructorId, MinusPointAddRequest request)
        {
            var student = await Get(id);
            if (student is null || Status.Is.InActive(student.StatusId))
                return new HttpCode<string> { StatusCode = HttpStatusCode.NotFound, Object = "Student not found" };

            var laboratory = await _laboratory.Get(request.LaboratoryId);
            if (laboratory is null || Status.Is.InActive(laboratory.StatusId))
                return new HttpCode<string> { StatusCode = HttpStatusCode.NotFound, Object = "Laboratory not found" };

            var minusPoint = await _minusPoint.Get(request.MinusPointId);
            if (minusPoint is null || Status.Is.InActive(minusPoint.StatusId))
                return new HttpCode<string> { StatusCode = HttpStatusCode.NotFound, Object = "Minus point reason not found" };

            var studentLab = await _laboratory.GetStudentLab(laboratory.Id, id);
            if (studentLab is null)
            {
                studentLab = new EN.StudentLaboratory
                {
                    LaboratoryId = laboratory.Id,
                    StudentId = id,
                    StatusId = Status.Active.Id,
                };

                await context.AddAsync(studentLab);
            }

            await context.AddAsync(new EN.StudentMinusPoint
            {
                InstructorId = instructorId,
                MinusPointId = request.MinusPointId,
                Remark = request.Remark,
                StatusId = Status.Active.Id,
                StudentLaboratoryId = studentLab.Id,
                Score = request.Score,
            });

            await context.SaveChangesAsync();
            CacheManager.Remove(CacheKey.Laboratory.Student(laboratory.Id, id));
            CacheManager.Remove(CacheKey.Student.LaboratoryDetail(id, laboratory.Id));
            return new HttpCode<string> { StatusCode = HttpStatusCode.Created, Object = "Minus point added" };
        }

        public async Task<HttpCode<string>> RemoveMinusPoint(int id, int instructorId, int studentMinusPointId)
        {
            var student = await Get(id);
            if (student is null || Status.Is.InActive(student.StatusId))
                return new HttpCode<string> { StatusCode = HttpStatusCode.NotFound, Object = "Student not found" };

            var studentMinusPoint = await context.StudentMinusPoints.Where(smp => smp.Id == studentMinusPointId &&
                                        Status.Is.Active(smp.StatusId)
                                    )
                                    .Include(smp => smp.StudentLaboratory)
                                        .ThenInclude(sl => sl.Laboratory)
                                    .FirstOrDefaultAsync();

            if (studentMinusPoint == null)
                return new HttpCode<string> { StatusCode = HttpStatusCode.NotFound, Object = "Minus point not exist" };

            studentMinusPoint.StatusId = Status.Inactive.Id;
            studentMinusPoint.InstructorId = instructorId;

            var laboratory = studentMinusPoint.StudentLaboratory.Laboratory;

            await context.SaveChangesAsync();
            CacheManager.Remove(CacheKey.Laboratory.Student(laboratory.Id, id));
            CacheManager.Remove(CacheKey.Student.LaboratoryDetail(id, laboratory.Id));
            return new HttpCode<string> { StatusCode = HttpStatusCode.OK, Object = "Minus point removed" };
        }

        public async Task<HttpCode<string>> CreateOrUpdateSelfAssessment(int id, SelfAssessmentRequest request)
        {
            var student = await Get(id);
            if (student is null || Status.Is.InActive(student.StatusId))
                return new HttpCode<string> { StatusCode = HttpStatusCode.NotFound, Object = "Student not found" };

            var laboratory = await _laboratory.GetDetail(request.LaboratoryId);
            if (laboratory is null || Status.Is.InActive(laboratory.StatusId))
                return new HttpCode<string> { StatusCode = HttpStatusCode.NotFound, Object = "Laboratory not found" };

            if (_laboratory.IsExpired(laboratory))
                return new HttpCode<string> { StatusCode = HttpStatusCode.Forbidden, Object = "Laboratory already closed" };

            var allSubTopics = new List<EN.SubTopic>();
            foreach (var subTopics in laboratory.Topics.Select(t => t.SubTopics))
                allSubTopics.AddRange(subTopics);

            var subTopic = allSubTopics.Find(s => s.Id == request.SubTopicId);
            if (subTopic is null || Status.Is.InActive(subTopic.StatusId))
                return new HttpCode<string> { StatusCode = HttpStatusCode.NotFound, Object = "Subtopic not found" };

            var studentLab = await _laboratory.GetStudentLab(request.LaboratoryId, id);
            if (studentLab is null)
            {
                studentLab = new EN.StudentLaboratory
                {
                    LaboratoryId = request.LaboratoryId,
                    StudentId = id,
                    StatusId = Status.Active.Id,
                };
                await context.AddAsync(studentLab);
            }

            var studentSubTopic = studentLab.StudentSubTopics.Where(ss => ss.SubTopicId == request.SubTopicId &&
                                                                ss.StatusId == Status.Active.Id
                                                            )
                                                            .FirstOrDefault();

            var isCreate = false;
            if (studentSubTopic is null)
            {
                studentSubTopic = new EN.StudentSubTopic
                {
                    StudentLaboratoryId = studentLab.Id,
                    SubTopicId = request.SubTopicId,
                    StatusId = Status.Active.Id,
                    SelfAssessment = request.SelfAssessment,
                };
                await context.AddAsync(studentSubTopic);
                isCreate = true;
            }
            else
            {
                studentSubTopic.SelfAssessment = request.SelfAssessment;
                context.Update(studentSubTopic);
            }

            await context.SaveChangesAsync();
            context.Entry(studentSubTopic).State = EntityState.Detached;
            CacheManager.Remove(CacheKey.Laboratory.Student(request.LaboratoryId, id));
            CacheManager.Remove(CacheKey.Student.LaboratoryDetail(id, request.LaboratoryId));

            if (isCreate) return new HttpCode<string> { StatusCode = HttpStatusCode.Created, Object = "Self assessment added." };
            return new HttpCode<string> { StatusCode = HttpStatusCode.OK, Object = "Self assessment updated." };
        }

        public async Task<HttpCode<string>> CreateOrUpdateLabScope(int id, LabScopeRequest request)
        {
            var student = await Get(id);
            if (student is null || Status.Is.InActive(student.StatusId))
                return new HttpCode<string> { StatusCode = HttpStatusCode.NotFound, Object = "Student not found" };

            var laboratory = await _laboratory.Get(request.LaboratoryId);
            if (laboratory is null || Status.Is.InActive(laboratory.StatusId))
                return new HttpCode<string> { StatusCode = HttpStatusCode.NotFound, Object = "Laboratory not found" };

            if (_laboratory.IsExpired(laboratory))
                return new HttpCode<string> { StatusCode = HttpStatusCode.Forbidden, Object = "Laboratory already closed" };

            var isCreate = false;
            var studentLab = await _laboratory.GetStudentLab(request.LaboratoryId, id);
            if (studentLab is null)
            {
                studentLab = new EN.StudentLaboratory
                {
                    LaboratoryId = laboratory.Id,
                    StudentId = id,
                    StatusId = Status.Active.Id,
                    KindOfWork = request.KindOfWork,
                    Material = request.Material,
                    Tooth = request.Tooth,
                };

                await context.AddAsync(studentLab);
                isCreate = true;
            }
            else
            {
                studentLab.KindOfWork = request.KindOfWork;
                studentLab.Material = request.Material;
                studentLab.Tooth = request.Tooth;

                context.Update(studentLab);
            }

            await context.SaveChangesAsync();
            context.Entry(studentLab).State = EntityState.Detached;
            CacheManager.Remove(CacheKey.Laboratory.Student(request.LaboratoryId, id));
            CacheManager.Remove(CacheKey.Student.LaboratoryDetail(id, request.LaboratoryId));

            if (isCreate) return new HttpCode<string> { StatusCode = HttpStatusCode.Created, Object = "Student laboratory scope added." };
            return new HttpCode<string> { StatusCode = HttpStatusCode.OK, Object = "Student laboratory scope updated." };
        }

        public async Task<HttpCode<string>> SubmitScore(int id, int instructorId, SubmitScoreRequest request)
        {

            var student = await Get(id);
            if (student is null || Status.Is.InActive(student.StatusId))
                return new HttpCode<string> { StatusCode = HttpStatusCode.NotFound, Object = "Student not found" };

            var instructor = await context.Instructors.FindAsync(instructorId);
            if (instructor is null || Status.Is.InActive(instructor.StatusId))
                return new HttpCode<string> { StatusCode = HttpStatusCode.NotFound, Object = "Instructor not found" };

            var laboratory = await _laboratory.GetDetail(request.LaboratoryId);
            if (laboratory is null || Status.Is.InActive(laboratory.StatusId))
                return new HttpCode<string> { StatusCode = HttpStatusCode.NotFound, Object = "Laboratory not found" };

            if (DateTime.Now > laboratory.EndDate)
                return new HttpCode<string> { StatusCode = HttpStatusCode.Forbidden, Object = "Laboratory already closed" };

            var allSubTopics = new List<EN.SubTopic>();
            foreach (var subTopics in laboratory.Topics.Select(t => t.SubTopics))
                allSubTopics.AddRange(subTopics);

            var subTopic = allSubTopics.Find(s => s.Id == request.SubTopicId);
            if (subTopic is null || Status.Is.InActive(subTopic.StatusId))
                return new HttpCode<string> { StatusCode = HttpStatusCode.NotFound, Object = "Subtopic not found" };

            if (DateTime.Now > subTopic.Topic.EndDate)
                return new HttpCode<string> { StatusCode = HttpStatusCode.Forbidden, Object = "Topic already closed" };

            var criteria = subTopic.Criterias.Where(c => c.Id == request.CriteriaId).FirstOrDefault();
            if (criteria is null || Status.Is.InActive(criteria.StatusId))
                return new HttpCode<string> { StatusCode = HttpStatusCode.NotFound, Object = "Criteria not found" };

            var studentLab = await _laboratory.GetStudentLab(laboratory.Id, id);
            if (studentLab is null)
            {
                studentLab = new EN.StudentLaboratory
                {
                    LaboratoryId = laboratory.Id,
                    StudentId = id,
                    StatusId = Status.Active.Id,
                };

                await context.AddAsync(studentLab);
            }

            var studentSubTopic = studentLab.StudentSubTopics.Where(ss => ss.SubTopicId == request.SubTopicId &&
                                                                ss.StatusId == Status.Active.Id
                                                            )
                                                            .FirstOrDefault();

            if (studentSubTopic is null)
            {
                studentSubTopic = new EN.StudentSubTopic
                {
                    StudentLaboratoryId = studentLab.Id,
                    SubTopicId = request.SubTopicId,
                    StatusId = Status.Active.Id,
                };

                await context.AddAsync(studentSubTopic);
            }

            var studentScore = await context.StudentScores.Where(ss => ss.StudentSubTopicId == studentSubTopic.Id &&
                                        ss.StatusId == Status.Active.Id &&
                                        ss.IsCritical
                                )
                                .AsNoTracking()
                                .FirstOrDefaultAsync();

            if (studentScore != null)
                return new HttpCode<string> { StatusCode = HttpStatusCode.Conflict, Object = "This subtopic is already critical. Cannot submit score" };

            studentScore = await context.StudentScores.Where(ss => ss.StudentSubTopicId == studentSubTopic.Id &&
                                        ss.StatusId == Status.Active.Id
                                )
                                .AsNoTracking()
                                .FirstOrDefaultAsync();

            if (studentScore is null ||
               (studentScore != null && !studentScore.IsCritical && (studentScore.CriteriaId != request.CriteriaId)))
            {
                await context.AddAsync(new EN.StudentScore
                {
                    InstructorId = instructorId,
                    StudentSubTopicId = studentSubTopic.Id,
                    CriteriaId = request.CriteriaId,
                    Remark = request.Remark,
                    StatusId = Status.Active.Id,
                    Score = criteria.Score
                });
            }

            if (studentScore != null)
            {
                studentScore.StatusId = Status.Inactive.Id;
                context.Update(studentScore);
            }

            await context.SaveChangesAsync();
            context.Entry(studentLab).State = EntityState.Detached;
            context.Entry(studentSubTopic).State = EntityState.Detached;
            if (studentScore != null) context.Entry(studentScore).State = EntityState.Detached;
            CacheManager.Remove(CacheKey.Laboratory.Student(request.LaboratoryId, id));
            CacheManager.Remove(CacheKey.Student.LaboratoryDetail(id, request.LaboratoryId));
            CacheManager.Remove(CacheKey.Student.Experience(id));

            return new HttpCode<string> { StatusCode = HttpStatusCode.Created, Object = "Student score submitted." };
        }

        public async Task<HttpCode<string>> SubmitCritical(int id, int instructorId, SubmitCriticalRequest request)
        {
            var student = await Get(id);
            if (student is null || Status.Is.InActive(student.StatusId))
                return new HttpCode<string> { StatusCode = HttpStatusCode.NotFound, Object = "Student not found" };

            var instructor = await context.Instructors.FindAsync(instructorId);
            if (instructor is null || Status.Is.InActive(instructor.StatusId))
                return new HttpCode<string> { StatusCode = HttpStatusCode.NotFound, Object = "Instructor not found" };

            var laboratory = await _laboratory.GetDetail(request.LaboratoryId);
            if (laboratory is null || Status.Is.InActive(laboratory.StatusId))
                return new HttpCode<string> { StatusCode = HttpStatusCode.NotFound, Object = "Laboratory not found" };

            if (DateTime.Now > laboratory.EndDate)
                return new HttpCode<string> { StatusCode = HttpStatusCode.Forbidden, Object = "Laboratory already closed" };

            var allSubTopics = new List<EN.SubTopic>();
            foreach (var subTopics in laboratory.Topics.Select(t => t.SubTopics))
                allSubTopics.AddRange(subTopics);

            var subTopic = allSubTopics.Find(s => s.Id == request.SubTopicId);
            if (subTopic is null || Status.Is.InActive(subTopic.StatusId))
                return new HttpCode<string> { StatusCode = HttpStatusCode.NotFound, Object = "Subtopic not found" };

            if (DateTime.Now > subTopic.Topic.EndDate)
                return new HttpCode<string> { StatusCode = HttpStatusCode.Forbidden, Object = "Topic already closed" };

            var studentLab = await _laboratory.GetStudentLab(laboratory.Id, id);
            if (studentLab is null)
            {
                studentLab = new EN.StudentLaboratory
                {
                    LaboratoryId = laboratory.Id,
                    StudentId = id,
                    StatusId = Status.Active.Id,
                };

                await context.AddAsync(studentLab);
            }

            var studentScores = new List<EN.StudentScore>();
            var studentSubTopics = new List<EN.StudentSubTopic>();
            var allCriticalSubtopics = allSubTopics.Where(s => s.CriticalId == subTopic.CriticalId).ToList();
            foreach (var criticalSubtopic in allCriticalSubtopics)
            {
                var studentSubTopic = studentLab.StudentSubTopics.Where(ss => ss.SubTopicId == criticalSubtopic.Id &&
                                                    ss.StatusId == Status.Active.Id
                                                )
                                                .FirstOrDefault();

                if (studentSubTopic is null)
                {
                    studentSubTopic = new EN.StudentSubTopic
                    {
                        StudentLaboratoryId = studentLab.Id,
                        SubTopicId = criticalSubtopic.Id,
                        StatusId = Status.Active.Id,
                    };

                    await context.AddAsync(studentSubTopic);
                    studentSubTopics.Add(studentSubTopic);

                }

                var newStudentScore = new EN.StudentScore
                {
                    InstructorId = instructorId,
                    StudentSubTopicId = studentSubTopic.Id,
                    IsCritical = true,
                    Remark = request.Remark,
                    StatusId = Status.Active.Id,
                    Score = 0
                };

                await context.AddAsync(newStudentScore);
                studentScores.Add(newStudentScore);
            }

            await context.SaveChangesAsync();
            context.Entry(studentLab).State = EntityState.Detached;
            foreach (var studentSubTopic in studentSubTopics)
                context.Entry(studentSubTopic).State = EntityState.Detached;
            foreach (var studentScore in studentScores)
                context.Entry(studentScore).State = EntityState.Detached;
            CacheManager.Remove(CacheKey.Laboratory.Student(request.LaboratoryId, id));
            CacheManager.Remove(CacheKey.Student.LaboratoryDetail(id, request.LaboratoryId));
            CacheManager.Remove(CacheKey.Student.Experience(id));

            return new HttpCode<string> { StatusCode = HttpStatusCode.Created, Object = "Student score submitted." };

        }

        public async Task<HttpCode<string>> CancelCritical(int id, SubmitCriticalRequest request)
        {
            var student = await Get(id);
            if (student is null || Status.Is.InActive(student.StatusId))
                return new HttpCode<string> { StatusCode = HttpStatusCode.NotFound, Object = "Student not found" };

            var laboratory = await _laboratory.GetDetail(request.LaboratoryId);
            if (laboratory is null || Status.Is.InActive(laboratory.StatusId))
                return new HttpCode<string> { StatusCode = HttpStatusCode.NotFound, Object = "Laboratory not found" };

            if (DateTime.Now > laboratory.EndDate)
                return new HttpCode<string> { StatusCode = HttpStatusCode.Forbidden, Object = "Laboratory already closed" };

            var allSubTopics = new List<EN.SubTopic>();
            foreach (var subTopics in laboratory.Topics.Select(t => t.SubTopics))
                allSubTopics.AddRange(subTopics);

            var subTopic = allSubTopics.Find(s => s.Id == request.SubTopicId);
            if (subTopic is null || Status.Is.InActive(subTopic.StatusId))
                return new HttpCode<string> { StatusCode = HttpStatusCode.NotFound, Object = "Subtopic not found" };

            if (DateTime.Now > subTopic.Topic.EndDate)
                return new HttpCode<string> { StatusCode = HttpStatusCode.Forbidden, Object = "Topic already closed" };

            var studentLab = await _laboratory.GetStudentLab(laboratory.Id, id);
            if (studentLab is null)
                return new HttpCode<string> { StatusCode = HttpStatusCode.NotFound, Object = "Student laboratory is not created" };

            var studentScores = new List<EN.StudentScore>();
            var allCriticalSubtopicIds = allSubTopics.Where(s => s.CriticalId == subTopic.CriticalId)
                                                    .Select(s => s.Id)
                                                    .ToList();

            var studentSubTopics = studentLab.StudentSubTopics.Where(
                                                    ss => allCriticalSubtopicIds.Contains(ss.SubTopicId) &&
                                                    ss.StatusId == Status.Active.Id
                                                )
                                                .ToList();

            foreach (var studentSubTopic in studentSubTopics)
            {
                var subTopicStudentScores = studentSubTopic.StudentScores.Where(ss => ss.IsCritical &&
                                                      ss.StatusId == Status.Active.Id
                                              )
                                              .ToList();

                if (subTopicStudentScores is null) continue;
                foreach (var studentScore in subTopicStudentScores)
                {
                    studentScore.StatusId = Status.Inactive.Id;
                    context.Update(studentScore);
                    studentScores.Add(studentScore);
                }
            }

            await context.SaveChangesAsync();
            context.Entry(studentLab).State = EntityState.Detached;
            foreach (var studentScore in studentScores)
                context.Entry(studentScore).State = EntityState.Detached;
            CacheManager.Remove(CacheKey.Laboratory.Student(request.LaboratoryId, id));
            CacheManager.Remove(CacheKey.Student.LaboratoryDetail(id, request.LaboratoryId));
            CacheManager.Remove(CacheKey.Student.Experience(id));

            return new HttpCode<string> { StatusCode = HttpStatusCode.OK, Object = "Student score critical cancelled." };
        }

        public async Task<HttpCode<List<LaboratoryImageResponse>>> GetLabImage(int id, int laboratoryId)
        {
            var student = await Get(id);
            if (student is null || Status.Is.InActive(student.StatusId))
                return new HttpCode<List<LaboratoryImageResponse>> { StatusCode = HttpStatusCode.NotFound };

            var laboratory = await _laboratory.GetDetail(laboratoryId);
            if (laboratory is null || Status.Is.InActive(laboratory.StatusId))
                return new HttpCode<List<LaboratoryImageResponse>> { StatusCode = HttpStatusCode.NotFound };

            var cacheKey = CacheKey.Student.LaboratoryImage(id, laboratoryId);
            var labImages = CacheManager.Get<List<LaboratoryImageResponse>>(cacheKey);
            if (labImages != null)
                return new HttpCode<List<LaboratoryImageResponse>> { StatusCode = HttpStatusCode.OK, Object = labImages };

            if (laboratory.LaboratoryImages is null || laboratory.LaboratoryImages?.Count == 0)
                return new HttpCode<List<LaboratoryImageResponse>> { StatusCode = HttpStatusCode.OK, Object = new List<LaboratoryImageResponse>() };

            var labImageIds = laboratory.LaboratoryImages.Where(li => li.StatusId == Status.Active.Id)
                                                        .Select(li => li.Id)
                                                        .ToList();

            var studentLabImages = await context.StudentLaboratoryImages.Where(sli => sli.StudentId == id &&
                                                                       labImageIds.Contains(sli.LaboratoryImageId) &&
                                                                       sli.StatusId == Status.Active.Id
                                                                    )
                                                                    .Include(sl => sl.Instructor)
                                                                    .AsNoTracking()
                                                                    .ToListAsync();

            labImages = new List<LaboratoryImageResponse>();
            foreach (var labImage in laboratory.LaboratoryImages.OrderBy(li => li.Sequence))
            {
                var response = await LaboratoryImageResponse.Map(labImage);
                var concernImages = studentLabImages.Where(sli => sli.LaboratoryImageId == labImage.Id).ToList();

                if (concernImages != null)
                {
                    var studentImageResponses = new List<LaboratoryImageResponse.StudentLaboratoryImageResponse>();

                    foreach (var concernImage in concernImages)
                        studentImageResponses.Add(new LaboratoryImageResponse.StudentLaboratoryImageResponse()
                        {
                            Id = concernImage.Id,
                            Image = concernImage.Image,
                            Remark = concernImage.Remark,
                            InstructorName = concernImage.Instructor is null ? null :
                                $"{concernImage.Instructor.Firstname} {concernImage.Instructor.Lastname}"
                        });

                    response.StudentLaboratoryImages = studentImageResponses;
                }

                labImages.Add(response);
            }

            CacheManager.Set(cacheKey, labImages);
            return new HttpCode<List<LaboratoryImageResponse>> { StatusCode = HttpStatusCode.OK, Object = labImages };
        }

        public async Task SaveLabScopeImage(int id, int laboratoryId, string url)
        {
            var studentLaboratory = await _laboratory.GetStudentLab(laboratoryId, id);
            if (studentLaboratory is null)
            {
                studentLaboratory = new Database.Entities.StudentLaboratory
                {
                    LaboratoryId = laboratoryId,
                    StudentId = id,
                    Image = url,
                    StatusId = Status.Active.Id,
                };
                await context.AddAsync(studentLaboratory);
            }
            else
            {
                studentLaboratory.Image = url;
                context.Update(studentLaboratory);
            }

            await context.SaveChangesAsync();

            CacheManager.Remove(CacheKey.Laboratory.Student(laboratoryId, id));
            CacheManager.Remove(CacheKey.Student.LaboratoryDetail(id, laboratoryId));
        }

        public async Task SaveLabImage(int id, int laboratoryId, int labImageId, string url)
        {
            var studentlabImage = await context.StudentLaboratoryImages.Where(i => i.StudentId == id &&
                                            i.LaboratoryImageId == labImageId &&
                                            Status.Is.Active(i.StatusId)
                                        )
                                        .AsNoTracking()
                                        .FirstOrDefaultAsync();

            if (studentlabImage is null)
            {
                await context.AddAsync(new EN.StudentLaboratoryImage
                {
                    StudentId = id,
                    LaboratoryImageId = labImageId,
                    StatusId = Status.Active.Id,
                    Image = url,
                });
            }
            else
            {
                studentlabImage.Image = url;
                context.Update(studentlabImage);
            }

            await context.SaveChangesAsync();
            CacheManager.Remove(CacheKey.Student.LaboratoryImage(id, laboratoryId));
        }

        public async Task<HttpCode<string>> ConfirmLabImage(int instructorId, int laboratoryId, int studentLabImageId)
        {
            var instructor = await context.Instructors.FindAsync(instructorId);
            if (instructor is null || Status.Is.InActive(instructor.StatusId))
                return new HttpCode<string> { StatusCode = HttpStatusCode.NotFound, Object = "Instructor not found" };

            var studentLabImage = await context.StudentLaboratoryImages.FindAsync(studentLabImageId);
            if (studentLabImage is null || Status.Is.InActive(studentLabImage?.StatusId ?? Status.Inactive.Id))
                return new HttpCode<string> { StatusCode = HttpStatusCode.NotFound, Object = "Student lab image not found" };

            var student = await Get(studentLabImage.StudentId);

            if (await _course.IsExpired(student.CourseId))
                return new HttpCode<string> { StatusCode = HttpStatusCode.Forbidden, Object = "Course is already closed" };

            var isConfirm = false;
            if (studentLabImage.InstructorConfirmId is null)
            {
                studentLabImage.InstructorConfirmId = instructorId;
                isConfirm = true;
            }
            else studentLabImage.InstructorConfirmId = null;

            context.Update(studentLabImage);
            await context.SaveChangesAsync();

            var cacheKey = CacheKey.Student.LaboratoryImage(studentLabImage.StudentId, laboratoryId);
            CacheManager.Remove(cacheKey);

            if (isConfirm)
                return new HttpCode<string> { StatusCode = HttpStatusCode.OK, Object = "Confrim success" };
            else
                return new HttpCode<string> { StatusCode = HttpStatusCode.OK, Object = "Cancel confrim success" };
        }

        public async Task<HttpCode<string>> ConfirmLabScopeImage(int id, int instructorId, int laboratoryId)
        {
            var instructor = await context.Instructors.FindAsync(instructorId);
            if (instructor is null || Status.Is.InActive(instructor.StatusId))
                return new HttpCode<string> { StatusCode = HttpStatusCode.NotFound, Object = "Instructor not found" };

            var student = await Get(id);
            if (student is null || Status.Is.InActive(student.StatusId))
                return new HttpCode<string> { StatusCode = HttpStatusCode.NotFound, Object = "Student not found" };

            if (await _course.IsExpired(student.CourseId))
                return new HttpCode<string> { StatusCode = HttpStatusCode.Forbidden, Object = "Course is already closed" };

            var laboratory = await _laboratory.GetDetail(laboratoryId);
            if (laboratory is null || Status.Is.InActive(laboratory.StatusId))
                return new HttpCode<string> { StatusCode = HttpStatusCode.NotFound, Object = "Laboratory not found" };

            var studentLab = await context.StudentLaboratories.Where(sl => sl.StudentId == id &&
                                                                    sl.LaboratoryId == laboratoryId)
                                                    .AsNoTracking()
                                                    .FirstOrDefaultAsync();

            if (studentLab is null || Status.Is.InActive(studentLab?.StatusId ?? Status.Inactive.Id))
                return new HttpCode<string> { StatusCode = HttpStatusCode.NotFound, Object = "Student laboratory not found" };

            var isConfirm = false;
            if (studentLab.InstructorConfirmId is null)
            {
                studentLab.InstructorConfirmId = instructorId;
                isConfirm = true;
            }
            else studentLab.InstructorConfirmId = null;

            context.Update(studentLab);
            await context.SaveChangesAsync();

            var cacheKey = CacheKey.Student.LaboratoryDetail(id, laboratoryId);
            CacheManager.Remove(cacheKey);

            if (isConfirm)
                return new HttpCode<string> { StatusCode = HttpStatusCode.OK, Object = "Confrim success" };
            else
                return new HttpCode<string> { StatusCode = HttpStatusCode.OK, Object = "Cancel confrim success" };
        }

        public async Task<int> HasLabImage(int id, EN.Laboratory laboratory)
        {
            if (laboratory?.LaboratoryImages == null || laboratory?.LaboratoryImages.Count == 0) return 0;

            var labImageIds = laboratory.LaboratoryImages.Where(li => li.StatusId == Status.Active.Id)
                                                       .Select(li => li.Id)
                                                       .ToList();

            var studentLabImages = await context.StudentLaboratoryImages.Where(sli => sli.StudentId == id &&
                                                           labImageIds.Contains(sli.LaboratoryImageId) &&
                                                           sli.StatusId == Status.Active.Id
                                                        )
                                                        .Include(sl => sl.Instructor)
                                                        .AsNoTracking()
                                                        .ToListAsync();

            var totalApproved = 0;
            foreach (EN.StudentLaboratoryImage laboratoryImage in studentLabImages)
            {
                totalApproved += laboratoryImage.InstructorConfirmId == null ? 0 : 1;
            }


            if (totalApproved >= labImageIds.Count) return 2;

            return 1;
        }

        public async Task<HttpCode<List<StudentToothConfirmResponse>>> GetToothConfirm(int id, int laboratoryId)
        {
            var studentLabToothConfirms = await context.StudentLaboratoryTeethConfirms
                         .Where(slc => slc.LaboratoryId == laboratoryId && slc.StudentId == id
                             && Status.Is.Active(slc.StatusId)
                         )
                         .Include(slc => slc.Instructor)
                         .AsNoTracking()
                         .ToListAsync();

            var responses = new List<StudentToothConfirmResponse>();
            if (studentLabToothConfirms != null && studentLabToothConfirms.Count > 0)
            {
                foreach (var data in studentLabToothConfirms)
                {
                    responses.Add(new StudentToothConfirmResponse
                    {
                        Confirm = StudentToothConfirmResponse.Map(data),
                        InstructorName = data.Instructor != null ?
                            Parse.ToSignature(data.Instructor.Firstname, data.Instructor.Lastname) : null
                    }
                    );
                }
            }

            return new HttpCode<List<StudentToothConfirmResponse>>
            { StatusCode = HttpStatusCode.OK, Object = responses };
        }

        public async Task<HttpCode<string>> CreateToothConfirm(int id, int laboratoryId)
        {
            var student = await Get(id);
            if (student is null || Status.Is.InActive(student.StatusId))
                return new HttpCode<string> { StatusCode = HttpStatusCode.NotFound, Object = "Student not found" };

            var laboratory = await _laboratory.GetDetail(laboratoryId);
            if (laboratory is null || Status.Is.InActive(laboratory.StatusId))
                return new HttpCode<string> { StatusCode = HttpStatusCode.NotFound, Object = "Laboratory not found" };

            if (_laboratory.IsExpired(laboratory))
                return new HttpCode<string> { StatusCode = HttpStatusCode.Forbidden, Object = "Laboratory already closed" };

            var studentToothConfirm = new EN.StudentLaboratoryTeethConfirm
            {
                StudentId = id,
                StatusId = Status.Active.Id,
                LaboratoryId = laboratory.Id
            };

            await context.AddAsync(studentToothConfirm);
            await context.SaveChangesAsync();

            return new HttpCode<string>
            { StatusCode = HttpStatusCode.Created, Object = "Created." };
        }

        public async Task<HttpCode<string>> StudentUpdateToothConfirm(int id, StudentToothConfirmRequest request)
        {
            var student = await Get(id);
            if (student is null || Status.Is.InActive(student.StatusId))
                return new HttpCode<string> { StatusCode = HttpStatusCode.NotFound, Object = "Student not found" };

            var laboratory = await _laboratory.GetDetail(request.LaboratoryId);
            if (laboratory is null || Status.Is.InActive(laboratory.StatusId))
                return new HttpCode<string> { StatusCode = HttpStatusCode.NotFound, Object = "Laboratory not found" };

            if (_laboratory.IsExpired(laboratory))
                return new HttpCode<string> { StatusCode = HttpStatusCode.Forbidden, Object = "Laboratory already closed" };


            var studentLabToothConfirm = await context.StudentLaboratoryTeethConfirms
                                    .Where(slc => slc.Id == request.ConfirmId
                                        && slc.StudentId == id
                                        && slc.LaboratoryId == laboratory.Id
                                        && Status.Is.Active(slc.StatusId)
                                    )
                                    .FirstOrDefaultAsync();

            if (studentLabToothConfirm == null)
                return new HttpCode<string> { StatusCode = HttpStatusCode.NotFound, Object = "Not found confirm data" };
            if (studentLabToothConfirm.InstructorId != null)
                return new HttpCode<string> { StatusCode = HttpStatusCode.Forbidden, Object = "Cannot update cause it is already checked." };

            studentLabToothConfirm.TeethNo = request.TeethNo;
            await context.SaveChangesAsync();

            return new HttpCode<string> { StatusCode = HttpStatusCode.OK, Object = "Updated" };
        }

        public async Task<HttpCode<string>> InstructorUpdateToothConfirm(int id, InstuctorToothConfirmRequest request, int instructorId)
        {
            var student = await Get(id);
            if (student is null || Status.Is.InActive(student.StatusId))
                return new HttpCode<string> { StatusCode = HttpStatusCode.NotFound, Object = "Student not found" };

            var laboratory = await _laboratory.GetDetail(request.LaboratoryId);
            if (laboratory is null || Status.Is.InActive(laboratory.StatusId))
                return new HttpCode<string> { StatusCode = HttpStatusCode.NotFound, Object = "Laboratory not found" };

            if (_laboratory.IsExpired(laboratory))
                return new HttpCode<string> { StatusCode = HttpStatusCode.Forbidden, Object = "Laboratory already closed" };

            var studentLabToothConfirm = await context.StudentLaboratoryTeethConfirms
                                    .Where(slc => slc.Id == request.ConfirmId
                                        && slc.StudentId == id
                                        && slc.LaboratoryId == laboratory.Id
                                        && Status.Is.Active(slc.StatusId)
                                    )
                                    .FirstOrDefaultAsync();

            if (studentLabToothConfirm == null)
                return new HttpCode<string> { StatusCode = HttpStatusCode.NotFound, Object = "Not found confirm data" };



            studentLabToothConfirm.TeethNo = request.TeethNo;
            studentLabToothConfirm.StudentId = id;
            studentLabToothConfirm.CanUse = request.CanNotUse ? false : request.CanUse;
            studentLabToothConfirm.CanNotUse = request.CanNotUse;
            studentLabToothConfirm.UseInLab = request.CanNotUse ? false : request.UseInLab;
            studentLabToothConfirm.UseInControl = request.CanNotUse ? false : request.UseInControl;
            studentLabToothConfirm.Comment = request.Comment;

            if (request.CanUse || request.CanNotUse || request.UseInLab || request.UseInControl)
            {
                studentLabToothConfirm.InstructorId = instructorId;
            }
            else
            {
                studentLabToothConfirm.InstructorId = null;
            }

            await context.SaveChangesAsync();
            return new HttpCode<string> { StatusCode = HttpStatusCode.OK, Object = "Updated" };
        }

        public async Task<HttpCode<string>> RemoveToothConfirm(int id, StudentToothConfirmRemoveRequest request)
        {
            var student = await Get(id);
            if (student is null || Status.Is.InActive(student.StatusId))
                return new HttpCode<string> { StatusCode = HttpStatusCode.NotFound, Object = "Student not found" };

            var laboratory = await _laboratory.GetDetail(request.LaboratoryId);
            if (laboratory is null || Status.Is.InActive(laboratory.StatusId))
                return new HttpCode<string> { StatusCode = HttpStatusCode.NotFound, Object = "Laboratory not found" };

            if (_laboratory.IsExpired(laboratory))
                return new HttpCode<string> { StatusCode = HttpStatusCode.Forbidden, Object = "Laboratory already closed" };

            var studentLabToothConfirm = await context.StudentLaboratoryTeethConfirms
                        .Where(slc => slc.Id == request.ConfirmId
                            && slc.StudentId == id
                            && slc.LaboratoryId == laboratory.Id
                            && Status.Is.Active(slc.StatusId)
                        )
                        .FirstOrDefaultAsync();

            if (studentLabToothConfirm == null)
                return new HttpCode<string> { StatusCode = HttpStatusCode.NotFound, Object = "Not found confirm data" };

            if (studentLabToothConfirm.InstructorId != null)
                return new HttpCode<string> { StatusCode = HttpStatusCode.Forbidden, Object = "Cannot change cause already checked." };

            studentLabToothConfirm.StatusId = Status.Inactive.Id;

            await context.SaveChangesAsync();

            return new HttpCode<string> { StatusCode = HttpStatusCode.OK, Object = "Removed" };

        }

        public async Task<HttpCode<StudentToothApprovalResponse>> GetToothApproval (int id, int laboratoryId)
        {
              var studentToothApproval = await context.StudentLaboratoryTeethApprovals.Where(ta =>
                                                        ta.StudentId == id &&
                                                        ta.LaboratoryId == laboratoryId &&
                                                        Status.Is.Active(ta.StatusId)
                                                    )
                                                    .Include(ta => ta.Instructor)
                                                    .AsNoTracking()
                                                    .FirstOrDefaultAsync();

            if (studentToothApproval == null) studentToothApproval = new EN.StudentLaboratoryTeethApproval();

            var response = StudentToothApprovalResponse.Map(studentToothApproval);
            return new HttpCode<StudentToothApprovalResponse>
            {
                StatusCode = HttpStatusCode.OK,
                Object = response
            };                                                 
        }

        public async Task<HttpCode<string>> CreateOrUpdateToothApproval (int id, int instructorId, StudentToothApprovalRequest request)
        {
            var student = await Get(id);
            if (student is null || Status.Is.InActive(student.StatusId))
                return new HttpCode<string> { StatusCode = HttpStatusCode.NotFound, Object = "Student not found" };

            var laboratory = await _laboratory.GetDetail(request.LaboratoryId);
            if (laboratory is null || Status.Is.InActive(laboratory.StatusId))
                return new HttpCode<string> { StatusCode = HttpStatusCode.NotFound, Object = "Laboratory not found" };

            if (_laboratory.IsExpired(laboratory))
                return new HttpCode<string> { StatusCode = HttpStatusCode.Forbidden, Object = "Laboratory already closed" };


            var studentToothApproval = await context.StudentLaboratoryTeethApprovals.Where(ta =>
                                                        ta.StudentId == id &&
                                                        ta.LaboratoryId == laboratory.Id &&
                                                        Status.Is.Active(ta.StatusId) &&
                                                        ta.Id == request.ApprovalId
                                                    )
                                                    .Include(ta => ta.Instructor)
                                                    .AsNoTracking()
                                                    .FirstOrDefaultAsync();

            if (studentToothApproval == null)
            {
                //create
                await context.AddAsync(new EN.StudentLaboratoryTeethApproval
                {
                    StudentId = id,
                    InstructorId = instructorId,
                    StatusId = Status.Active.Id,
                    IsApproved = request.IsApproved,
                    Comment = request.Comment,
                    LaboratoryId = laboratory.Id,
                });
            } 
            else
            {
                //update
                studentToothApproval.InstructorId = instructorId;
                studentToothApproval.Comment = request.Comment;
                studentToothApproval.IsApproved = request.IsApproved;

                context.Update(studentToothApproval);
            }

            await context.SaveChangesAsync();

            return new HttpCode<string>
            {
                StatusCode = HttpStatusCode.OK,
                Object = "Updated"
            };
        }
    }
}
