﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models.Responses
{
    public class GroupStudentResponse
    {
        public int StudentGroupId { get; set; }
        public List<StudentResponse> Students { get; set; }
    }
}
