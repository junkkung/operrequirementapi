﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using EN = Database.Entities;

namespace Models.Responses
{
    public class StudentExperienceResponse
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public bool IsAchieve { get; set; }

        public StudentExperienceResponse(EN.Experience experience)
        {
            this.Id = experience?.Id ?? 0;
            this.Name = experience?.Name;
            this.Description = experience?.Description;
        }

        public static StudentExperienceResponse Map(EN.Experience experience)
        {
            var configuration = new MapperConfiguration(cfg => cfg.CreateMap<EN.Experience, StudentExperienceResponse>()
                .ForMember(des => des.IsAchieve, opt => opt.Ignore())
            );
            var mapper = configuration.CreateMapper();
            return mapper.Map<EN.Experience, StudentExperienceResponse>(experience);
        }
    }
}
