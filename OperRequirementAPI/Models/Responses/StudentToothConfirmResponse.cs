﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using EN = Database.Entities;

namespace Models.Responses
{
    public class StudentToothConfirmResponse
    {
        public ConfirmResponse Confirm { get; set; }

        public string InstructorName { get; set; }

        public class ConfirmResponse
        {
            public int Id { get; set; }
            public int StudentId { get; set; }
            public string TeethNo { get; set; }
            public bool CanUse { get; set; }
            public bool CanNotUse { get; set; }
            public bool UseInLab { get; set; }
            public bool UseInControl { get; set; }
            public string Comment { get; set; }
            public int LaboratoryId { get; set; }
            public DateTimeOffset UpdateDate { get; set; }
        }

        public static ConfirmResponse Map<T>(T input)
        {
            var configuration = new MapperConfiguration(cfg => cfg.CreateMap<T, ConfirmResponse>());
            var mapper = configuration.CreateMapper();
            return mapper.Map<T, ConfirmResponse>(input);
        }
    }
}
