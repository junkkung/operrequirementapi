﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using EN = Database.Entities;

namespace Models.Responses
{
    public class LaboratoryResponse
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int TotalScore { get; set; }
        public int HasLabImage { get; set; }
        public bool HasToothConfirm { get; set; }

        public LaboratoryResponse() { }

        public LaboratoryResponse(EN.Laboratory laboratory)
        {
            this.Id = laboratory.Id;
            this.Name = laboratory.Name;
            this.Description = laboratory.Description;
            this.HasToothConfirm = laboratory.HasToothConfirm;
        }

        public static LaboratoryResponse Map<T>(T input)
        {
            var configuration = new MapperConfiguration(cfg => cfg.CreateMap<T, LaboratoryResponse>());
            var mapper = configuration.CreateMapper();
            return mapper.Map<T, LaboratoryResponse>(input);
        }
    }
}
