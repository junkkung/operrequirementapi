﻿using AutoMapper;
using Database;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EN = Database.Entities;

namespace Models.Responses
{
    public class StudentMinusPointResponse
    {
        public int Id { get; set; }
        public string MinusPointName { get; set; }
        public string LaboratoryName { get; set; }
        public string InstructorName { get; set; }
        public string Remark { get; set; }
        public int Score { get; set; }
        public DateTimeOffset CreateDate { get; set; }
        public int StatusId { get; set; }

        public static async Task<StudentMinusPointResponse> Map(EN.StudentMinusPoint input)
        {
            var configuration = new MapperConfiguration(cfg => cfg.CreateMap<EN.StudentMinusPoint, StudentMinusPointResponse>()
                //.ForMember(d => d.MinusPointName, opt => opt.MapFrom(s => s.MinusPoint.Name))
                .ForMember(d => d.LaboratoryName, opt => opt.MapFrom(s => s.StudentLaboratory.Laboratory.Name))
                .ForMember(d => d.InstructorName, opt => opt.MapFrom(s => s.Instructor.Firstname))
            );
            var mapper = configuration.CreateMapper();
            return mapper.Map<EN.StudentMinusPoint, StudentMinusPointResponse>(input);
        }

        public static async Task<List<StudentMinusPointResponse>> Map(List<EN.StudentMinusPoint> inputs)
        {
            var responses = new List<StudentMinusPointResponse>();
            foreach (var input in inputs)
            {
                var response = await Map(input);
                responses.Add(response);
            }


            return responses;
        }
    }
}
