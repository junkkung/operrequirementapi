﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;

namespace Models.Responses
{
    public class HttpCode <T>
    {
        public HttpStatusCode StatusCode { get; set; }
        public T Object { get; set; }

        public bool IsOK() => this.StatusCode == HttpStatusCode.OK;
        public bool IsNotFound() => this.StatusCode == HttpStatusCode.NotFound;
        public bool IsUnauthorized() => this.StatusCode == HttpStatusCode.Unauthorized;
        public bool IsGone() => this.StatusCode == HttpStatusCode.Gone;
        public bool IsNotAcceptable() => this.StatusCode == HttpStatusCode.NotAcceptable;
        public bool IsBadRequest() => this.StatusCode == HttpStatusCode.BadRequest;
        public bool IsCreated() => this.StatusCode == HttpStatusCode.Created;
        public bool IsConflict() => this.StatusCode == HttpStatusCode.Conflict;
        public bool IsNotImplemented() => this.StatusCode == HttpStatusCode.NotImplemented;
        public bool IsForbidden() => this.StatusCode == HttpStatusCode.Forbidden;
    }
}
