﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using EN = Database.Entities;

namespace Models.Responses
{
    public class LaboratoryImageResponse
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public List<StudentLaboratoryImageResponse> StudentLaboratoryImages { get; set; }


        public class StudentLaboratoryImageResponse
        {
            public int Id { get; set; }
            public string Image { get; set; }
            public string Remark { get; set; }
            public string InstructorName { get; set; }
        }

        public static async Task<LaboratoryImageResponse> Map(EN.LaboratoryImage input)
        {
            var configuration = new MapperConfiguration(cfg => cfg.CreateMap<EN.LaboratoryImage, LaboratoryImageResponse>()
                .ForMember(des => des.StudentLaboratoryImages, opt => opt.Ignore())
            );
            var mapper = configuration.CreateMapper();
            return mapper.Map<EN.LaboratoryImage, LaboratoryImageResponse>(input);
        }
    }
}
