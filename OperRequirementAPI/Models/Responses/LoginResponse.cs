﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models.Responses
{
    public class LoginResponse
    {
        public string AccessToken { get; set; }
        public string RefreshToken { get; set; }
        public int StudentId { get; set; }
    }
}
