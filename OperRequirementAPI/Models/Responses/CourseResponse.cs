﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;

namespace Models.Responses
{
    public class CourseResponse
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int SemesterYear { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int StatusId { get; set; }

        public static CourseResponse Map<T>(T input)
        {
            var configuration = new MapperConfiguration(cfg => cfg.CreateMap<T, CourseResponse>());
            var mapper = configuration.CreateMapper();
            return mapper.Map<T, CourseResponse>(input);
        }

        public static List<CourseResponse> Map<T>(List<T> inputs)
        {
            var responses = new List<CourseResponse>();
            if (inputs?.Count > 0) foreach (var input in inputs) responses.Add(Map(input));
            return responses;
        }
    }
}
