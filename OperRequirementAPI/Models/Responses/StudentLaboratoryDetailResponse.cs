﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Text;
using EN = Database.Entities;

namespace Models.Responses
{
    public class StudentLaboratoryDetailResponse
    {
        public int? Id { get; set; }
        public string Tooth { get; set; }
        public string KindOfWork { get; set; }
        public string Material { get; set; }
        public string Image { get; set; }
        public List<TopicResponse> Topics { get; set; }
        public LaboratoryResponse Laboratory { get; set; }
        public string InstructorName { get; set; }

        public StudentLaboratoryDetailResponse() { }

        public StudentLaboratoryDetailResponse(EN.StudentLaboratory studentLaboratory)
        {
            this.Id = studentLaboratory?.Id;
            this.Tooth = studentLaboratory?.Tooth;
            this.KindOfWork = studentLaboratory?.KindOfWork;
            this.Material = studentLaboratory?.Material;
            this.Image = studentLaboratory?.Image;
        }

        public class TopicResponse
        {
            public int Id { get; set; }
            public string Name { get; set; }
            public string Description { get; set; }
            public List<SubTopicResponse> SubTopics { get; set; }
            public int Score { get; set; }
            public int TotalScore { get; set; }
            public bool AllScored { get; set; }

            public TopicResponse() { }

            public TopicResponse (EN.Topic topic)
            {
                this.Id = topic?.Id ?? 0;
                this.Name = topic?.Name;
                this.Description = topic?.Description;
            }


            public static TopicResponse Map(EN.Topic topic)
            {
                var configuration = new MapperConfiguration(cfg => cfg.CreateMap<EN.Topic, TopicResponse>()
                    .ForMember(des => des.SubTopics, opt => opt.Ignore())
                    .ForMember(des => des.Score, opt => opt.Ignore())
                    .ForMember(des => des.TotalScore, opt => opt.Ignore())
                );
                var mapper = configuration.CreateMapper();
                return mapper.Map<EN.Topic, TopicResponse>(topic);
            }
        }

        public class SubTopicResponse
        {
            public int Id { get; set; }
            public string Name { get; set; }
            public string Description { get; set; }
            public List<CriteriaResponse> Criterias { get; set; }
            public CriticalResponse Critical { get; set; }
            public int? SelfAssessment { get; set; }
            public bool IsLabel { get; set; }

            public SubTopicResponse() { }

            public SubTopicResponse(EN.SubTopic subTopic)
            {
                this.Id = subTopic?.Id ?? 0 ;
                this.Name = subTopic?.Name;
                this.Description = subTopic?.Description;
                this.IsLabel = subTopic?.IsLabel ?? false;
            }

            public static SubTopicResponse Map(EN.SubTopic subTopic)
            {
                var configuration = new MapperConfiguration(cfg => cfg.CreateMap<EN.SubTopic, SubTopicResponse>()
                    .ForMember(des => des.Criterias, opt => opt.Ignore())
                    .ForMember(des => des.Critical, opt => opt.Ignore())
                    .ForMember(des => des.SelfAssessment, opt => opt.Ignore())
                );
                var mapper = configuration.CreateMapper();
                return mapper.Map<EN.SubTopic, SubTopicResponse>(subTopic);
            }
        }

        public class CriticalResponse
        {
            public int Id { get; set; }
            public string Name { get; set; }
            public bool IsCritical { get; set; }
            public string InstructorName { get; set; }
            public string Remark { get; set; }
            public DateTimeOffset UpdateDate { get; set; }

            public CriticalResponse() { }
            public CriticalResponse(EN.Critical critical)
            {
                this.Id = critical?.Id ?? 0;
                this.Name = critical?.Name;              
            }

            public static CriticalResponse Map(EN.Critical critical)
            {
                var configuration = new MapperConfiguration(cfg => cfg.CreateMap<EN.Critical, CriticalResponse>()
                    .ForMember(des => des.InstructorName, opt => opt.Ignore())
                    .ForMember(des => des.Remark, opt => opt.Ignore())
                );
                var mapper = configuration.CreateMapper();
                return mapper.Map<EN.Critical, CriticalResponse>(critical);
            }
        }

        public class CriteriaResponse
        {
            public int Id { get; set; }
            public string Name { get; set; }
            public int CriteriaTypeId { get; set; }
            public string InstructorName { get; set; }
            public DateTimeOffset UpdateDate { get; set; }


            public CriteriaResponse() { }
            public CriteriaResponse(EN.Criteria criteria)
            {
                this.Id = criteria?.Id ?? 0;
                this.Name = criteria?.Name;
                this.CriteriaTypeId = criteria?.CriteriaTypeId ?? 0;
            }

            public static CriteriaResponse Map(EN.Criteria criteria)
            {
                var configuration = new MapperConfiguration(cfg => cfg.CreateMap<EN.Criteria, CriteriaResponse>()
                    .ForMember(des => des.InstructorName, opt => opt.Ignore())
                );
                var mapper = configuration.CreateMapper();
                return mapper.Map<EN.Criteria, CriteriaResponse>(criteria);
            }

            public static IEnumerable<CriteriaResponse> Map(IEnumerable<EN.Criteria> criterias)
            {
                var criteriaResponses = new List<CriteriaResponse>();
                foreach (var criteria in criterias)
                    criteriaResponses.Add(Map(criteria));

                return criteriaResponses;
            }
        }


        public static StudentLaboratoryDetailResponse Map(EN.StudentLaboratory studentLaboratory)
        {
            var configuration = new MapperConfiguration(cfg => cfg.CreateMap<EN.StudentLaboratory, StudentLaboratoryDetailResponse>()
                .ForMember(des => des.Topics, opt => opt.Ignore())
                .ForMember(des => des.Laboratory, opt => opt.Ignore())
                .ForMember(des => des.InstructorName, opt => opt.Ignore())
            );
            var mapper = configuration.CreateMapper();
            return mapper.Map<EN.StudentLaboratory, StudentLaboratoryDetailResponse>(studentLaboratory);
        }
    }
}
