﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using EN = Database.Entities;

namespace Models.Responses
{
    public class StudentResponse
    {
        public int Id { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Image { get; set; }
        public string Email { get; set; }
        public string Telephone { get; set; }
        public int CourseId { get; set; }
        public string UniversityIdNumber { get; set; }
        public int Number { get; set; }
        public int Group { get; set; }
        public int StatusId { get; set; }

        public StudentResponse() { }

        public StudentResponse (EN.Student student)
        {
            this.Id = student?.Id ?? 0;
            this.Firstname = student?.Firstname;
            this.Lastname = student?.Lastname;
            this.Image = student?.Image;
            this.Email = student?.Email;
            this.Telephone = student?.Telephone;
            this.CourseId = student?.CourseId ?? 0;
            this.UniversityIdNumber = student?.UniversityIdNumber;
            this.Number = student?.Number ?? 0;
            this.Group = student?.Group ?? 0;
            this.StatusId = student?.StatusId ?? 0;
        }

        public static StudentResponse Map<T>(T input)
        {
            var configuration = new MapperConfiguration(cfg => cfg.CreateMap<T, StudentResponse>());
            var mapper = configuration.CreateMapper();
            return mapper.Map<T, StudentResponse>(input);
        }
    }
}
