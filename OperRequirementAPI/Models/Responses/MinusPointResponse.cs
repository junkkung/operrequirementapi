﻿using System;
using System.Collections.Generic;
using System.Text;

using EN = Database.Entities;

namespace Models.Responses
{
    public class MinusPointResponse
    {
        public List<EN.MinusPoint> MinusPoints { get; set; }
        public int MaxMinusPointPerTime { get; set; }
    }
}
