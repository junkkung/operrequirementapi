﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using Utilities;
using EN = Database.Entities;

namespace Models.Responses
{
    public class StudentToothApprovalResponse
    {
        public int Id { get; set; }
        public bool? IsApproved { get; set; }
        public string Comment { get; set; }
        public string InstructorName { get; set; }
        public DateTimeOffset UpdateDate { get; set; }

        public static StudentToothApprovalResponse Map(EN.StudentLaboratoryTeethApproval teethApproval)
        {
            var InstructorName = Parse.ToSignature(teethApproval?.Instructor?.Firstname, teethApproval?.Instructor?.Lastname);
            var configuration = new MapperConfiguration(cfg => cfg.CreateMap<EN.StudentLaboratoryTeethApproval, StudentToothApprovalResponse>()
                 .ForMember(d => d.InstructorName, opt => opt.MapFrom(s => InstructorName))
            );
            var mapper = configuration.CreateMapper();
            return mapper.Map<EN.StudentLaboratoryTeethApproval, StudentToothApprovalResponse>(teethApproval);
        }
    }
}
