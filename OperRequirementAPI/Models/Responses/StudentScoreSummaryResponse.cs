﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models.Responses
{
    public class StudentScoreSummaryResponse
    {
        public int TotalMinusPoint { get; set; }
        public int SummaryScore { get; set; }
        public int TotalScore { get; set; }
        public int NetScore { get; set; }
        public List<LabResponse> LaboratoriesSummary { get; set; }

        public class LabResponse
        {
            public LaboratoryResponse LaboratoryResponse { get; set; }
            public int Score { get; set; }
            public bool IsLabDone { get; set; }
        }
    }
}
