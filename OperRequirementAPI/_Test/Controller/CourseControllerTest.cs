﻿using System;
using System.Collections.Generic;
using System.Text;
using TU = _Test.Utilities;
using UT = Utilities;
using EN = Database.Entities;
using Database;
using Microsoft.Extensions.Configuration;
using API.Controllers;
using Bogus;
using Xunit;
using System.Threading.Tasks;
using _Test.Utilities.Generator;
using Microsoft.AspNetCore.Mvc;
using Models.Responses;
using System.Linq;
using Moq;
using System.Security.Claims;

namespace _Test.Controller
{
    public class CourseControllerTest : IDisposable
    {
        private readonly OperRequirementContext context;
        private readonly IConfiguration appSetting;
        private readonly CourseController controller;
        private readonly Faker faker;

        public CourseControllerTest()
        {
            this.context = TU.Utils.GetMemoryContext($"CourseController{Guid.NewGuid().ToString()}");
            this.appSetting = TU.Utils.InitConfiguration();
            var controllers = new TU.NewController(this.context);
            this.controller = controllers.CourseController;
            this.faker = new Faker();
        }

        public void Dispose()
        {
            UT.CacheManager.RemoveAll();
            context.Database.EnsureDeleted();
        }

        [Fact]
        public async Task GetAllCourse()
        {
            //Assign
            var course1 = await CourseGenerator.Generate(context, new EN.Course { SemesterYear = 3 });
            var course2 = await CourseGenerator.Generate(context, new EN.Course { SemesterYear = 1 });
            var course3 = await CourseGenerator.Generate(context, new EN.Course { SemesterYear = 2 });

            //Act
            var getResponse = await controller.GetAll();
            var responses = (getResponse as ObjectResult).Value as List<CourseResponse>;

            //Assert
            Assert.Equal(typeof(OkObjectResult), getResponse.GetType());
            Assert.Equal(3, responses.Count);

            var expectedList = new List<EN.Course> { course1, course3, course2 };
            for (var i = 0; i < responses.Count; i++)
            {
                var response = responses[i];
                Assert.Equal(expectedList[i].Id, response.Id);
                Assert.Equal(expectedList[i].Name, response.Name);
                Assert.Equal(expectedList[i].SemesterYear, response.SemesterYear);
                Assert.Equal(expectedList[i].StartDate.ToString(), response.StartDate.ToString());
                Assert.Equal(expectedList[i].EndDate.ToString(), response.EndDate.ToString());
            }
        }

        [Fact]
        public async Task GetStudentByCourseId()
        {
            //Assign
            var iToken = new Mock<UT.IToken>();
            iToken.Setup(a => a.IsInStructorRequest(It.IsAny<ClaimsPrincipal>()))
                .Returns(true);
            var controllers = new TU.NewController(context, iToken.Object);
            var controller = controllers.CourseController;

            var course1 = await CourseGenerator.Generate(context);
            var course2 = await CourseGenerator.Generate(context);

            var students = new List<EN.Student>();
            for (var i = 0; i < 10; i++)
                students.Add(await StudentGenerator.Generate(context, appSetting, new EN.Student
                {
                    Group = i <= 2 ? 1 : (i > 2 && i <= 5 ? 2 : 3),
                    CourseId = course1.Id
                }));

            for (var i = 0; i < 7; i++)
                students.Add(await StudentGenerator.Generate(context, appSetting, new EN.Student
                {
                    Group = i <= 2 ? 1 : (i > 2 && i <= 5 ? 2 : 3),
                    CourseId = course2.Id
                }));

            //Act
            var response = await controller.GetStudent(course1.Id);
            var groups = (response as ObjectResult).Value as List<GroupStudentResponse>;
            //Assert
            Assert.Equal(typeof(OkObjectResult), response.GetType());
            Assert.Equal(3, groups.Count);

            var group = groups[0];
            Assert.Equal(1, group.StudentGroupId);
            var studentIds = group.Students.Select(s => s.Id);
            Assert.Equal(3, studentIds.Count());
            Assert.Contains(students[0].Id, studentIds);
            Assert.Contains(students[1].Id, studentIds);
            Assert.Contains(students[2].Id, studentIds);

            group = groups[1];
            Assert.Equal(2, group.StudentGroupId);
            studentIds = group.Students.Select(s => s.Id);
            Assert.Equal(3, studentIds.Count());
            Assert.Contains(students[3].Id, studentIds);
            Assert.Contains(students[4].Id, studentIds);
            Assert.Contains(students[5].Id, studentIds);

            group = groups[2];
            Assert.Equal(3, group.StudentGroupId);
            studentIds = group.Students.Select(s => s.Id);
            Assert.Equal(4, studentIds.Count());
            Assert.Contains(students[6].Id, studentIds);
            Assert.Contains(students[7].Id, studentIds);
            Assert.Contains(students[8].Id, studentIds);
            Assert.Contains(students[9].Id, studentIds);
        }

        [Fact]
        public async Task GetStudentByCourseIdNotExisting_OK()
        {
            //Assign
            var iToken = new Mock<UT.IToken>();
            iToken.Setup(a => a.IsInStructorRequest(It.IsAny<ClaimsPrincipal>()))
                .Returns(true);
            var controllers = new TU.NewController(context, iToken.Object);
            var controller = controllers.CourseController;

            //Act
            var response = await controller.GetStudent(1);
            var groups = (response as ObjectResult).Value;

            //Assert
            Assert.Equal(typeof(OkObjectResult), response.GetType());
            Assert.Null(groups);
        }

        [Fact]
        public async Task GetStudentByCourseIdNoStudentInCourse_OK()
        {
            //Assign
            var iToken = new Mock<UT.IToken>();
            iToken.Setup(a => a.IsInStructorRequest(It.IsAny<ClaimsPrincipal>()))
                .Returns(true);
            var controllers = new TU.NewController(context, iToken.Object);
            var controller = controllers.CourseController;

            var course = await CourseGenerator.Generate(context);

            //Act
            var response = await controller.GetStudent(course.Id);
            var groups = (response as ObjectResult).Value;

            //Assert
            Assert.Equal(typeof(OkObjectResult), response.GetType());
            Assert.Null(groups);
        }
    }
}
