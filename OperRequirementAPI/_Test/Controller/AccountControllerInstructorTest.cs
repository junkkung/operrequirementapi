﻿using TU = _Test.Utilities;
using UT = Utilities;
using API.Controllers;
using Bogus;
using Database;
using Microsoft.Extensions.Configuration;
using Models.Functions;
using Models.Requests;
using Models.Responses;
using Moq;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Utilities;
using Xunit;
using _Test.Utilities.Preset;
using _Test.Utilities.Generator;
using Microsoft.AspNetCore.Mvc;

namespace _Test.Controller
{
    public class AccountControllerInstructorTest : IDisposable
    {
        private readonly OperRequirementContext context;
        private readonly IConfiguration appSetting;
        private readonly AccountController controller;
        private readonly Faker faker;

        public AccountControllerInstructorTest()
        {
            this.context = TU.Utils.GetMemoryContext($"AccountInstructorController{Guid.NewGuid().ToString()}");
            this.appSetting = TU.Utils.InitConfiguration();
            var controllers = new TU.NewController(this.context);
            this.controller = controllers.AccountController;
            this.faker = new Faker();
        }

        public void Dispose()
        {
            UT.CacheManager.RemoveAll();
            context.Database.EnsureDeleted();
        }

        [Fact]
        public async Task LoginInstructor()
        {
            // Assign
            await CommonPreset.Generate(context);
            var username = faker.Internet.UserName();
            var password = faker.Internet.Password();
            var instructor = await InstructorGenerator.Generate(context, appSetting, new Database.Entities.Instructor
            {
                Username = username,
                Password = password
            });

            var loginRequest = new LoginRequest { Username = username, Password = password };

            // Act
            var loginResponse = await controller.LoginInstructor(loginRequest);
            var response = (loginResponse as ObjectResult).Value as LoginResponse;
            // Assert
            Assert.Equal(typeof(OkObjectResult), loginResponse.GetType());
            Assert.NotNull(response.AccessToken);
            Assert.NotNull(response.RefreshToken);
        }

        [Fact]
        public async Task LoginNotExistingInstructor_NotFound()
        {
            await CommonPreset.Generate(context);
            var username = faker.Internet.UserName();
            var password = faker.Internet.Password();

            var loginRequest = new LoginRequest { Username = username, Password = password };

            // Act
            var loginResponse = await controller.LoginInstructor(loginRequest);

            // Assert
            Assert.Equal(typeof(NotFoundObjectResult), loginResponse.GetType());
        }

        [Fact]
        public async Task LoginInstructorPasswordMistake_Unauthorized()
        {
            // Assign
            await CommonPreset.Generate(context);
            var username = faker.Internet.UserName();
            var password = faker.Internet.Password();
            var instructor = await InstructorGenerator.Generate(context, appSetting, new Database.Entities.Instructor
            {
                Username = username,
                Password = password
            });

            var loginRequest = new LoginRequest { Username = username, Password = faker.Internet.Password()
        };

            // Act
            var loginResponse = await controller.LoginInstructor(loginRequest);

            // Assert
            Assert.Equal(typeof(BadRequestObjectResult), loginResponse.GetType());
        }

        [Theory]
        [InlineData("Abc12354")]
        [InlineData("ABC12354")]
        public async Task LoginInstructorNoCaseSensitiveForUsername(string usernameInput)
        {
            // Assign
            await CommonPreset.Generate(context);
            var username = "abc12354";
            var password = faker.Internet.Password();
            var instructor = await InstructorGenerator.Generate(context, appSetting, new Database.Entities.Instructor
            {
                Username = username,
                Password = password
            });

            var loginRequest = new LoginRequest { Username = usernameInput, Password = password };

            // Act
            var loginResponse = await controller.LoginInstructor(loginRequest);
            var response = (loginResponse as ObjectResult).Value as LoginResponse;
            // Assert
            Assert.Equal(typeof(OkObjectResult), loginResponse.GetType());
            Assert.NotNull(response.AccessToken);
            Assert.NotNull(response.RefreshToken);
        }

        [Fact]
        public async Task GetNewAccessTokenWithValidRefreshToken()
        {
            //Assign
            var preset = await LoginPreset.GenerateInstructor(this.context, this.appSetting, this.controller);
            LoginPreset.AddToken(this.controller, preset.RefreshToken, preset.Instructor.Id, UT.TokenRole.Instructor);
            //Act
            var refreshResponse = await controller.RefreshAccessInstructor();
            var response = (refreshResponse as ObjectResult).Value as LoginResponse;

            //Assert
            Assert.Equal(typeof(OkObjectResult), refreshResponse.GetType());
            Assert.NotNull(response.AccessToken);
            Assert.NotNull(response.RefreshToken);
            Assert.NotEqual(preset.AccessToken, response.AccessToken);
            Assert.NotEqual(preset.RefreshToken, response.RefreshToken);
        }

        [Fact]
        public async Task GetNewAccessTokenWithNotMatchRefreshToken_BadRequest()
        {
            //Assign
            var preset = await LoginPreset.GenerateInstructor(this.context, this.appSetting, this.controller);
            LoginPreset.AddToken(this.controller, faker.Internet.UserName(), preset.Instructor.Id, UT.TokenRole.Instructor);
            //Act
            var refreshResponse = await controller.RefreshAccessInstructor();

            //Assert
            Assert.Equal(typeof(BadRequestResult), refreshResponse.GetType());
        }

        [Fact]
        public async Task GetNewAccessTokenWithNotFoundInstructor_BadRequest()
        {
            //Assign
            var preset = await LoginPreset.GenerateInstructor(this.context, this.appSetting, this.controller);
            LoginPreset.AddToken(this.controller, faker.Internet.UserName(), 9999, UT.TokenRole.Instructor);
            //Act
            var refreshResponse = await controller.RefreshAccessInstructor();

            //Assert
            Assert.Equal(typeof(BadRequestResult), refreshResponse.GetType());
        }

        [Fact]
        public async Task GetNewAccessTokenWithInstructorInactived_BadRequest()
        {
            //Assign
            var preset = await LoginPreset.GenerateInstructor(this.context, this.appSetting, this.controller);
            preset.Instructor.StatusId = Status.Inactive.Id;
            context.Instructors.Update(preset.Instructor);
            await context.SaveChangesAsync();
            LoginPreset.AddToken(this.controller, preset.RefreshToken, preset.Instructor.Id, UT.TokenRole.Instructor);
            //Act
            var refreshResponse = await controller.RefreshAccessInstructor();

            //Assert
            Assert.Equal(typeof(BadRequestResult), refreshResponse.GetType());
        }

        [Fact]
        public async Task SignUpInstructorAllField()
        {
            //Assign
            await CommonPreset.Generate(context);
            var request = new SignUpInstructorRequest
            {
                Firstname = faker.Name.FirstName(),
                Lastname = faker.Name.LastName(),
                Username = faker.Internet.UserName(),
                Password = "1Aa2345@",
                Image = faker.Random.String(10),
                Email = faker.Internet.Email(),
                Telephone = faker.Phone.PhoneNumber(),
                ConfirmCode = (await InstructorConfirmCodeGenerator.Generate(context)).Code,
            };

            //Act
            var response = await controller.SignUpInstructor(request);

            //Assert
            Assert.Equal(typeof(CreatedResult), response.GetType());

        }

        [Fact]
        public async Task SignUpInstructorOnlyRequiredField()
        {
            //Assign
            await CommonPreset.Generate(context);
            var request = new SignUpInstructorRequest
            {
                Firstname = faker.Name.FirstName(),
                Lastname = faker.Name.LastName(),
                Username = faker.Internet.UserName(),
                Password = "1Aa2345@",
                //Image = faker.Random.String(10),
                //Email = faker.Internet.Email(),
                //Telephone = faker.Phone.PhoneNumber(),
                ConfirmCode = (await InstructorConfirmCodeGenerator.Generate(context)).Code,
            };

            //Act
            var response = await controller.SignUpInstructor(request);

            //Assert
            Assert.Equal(typeof(CreatedResult), response.GetType());

        }

        [Fact]
        public async Task SignUpInstructorUsernameAlreadyExist_Conflict()
        {
            //Assign
            await CommonPreset.Generate(context);
            var instructor = await InstructorGenerator.Generate(context, appSetting);
            var request = new SignUpInstructorRequest
            {
                Firstname = faker.Name.FirstName(),
                Lastname = faker.Name.LastName(),
                Username = instructor.Username,
                Password = "1Aa2345@",
                ConfirmCode = (await InstructorConfirmCodeGenerator.Generate(context)).Code,
            };

            //Act
            var response = await controller.SignUpInstructor(request) as ConflictObjectResult;

            //Assert
            Assert.Equal((int)HttpStatusCode.Conflict, response.StatusCode);

        }

        [Theory]
        [InlineData("aB3&ae$", typeof(BadRequestObjectResult))] // range < 8
        [InlineData("aB3sevae", typeof(BadRequestObjectResult))] // special charactor
        [InlineData("aBeas&ae", typeof(BadRequestObjectResult))] // numeric
        [InlineData("a3&ssddae$", typeof(BadRequestObjectResult))] // capital
        public async Task SignUpInstructorPassword_BadRequest(string password, Type expected)
        {
            //Assign
            await CommonPreset.Generate(context);
            var request = new SignUpInstructorRequest
            {
                Firstname = faker.Name.FirstName(),
                Lastname = faker.Name.LastName(),
                Username = faker.Internet.UserName(),
                Password = password,
                ConfirmCode = (await InstructorConfirmCodeGenerator.Generate(context)).Code,
            };

            //Act
            var response = await controller.SignUpInstructor(request);

            //Assert
            Assert.Equal(expected, response.GetType());

        }

        [Fact]
        public async Task SignUpInstructorConfirmCodeNotExist_NotFound()
        {
            //Assign
            await CommonPreset.Generate(context);
            var request = new SignUpInstructorRequest
            {
                Firstname = faker.Name.FirstName(),
                Lastname = faker.Name.LastName(),
                Username = faker.Internet.UserName(),
                Password = "1Aa2345@",
                ConfirmCode = faker.Random.String(10),
            };

            //Act
            var response = await controller.SignUpInstructor(request);

            //Assert
            Assert.Equal(typeof(NotFoundObjectResult), response.GetType());

        }

        [Fact]
        public async Task SignUpWillChangeUsernameToLower()
        {
            //Assign
            await CommonPreset.Generate(context);
            var request = new SignUpInstructorRequest
            {
                Firstname = faker.Name.FirstName(),
                Lastname = faker.Name.LastName(),
                Username = "AAb12345",
                Password = "1Aa2345@",
                Image = faker.Random.String(10),
                Email = faker.Internet.Email(),
                Telephone = faker.Phone.PhoneNumber(),
                ConfirmCode = (await InstructorConfirmCodeGenerator.Generate(context)).Code,
            };

            //Act
            var response = await controller.SignUpInstructor(request);
            var instructor = await context.Instructors.FindAsync(1);

            //Assert
            Assert.Equal(typeof(CreatedResult), response.GetType());
            Assert.Equal(request.Username.ToLower(), instructor.Username);
        }

        [Fact]
        public async Task ChangePasswordAndTryLogin()
        {
            // Assign
            await CommonPreset.Generate(context);
            var username = faker.Internet.UserName();
            var password = faker.Internet.Password();
            var instructor = await InstructorGenerator.Generate(context, appSetting, new Database.Entities.Instructor
            {
                Username = username,
                Password = password
            });

            var newPassword = "/12ABbde$";
            var changePasswordRequest = new ChangePasswordRequest
            {
                Username = username,
                NewPassword = newPassword,
                ConfirmCode = (await InstructorConfirmCodeGenerator.Generate(context)).Code
            };

            var loginRequest = new LoginRequest { Username = username, Password = newPassword };

            // Act
            var changePasswordResponse = await controller.ChangePasswordInstructor(changePasswordRequest);
            var loginResponse = await controller.LoginInstructor(loginRequest);
            var Response = (loginResponse as ObjectResult).Value as LoginResponse;
            // Assert
            Assert.Equal(typeof(OkObjectResult), changePasswordResponse.GetType());
            Assert.Equal(typeof(OkObjectResult), loginResponse.GetType());
            Assert.NotNull(Response.AccessToken);
            Assert.NotNull(Response.RefreshToken);
        }

        [Fact]
        public async Task ChangePasswordConfirmCode_NotFound()
        {
            // Assign
            await CommonPreset.Generate(context);
            var username = faker.Internet.UserName();
            var password = faker.Internet.Password();
            var instructor = await InstructorGenerator.Generate(context, appSetting, new Database.Entities.Instructor
            {
                Username = username,
                Password = password
            });

            var newPassword = "/12ABbde$";
            var changePasswordRequest = new ChangePasswordRequest
            {
                Username = username,
                NewPassword = newPassword,
                ConfirmCode = faker.Random.String(10)
            };

            // Act
            var changePasswordResponse = await controller.ChangePasswordInstructor(changePasswordRequest);

            // Assert
            Assert.Equal(typeof(NotFoundObjectResult), changePasswordResponse.GetType());
        }

        [Fact]
        public async Task ChangePasswordUser_NotFound()
        {
            // Assign
            await CommonPreset.Generate(context);
            var username = faker.Internet.UserName();
            var password = faker.Internet.Password();
            var instructor = await InstructorGenerator.Generate(context, appSetting, new Database.Entities.Instructor
            {
                Username = username,
                Password = password
            });

            var newPassword = "/12ABbde$";
            var changePasswordRequest = new ChangePasswordRequest
            {
                Username = faker.Random.String(10),
                NewPassword = newPassword,
                ConfirmCode = (await InstructorConfirmCodeGenerator.Generate(context)).Code
            };

            // Act
            var changePasswordResponse = await controller.ChangePasswordInstructor(changePasswordRequest);

            // Assert
            Assert.Equal(typeof(NotFoundObjectResult), changePasswordResponse.GetType());
        }

        [Theory]
        [InlineData("aB3&ae$", typeof(BadRequestObjectResult))] // range < 8
        [InlineData("aB3sevae", typeof(BadRequestObjectResult))] // special charactor
        [InlineData("aBeas&ae", typeof(BadRequestObjectResult))] // numeric
        [InlineData("a3&ssddae$", typeof(BadRequestObjectResult))] // capital
        public async Task ChangePasswordPasswordWrongFormat_BadRequest(string password, Type expected)
        {
            // Assign
            await CommonPreset.Generate(context);
            var username = faker.Internet.UserName();
            var instructor = await InstructorGenerator.Generate(context, appSetting, new Database.Entities.Instructor
            {
                Username = username,
                Password = faker.Internet.Password()
            });

            var changePasswordRequest = new ChangePasswordRequest
            {
                Username = username,
                NewPassword = password,
                ConfirmCode = (await InstructorConfirmCodeGenerator.Generate(context)).Code
            };

            // Act
            var changePasswordResponse = await controller.ChangePasswordInstructor(changePasswordRequest);

            // Assert
            Assert.Equal(expected, changePasswordResponse.GetType());
        }
    }
}
