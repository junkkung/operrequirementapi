﻿using _Test.Utilities.Generator;
using API.Controllers;
using Bogus;
using Database;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using TU = _Test.Utilities;
using UT = Utilities;
using EN = Database.Entities;
using FN = Models.Functions;
using Microsoft.AspNetCore.Mvc;
using Models.Responses;
using System.Linq;
using Moq;
using System.Security.Claims;
using Models.Requests;
using Microsoft.EntityFrameworkCore;
using System.Net;

namespace _Test.Controller
{
    public class StudentControllerTest : IDisposable
    {
        private readonly OperRequirementContext context;
        private readonly IConfiguration appSetting;
        private readonly StudentController controller;
        private readonly Faker faker;

        public StudentControllerTest()
        {
            this.context = TU.Utils.GetMemoryContext($"StudentController{Guid.NewGuid().ToString()}");
            this.appSetting = TU.Utils.InitConfiguration();
            var controllers = new TU.NewController(this.context);
            this.controller = controllers.StudentController;
            this.faker = new Faker();
        }        

        public void Dispose()
        {
            UT.CacheManager.RemoveAll();
            context.Database.EnsureDeleted();
        }       

        [Theory]
        [InlineData(1, 100, 1, 100, 10, 1, -91, -90)]
        [InlineData(1, 100, 1, 10, 10, 1, -1, 0)]
        [InlineData(1, 100, 1, 0, 10, 1, 9, 10)]
        [InlineData(5, 100, 5, 100, 10, 3, -93, -88)]
        public async Task GetStudentScoreSummaryOneLab(int totalSubTopicAdd, int expectTotalScore, int expectStudentScore,
                                    int startMinusPoint, int maxMinusPointPerTime, int minusPointScore, int expectTotalMinusPoint, int expectNetScore)
        {
            //Assign
            await TU.Preset.CommonPreset.Generate(context);
            var iToken = new Mock<UT.IToken>();
            iToken.Setup(a => a.CanProcess(It.IsAny<int>(), It.IsAny<ClaimsPrincipal>()))
                .Returns(true);
            var controllers = new TU.NewController(context, iToken.Object);
            var controller = controllers.StudentController;

            var minusPointConfig = await ConfigGenerator.Generate(context, new EN.Config
            {
                Id = 1,
                Value = $"{startMinusPoint}"
            });

            var maxMinusPointPerTimeConfig = await ConfigGenerator.Generate(context, new EN.Config
            {
                Id = 2,
                Value = $"{maxMinusPointPerTime}"
            });

            var course = await CourseGenerator.Generate(context);
            var student = await StudentGenerator.Generate(context, appSetting, new EN.Student
            {
                CourseId = course.Id
            });

            var preset = await TU.Preset.LabPreset.PrepareGoodLabWithSomeStudentScore(
                context, appSetting, course, student,
                new TU.Preset.LabPreset.Data
                {
                    GoodScore = 1,
                    FairScore = 0,
                    TotalTopic = 10,
                    TotalSubTopic = 10,
                    TotalSubTopicAdd = totalSubTopicAdd
                }
            );

           var minusPointLab = await StudentMinusPointGenerator.Generate(context, appSetting, new EN.StudentMinusPoint
            {
                StudentLaboratoryId = preset.StudentLaboratory.Id,
                Score = minusPointScore,
            });

            //Act
            var response = await controller.GetLabSummary(student.Id);
            var scoreSummary = (response as ObjectResult).Value as StudentScoreSummaryResponse;

            //Assert
            Assert.Equal(typeof(OkObjectResult), response.GetType());

            Assert.Equal(expectStudentScore, scoreSummary.SummaryScore);
            Assert.Equal(expectTotalMinusPoint, scoreSummary.TotalMinusPoint);
            Assert.Equal(expectNetScore, scoreSummary.NetScore);
            Assert.Equal(expectTotalScore, scoreSummary.TotalScore);
            Assert.Single(scoreSummary.LaboratoriesSummary);

            var labSummary = scoreSummary.LaboratoriesSummary[0];
            Assert.Equal(preset.Laboratory.Id, labSummary.LaboratoryResponse.Id);
            Assert.Equal(preset.Laboratory.Name, labSummary.LaboratoryResponse.Name);
            Assert.Equal(preset.Laboratory.Description, labSummary.LaboratoryResponse.Description);
            Assert.Equal(expectTotalScore, labSummary.LaboratoryResponse.TotalScore);
        }

        [Fact]
        public async Task GetStudentScoreSummaryManyLab()
        {
            //Assign
            await TU.Preset.CommonPreset.Generate(context);
            var iToken = new Mock<UT.IToken>();
            iToken.Setup(a => a.CanProcess(It.IsAny<int>(), It.IsAny<ClaimsPrincipal>()))
                .Returns(true);
            var controllers = new TU.NewController(context, iToken.Object);
            var controller = controllers.StudentController;

            var minusPointConfig = await ConfigGenerator.Generate(context, new EN.Config
            {
                Id = 1,
                Value = $"{140}"
            });

            var maxMinusPointPerTime = await ConfigGenerator.Generate(context, new EN.Config
            {
                Id = 2,
                Value = $"{10}"
            });

            var course = await CourseGenerator.Generate(context);
            var student = await StudentGenerator.Generate(context, appSetting, new EN.Student
            {
                CourseId = course.Id
            });

            var lab1Data = new TU.Preset.LabPreset.Data
            {
                GoodScore = 1,
                FairScore = 0,
                TotalTopic = 10,
                TotalSubTopic = 10,
                TotalSubTopicAdd = 5
            };
            var lab1TotalScore = (1 + 0) * 10 * 10;
            var presetLab1 = await TU.Preset.LabPreset.PrepareGoodLabWithSomeStudentScore(
                context, appSetting, course, student, lab1Data
            );

            var minusPointLab1 = await StudentMinusPointGenerator.Generate(context, appSetting, new EN.StudentMinusPoint
            {
                StudentLaboratoryId = presetLab1.StudentLaboratory.Id,
                Score = 10,
            });

            var lab2Data = new TU.Preset.LabPreset.Data
            {
                GoodScore = 3,
                FairScore = 0,
                TotalTopic = 15,
                TotalSubTopic = 20,
                TotalSubTopicAdd = 17
            };
            var lab2TotalScore = (3 + 0) * 15 * 20;
            var presetLab2 = await TU.Preset.LabPreset.PrepareGoodLabWithSomeStudentScore(
                context, appSetting, course, student, lab2Data
            );

            var minusPointLab2 = await StudentMinusPointGenerator.Generate(context, appSetting, new EN.StudentMinusPoint
            {
                StudentLaboratoryId = presetLab2.StudentLaboratory.Id,
                Score = 8,
            });

            var lab3Data = new TU.Preset.LabPreset.Data
            {
                GoodScore = 2,
                FairScore = 2,
                TotalTopic = 2,
                TotalSubTopic = 30,
                TotalSubTopicAdd = 25
            };
            var lab3TotalScore = (2 + 2) * 2 * 30;
            var presetLab3 = await TU.Preset.LabPreset.PrepareGoodLabWithSomeStudentScore(
                context, appSetting, course, student, lab3Data
            );

            var minusPointLab3 = await StudentMinusPointGenerator.Generate(context, appSetting, new EN.StudentMinusPoint
            {
                StudentLaboratoryId = presetLab3.StudentLaboratory.Id,
                Score = 23,
            });

            //Act
            var response = await controller.GetLabSummary(student.Id);
            var scoreSummary = (response as ObjectResult).Value as StudentScoreSummaryResponse;

            //Assert
            Assert.Equal(typeof(OkObjectResult), response.GetType());

            var totalStudentScore = (1 * 5) + (3 * 17) + ((2 + 2) * 25);
            Assert.Equal(totalStudentScore, scoreSummary.SummaryScore);

            var maxMinusPoint = int.Parse(maxMinusPointPerTime.Value);
            var totalMinusPoint = (maxMinusPoint - minusPointLab1.Score) + 
                (maxMinusPoint - minusPointLab2.Score) + 
                (maxMinusPoint - minusPointLab3.Score) - 
                int.Parse(minusPointConfig.Value);
            Assert.Equal(totalMinusPoint, scoreSummary.TotalMinusPoint);
            Assert.Equal(totalStudentScore + totalMinusPoint, scoreSummary.NetScore);
            Assert.Equal(lab1TotalScore + lab2TotalScore + lab3TotalScore, scoreSummary.TotalScore);
            Assert.Equal(3, scoreSummary.LaboratoriesSummary.Count);

            var presets = new List<TU.Preset.LabPreset> { presetLab1, presetLab2, presetLab3 };
            var expectsTotalScore = new List<int> { lab1TotalScore, lab2TotalScore, lab3TotalScore };
            for(var i = 0; i < scoreSummary.LaboratoriesSummary.Count; i++)
            {
                var preset = presets[i];
                var summary = scoreSummary.LaboratoriesSummary[i];
               
                Assert.Equal(preset.Laboratory.Id, summary.LaboratoryResponse.Id);
                Assert.Equal(preset.Laboratory.Name, summary.LaboratoryResponse.Name);
                Assert.Equal(preset.Laboratory.Description, summary.LaboratoryResponse.Description);
                Assert.Equal(expectsTotalScore[i], summary.LaboratoryResponse.TotalScore);
            }

        }

        [Fact]
        public async Task GetMinusPointOneLab()
        {
            //Assign
            await TU.Preset.CommonPreset.Generate(context);
            var iToken = new Mock<UT.IToken>();
            iToken.Setup(a => a.CanProcess(It.IsAny<int>(), It.IsAny<ClaimsPrincipal>()))
                .Returns(true);
            var controllers = new TU.NewController(context, iToken.Object);
            var controller = controllers.StudentController;

            var course = await CourseGenerator.Generate(context);
            var student = await StudentGenerator.Generate(context, appSetting, new EN.Student
            {
                CourseId = course.Id
            });

            var preset = await TU.Preset.LabPreset.PrepareGoodLabWithSomeStudentScore(
                context, appSetting, course, student,
                new TU.Preset.LabPreset.Data
                {
                    GoodScore = 1,
                    FairScore = 0,
                    TotalTopic = 10,
                    TotalSubTopic = 10,
                    TotalSubTopicAdd = 10
                }
            );

            var minusPoint = await MinusPointGenerator.Generate(context);
            var instructor = await InstructorGenerator.Generate(context, appSetting);

            var minusPointLab = await StudentMinusPointGenerator.Generate(context, appSetting, new EN.StudentMinusPoint
            {
                MinusPointId = minusPoint.Id,
                StudentLaboratoryId = preset.StudentLaboratory.Id,
                Score = -3,
                InstructorId = instructor.Id,                
            });

            //Act
            var response = await controller.GetMinusPoint(student.Id);
            var minusPointGets = (response as ObjectResult).Value as List<StudentMinusPointResponse>;

            //Assert
            Assert.Single(minusPointGets);

            var minusPointGet = minusPointGets[0];
            Assert.Equal(instructor.Firstname, minusPointGet.InstructorName);
            Assert.Equal(preset.Laboratory.Name, minusPointGet.LaboratoryName);
            Assert.Equal(minusPoint.Name, minusPointGet.MinusPointName);
            Assert.Equal(minusPointLab.Remark, minusPointGet.Remark);
            Assert.Equal(minusPointLab.Score, minusPointGet.Score);
            Assert.Equal(minusPointLab.CreateDate, minusPointGet.CreateDate);
            Assert.Equal(minusPointLab.StatusId, minusPointGet.StatusId);
        }

        [Fact]
        public async Task GetMinusPointManyLab()
        {
            //Assign
            await TU.Preset.CommonPreset.Generate(context);
            var iToken = new Mock<UT.IToken>();
            iToken.Setup(a => a.CanProcess(It.IsAny<int>(), It.IsAny<ClaimsPrincipal>()))
                .Returns(true);
            var controllers = new TU.NewController(context, iToken.Object);
            var controller = controllers.StudentController;

            var course = await CourseGenerator.Generate(context);
            var student = await StudentGenerator.Generate(context, appSetting, new EN.Student
            {
                CourseId = course.Id
            });

            var preset1 = await TU.Preset.LabPreset.PrepareGoodLabWithSomeStudentScore(
                context, appSetting, course, student,
                new TU.Preset.LabPreset.Data
                {
                    GoodScore = 1,
                    FairScore = 0,
                    TotalTopic = 10,
                    TotalSubTopic = 10,
                    TotalSubTopicAdd = 10
                }
            );

            var preset2 = await TU.Preset.LabPreset.PrepareGoodLabWithSomeStudentScore(
                context, appSetting, course, student,
                new TU.Preset.LabPreset.Data
                {
                    GoodScore = 1,
                    FairScore = 0,
                    TotalTopic = 10,
                    TotalSubTopic = 10,
                    TotalSubTopicAdd = 10
                }
            );

            var minusPoint1 = await MinusPointGenerator.Generate(context);
            var instructor1 = await InstructorGenerator.Generate(context, appSetting);

            var minusPointLab1 = await StudentMinusPointGenerator.Generate(context, appSetting, new EN.StudentMinusPoint
            {
                MinusPointId = minusPoint1.Id,
                StudentLaboratoryId = preset1.StudentLaboratory.Id,
                Score = -3,
                InstructorId = instructor1.Id,
            });

            var minusPoint2 = await MinusPointGenerator.Generate(context);
            var instructor2 = await InstructorGenerator.Generate(context, appSetting);

            var minusPointLab2 = await StudentMinusPointGenerator.Generate(context, appSetting, new EN.StudentMinusPoint
            {
                MinusPointId = minusPoint2.Id,
                StudentLaboratoryId = preset2.StudentLaboratory.Id,
                Score = 10,
                InstructorId = instructor2.Id,
            });

            //Act
            var response = await controller.GetMinusPoint(student.Id);
            var minusPointGets = (response as ObjectResult).Value as List<StudentMinusPointResponse>;

            //Assert
            Assert.Equal(2, minusPointGets.Count);

            var minusPointGet = minusPointGets[0];
            Assert.Equal(instructor1.Firstname, minusPointGet.InstructorName);
            Assert.Equal(preset1.Laboratory.Name, minusPointGet.LaboratoryName);
            Assert.Equal(minusPoint1.Name, minusPointGet.MinusPointName);
            Assert.Equal(minusPointLab1.Remark, minusPointGet.Remark);
            Assert.Equal(minusPointLab1.Score, minusPointGet.Score);
            Assert.Equal(minusPointLab1.CreateDate, minusPointGet.CreateDate);
            Assert.Equal(minusPointLab1.StatusId, minusPointGet.StatusId);

            minusPointGet = minusPointGets[1];
            Assert.Equal(instructor2.Firstname, minusPointGet.InstructorName);
            Assert.Equal(preset2.Laboratory.Name, minusPointGet.LaboratoryName);
            Assert.Equal(minusPoint2.Name, minusPointGet.MinusPointName);
            Assert.Equal(minusPointLab2.Remark, minusPointGet.Remark);
            Assert.Equal(minusPointLab2.Score, minusPointGet.Score);
            Assert.Equal(minusPointLab2.CreateDate, minusPointGet.CreateDate);
            Assert.Equal(minusPointLab2.StatusId, minusPointGet.StatusId);
        }       

        [Fact]
        public async Task GetStudentLabDetail()
        {
            //Arrange
            await TU.Preset.CommonPreset.Generate(context);
            var iToken = new Mock<UT.IToken>();
            iToken.Setup(a => a.CanProcess(It.IsAny<int>(), It.IsAny<ClaimsPrincipal>()))
                .Returns(true);
            var controllers = new TU.NewController(context, iToken.Object);
            var controller = controllers.StudentController;

            var labPreset = await TU.Preset.LabPreset.PrepareEmptyLab(context, appSetting);
            var studentPreset = await TU.Preset.StudentPreset.PrepareEmptyStudent(context, appSetting);

            //Act
            var response = await controller.GetLabDetail(studentPreset.Student.Id, labPreset.Laboratory.Id);
            var labDetail = (response as ObjectResult).Value as StudentLaboratoryDetailResponse;

            //Assert
            Assert.Equal(typeof(OkObjectResult), response.GetType());
            Assert.NotNull(labDetail);
        }

        [Fact]
        public async Task GetStudentLabDetailWhenNotExistStudent()
        {
            //Arrange
            await TU.Preset.CommonPreset.Generate(context);
            var iToken = new Mock<UT.IToken>();
            iToken.Setup(a => a.CanProcess(It.IsAny<int>(), It.IsAny<ClaimsPrincipal>()))
                .Returns(true);
            var controllers = new TU.NewController(context, iToken.Object);
            var controller = controllers.StudentController;

            var labPreset = await TU.Preset.LabPreset.PrepareEmptyLab(context, appSetting);

            //Act
            var response = await controller.GetLabDetail(10, labPreset.Laboratory.Id);

            //Assert
            Assert.Equal(typeof(NotFoundObjectResult), response.GetType());
        }

        [Fact]
        public async Task GetStudentLabDetailWhenNotExistLaboratory()
        {
            //Arrange
            await TU.Preset.CommonPreset.Generate(context);
            var iToken = new Mock<UT.IToken>();
            iToken.Setup(a => a.CanProcess(It.IsAny<int>(), It.IsAny<ClaimsPrincipal>()))
                .Returns(true);
            var controllers = new TU.NewController(context, iToken.Object);
            var controller = controllers.StudentController;

            var studentPreset = await TU.Preset.StudentPreset.PrepareEmptyStudent(context, appSetting);

            //Act
            var response = await controller.GetLabDetail(studentPreset.Student.Id, 10);

            //Assert
            Assert.Equal(typeof(NotFoundObjectResult), response.GetType());
        }

        [Fact]
        public async Task GetStudentExperience()
        {
            //Arrange
            await TU.Preset.CommonPreset.Generate(context);
            var iToken = new Mock<UT.IToken>();
            iToken.Setup(a => a.CanProcess(It.IsAny<int>(), It.IsAny<ClaimsPrincipal>()))
                .Returns(true);
            var controllers = new TU.NewController(context, iToken.Object);
            var controller = controllers.StudentController;

            var labPreset = await TU.Preset.LabPreset.PrepareEmptyLab(context, appSetting);
            var student = await StudentGenerator.Generate(context, appSetting);

            //Act
            var response = await controller.GetExperience(student.Id);
            var experiences = (response as ObjectResult).Value as List<StudentExperienceResponse>;

            //Assert
            Assert.Equal(typeof(OkObjectResult), response.GetType());
            Assert.Single(experiences);
            var experience = experiences[0];

            Assert.Equal(labPreset.Experience.Id, experience.Id);
            Assert.Equal(labPreset.Experience.Name, experience.Name);
            Assert.Equal(labPreset.Experience.Description, experience.Description);
            Assert.False(experience.IsAchieve);
        }

        [Fact]
        public async Task GetStudentExperienceWhenStudentNotExist()
        {
            //Arrange
            await TU.Preset.CommonPreset.Generate(context);
            var iToken = new Mock<UT.IToken>();
            iToken.Setup(a => a.CanProcess(It.IsAny<int>(), It.IsAny<ClaimsPrincipal>()))
                .Returns(true);
            var controllers = new TU.NewController(context, iToken.Object);
            var controller = controllers.StudentController;

            var labPreset = await TU.Preset.LabPreset.PrepareEmptyLab(context, appSetting);

            //Act
            var response = await controller.GetExperience(10);

            //Assert
            Assert.Equal(typeof(NotFoundObjectResult), response.GetType());
        }

        [Fact]
        public async Task AddMinusPoint()
        {
            //Arrange
            await TU.Preset.CommonPreset.Generate(context);
            var iToken = new Mock<UT.IToken>();
            iToken.Setup(a => a.IsInStructorRequest(It.IsAny<ClaimsPrincipal>()))
                .Returns(true);
            var controllers = new TU.NewController(context, iToken.Object);
            var controller = controllers.StudentController;

            await TU.Preset.CommonPreset.Generate(context);
            var MaxMinusPointPerTime = await ConfigGenerator.Generate(context, new EN.Config
            {
                Id = 2,
                Name = "MaxMinusPointPerTime",
                Value = "10"
            });
            var course = await CourseGenerator.Generate(context);
            var instructor = await InstructorGenerator.Generate(context, appSetting);
            controller.ControllerContext.HttpContext = TU.Utils.AddClaimPrinciple(instructor.Id, UT.TokenRole.Instructor);

            var student = await StudentGenerator.Generate(context, appSetting, new EN.Student
            {
                CourseId = course.Id
            });

            var presetLab = await TU.Preset.LabPreset.PrepareGoodLabWithSomeStudentScore(
                context, appSetting, course, student, new TU.Preset.LabPreset.Data()
            );

            var minusPoint = await MinusPointGenerator.Generate(context);

            var request = new MinusPointAddRequest
            {
                LaboratoryId = presetLab.Laboratory.Id,
                MinusPointId = minusPoint.Id,
                Score = faker.Random.Int(0, 10),
                Remark = faker.Name.FirstName()
            };

            //Act
            var response = await controller.AddMinusPoint(student.Id,request);
            var studentMinusPoints = await context.StudentMinusPoints.Select(s => s).AsNoTracking().ToListAsync();

            //Assert
            Assert.Equal(typeof(CreatedResult), response.GetType());
            Assert.Single(studentMinusPoints);
        }

        [Fact]
        public async Task AddMinusPointWhenStudentNotExisting_NotFound()
        {
            //Arrange
            await TU.Preset.CommonPreset.Generate(context);
            var iToken = new Mock<UT.IToken>();
            iToken.Setup(a => a.IsInStructorRequest(It.IsAny<ClaimsPrincipal>()))
                .Returns(true);
            var controllers = new TU.NewController(context, iToken.Object);
            var controller = controllers.StudentController;

            await TU.Preset.CommonPreset.Generate(context);
            var MaxMinusPointPerTime = await ConfigGenerator.Generate(context, new EN.Config
            {
                Id = 2,
                Name = "MaxMinusPointPerTime",
                Value = "10"
            });
            var course = await CourseGenerator.Generate(context);
            var instructor = await InstructorGenerator.Generate(context, appSetting);
            controller.ControllerContext.HttpContext = TU.Utils.AddClaimPrinciple(instructor.Id, UT.TokenRole.Instructor);

            var student = await StudentGenerator.Generate(context, appSetting, new EN.Student
            {
                CourseId = course.Id
            });

            var presetLab = await TU.Preset.LabPreset.PrepareGoodLabWithSomeStudentScore(
                context, appSetting, course, student, new TU.Preset.LabPreset.Data()
            );

            var minusPoint = await MinusPointGenerator.Generate(context);

            var request = new MinusPointAddRequest
            {
                LaboratoryId = presetLab.Laboratory.Id,
                MinusPointId = minusPoint.Id,
                Score = faker.Random.Int(0, 10),
                Remark = faker.Name.FirstName()
            };

            //Act
            var response = await controller.AddMinusPoint(1000, request);
            var studentMinusPoints = await context.StudentMinusPoints.Select(s => s).AsNoTracking().ToListAsync();

            //Assert
            Assert.Equal(typeof(NotFoundObjectResult), response.GetType());
        }

        [Fact]
        public async Task AddMinusPointWhenLaboratoryNotExisting_NotFound()
        {
            //Arrange
            await TU.Preset.CommonPreset.Generate(context);
            var iToken = new Mock<UT.IToken>();
            iToken.Setup(a => a.IsInStructorRequest(It.IsAny<ClaimsPrincipal>()))
                .Returns(true);
            var controllers = new TU.NewController(context, iToken.Object);
            var controller = controllers.StudentController;

            await TU.Preset.CommonPreset.Generate(context);
            var MaxMinusPointPerTime = await ConfigGenerator.Generate(context, new EN.Config
            {
                Id = 2,
                Name = "MaxMinusPointPerTime",
                Value = "10"
            });
            var course = await CourseGenerator.Generate(context);
            var instructor = await InstructorGenerator.Generate(context, appSetting);
            controller.ControllerContext.HttpContext = TU.Utils.AddClaimPrinciple(instructor.Id, UT.TokenRole.Instructor);

            var student = await StudentGenerator.Generate(context, appSetting, new EN.Student
            {
                CourseId = course.Id
            });

            var minusPoint = await MinusPointGenerator.Generate(context);

            var request = new MinusPointAddRequest
            {
                LaboratoryId = 1000,
                MinusPointId = minusPoint.Id,
                Score = faker.Random.Int(0, 10),
                Remark = faker.Name.FirstName()
            };

            //Act
            var response = await controller.AddMinusPoint(student.Id, request);
            var studentMinusPoints = await context.StudentMinusPoints.Select(s => s).AsNoTracking().ToListAsync();

            //Assert
            Assert.Equal(typeof(NotFoundObjectResult), response.GetType());
        }

        [Fact]
        public async Task AddMinusPointWhenMinusPointNotExisting_NotFound()
        {
            //Arrange
            await TU.Preset.CommonPreset.Generate(context);
            var iToken = new Mock<UT.IToken>();
            iToken.Setup(a => a.IsInStructorRequest(It.IsAny<ClaimsPrincipal>()))
                .Returns(true);
            var controllers = new TU.NewController(context, iToken.Object);
            var controller = controllers.StudentController;

            await TU.Preset.CommonPreset.Generate(context);
            var MaxMinusPointPerTime = await ConfigGenerator.Generate(context, new EN.Config
            {
                Id = 2,
                Name = "MaxMinusPointPerTime",
                Value = "10"
            });
            var course = await CourseGenerator.Generate(context);
            var instructor = await InstructorGenerator.Generate(context, appSetting);
            controller.ControllerContext.HttpContext = TU.Utils.AddClaimPrinciple(instructor.Id, UT.TokenRole.Instructor);

            var student = await StudentGenerator.Generate(context, appSetting, new EN.Student
            {
                CourseId = course.Id
            });

            var presetLab = await TU.Preset.LabPreset.PrepareGoodLabWithSomeStudentScore(
                context, appSetting, course, student, new TU.Preset.LabPreset.Data()
            );

            var request = new MinusPointAddRequest
            {
                LaboratoryId = presetLab.Laboratory.Id,
                MinusPointId = 200,
                Score = faker.Random.Int(0, 10),
                Remark = faker.Name.FirstName()
            };

            //Act
            var response = await controller.AddMinusPoint(student.Id, request);
            var studentMinusPoints = await context.StudentMinusPoints.Select(s => s).AsNoTracking().ToListAsync();

            //Assert
            Assert.Equal(typeof(NotFoundObjectResult), response.GetType());
        }

        [Fact]
        public async Task SelfAssessmentCreate_Create()
        {
            // Arrange
            await TU.Preset.CommonPreset.Generate(context);
            var iToken = new Mock<UT.IToken>();
            iToken.Setup(a => a.IsStudentRequest(It.IsAny<int>(), It.IsAny<ClaimsPrincipal>()))
                .Returns(true);

            var controllers = new TU.NewController(context, iToken.Object);

            var studentFunction = new Mock<FN.IStudent>();
            studentFunction.Setup(s => s.CreateOrUpdateSelfAssessment(It.IsAny<int>(), It.IsAny<SelfAssessmentRequest>()))
                .Returns(Task.FromResult(new HttpCode<string> { StatusCode = HttpStatusCode.Created, Object = "" }));

            var controller = new StudentController(context, studentFunction.Object, appSetting, iToken.Object);                       

            // Act
            var response = await controller.CreateOrUpdateSelfAssessment(1, null);

            // Assert
            Assert.Equal(typeof(CreatedResult), response.GetType());
        }

        [Fact]
        public async Task SelfAssessmentUpdate_Ok()
        {
            // Arrange
            await TU.Preset.CommonPreset.Generate(context);
            var iToken = new Mock<UT.IToken>();
            iToken.Setup(a => a.IsStudentRequest(It.IsAny<int>(), It.IsAny<ClaimsPrincipal>()))
                .Returns(true);

            var controllers = new TU.NewController(context, iToken.Object);

            var studentFunction = new Mock<FN.IStudent>();
            studentFunction.Setup(s => s.CreateOrUpdateSelfAssessment(It.IsAny<int>(), It.IsAny<SelfAssessmentRequest>()))
                .Returns(Task.FromResult(new HttpCode<string> { StatusCode = HttpStatusCode.OK, Object = "" }));

            var controller = new StudentController(context, studentFunction.Object, appSetting, iToken.Object);

            // Act
            var response = await controller.CreateOrUpdateSelfAssessment(1, null);

            // Assert
            Assert.Equal(typeof(OkObjectResult), response.GetType());
        }

        [Fact]
        public async Task SelfAssessmentCreateOrUpdateNotFound_NotFound()
        {
            // Arrange
            await TU.Preset.CommonPreset.Generate(context);
            var iToken = new Mock<UT.IToken>();
            iToken.Setup(a => a.IsStudentRequest(It.IsAny<int>(), It.IsAny<ClaimsPrincipal>()))
                .Returns(true);

            var controllers = new TU.NewController(context, iToken.Object);

            var studentFunction = new Mock<FN.IStudent>();
            studentFunction.Setup(s => s.CreateOrUpdateSelfAssessment(It.IsAny<int>(), It.IsAny<SelfAssessmentRequest>()))
                .Returns(Task.FromResult(new HttpCode<string> { StatusCode = HttpStatusCode.NotFound, Object = "" }));

            var controller = new StudentController(context, studentFunction.Object, appSetting, iToken.Object);

            // Act
            var response = await controller.CreateOrUpdateSelfAssessment(1, null);

            // Assert
            Assert.Equal(typeof(NotFoundObjectResult), response.GetType());
        }

        [Fact]
        public async Task LabScopeCreateOrUpdate_Create()
        {
            // Arrange
            await TU.Preset.CommonPreset.Generate(context);
            var iToken = new Mock<UT.IToken>();
            iToken.Setup(a => a.IsStudentRequest(It.IsAny<int>(), It.IsAny<ClaimsPrincipal>()))
                .Returns(true);

            var controllers = new TU.NewController(context, iToken.Object);

            var studentFunction = new Mock<FN.IStudent>();
            studentFunction.Setup(s => s.CreateOrUpdateLabScope(It.IsAny<int>(), It.IsAny<LabScopeRequest>()))
                .Returns(Task.FromResult(new HttpCode<string> { StatusCode = HttpStatusCode.Created, Object = "" }));

            var controller = new StudentController(context, studentFunction.Object, appSetting, iToken.Object);

            // Act
            var response = await controller.CreateOrUpdateLabScope(1, null);

            // Assert
            Assert.Equal(typeof(CreatedResult), response.GetType());
        }

        [Fact]
        public async Task LabScopeCreateOrUpdate_Update()
        {
            // Arrange
            await TU.Preset.CommonPreset.Generate(context);
            var iToken = new Mock<UT.IToken>();
            iToken.Setup(a => a.IsStudentRequest(It.IsAny<int>(), It.IsAny<ClaimsPrincipal>()))
                .Returns(true);

            var controllers = new TU.NewController(context, iToken.Object);

            var studentFunction = new Mock<FN.IStudent>();
            studentFunction.Setup(s => s.CreateOrUpdateLabScope(It.IsAny<int>(), It.IsAny<LabScopeRequest>()))
                .Returns(Task.FromResult(new HttpCode<string> { StatusCode = HttpStatusCode.OK, Object = "" }));

            var controller = new StudentController(context, studentFunction.Object, appSetting, iToken.Object);

            // Act
            var response = await controller.CreateOrUpdateLabScope(1, null);

            // Assert
            Assert.Equal(typeof(OkObjectResult), response.GetType());
        }

        [Fact]
        public async Task LabScopeCreateOrUpdate_NotFound()
        {
            // Arrange
            await TU.Preset.CommonPreset.Generate(context);
            var iToken = new Mock<UT.IToken>();
            iToken.Setup(a => a.IsStudentRequest(It.IsAny<int>(), It.IsAny<ClaimsPrincipal>()))
                .Returns(true);

            var controllers = new TU.NewController(context, iToken.Object);

            var studentFunction = new Mock<FN.IStudent>();
            studentFunction.Setup(s => s.CreateOrUpdateLabScope(It.IsAny<int>(), It.IsAny<LabScopeRequest>()))
                .Returns(Task.FromResult(new HttpCode<string> { StatusCode = HttpStatusCode.NotFound, Object = "" }));

            var controller = new StudentController(context, studentFunction.Object, appSetting, iToken.Object);

            // Act
            var response = await controller.CreateOrUpdateLabScope(1, null);

            // Assert
            Assert.Equal(typeof(NotFoundObjectResult), response.GetType());
        }

        [Fact]
        public async Task SubmitScore_Create()
        {
            // Arrange
            await TU.Preset.CommonPreset.Generate(context);
            var iToken = new Mock<UT.IToken>();
            iToken.Setup(a => a.IsInStructorRequest(It.IsAny<ClaimsPrincipal>()))
                .Returns(true);

            var controllers = new TU.NewController(context, iToken.Object);

            var studentFunction = new Mock<FN.IStudent>();
            studentFunction.Setup(s => s.SubmitScore(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<SubmitScoreRequest>()))
                .Returns(Task.FromResult(new HttpCode<string> { StatusCode = HttpStatusCode.Created, Object = "" }));

            var instructor = await InstructorGenerator.Generate(context, appSetting);
            var controller = new StudentController(context, studentFunction.Object, appSetting, iToken.Object);
            controller.ControllerContext.HttpContext = TU.Utils.AddClaimPrinciple(instructor.Id, UT.TokenRole.Instructor);

            // Act
            var response = await controller.SubmitScore(1, null);

            // Assert
            Assert.Equal(typeof(CreatedResult), response.GetType());
        }

        [Fact]
        public async Task SubmitScore_NotFound()
        {
            // Arrange
            await TU.Preset.CommonPreset.Generate(context);
            var iToken = new Mock<UT.IToken>();
            iToken.Setup(a => a.IsInStructorRequest(It.IsAny<ClaimsPrincipal>()))
                .Returns(true);

            var controllers = new TU.NewController(context, iToken.Object);

            var studentFunction = new Mock<FN.IStudent>();
            studentFunction.Setup(s => s.SubmitScore(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<SubmitScoreRequest>()))
                .Returns(Task.FromResult(new HttpCode<string> { StatusCode = HttpStatusCode.NotFound, Object = "" }));

            var instructor = await InstructorGenerator.Generate(context, appSetting);
            var controller = new StudentController(context, studentFunction.Object, appSetting, iToken.Object);
            controller.ControllerContext.HttpContext = TU.Utils.AddClaimPrinciple(instructor.Id, UT.TokenRole.Instructor);

            // Act
            var response = await controller.SubmitScore(1, null);

            // Assert
            Assert.Equal(typeof(NotFoundObjectResult), response.GetType());
        }

        [Fact]
        public async Task SubmitCritical_Create()
        {
            // Arrange
            await TU.Preset.CommonPreset.Generate(context);
            var iToken = new Mock<UT.IToken>();
            iToken.Setup(a => a.IsInStructorRequest(It.IsAny<ClaimsPrincipal>()))
                .Returns(true);

            var controllers = new TU.NewController(context, iToken.Object);

            var studentFunction = new Mock<FN.IStudent>();
            studentFunction.Setup(s => s.SubmitCritical(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<SubmitCriticalRequest>()))
                .Returns(Task.FromResult(new HttpCode<string> { StatusCode = HttpStatusCode.Created, Object = "" }));

            var instructor = await InstructorGenerator.Generate(context, appSetting);
            var controller = new StudentController(context, studentFunction.Object, appSetting, iToken.Object);
            controller.ControllerContext.HttpContext = TU.Utils.AddClaimPrinciple(instructor.Id, UT.TokenRole.Instructor);

            // Act
            var response = await controller.SubmitCritical(1, null);

            // Assert
            Assert.Equal(typeof(CreatedResult), response.GetType());
        }

        [Fact]
        public async Task SubmitCritical_NotFound()
        {
            // Arrange
            await TU.Preset.CommonPreset.Generate(context);
            var iToken = new Mock<UT.IToken>();
            iToken.Setup(a => a.IsInStructorRequest(It.IsAny<ClaimsPrincipal>()))
                .Returns(true);

            var controllers = new TU.NewController(context, iToken.Object);

            var studentFunction = new Mock<FN.IStudent>();
            studentFunction.Setup(s => s.SubmitCritical(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<SubmitCriticalRequest>()))
                .Returns(Task.FromResult(new HttpCode<string> { StatusCode = HttpStatusCode.NotFound, Object = "" }));

            var instructor = await InstructorGenerator.Generate(context, appSetting);
            var controller = new StudentController(context, studentFunction.Object, appSetting, iToken.Object);
            controller.ControllerContext.HttpContext = TU.Utils.AddClaimPrinciple(instructor.Id, UT.TokenRole.Instructor);

            // Act
            var response = await controller.SubmitCritical(1, null);

            // Assert
            Assert.Equal(typeof(NotFoundObjectResult), response.GetType());
        }

        [Fact]
        public async Task GetStudentLabImage_OK()
        {
            // Arrange
            await TU.Preset.CommonPreset.Generate(context);
            var iToken = new Mock<UT.IToken>();
            iToken.Setup(a => a.CanProcess(It.IsAny<int>(), It.IsAny<ClaimsPrincipal>()))
                .Returns(true);

            var controllers = new TU.NewController(context, iToken.Object);

            var studentFunction = new Mock<FN.IStudent>();
            studentFunction.Setup(s => s.GetLabImage(It.IsAny<int>(), It.IsAny<int>()))
                .Returns(Task.FromResult(new HttpCode<List<LaboratoryImageResponse>> { StatusCode = HttpStatusCode.OK }));

            var controller = new StudentController(context, studentFunction.Object, appSetting, iToken.Object);
            controller.ControllerContext.HttpContext = TU.Utils.AddClaimPrinciple(1, UT.TokenRole.Instructor);
            // Act
            var response = await controller.GetLabImages(1, 1);

            // Assert
            Assert.Equal(typeof(OkObjectResult), response.GetType());
        }

        [Fact]
        public async Task GetStudentLabImage_NotFound()
        {
            // Arrange
            await TU.Preset.CommonPreset.Generate(context);
            var iToken = new Mock<UT.IToken>();
            iToken.Setup(a => a.CanProcess(It.IsAny<int>(), It.IsAny<ClaimsPrincipal>()))
                .Returns(true);

            var controllers = new TU.NewController(context, iToken.Object);

            var studentFunction = new Mock<FN.IStudent>();
            studentFunction.Setup(s => s.GetLabImage(It.IsAny<int>(), It.IsAny<int>()))
                .Returns(Task.FromResult(new HttpCode<List<LaboratoryImageResponse>> { StatusCode = HttpStatusCode.NotFound }));

            var controller = new StudentController(context, studentFunction.Object, appSetting, iToken.Object);
            controller.ControllerContext.HttpContext = TU.Utils.AddClaimPrinciple(1, UT.TokenRole.Instructor);
            // Act
            var response = await controller.GetLabImages(1, 1);

            // Assert
            Assert.Equal(typeof(NotFoundObjectResult), response.GetType());
        }
    }
}
