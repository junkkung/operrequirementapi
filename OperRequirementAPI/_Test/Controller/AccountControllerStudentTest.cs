﻿using _Test.Utilities.Generator;
using _Test.Utilities.Preset;
using API.Controllers;
using Bogus;
using Database;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Models.Requests;
using Models.Responses;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using TU = _Test.Utilities;
using UT = Utilities;

namespace _Test.Controller
{
    public class AccountControllerStudentTest : IDisposable
    {
        private readonly OperRequirementContext context;
        private readonly IConfiguration appSetting;
        private readonly AccountController controller;
        private readonly Faker faker;

        public AccountControllerStudentTest()
        {
            this.context = TU.Utils.GetMemoryContext($"AccountStudentController{Guid.NewGuid().ToString()}");
            this.appSetting = TU.Utils.InitConfiguration();
            var controllers = new TU.NewController(this.context);
            this.controller = controllers.AccountController;
            this.faker = new Faker();
        }

        public void Dispose()
        {
            UT.CacheManager.RemoveAll();
            context.Database.EnsureDeleted();
        }

        [Fact]
        public async Task LoginStudent()
        {
            // Assign
            await CommonPreset.Generate(context);
            var username = faker.Internet.UserName();
            var password = faker.Internet.Password();
            var student = await StudentGenerator.Generate(context, appSetting, new Database.Entities.Student
            {
                Username = username,
                Password = password
            });

            var loginRequest = new LoginRequest { Username = username, Password = password };

            // Act
            var loginResponse = await controller.LoginStudent(loginRequest);
            var response = (loginResponse as ObjectResult).Value as LoginResponse;
            // Assert
            Assert.Equal(typeof(OkObjectResult), loginResponse.GetType());
            Assert.NotNull(response.AccessToken);
            Assert.NotNull(response.RefreshToken);
            Assert.Equal(student.Id, response.StudentId);
        }

        [Fact]
        public async Task LoginNotExistingStudent_NotFound()
        {
            await CommonPreset.Generate(context);
            var username = faker.Internet.UserName();
            var password = faker.Internet.Password();

            var loginRequest = new LoginRequest { Username = username, Password = password };

            // Act
            var loginResponse = await controller.LoginStudent(loginRequest);

            // Assert
            Assert.Equal(typeof(NotFoundObjectResult), loginResponse.GetType());
        }

        [Fact]
        public async Task LoginStudentPasswordMistake_Unauthorized()
        {
            // Assign
            await CommonPreset.Generate(context);
            var username = faker.Internet.UserName();
            var password = faker.Internet.Password();
            var student = await StudentGenerator.Generate(context, appSetting, new Database.Entities.Student
            {
                Username = username,
                Password = password
            });

            var loginRequest = new LoginRequest
            {
                Username = username,
                Password = faker.Internet.Password()
            };

            // Act
            var loginResponse = await controller.LoginStudent(loginRequest);

            // Assert
            Assert.Equal(typeof(BadRequestObjectResult), loginResponse.GetType());
        }

        [Theory]
        [InlineData("TTtt12")]
        [InlineData("TttT12")]
        public async Task LoginInstructorNoCaseSensitiveForUsername(string inputUsername)
        {
            // Assign
            await CommonPreset.Generate(context);
            var username = "tttt12";
            var password = faker.Internet.Password();
            var student = await StudentGenerator.Generate(context, appSetting, new Database.Entities.Student
            {
                Username = username,
                Password = password
            });

            var loginRequest = new LoginRequest { Username = inputUsername, Password = password };

            // Act
            var loginResponse = await controller.LoginStudent(loginRequest);
            var response = (loginResponse as ObjectResult).Value as LoginResponse;
            // Assert
            Assert.Equal(typeof(OkObjectResult), loginResponse.GetType());
            Assert.NotNull(response.AccessToken);
            Assert.NotNull(response.RefreshToken);
            Assert.Equal(student.Id, response.StudentId);
        }

        [Fact]
        public async Task GetNewAccessTokenWithValidRefreshToken()
        {
            //Assign
            var preset = await LoginPreset.GenerateStudent(this.context, this.appSetting, this.controller);
            LoginPreset.AddToken(this.controller, preset.RefreshToken, preset.Student.Id, UT.TokenRole.Student);
            //Act
            var refreshResponse = await controller.RefreshAccessStudent();
            var response = (refreshResponse as ObjectResult).Value as LoginResponse;

            //Assert
            Assert.Equal(typeof(OkObjectResult), refreshResponse.GetType());
            Assert.NotNull(response.AccessToken);
            Assert.NotNull(response.RefreshToken);
            Assert.NotEqual(preset.AccessToken, response.AccessToken);
            Assert.NotEqual(preset.RefreshToken, response.RefreshToken);
        }

        [Fact]
        public async Task GetNewAccessTokenWithNotMatchRefreshToken_BadRequest()
        {
            //Assign
            var preset = await LoginPreset.GenerateStudent(this.context, this.appSetting, this.controller);
            LoginPreset.AddToken(this.controller, faker.Internet.UserName(), preset.Student.Id, UT.TokenRole.Student);
            //Act
            var refreshResponse = await controller.RefreshAccessStudent() as BadRequestResult;

            //Assert
            Assert.Equal(typeof(BadRequestResult), refreshResponse.GetType());
        }

        [Fact]
        public async Task GetNewAccessTokenWithNotFoundStudent_BadRequest()
        {
            //Assign
            var preset = await LoginPreset.GenerateStudent(this.context, this.appSetting, this.controller);
            LoginPreset.AddToken(this.controller, faker.Internet.UserName(), 9999, UT.TokenRole.Student);
            //Act
            var refreshResponse = await controller.RefreshAccessStudent() as BadRequestResult;

            //Assert
            Assert.Equal(typeof(BadRequestResult), refreshResponse.GetType());
        }

        [Fact]
        public async Task GetNewAccessTokenWithStudentInactived_BadRequest()
        {
            //Assign
            var preset = await LoginPreset.GenerateStudent(this.context, this.appSetting, this.controller);
            preset.Student.StatusId = UT.Status.Inactive.Id;
            context.Students.Update(preset.Student);
            await context.SaveChangesAsync();
            LoginPreset.AddToken(this.controller, preset.RefreshToken, preset.Student.Id, UT.TokenRole.Student);
            //Act
            var refreshResponse = await controller.RefreshAccessStudent() as BadRequestResult;

            //Assert
            Assert.Equal(typeof(BadRequestResult), refreshResponse.GetType());
        }

        [Fact]
        public async Task SignUpStudentAllField()
        {
            //Assign
            await CommonPreset.Generate(context);
            var course = await CourseGenerator.Generate(context);
            var request = new SignUpStudentRequest
            {
                Firstname = faker.Name.FirstName(),
                Lastname = faker.Name.LastName(),
                Username = faker.Internet.UserName(),
                Password = "1Aa2345@",
                Email = faker.Internet.Email(),
                Telephone = faker.Phone.PhoneNumber(),
                Number = faker.Random.Number(1, 1000),
                StudentGroupId = faker.Random.Number(1, 100),
                UniversityIdNumber = faker.Random.String(10),
                ConfirmCode = (await StudentConfirmCodeGenerator.Generate(context, new Database.Entities.StudentConfrimCode
                {
                    CourseId = course.Id
                })).Code,
            };

            //Act
            var response = await controller.SignUpStudent(request);

            //Assert
            Assert.Equal(typeof(CreatedResult), response.GetType());

        }

        [Fact]
        public async Task SignUpStudentOnlyRequiredField()
        {
            //Assign
            await CommonPreset.Generate(context);
            var course = await CourseGenerator.Generate(context);
            var request = new SignUpStudentRequest
            {
                Firstname = faker.Name.FirstName(),
                Lastname = faker.Name.LastName(),
                Username = faker.Internet.UserName(),
                Password = "1Aa2345@",
                Email = faker.Internet.Email(),
                //Telephone = faker.Phone.PhoneNumber(),
                Number = faker.Random.Number(1, 1000),
                StudentGroupId = faker.Random.Number(1, 100),
                UniversityIdNumber = faker.Random.String(10),
                ConfirmCode = (await StudentConfirmCodeGenerator.Generate(context, new Database.Entities.StudentConfrimCode
                {
                    CourseId = course.Id
                })).Code,
            };

            //Act
            var response = await controller.SignUpStudent(request);

            //Assert
            Assert.Equal(typeof(CreatedResult), response.GetType());

        }

        [Fact]
        public async Task SignUpStudentUsernameAlreadyExist_Conflict()
        {
            //Assign
            await CommonPreset.Generate(context);
            var student = await StudentGenerator.Generate(context, appSetting);
            var course = await CourseGenerator.Generate(context);
            var request = new SignUpStudentRequest
            {
                Firstname = faker.Name.FirstName(),
                Lastname = faker.Name.LastName(),
                Username = student.Username,
                Password = "1Aa2345@",
                Email = faker.Internet.Email(),
                //Telephone = faker.Phone.PhoneNumber(),
                Number = faker.Random.Number(1, 1000),
                StudentGroupId = faker.Random.Number(1, 100),
                UniversityIdNumber = faker.Random.String(10),
                ConfirmCode = (await StudentConfirmCodeGenerator.Generate(context, new Database.Entities.StudentConfrimCode
                {
                    CourseId = course.Id
                })).Code,
            };

            //Act
            var response = await controller.SignUpStudent(request);

            //Assert
            Assert.Equal(typeof(ConflictObjectResult), response.GetType());

        }

        [Theory]
        [InlineData("aB3&ae$", typeof(BadRequestObjectResult))] // range < 8
        [InlineData("aB3sevae", typeof(BadRequestObjectResult))] // special charactor
        [InlineData("aBeas&ae", typeof(BadRequestObjectResult))] // numeric
        [InlineData("a3&ssddae$", typeof(BadRequestObjectResult))] // capital
        public async Task SignUpStudentPassword_BadRequest(string password, Type expected)
        {
            //Assign
            await CommonPreset.Generate(context);
            var course = await CourseGenerator.Generate(context);
            var request = new SignUpStudentRequest
            {
                Firstname = faker.Name.FirstName(),
                Lastname = faker.Name.LastName(),
                Username = faker.Internet.UserName(),
                Password = password,
                Email = faker.Internet.Email(),
                //Telephone = faker.Phone.PhoneNumber(),
                Number = faker.Random.Number(1, 1000),
                StudentGroupId = faker.Random.Number(1, 100),
                UniversityIdNumber = faker.Random.String(10),
                ConfirmCode = (await StudentConfirmCodeGenerator.Generate(context, new Database.Entities.StudentConfrimCode
                {
                    CourseId = course.Id
                })).Code,
            };

            //Act
            var response = await controller.SignUpStudent(request);

            //Assert
            Assert.Equal(expected, response.GetType());

        }

        [Fact]
        public async Task SignUpStudentConfirmCodeNotExist_NotFound()
        {
            //Assign
            await CommonPreset.Generate(context);
            var course = await CourseGenerator.Generate(context);
            var request = new SignUpStudentRequest
            {
                Firstname = faker.Name.FirstName(),
                Lastname = faker.Name.LastName(),
                Username = faker.Internet.UserName(),
                Password = "1Aa2345@",
                Email = faker.Internet.Email(),
                //Telephone = faker.Phone.PhoneNumber(),
                Number = faker.Random.Number(1, 1000),
                StudentGroupId = faker.Random.Number(1, 100),
                UniversityIdNumber = faker.Random.String(10),
                ConfirmCode = faker.Random.String(10),
            };

            //Act
            var response = await controller.SignUpStudent(request);

            //Assert
            Assert.Equal(typeof(NotFoundObjectResult), response.GetType());

        }

        [Fact]
        public async Task SignUpStudentInactiveCourse_NotFound()
        {
            //Assign
            await CommonPreset.Generate(context);
            var course = await CourseGenerator.Generate(context, new Database.Entities.Course
            {
                StartDate = DateTime.Now.AddDays(-10),
                EndDate = DateTime.Now.AddDays(-9)
            });
            var request = new SignUpStudentRequest
            {
                Firstname = faker.Name.FirstName(),
                Lastname = faker.Name.LastName(),
                Username = faker.Internet.UserName(),
                Password = "1Aa2345@",
                Email = faker.Internet.Email(),
                //Telephone = faker.Phone.PhoneNumber(),
                Number = faker.Random.Number(1, 1000),
                StudentGroupId = faker.Random.Number(1, 100),
                UniversityIdNumber = faker.Random.String(10),
                ConfirmCode = (await StudentConfirmCodeGenerator.Generate(context, new Database.Entities.StudentConfrimCode
                {
                    CourseId = course.Id
                })).Code,
            };

            //Act
            var response = await controller.SignUpStudent(request);

            //Assert
            Assert.Equal(typeof(NotFoundObjectResult), response.GetType());
        }

        [Fact]
        public async Task SignUpStudentInactiveCourseConfirmCode_NotFound()
        {
            //Assign
            await CommonPreset.Generate(context);
            var inActiveCourse = await CourseGenerator.Generate(context, new Database.Entities.Course
            {
                StartDate = DateTime.Now.AddDays(-10),
                EndDate = DateTime.Now.AddDays(-9)
            });

            var activeCourse = await CourseGenerator.Generate(context);
            var request = new SignUpStudentRequest
            {
                Firstname = faker.Name.FirstName(),
                Lastname = faker.Name.LastName(),
                Username = faker.Internet.UserName(),
                Password = "1Aa2345@",
                Email = faker.Internet.Email(),
                //Telephone = faker.Phone.PhoneNumber(),
                Number = faker.Random.Number(1, 1000),
                StudentGroupId = faker.Random.Number(1, 100),
                UniversityIdNumber = faker.Random.String(10),
                ConfirmCode = (await StudentConfirmCodeGenerator.Generate(context, new Database.Entities.StudentConfrimCode
                {
                    CourseId = inActiveCourse.Id
                })).Code,
            };

            //Act
            var response = await controller.SignUpStudent(request);

            //Assert
            Assert.Equal(typeof(NotFoundObjectResult), response.GetType());
        }

        [Fact]
        public async Task SignUpStudentNeedToClearCourseCache()
        {
            //Assign
            await CommonPreset.Generate(context);
            var course = await CourseGenerator.Generate(context);
            var request = new SignUpStudentRequest
            {
                Firstname = faker.Name.FirstName(),
                Lastname = faker.Name.LastName(),
                Username = faker.Internet.UserName(),
                Password = "1Aa2345@",
                Email = faker.Internet.Email(),
                Telephone = faker.Phone.PhoneNumber(),
                Number = faker.Random.Number(1, 1000),
                StudentGroupId = faker.Random.Number(1, 100),
                UniversityIdNumber = faker.Random.String(10),
                ConfirmCode = (await StudentConfirmCodeGenerator.Generate(context, new Database.Entities.StudentConfrimCode
                {
                    CourseId = course.Id
                })).Code,
            };

            UT.CacheManager.Set(UT.CacheKey.Course.Id(course.Id), new object());

            //Act
            var cacheBefore = UT.CacheManager.Get<object>(UT.CacheKey.Course.Id(course.Id));
            var response = await controller.SignUpStudent(request);
            var cacheAfter = UT.CacheManager.Get<object>(UT.CacheKey.Course.Id(course.Id));

            //Assert
            Assert.NotNull(cacheBefore);
            Assert.Null(cacheAfter);
        }

        [Fact]
        public async Task SignUpWillChangeUsernameToLower()
        {
            //Assign
            await CommonPreset.Generate(context);
            var course = await CourseGenerator.Generate(context);
            var request = new SignUpStudentRequest
            {
                Firstname = faker.Name.FirstName(),
                Lastname = faker.Name.LastName(),
                Username = faker.Internet.UserName(),
                Password = "1Aa2345@",
                Email = faker.Internet.Email(),
                Telephone = faker.Phone.PhoneNumber(),
                Number = faker.Random.Number(1, 1000),
                StudentGroupId = faker.Random.Number(1, 100),
                UniversityIdNumber = faker.Random.String(10),
                ConfirmCode = (await StudentConfirmCodeGenerator.Generate(context, new Database.Entities.StudentConfrimCode
                {
                    CourseId = course.Id
                })).Code,
            };

            //Act
            var response = await controller.SignUpStudent(request);
            var student = await context.Students.FindAsync(1);

            //Assert
            Assert.Equal(typeof(CreatedResult), response.GetType());
            Assert.Equal(request.Username.ToLower(), student.Username);
        }

        [Fact]
        public async Task ChangePasswordAndTryLogin()
        {
            // Assign
            await CommonPreset.Generate(context);
            var username = faker.Internet.UserName();
            var password = faker.Internet.Password();
            var course = await CourseGenerator.Generate(context);
            var student = await StudentGenerator.Generate(context, appSetting, new Database.Entities.Student
            {
                Username = username,
                Password = password,
                CourseId = course.Id
            });

            var newPassword = "/12ABbde$";
            var changePasswordRequest = new ChangePasswordRequest
            {
                Username = username,
                NewPassword = newPassword,
                ConfirmCode = (await StudentConfirmCodeGenerator.Generate(context, new Database.Entities.StudentConfrimCode
                {
                    CourseId = course.Id
                })).Code,
            };

            var loginRequest = new LoginRequest { Username = username, Password = newPassword };

            // Act
            var changePasswordResponse = await controller.ChangePasswordStudent(changePasswordRequest);
            var loginResponse = await controller.LoginStudent(loginRequest);
            var Response = (loginResponse as ObjectResult).Value as LoginResponse;
            // Assert
            Assert.Equal(typeof(OkObjectResult), changePasswordResponse.GetType());
            Assert.Equal(typeof(OkObjectResult), loginResponse.GetType());
            Assert.NotNull(Response.AccessToken);
            Assert.NotNull(Response.RefreshToken);
        }

        [Fact]
        public async Task ChangePasswordConfirmCode_NotFound()
        {
            // Assign
            await CommonPreset.Generate(context);
            var username = faker.Internet.UserName();
            var password = faker.Internet.Password();
            var course = await CourseGenerator.Generate(context);
            var student = await StudentGenerator.Generate(context, appSetting, new Database.Entities.Student
            {
                Username = username,
                Password = password,
                CourseId = course.Id
            });

            var newPassword = "/12ABbde$";
            var changePasswordRequest = new ChangePasswordRequest
            {
                Username = username,
                NewPassword = newPassword,
                ConfirmCode = faker.Random.String(10)
            };

            // Act
            var changePasswordResponse = await controller.ChangePasswordStudent(changePasswordRequest);

            // Assert
            Assert.Equal(typeof(NotFoundObjectResult), changePasswordResponse.GetType());
        }

        [Fact]
        public async Task ChangePasswordUser_NotFound()
        {
            // Assign
            await CommonPreset.Generate(context);
            var username = faker.Internet.UserName();
            var password = faker.Internet.Password();
            var course = await CourseGenerator.Generate(context);
            var student = await StudentGenerator.Generate(context, appSetting, new Database.Entities.Student
            {
                Username = username,
                Password = password,
                CourseId = course.Id
            });

            var newPassword = "/12ABbde$";
            var changePasswordRequest = new ChangePasswordRequest
            {
                Username = faker.Random.String(10),
                NewPassword = newPassword,
                ConfirmCode = (await StudentConfirmCodeGenerator.Generate(context, new Database.Entities.StudentConfrimCode
                {
                    CourseId = course.Id
                })).Code,
            };

            // Act
            var changePasswordResponse = await controller.ChangePasswordStudent(changePasswordRequest);

            // Assert
            Assert.Equal(typeof(NotFoundObjectResult), changePasswordResponse.GetType());
        }

        [Theory]
        [InlineData("aB3&ae$", typeof(BadRequestObjectResult))] // range < 8
        [InlineData("aB3sevae", typeof(BadRequestObjectResult))] // special charactor
        [InlineData("aBeas&ae", typeof(BadRequestObjectResult))] // numeric
        [InlineData("a3&ssddae$", typeof(BadRequestObjectResult))] // capital
        public async Task ChangePasswordPasswordWrongFormat_BadRequest(string password, Type expected)
        {
            // Assign
            await CommonPreset.Generate(context);
            var username = faker.Internet.UserName();
            var course = await CourseGenerator.Generate(context);
            var student = await StudentGenerator.Generate(context, appSetting, new Database.Entities.Student
            {
                Username = username,
                Password = faker.Internet.Password(),
                CourseId = course.Id
            });

            var changePasswordRequest = new ChangePasswordRequest
            {
                Username = username,
                NewPassword = password,
                ConfirmCode = (await StudentConfirmCodeGenerator.Generate(context, new Database.Entities.StudentConfrimCode
                {
                    CourseId = course.Id
                })).Code,
            };

            // Act
            var changePasswordResponse = await controller.ChangePasswordStudent(changePasswordRequest);

            // Assert
            Assert.Equal(expected, changePasswordResponse.GetType());
        }
    }
}
