﻿using Bogus;
using Database;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Text;
using Utilities;
using Xunit;
using TU = _Test.Utilities;
using UT = Utilities;

namespace _Test.Function
{
    public class TokenTest : IDisposable
    {
        private readonly IConfiguration appSetting;
        private readonly UT.IToken _token;
        private readonly Faker faker;

        public TokenTest()
        {
            this.appSetting = TU.Utils.InitConfiguration();
            this._token = new UT.Token(appSetting);
            this.faker = new Faker();
        }

        public void Dispose()
        {
            UT.CacheManager.RemoveAll();
        }

        [Fact]
        public void CanProcessInstructor_DonotCareAboutIdentifier()
        {
            // Arrange
            var role = UT.TokenRole.Instructor;

            var claim = new ClaimsPrincipal(new ClaimsIdentity(new Claim[]
            {
                new Claim("identifier", faker.Random.Number(0, 999).ToString()),
                new Claim(ClaimTypes.Role, role),
            }));

            // Act
            var result = _token.CanProcess(faker.Random.Number(1000, 9999), claim);

            // Assert
            Assert.True(result);
        }

        [Theory]
        [InlineData(1, "1", true)]
        [InlineData(98765, "98765", true)]
        [InlineData(1, "10", false)]
        public void CanProcessStudent_CareAboutIdentifier(int identifier, string claimIdentifier, bool expectResult)
        {
            // Arrange
            var role = UT.TokenRole.Student;

            var claim = new ClaimsPrincipal(new ClaimsIdentity(new Claim[]
            {
                new Claim("identifier", claimIdentifier),
                new Claim(ClaimTypes.Role, role),
            }));

            // Act
            var result = _token.CanProcess(identifier, claim);

            // Assert
            Assert.Equal(expectResult, result);
        }       
    }
}
