﻿using Bogus;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;
using TU = _Test.Utilities;
using UT = Utilities;
using EN = Database.Entities;

namespace _Test.Function
{
    public class ParseTest : IDisposable
    {
        private readonly Faker faker;

        public ParseTest()
        {
            this.faker = new Faker();
        }

        public void Dispose()
        {
            UT.CacheManager.RemoveAll();
        }

        [Theory]
        [InlineData("AA", "BB", "AA B.")]
        [InlineData("VV", "D", "VV D.")]
        [InlineData("VV", "", "VV ")]
        [InlineData("", "DAE", "D.")]
        [InlineData("", "", "")]
        public void ToSignature(string firstname, string lastname, string expectResult)
        {
            //Arrange
            
            //Act
            var signature = UT.Parse.ToSignature(firstname, lastname);

            //Assert
            Assert.Equal(expectResult, signature);
        }
    }
}
