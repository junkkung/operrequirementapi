﻿using System;
using System.Collections.Generic;
using System.Text;
using TU = _Test.Utilities;
using UT = Utilities;
using EN = Database.Entities;
using FN = Models.Functions;
using Database;
using Microsoft.Extensions.Configuration;
using Bogus;
using Models.Functions;
using Xunit;
using System.Threading.Tasks;
using _Test.Utilities.Generator;
using System.Linq;

namespace _Test.Function
{
    public class ConfigTest : IDisposable
    {
        private readonly OperRequirementContext context;
        private readonly IConfiguration appSetting;
        private readonly IConfig _config;
        private readonly Faker faker;

        public ConfigTest()
        {
            this.context = TU.Utils.GetMemoryContext($"Config{Guid.NewGuid().ToString()}");
            this.appSetting = TU.Utils.InitConfiguration();
            var controllers = new TU.NewController(this.context);
            this._config = controllers.Config;
            this.faker = new Faker();
        }

        public void Dispose()
        {
            UT.CacheManager.RemoveAll();
            context.Database.EnsureDeleted();
        }

        [Fact]
        public async Task GetAll()
        {
            //Assign
            var config1 = await ConfigGenerator.Generate(context, new EN.Config
            {
                Id = 1,
                Value = faker.Name.FirstName()
            });

            var config2 = await ConfigGenerator.Generate(context, new EN.Config
            {
                Id = 2,
                Value = faker.Name.FirstName()
            });

            var config3 = await ConfigGenerator.Generate(context, new EN.Config
            {
                Id = 3,
                Value = faker.Name.FirstName()
            });

            var configList = new List<EN.Config> { config1, config2, config3 };

            //Act
            var configs = await _config.Get();

            //Assert
            Assert.Equal(3, configs.Count);

            for(var i = 0; i < configs.Count; i++)
            {
                var configGet = configs[i];
                Assert.Equal(configList[i].Id, configs[i].Id);
                Assert.Equal(configList[i].Name, configs[i].Name);
                Assert.Equal(configList[i].Description, configs[i].Description);
                Assert.Equal(configList[i].Value, configs[i].Value);
                Assert.Equal(configList[i].Unit, configs[i].Unit);
            }         
        }

        [Fact]
        public async Task GetAndParse()
        {
            //Assign
            var config1 = await ConfigGenerator.Generate(context, new EN.Config
            {
                Id = 1,
                Value = "1"
            });

            var config2 = await ConfigGenerator.Generate(context, new EN.Config
            {
                Id = 2,
                Value = "AABBCC!!11"
            });

            var config3 = await ConfigGenerator.Generate(context, new EN.Config
            {
                Id = 3,
                Value = "2020-05-03 00:00:00"
            });

            //Act
            var value1 = await _config.Get<int>(config1.Id);
            var value2 = await _config.Get<object>(config2.Id);
            var value3 = await _config.Get<DateTime>(config3.Id);

            //Assert
            Assert.Equal(int.Parse(config1.Value), value1);
            Assert.Equal((object)(config2.Value), value2);
            Assert.Equal(DateTime.Parse(config3.Value), value3);
        }
    }
}
