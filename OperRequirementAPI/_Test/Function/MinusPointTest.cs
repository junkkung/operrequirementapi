﻿using System;
using System.Collections.Generic;
using System.Text;
using TU = _Test.Utilities;
using UT = Utilities;
using EN = Database.Entities;
using FN = Models.Functions;
using Database;
using Microsoft.Extensions.Configuration;
using Bogus;
using Models.Functions;
using Xunit;
using System.Threading.Tasks;
using _Test.Utilities.Generator;
using System.Linq;
using Models.Responses;
using Microsoft.EntityFrameworkCore;
using Models.Requests;
using System.Net;

namespace _Test.Function
{
    public class MinusPointTest : IDisposable
    {
        private readonly OperRequirementContext context;
        private readonly IConfiguration appSetting;
        private readonly IMinusPoint _minusPoint;
        private readonly Faker faker;

        public MinusPointTest()
        {
            this.context = TU.Utils.GetMemoryContext($"MinusPoint{Guid.NewGuid().ToString()}");
            this.appSetting = TU.Utils.InitConfiguration();
            var controllers = new TU.NewController(this.context);
            this._minusPoint = controllers.MinusPoint;
            this.faker = new Faker();
        }

        public void Dispose()
        {
            UT.CacheManager.RemoveAll();
            context.Database.EnsureDeleted();
        }
    
        [Theory]
        [InlineData("10", 10, 0)]
        [InlineData("10", 5, 5)]
        [InlineData("10", 7, 3)]
        [InlineData("10", 0, 10)]
        [InlineData("100", 1000, -900)]
        public async Task CheckMinusPoint(string initial, int score, int expectMinusPoint)
        {
            // Arrange
            var MaxMinusPointPerTime = await ConfigGenerator.Generate(context, new EN.Config
            {
                Id = 2,
                Name = "MaxMinusPointPerTime",
                Value = initial
            });

            // Act
            var minusPoint = await _minusPoint.Calculate(score);

            // Assert
            Assert.Equal(expectMinusPoint, minusPoint);
        }
    }
}
