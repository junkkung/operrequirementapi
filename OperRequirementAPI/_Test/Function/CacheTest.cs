﻿using System;
using System.Collections.Generic;
using System.Text;
using TU = _Test.Utilities;
using UT = Utilities;
using EN = Database.Entities;
using Xunit;

namespace _Test.Function
{
    public class CacheTest : IDisposable
    {
        public void Dispose()
        {
            UT.CacheManager.RemoveStartWithOrContain();
        }

        [Fact]
        public void SetAndGetCache()
        {
            // Assign
            var cacheKey = "key";
            var obj = new object();
            // Act
            UT.CacheManager.Set(cacheKey, obj);
            var cache = UT.CacheManager.Get<object>(cacheKey);
            // Assert
            Assert.Equal(obj, cache);
        }

        [Fact]
        public void SetNullAndGetCache()
        {
            // Assign
            var cacheKey = "key";
            // Act
            UT.CacheManager.Set(cacheKey, null);
            var cache = UT.CacheManager.Get<object>(cacheKey);
            // Assert
            Assert.Null(cache);
        }

        [Fact]
        public void SetEmptyAndGetCache()
        {
            // Assign
            var cacheKey = "key";
            var obj = new List<string>();
            // Act
            UT.CacheManager.Set(cacheKey, obj);
            var cache = UT.CacheManager.Get<object>(cacheKey);
            // Assert
            Assert.Equal(obj, cache);
        }

        [Fact]
        public void RemoveCache()
        {
            // Assign
            var cacheKey = "key";
            var obj = new object();
            // Act
            UT.CacheManager.Set(cacheKey, obj);
            UT.CacheManager.Remove(cacheKey);
            var cache = UT.CacheManager.Get<object>(cacheKey);
            // Assert
            Assert.Null(cache);
        }

        [Fact]
        public void RemoveStartWithOrContain()
        {
            // Assign
            var cacheKeys = new List<string>
            {
                "key.1.a.",
                "key.1.b.",
                "a.key.1.",
                "a.key.12.",
                "a.a.a.a.1."
            };
            var obj = new object();
            // Act
            foreach (var cacheKey in cacheKeys)
                UT.CacheManager.Set(cacheKey, obj);

            UT.CacheManager.RemoveStartWithOrContain("key.1.");
            var allCacheKeys = UT.CacheManager.GetKeys();
            // Assert
            Assert.Equal(2, allCacheKeys.Length);
            Assert.Equal("a.a.a.a.1.", allCacheKeys[0]);
            Assert.Equal("a.key.12.", allCacheKeys[1]);
        }

        [Fact]
        public void RemoveAll()
        {
            // Assign
            var cacheKeys = new List<string>
            {
                "key.1.a.",
                "key.1.b.",
                "a.key.1.",
                "a.key.12.",
                "a.a.a.a.1."
            };
            var obj = new object();
            // Act
            foreach (var cacheKey in cacheKeys)
                UT.CacheManager.Set(cacheKey, obj);

            UT.CacheManager.RemoveAll();
            var allCacheKeys = UT.CacheManager.GetKeys();
            // Assert
            Assert.Empty(allCacheKeys);
        }

        [Fact]
        public void RemoveAllWhenNoAnyCache_NotExeption()
        {
            // Act
            UT.CacheManager.RemoveAll();
            var allCacheKeys = UT.CacheManager.GetKeys();
            // Assert
            Assert.Empty(allCacheKeys);
        }
    }
}
