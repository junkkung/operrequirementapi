﻿using System;
using System.Collections.Generic;
using System.Text;
using TU = _Test.Utilities;
using UT = Utilities;
using EN = Database.Entities;
using FN = Models.Functions;
using Database;
using Microsoft.Extensions.Configuration;
using Bogus;
using Xunit;
using System.Threading.Tasks;
using _Test.Utilities.Generator;
using System.Linq;

namespace _Test.Function
{
    public class ExperienceTest : IDisposable
    {
        private readonly OperRequirementContext context;
        private readonly IConfiguration appSetting;
        private readonly Faker faker;
        private readonly FN.IExperience _experience;

        public ExperienceTest()
        {
            this.context = TU.Utils.GetMemoryContext($"Experience{Guid.NewGuid().ToString()}");
            this.appSetting = TU.Utils.InitConfiguration();
            var controllers = new TU.NewController(this.context);
            this._experience = controllers.Experience;
            this.faker = new Faker();
        }

        public void Dispose()
        {
            UT.CacheManager.RemoveAll();
            context.Database.EnsureDeleted();
        }

        [Fact]
        public async Task GetAllExperience()
        {
            // Arrange
            var experienceA = await ExperienceGenerator.Generate(context, new EN.Experience { Sequence = 2 });
            var experienceB = await ExperienceGenerator.Generate(context, new EN.Experience { Sequence = 1 });

            var topicA = await TopicGenerator.Generate(context, new EN.Topic { ExperienceId = experienceA.Id });
            var topicB = await TopicGenerator.Generate(context, new EN.Topic 
            { 
                ExperienceId = experienceA.Id,
                StatusId = UT.Status.Inactive.Id
            });
            var topicC = await TopicGenerator.Generate(context, new EN.Topic { ExperienceId = experienceB.Id });

            // Act
            var experiences = await _experience.GetAll();

            // Assert
            Assert.Equal(2, experiences.Count);

            var experience = experiences[0];
            Assert.Equal(experienceB.Id, experience.Id);
            Assert.Equal(experienceB.Name, experience.Name);
            Assert.Equal(experienceB.Description, experience.Description);

            Assert.Single(experience.Topics);
            var topic = experience.Topics.ToList()[0];
            Assert.Equal(topicC.Id, topic.Id);
            Assert.Equal(topicC.Name, topic.Name);
            Assert.Equal(topicC.Description, topic.Description);
            Assert.Equal(topicC.ExperienceId, topic.ExperienceId);

            experience = experiences[1];
            Assert.Equal(experienceA.Id, experience.Id);
            Assert.Equal(experienceA.Name, experience.Name);
            Assert.Equal(experienceA.Description, experience.Description);

            Assert.Equal(2, experience.Topics.Count);
            topic = experience.Topics.ToList()[0];
            Assert.Equal(topicA.Id, topic.Id);
            Assert.Equal(topicA.Name, topic.Name);
            Assert.Equal(topicA.Description, topic.Description);
            Assert.Equal(topicA.ExperienceId, topic.ExperienceId);

            topic = experience.Topics.ToList()[1];
            Assert.Equal(topicB.Id, topic.Id);
            Assert.Equal(topicB.Name, topic.Name);
            Assert.Equal(topicB.Description, topic.Description);
            Assert.Equal(topicB.ExperienceId, topic.ExperienceId);
        }

        [Fact]
        public async Task GetAllExperienceWithSomeInActiveExperience()
        {
            // Arrange
            var experienceA = await ExperienceGenerator.Generate(context, new EN.Experience 
            { 
                Sequence = 2,
                StatusId = UT.Status.Inactive.Id
            });
            var experienceB = await ExperienceGenerator.Generate(context, new EN.Experience { Sequence = 1 });

            var topicA = await TopicGenerator.Generate(context, new EN.Topic { ExperienceId = experienceA.Id });
            var topicB = await TopicGenerator.Generate(context, new EN.Topic
            {
                ExperienceId = experienceA.Id,
                StatusId = UT.Status.Inactive.Id
            });
            var topicC = await TopicGenerator.Generate(context, new EN.Topic { ExperienceId = experienceB.Id });

            // Act
            var experiences = await _experience.GetAll();

            // Assert
            Assert.Single(experiences);

            var experience = experiences[0];
            Assert.Equal(experienceB.Id, experience.Id);
            Assert.Equal(experienceB.Name, experience.Name);
            Assert.Equal(experienceB.Description, experience.Description);

            Assert.Single(experience.Topics);
            var topic = experience.Topics.ToList()[0];
            Assert.Equal(topicC.Id, topic.Id);
            Assert.Equal(topicC.Name, topic.Name);
            Assert.Equal(topicC.Description, topic.Description);
            Assert.Equal(topicC.ExperienceId, topic.ExperienceId);           
        }

        [Fact]
        public async Task CheckCacheGetAllExperience()
        {
            // Arrange
            var experienceA = await ExperienceGenerator.Generate(context, new EN.Experience { Sequence = 2 });
            var experienceB = await ExperienceGenerator.Generate(context, new EN.Experience { Sequence = 1 });

            var topicA = await TopicGenerator.Generate(context, new EN.Topic { ExperienceId = experienceA.Id });
            var topicB = await TopicGenerator.Generate(context, new EN.Topic
            {
                ExperienceId = experienceA.Id,
                StatusId = UT.Status.Inactive.Id
            });
            var topicC = await TopicGenerator.Generate(context, new EN.Topic { ExperienceId = experienceB.Id });

            // Act
            var cacheKey = UT.CacheKey.Experience.All();
            var cacheBefore = UT.CacheManager.Get<List<EN.Experience>>(cacheKey);
            var experiences = await _experience.GetAll();
            var cacheAfter = UT.CacheManager.Get<List<EN.Experience>>(cacheKey);

            // Assert
            Assert.Null(cacheBefore);
            Assert.Equal(experiences, cacheAfter);
        }

        [Fact]
        public async Task IsAchieve_True()
        {
            //Arrange
            var subTopicA = await SubTopicGenerator.Generate(context);
            var subTopicB = await SubTopicGenerator.Generate(context);
            var subTopicC = await SubTopicGenerator.Generate(context);

            var studentSubTopicA = await StudentSubTopicGenerator.Generate(context, new EN.StudentSubTopic
            {
                SubTopicId = subTopicA.Id,
            });

            var studentScoreA = await StudentScoreGenerator.Generate(context, appSetting, new EN.StudentScore
            {
                // no criteria
                StudentSubTopicId = subTopicA.Id,
                Score = 1
            });

            studentSubTopicA.StudentScores.Add(studentScoreA);

            var studentSubTopicB = await StudentSubTopicGenerator.Generate(context, new EN.StudentSubTopic
            {
                SubTopicId = subTopicB.Id,               
            });

            var studentScoreB = await StudentScoreGenerator.Generate(context, appSetting, new EN.StudentScore
            {
                // criteria exist score 0
                StudentSubTopicId = subTopicB.Id,
                CriteriaId = (await CriteriaGenerator.Generate(context)).Id,
                Score = 0
            });

            studentSubTopicB.StudentScores.Add(studentScoreB);

            var studentSubTopicC = await StudentSubTopicGenerator.Generate(context, new EN.StudentSubTopic
            {
                SubTopicId = subTopicC.Id,
            });

            var studentScoreC = await StudentScoreGenerator.Generate(context, appSetting, new EN.StudentScore
            {
                // critical
                StudentSubTopicId = subTopicC.Id,
                IsCritical = true,
                Score = 0
            });

            studentSubTopicC.StudentScores.Add(studentScoreC);

            var subTopics = new List<EN.SubTopic> { subTopicA, subTopicB, subTopicC };
            var studentSubTopics = new List<EN.StudentSubTopic> { studentSubTopicA, studentSubTopicB, studentSubTopicC };
            //Act
            var isAchieve = _experience.IsAchieve(subTopics, studentSubTopics);

            //Assert
            Assert.True(isAchieve);
        }

        [Fact]
        public async Task IsAchieve_False_WhenStudentSubTopicNotEqualWithSubTopicCount()
        {
            //Arrange
            var subTopicA = await SubTopicGenerator.Generate(context);
            var subTopicB = await SubTopicGenerator.Generate(context);
            var subTopicC = await SubTopicGenerator.Generate(context);

            var studentSubTopicA = await StudentSubTopicGenerator.Generate(context, new EN.StudentSubTopic
            {
                SubTopicId = subTopicA.Id,
            });

            var studentScoreA = await StudentScoreGenerator.Generate(context, appSetting, new EN.StudentScore
            {
                StudentSubTopicId = subTopicA.Id,
                Score = 1
            });

            studentSubTopicA.StudentScores.Add(studentScoreA);

            var studentSubTopicB = await StudentSubTopicGenerator.Generate(context, new EN.StudentSubTopic
            {
                SubTopicId = subTopicB.Id,
            });

            var studentScoreB = await StudentScoreGenerator.Generate(context, appSetting, new EN.StudentScore
            {
                StudentSubTopicId = subTopicB.Id,
                Score = 0
            });

            studentSubTopicB.StudentScores.Add(studentScoreB);

            var studentSubTopicC = await StudentSubTopicGenerator.Generate(context, new EN.StudentSubTopic
            {
                SubTopicId = subTopicC.Id,
            });

            var subTopics = new List<EN.SubTopic> { subTopicA, subTopicB, subTopicC };
            var studentSubTopics = new List<EN.StudentSubTopic> { studentSubTopicA, studentSubTopicB, studentSubTopicC };
            //Act
            var isAchieve = _experience.IsAchieve(subTopics, studentSubTopics);

            //Assert
            Assert.False(isAchieve);
        }

        [Fact]
        public async Task IsAchieve_False_WhenSubTopicNotExist()
        {
            //Arrange
            var subTopicA = await SubTopicGenerator.Generate(context);
            var subTopicB = await SubTopicGenerator.Generate(context);
            var subTopicC = await SubTopicGenerator.Generate(context);

            var studentSubTopicA = await StudentSubTopicGenerator.Generate(context, new EN.StudentSubTopic
            {
                SubTopicId = subTopicA.Id,
            });

            var studentSubTopicB = await StudentSubTopicGenerator.Generate(context, new EN.StudentSubTopic
            {
                SubTopicId = subTopicB.Id,
            });

            var studentSubTopicC = await StudentSubTopicGenerator.Generate(context, new EN.StudentSubTopic
            {
                SubTopicId = subTopicC.Id,
            });

            var studentSubTopics = new List<EN.StudentSubTopic> { studentSubTopicA, studentSubTopicB, studentSubTopicC };
            //Act
            var isAchieve = _experience.IsAchieve(null, studentSubTopics);

            //Assert
            Assert.False(isAchieve);
        }

        [Fact]
        public async Task IsAchieve_False_WhenStudentSubTopicNotExist()
        {
            //Arrange
            var subTopicA = await SubTopicGenerator.Generate(context);
            var subTopicB = await SubTopicGenerator.Generate(context);
            var subTopicC = await SubTopicGenerator.Generate(context);

            var studentSubTopicA = await StudentSubTopicGenerator.Generate(context, new EN.StudentSubTopic
            {
                SubTopicId = subTopicA.Id,
            });

            var studentSubTopicB = await StudentSubTopicGenerator.Generate(context, new EN.StudentSubTopic
            {
                SubTopicId = subTopicB.Id,
            });

            var studentSubTopicC = await StudentSubTopicGenerator.Generate(context, new EN.StudentSubTopic
            {
                SubTopicId = subTopicC.Id,
            });

            var subTopics = new List<EN.SubTopic> { subTopicA, subTopicB };

            //Act
            var isAchieve = _experience.IsAchieve(subTopics, null);

            //Assert
            Assert.False(isAchieve);
        }

        [Fact]
        public async Task IsAchieve_False_WhenStudentSubTopicIsInActive()
        {
            //Arrange
            var subTopicA = await SubTopicGenerator.Generate(context);
            var subTopicB = await SubTopicGenerator.Generate(context);
            var subTopicC = await SubTopicGenerator.Generate(context);

            var studentSubTopicA = await StudentSubTopicGenerator.Generate(context, new EN.StudentSubTopic
            {
                SubTopicId = subTopicA.Id,
            });

            var studentScoreA = await StudentScoreGenerator.Generate(context, appSetting, new EN.StudentScore
            {
                StudentSubTopicId = subTopicA.Id,
                Score = 1
            });

            studentSubTopicA.StudentScores.Add(studentScoreA);

            var studentSubTopicB = await StudentSubTopicGenerator.Generate(context, new EN.StudentSubTopic
            {
                SubTopicId = subTopicB.Id,
            });

            var studentScoreB = await StudentScoreGenerator.Generate(context, appSetting, new EN.StudentScore
            {
                StudentSubTopicId = subTopicB.Id,
                Score = 0
            });

            studentSubTopicB.StudentScores.Add(studentScoreB);

            var studentSubTopicC = await StudentSubTopicGenerator.Generate(context, new EN.StudentSubTopic
            {
                SubTopicId = subTopicC.Id,
            });

            var studentScoreC = await StudentScoreGenerator.Generate(context, appSetting, new EN.StudentScore
            {
                StudentSubTopicId = subTopicC.Id,
                CriteriaId = (await CriticalGenerator.Generate(context)).Id,
                Score = 0,
                StatusId = UT.Status.Inactive.Id
            });

            studentSubTopicC.StudentScores.Add(studentScoreC);

            var subTopics = new List<EN.SubTopic> { subTopicA, subTopicB, subTopicC };
            var studentSubTopics = new List<EN.StudentSubTopic> { studentSubTopicA, studentSubTopicB, studentSubTopicC };
            //Act
            var isAchieve = _experience.IsAchieve(subTopics, studentSubTopics);

            //Assert
            Assert.False(isAchieve);
        }
    }
}
