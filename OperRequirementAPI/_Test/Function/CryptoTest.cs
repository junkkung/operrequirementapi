﻿using Bogus;
using Database;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;
using TU = _Test.Utilities;
using UT = Utilities;

namespace _Test.Function
{
    public class CryptoTest : IDisposable
    {
        private readonly IConfiguration appSetting;
        private readonly Faker faker;

        public CryptoTest()
        {
            this.appSetting = TU.Utils.InitConfiguration();
            this.faker = new Faker();
        }

        public void Dispose()
        {
            UT.CacheManager.RemoveAll();
        }

        [Theory()]
        [InlineData(null, false)]
        [InlineData("", false)]
        [InlineData("aB3&ae$#", true)]
        [InlineData("aB3&ae$", false)] // range < 8
        [InlineData("aB3sevae", false)] // special charactor
        [InlineData("aBeas&ae", false)] // numeric
        [InlineData("a3&ssddae$", false)] // capital
        public void PasswordAcceptable(string password, bool expected)
        {
            // Act
            var result = UT.Crypto.IsPasswordAcceptable(password);

            // Assert
            Assert.Equal(expected, result);
        }

    
    }
}
