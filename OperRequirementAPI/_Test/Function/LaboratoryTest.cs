﻿using System;
using System.Collections.Generic;
using System.Text;
using TU = _Test.Utilities;
using UT = Utilities;
using EN = Database.Entities;
using FN = Models.Functions;
using Database;
using Microsoft.Extensions.Configuration;
using Bogus;
using Models.Functions;
using Xunit;
using System.Threading.Tasks;
using _Test.Utilities.Generator;
using System.Linq;
using Utilities;

namespace _Test.Function
{
    public class LaboratoryTest : IDisposable
    {
        private readonly OperRequirementContext context;
        private readonly IConfiguration appSetting;       
        private readonly ILaboratory _laboratory;
        private readonly Faker faker;

        public LaboratoryTest()
        {
            this.context = TU.Utils.GetMemoryContext($"Laboratory{Guid.NewGuid().ToString()}");
            this.appSetting = TU.Utils.InitConfiguration();
            var controllers = new TU.NewController(this.context);
            this._laboratory = controllers.Laboratory;
            this.faker = new Faker();
        }

        public void Dispose()
        {
            UT.CacheManager.RemoveAll();
            context.Database.EnsureDeleted();
        }

        [Theory]
        [InlineData(0, 0, 0)]
        [InlineData(1, 0, 1)]
        [InlineData(4, 20, 24)]
        [InlineData(0, 20, 20)]
        public async Task GetTotalScore1Lab1Topic1Subtopic2Criteria(int goodScore, int fairScore, int expectScore)
        {
            // Assign
            var laboratory = await LaboratoryGenerator.Generate(context);
            var topic = await TopicGenerator.Generate(context, new EN.Topic { LaboratoryId = laboratory.Id });
            var subTopic = await SubTopicGenerator.Generate(context, new EN.SubTopic { TopicId = topic.Id});
            var goodCriteria = await CriteriaGenerator.Generate(context, new EN.Criteria
            {
                SubTopicId = subTopic.Id,
                CriteriaTypeId = FN.Criteria.Type.Good.Id,
                Score = goodScore
            });

            var fairCriteria = await CriteriaGenerator.Generate(context, new EN.Criteria
            {
                SubTopicId = subTopic.Id,
                CriteriaTypeId = FN.Criteria.Type.FairOrUnAcceptable.Id,
                Score = fairScore
            });

            // Act
            var totalScore = await _laboratory.GetTotalScore(laboratory.Id);

            // Assert
            Assert.Equal(expectScore, totalScore);
        }

        [Theory]
        [InlineData(0, 0, 10, 0)]
        [InlineData(1, 0, 2,  2)]
        [InlineData(4, 20, 3, 72)]
        [InlineData(0, 20, 10, 200)]
        public async Task GetTotalScore1Lab1TopicManySubtopicsManyCriteria(int goodScore, int fairScore,
                                        int totalSubTopic, int expectScore)
        {
            // Assign
            var laboratory = await LaboratoryGenerator.Generate(context);
            var topic = await TopicGenerator.Generate(context, new EN.Topic { LaboratoryId = laboratory.Id });
            var subTopics = await SubTopicGenerator.Generate(context, topic.Id, totalSubTopic);

            foreach (var subTopic in subTopics)
            {
                await CriteriaGenerator.Generate(context, new EN.Criteria
                {
                    SubTopicId = subTopic.Id,
                    CriteriaTypeId = FN.Criteria.Type.Good.Id,
                    Score = goodScore
                });

                await CriteriaGenerator.Generate(context, new EN.Criteria
                {
                    SubTopicId = subTopic.Id,
                    CriteriaTypeId = FN.Criteria.Type.FairOrUnAcceptable.Id,
                    Score = fairScore
                });
            }

            // Act
            var totalScore = await _laboratory.GetTotalScore(laboratory.Id);

            // Assert
            Assert.Equal(expectScore, totalScore);
        }

        [Theory]
        [InlineData(0, 0, 2, 10, 0)]
        [InlineData(1, 0, 2, 2, 4)]
        [InlineData(4, 20, 10, 3, 720)]
        [InlineData(0, 20, 10, 10, 2000)]
        public async Task GetTotalScore1LabManyTopicManySubtopicsManyCriteria(int goodScore, int fairScore,
                                        int totalTopic, int totalSubTopic, int expectScore)
        {
            // Assign
            var laboratory = await LaboratoryGenerator.Generate(context);
            var experience = await ExperienceGenerator.Generate(context);

            for (var i = 0; i < totalTopic; i++)
            {
                var topic = await TopicGenerator.Generate(context, new EN.Topic 
                { 
                    LaboratoryId = laboratory.Id,
                    ExperienceId = experience.Id
                });

                for(var j = 0; j < totalSubTopic; j++)
                {
                    var subTopic = await SubTopicGenerator.Generate(context, new EN.SubTopic
                    {
                        TopicId = topic.Id
                    });

                    await CriteriaGenerator.Generate(context, new EN.Criteria
                    {
                        SubTopicId = subTopic.Id,
                        CriteriaTypeId = FN.Criteria.Type.Good.Id,
                        Score = goodScore
                    });

                    await CriteriaGenerator.Generate(context, new EN.Criteria
                    {
                        SubTopicId = subTopic.Id,
                        CriteriaTypeId = FN.Criteria.Type.FairOrUnAcceptable.Id,
                        Score = fairScore
                    });
                }
            }
    
            // Act
            var totalScore = await _laboratory.GetTotalScore(laboratory.Id);

            // Assert
            Assert.Equal(expectScore, totalScore);
        }

        [Fact]
        public async Task GetTotalScoreWhenCriteriaNotExist_Return_0()
        {
            // Assign
            var laboratory = await LaboratoryGenerator.Generate(context);
            var topic = await TopicGenerator.Generate(context, new EN.Topic { LaboratoryId = laboratory.Id });
            var subTopic = await SubTopicGenerator.Generate(context, new EN.SubTopic { TopicId = topic.Id });

            // Act
            var totalScore = await _laboratory.GetTotalScore(laboratory.Id);

            // Assert
            Assert.Equal(0, totalScore);
        }

        [Fact]
        public async Task GetTotalScoreWhenSubTopicNotExist_Return_0()
        {
            // Assign
            var laboratory = await LaboratoryGenerator.Generate(context);
            var topic = await TopicGenerator.Generate(context, new EN.Topic { LaboratoryId = laboratory.Id });

            // Act
            var totalScore = await _laboratory.GetTotalScore(laboratory.Id);

            // Assert
            Assert.Equal(0, totalScore);
        }

        [Fact]
        public async Task GetTotalScoreWhenTopicNotExist_Return_0()
        {
            // Assign
            var laboratory = await LaboratoryGenerator.Generate(context);

            // Act
            var totalScore = await _laboratory.GetTotalScore(laboratory.Id);

            // Assert
            Assert.Equal(0, totalScore);
        }     

        [Fact]
        public async Task GetLaboratoryDetail()
        {            
            // Arrange
            var labPreset = await TU.Preset.LabPreset.PrepareEmptyLab(context, appSetting);
            var critical1 = await CriticalGenerator.Generate(context);
            var critical2 = await CriticalGenerator.Generate(context);

            //  Topic A
            var topicA = await labPreset.AddTopic();
            var subTopicA1 = await labPreset.AddSubTopic(topicA.Id, critical2.Id);
            var goodCriteriaA1 = await labPreset.AddCriteria(subTopicA1.Id, FN.Criteria.Type.Good.Id);
            var fairCriteriaA1 = await labPreset.AddCriteria(subTopicA1.Id, FN.Criteria.Type.FairOrUnAcceptable.Id);

            var subTopicA2 = await labPreset.AddSubTopic(topicA.Id, critical1.Id);
            var goodCriteriaA2 = await labPreset.AddCriteria(subTopicA2.Id, FN.Criteria.Type.Good.Id);
            var fairCriteriaA2 = await labPreset.AddCriteria(subTopicA2.Id, FN.Criteria.Type.FairOrUnAcceptable.Id);

            // Topic B
            var topicB = await labPreset.AddTopic();
            var subTopicB1 = await labPreset.AddSubTopic(topicB.Id, critical2.Id);
            var goodCriteriaB1 = await labPreset.AddCriteria(subTopicB1.Id, FN.Criteria.Type.Good.Id);
            var fairCriteriaB1 = await labPreset.AddCriteria(subTopicB1.Id, FN.Criteria.Type.FairOrUnAcceptable.Id);

            // Act
            var labDetail = await _laboratory.GetDetail(labPreset.Laboratory.Id);

            // Assert
            var laboratory = labPreset.Laboratory;
            Assert.Equal(laboratory.Id, labDetail.Id);
            Assert.Equal(laboratory.Name, labDetail.Name);
            Assert.Equal(laboratory.Sequence, labDetail.Sequence);
            Assert.Equal(laboratory.Description, labDetail.Description);
            Assert.Equal(2, labDetail.Topics.Count);

            //[0]
            var topic = labDetail.Topics.ToList()[0];
            Assert.Equal(topicA.Id, topic.Id);
            Assert.Equal(topicA.Name, topic.Name);
            Assert.Equal(topicA.Description, topic.Description);
            Assert.Equal(topicA.Sequence, topic.Sequence);
            Assert.Equal(topicA.ExperienceId, topic.ExperienceId);
            Assert.Equal(topicA.LaboratoryId, topic.LaboratoryId);

            var subTopics = topic.SubTopics.ToList();
            var subTopic = subTopics[0];
            Assert.Equal(subTopicA1.Id, subTopic.Id);
            Assert.Equal(subTopicA1.Name, subTopic.Name);
            Assert.Equal(subTopicA1.Description, subTopic.Description);
            Assert.Equal(subTopicA1.CriticalId, subTopic.CriticalId);
            Assert.Equal(subTopicA1.IsLabel, subTopic.IsLabel);
            Assert.Equal(subTopicA1.TopicId, subTopic.TopicId);
            Assert.Equal(subTopicA1.Sequence, subTopic.Sequence);

            var criterias = subTopic.Criterias.ToList();
            var criteria = criterias[0];
            Assert.Equal(goodCriteriaA1.Id, criteria.Id);
            Assert.Equal(goodCriteriaA1.Name, criteria.Name);
            Assert.Equal(goodCriteriaA1.Score, criteria.Score);
            Assert.Equal(goodCriteriaA1.SubTopicId, criteria.SubTopicId);

            criteria = criterias[1];
            Assert.Equal(fairCriteriaA1.Id, criteria.Id);
            Assert.Equal(fairCriteriaA1.Name, criteria.Name);
            Assert.Equal(fairCriteriaA1.Score, criteria.Score);
            Assert.Equal(fairCriteriaA1.SubTopicId, criteria.SubTopicId);

            Assert.Equal(critical2.Id, subTopic.Critical.Id);
            Assert.Equal(critical2.Name, subTopic.Critical.Name);

            subTopic = subTopics[1];
            Assert.Equal(subTopicA2.Id, subTopic.Id);
            Assert.Equal(subTopicA2.Name, subTopic.Name);
            Assert.Equal(subTopicA2.Description, subTopic.Description);
            Assert.Equal(subTopicA2.CriticalId, subTopic.CriticalId);
            Assert.Equal(subTopicA2.IsLabel, subTopic.IsLabel);
            Assert.Equal(subTopicA2.TopicId, subTopic.TopicId);
            Assert.Equal(subTopicA2.Sequence, subTopic.Sequence);

            criterias = subTopic.Criterias.ToList();
            criteria = criterias[0];
            Assert.Equal(goodCriteriaA2.Id, criteria.Id);
            Assert.Equal(goodCriteriaA2.Name, criteria.Name);
            Assert.Equal(goodCriteriaA2.Score, criteria.Score);
            Assert.Equal(goodCriteriaA2.SubTopicId, criteria.SubTopicId);

            criteria = criterias[1];
            Assert.Equal(fairCriteriaA2.Id, criteria.Id);
            Assert.Equal(fairCriteriaA2.Name, criteria.Name);
            Assert.Equal(fairCriteriaA2.Score, criteria.Score);
            Assert.Equal(fairCriteriaA2.SubTopicId, criteria.SubTopicId);

            Assert.Equal(critical1.Id, subTopic.Critical.Id);
            Assert.Equal(critical1.Name, subTopic.Critical.Name);

            // [1]
            topic = labDetail.Topics.ToList()[1];
            Assert.Equal(topicB.Id, topic.Id);
            Assert.Equal(topicB.Name, topic.Name);
            Assert.Equal(topicB.Description, topic.Description);
            Assert.Equal(topicB.Sequence, topic.Sequence);
            Assert.Equal(topicB.ExperienceId, topic.ExperienceId);
            Assert.Equal(topicB.LaboratoryId, topic.LaboratoryId);

            subTopics = topic.SubTopics.ToList();
            subTopic = subTopics[0];
            Assert.Equal(subTopicB1.Id, subTopic.Id);
            Assert.Equal(subTopicB1.Name, subTopic.Name);
            Assert.Equal(subTopicB1.Description, subTopic.Description);
            Assert.Equal(subTopicB1.CriticalId, subTopic.CriticalId);
            Assert.Equal(subTopicB1.IsLabel, subTopic.IsLabel);
            Assert.Equal(subTopicB1.TopicId, subTopic.TopicId);
            Assert.Equal(subTopicB1.Sequence, subTopic.Sequence);

            criterias = subTopic.Criterias.ToList();
            criteria = criterias[0];
            Assert.Equal(goodCriteriaB1.Id, criteria.Id);
            Assert.Equal(goodCriteriaB1.Name, criteria.Name);
            Assert.Equal(goodCriteriaB1.Score, criteria.Score);
            Assert.Equal(goodCriteriaB1.SubTopicId, criteria.SubTopicId);

            criteria = criterias[1];
            Assert.Equal(fairCriteriaB1.Id, criteria.Id);
            Assert.Equal(fairCriteriaB1.Name, criteria.Name);
            Assert.Equal(fairCriteriaB1.Score, criteria.Score);
            Assert.Equal(fairCriteriaB1.SubTopicId, criteria.SubTopicId);

            Assert.Equal(critical2.Id, subTopic.Critical.Id);
            Assert.Equal(critical2.Name, subTopic.Critical.Name);
        }

        [Fact]
        public async Task CheckCacheGetLaboratoryDetail()
        {
            // Assign
            var laboratory = await LaboratoryGenerator.Generate(context);
            var critical1 = await CriticalGenerator.Generate(context);
            var critical2 = await CriticalGenerator.Generate(context);

            //  Topic A
            var topicA = await TopicGenerator.Generate(context, new EN.Topic
            {
                LaboratoryId = laboratory.Id,
            });

            var subTopicA1 = await SubTopicGenerator.Generate(context, new EN.SubTopic
            {
                TopicId = topicA.Id,
                CriticalId = critical2.Id,
            });
            var goodCriteriaA1 = await CriteriaGenerator.Generate(context, new EN.Criteria
            {
                SubTopicId = subTopicA1.Id,
                CriteriaTypeId = FN.Criteria.Type.Good.Id,
            });

            var fairCriteriaA1 = await CriteriaGenerator.Generate(context, new EN.Criteria
            {
                SubTopicId = subTopicA1.Id,
                CriteriaTypeId = FN.Criteria.Type.FairOrUnAcceptable.Id,
            });

            var subTopicA2 = await SubTopicGenerator.Generate(context, new EN.SubTopic
            {
                TopicId = topicA.Id,
                CriticalId = critical1.Id,
            });
            var goodCriteriaA2 = await CriteriaGenerator.Generate(context, new EN.Criteria
            {
                SubTopicId = subTopicA2.Id,
                CriteriaTypeId = FN.Criteria.Type.Good.Id,
            });

            var fairCriteriaA2 = await CriteriaGenerator.Generate(context, new EN.Criteria
            {
                SubTopicId = subTopicA2.Id,
                CriteriaTypeId = FN.Criteria.Type.FairOrUnAcceptable.Id,
            });

            // Topic B
            var topicB = await TopicGenerator.Generate(context, new EN.Topic
            {
                LaboratoryId = laboratory.Id,
            });
            var subTopicB1 = await SubTopicGenerator.Generate(context, new EN.SubTopic
            {
                TopicId = topicB.Id,
                CriticalId = critical2.Id,
            });
            var goodCriteriaB1 = await CriteriaGenerator.Generate(context, new EN.Criteria
            {
                SubTopicId = subTopicB1.Id,
                CriteriaTypeId = FN.Criteria.Type.Good.Id,
            });

            var fairCriteriaB1 = await CriteriaGenerator.Generate(context, new EN.Criteria
            {
                SubTopicId = subTopicB1.Id,
                CriteriaTypeId = FN.Criteria.Type.FairOrUnAcceptable.Id,
            });

            // Act
            var cacheKey = CacheKey.Laboratory.Detail(laboratory.Id);
            var cacheBofore = CacheManager.Get<EN.Laboratory>(cacheKey);
            var labDetail = await _laboratory.GetDetail(laboratory.Id);
            var cacheAfter = CacheManager.Get<EN.Laboratory>(cacheKey);

            // Assert
            Assert.Null(cacheBofore);
            Assert.Equal(labDetail, cacheAfter);
        }

        [Fact]
        public async Task GetLaboratoryDetailNotActive_ReturnNull()
        {
            // Assign
            var laboratory = await LaboratoryGenerator.Generate(context, new EN.Laboratory
            {
                StatusId = UT.Status.Inactive.Id
            });

            // Act
            var detail = await _laboratory.GetDetail(laboratory.Id);

            // Assert
            Assert.Null(detail);
        }

        [Fact]
        public async Task GetLaboratoryDetailNotExist_ReturnNull()
        {
            // Assign

            // Act
            var detail = await _laboratory.GetDetail(100);

            // Assert
            Assert.Null(detail);
        }
    }
}
