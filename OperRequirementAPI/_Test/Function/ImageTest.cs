﻿using Microsoft.Extensions.Configuration;
using Models.Functions;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;
using UT = Utilities;
using TU = _Test.Utilities;

namespace _Test.Function
{
    public class ImageTest : IDisposable
    {
        private readonly IImage _image;
        private readonly IConfiguration appSetting;

        public ImageTest()
        {
            this.appSetting = TU.Utils.InitConfiguration();
            _image = new Image(appSetting);
        }

        public void Dispose()
        {
            //UT.CacheManager.RemoveAll();
        }

        [Theory]
        [InlineData("aaa.jpg", true)]
        [InlineData("aaa.png", true)]
        [InlineData("aaa.tiff", true)]
        [InlineData("aaa.heif", true)]
        [InlineData("aaa.gif", true)]
        [InlineData("aaa.bmp", true)]
        [InlineData("aaa.jpeg", true)]
        [InlineData("a.jpg", true)]
        [InlineData("a.heif", true)]
        [InlineData("aaa.JPG", true)]
        [InlineData(".jpg", false)]
        [InlineData(".jpeg", false)]
        [InlineData("aaa.aaa", false)]
        [InlineData("aaaaaaa", false)]
        [InlineData("aaa.jpdklajf.exe", false)]      
        [InlineData("aaa..rvvr.d.vr.rv.", false)]
        public void CheckImageFileType(string name, bool expectedResult)
        {
            //Arrange

            //Act
            var result = _image.IsFileFormatAcceptable(name);

            //Arrange
            Assert.Equal(expectedResult, result);

        }

    }
}
