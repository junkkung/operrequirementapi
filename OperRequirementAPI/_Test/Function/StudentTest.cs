﻿using System;
using System.Collections.Generic;
using System.Text;
using TU = _Test.Utilities;
using UT = Utilities;
using EN = Database.Entities;
using FN = Models.Functions;
using Database;
using Microsoft.Extensions.Configuration;
using Bogus;
using Models.Functions;
using Xunit;
using System.Threading.Tasks;
using _Test.Utilities.Generator;
using System.Linq;
using Models.Responses;
using Microsoft.EntityFrameworkCore;
using Models.Requests;
using System.Net;

namespace _Test.Function
{
    public class StudentTest : IDisposable
    {
        private readonly OperRequirementContext context;
        private readonly IConfiguration appSetting;
        private readonly IStudent _student;
        private readonly ILaboratory _laboratory;
        private readonly Faker faker;

        public StudentTest()
        {
            this.context = TU.Utils.GetMemoryContext($"Student{Guid.NewGuid().ToString()}");
            this.appSetting = TU.Utils.InitConfiguration();
            var controllers = new TU.NewController(this.context);
            this._student = controllers.Student;
            this._laboratory = controllers.Laboratory;            
            this.faker = new Faker();
        }

        public void Dispose()
        {
            UT.CacheManager.RemoveAll();
            context.Database.EnsureDeleted();
        }

        [Fact]
        public async Task CacheCheckGetStudentByCourseId()
        {
            //Assign
            var course = await CourseGenerator.Generate(context);

            var students = new List<EN.Student>();
            for (var i = 0; i < 10; i++)
                students.Add(await StudentGenerator.Generate(context, appSetting, new EN.Student
                {
                    Group = i <= 2 ? 1 : (i > 2 && i <= 5 ? 2 : 3),
                    CourseId = course.Id
                }));

            //Act
            var studentsByCourseId = await _student.GetByCourseId(course.Id);

            //Assert
            var fromCache = UT.CacheManager.Get<List<EN.Student>>(UT.CacheKey.Student.Course(course.Id));
            Assert.Same(studentsByCourseId, fromCache);
        }

        [Theory]
        [InlineData(0, 0, 10, 10, 0)]
        [InlineData(1, 0, 10, 10, 100)]
        [InlineData(1, 1, 10, 10, 200)]
        [InlineData(0, 1, 10, 10, 100)]
        [InlineData(10, 1, 2, 100, 2200)]
        public async Task GetSummaryScoreInLaboratoryCaseAllSubtopicAdded(int goodScore, int fairScore, int totalTopic, int totalSubTopic,
                                        int expectScore)
        {
            // Assign
            await TU.Preset.CommonPreset.Generate(context);
            var preset = await TU.Preset.LabPreset.PrepareGoodLab(context, appSetting, new TU.Preset.LabPreset.Data
            {
                TotalTopic = totalTopic,
                TotalSubTopic = totalSubTopic,
                GoodScore = goodScore,
                FairScore = fairScore
            });

            var student = await StudentGenerator.Generate(context, appSetting);
            var studentLaboratory = await StudentLaboratoryGenerator.Generate(context, new EN.StudentLaboratory
            {
                LaboratoryId = preset.Laboratory.Id,
                StudentId = student.Id
            });

            var studentSubTopics = new List<EN.StudentSubTopic>();
            foreach (var subTopic in preset.SubTopics)
                studentSubTopics.Add(await StudentSubTopicGenerator.Generate(context, new EN.StudentSubTopic
                {
                    SubTopicId = subTopic.Id,
                    StudentLaboratoryId = studentLaboratory.Id
                }));

            foreach (var criteria in preset.Criterias)
            {
                var studentSubTopic = studentSubTopics.Where(s => s.SubTopicId == criteria.SubTopicId).FirstOrDefault();

                await StudentScoreGenerator.Generate(context, appSetting, new EN.StudentScore
                {
                    StudentSubTopicId = studentSubTopic.Id,
                    CriteriaId = criteria.Id,
                    Score = criteria.Score
                });
            }

            // Act
            var studentScores = await _student.GetScore(student.Id, preset.Laboratory.Id);
            var totalScore = _student.LabScoreSummary(studentScores, preset.Laboratory);

            // Assert
            Assert.Equal(expectScore, totalScore);
        }

        [Theory]
        [InlineData(0, 0, 10, 10, 1, 0)]
        [InlineData(1, 0, 10, 10, 1, 1)]
        [InlineData(1, 1, 10, 10, 10, 20)]
        [InlineData(0, 3, 10, 10, 10, 30)]
        [InlineData(10, 1, 2, 100, 50, 550)]
        public async Task GetSummaryScoreInLaboratoryCaseSomeSubTopicAdd(int goodScore, int fairScore, int totalTopic, int totalSubTopic,
                                       int totalSubTopicAdd, int expectScore)
        {
            // Assign
            await TU.Preset.CommonPreset.Generate(context);
            var preset = await TU.Preset.LabPreset.PrepareGoodLab(context, appSetting, new TU.Preset.LabPreset.Data
            {
                TotalTopic = totalTopic,
                TotalSubTopic = totalSubTopic,
                GoodScore = goodScore,
                FairScore = fairScore
            });

            var student = await StudentGenerator.Generate(context, appSetting);
            var studentLaboratory = await StudentLaboratoryGenerator.Generate(context, new EN.StudentLaboratory
            {
                LaboratoryId = preset.Laboratory.Id,
                StudentId = student.Id
            });

            for (var i = 0; i < totalSubTopicAdd; i++)
            {
                var studentSubTopic = await StudentSubTopicGenerator.Generate(context, new EN.StudentSubTopic
                {
                    SubTopicId = preset.SubTopics[i].Id,
                    StudentLaboratoryId = studentLaboratory.Id
                });

                var criterias = preset.Criterias.Where(c => c.SubTopicId == preset.SubTopics[i].Id).ToList();

                await StudentScoreGenerator.Generate(context, appSetting, new EN.StudentScore
                {
                    StudentSubTopicId = studentSubTopic.Id,
                    CriteriaId = criterias[0].Id,
                    Score = criterias[0].Score
                });

                await StudentScoreGenerator.Generate(context, appSetting, new EN.StudentScore
                {
                    StudentSubTopicId = studentSubTopic.Id,
                    CriteriaId = criterias[1].Id,
                    Score = criterias[1].Score
                });
            }

            // Act
            var studentScores = await _student.GetScore(student.Id, preset.Laboratory.Id);
            var totalScore = _student.LabScoreSummary(studentScores, preset.Laboratory);

            // Assert
            Assert.Equal(expectScore, totalScore);
        }

        [Fact]
        public async Task GetSummaryScoreInLaboratoryEmptyStudentScore_Return_0()
        {
            // Assign
            var laboratory = await LaboratoryGenerator.Generate(context);
            var studentScores = new List<EN.StudentScore>();

            // Act
            var totalScore = _student.LabScoreSummary(studentScores, laboratory);

            // Assert
            Assert.Equal(0, totalScore);
        }


        [Fact]
        public async Task GetLabScoreSummaryWhenSomeScoreCreateDateOverLaboratoryEndDate_Return_0()
        {

            // Assign
            await TU.Preset.CommonPreset.Generate(context);
            var preset = await TU.Preset.LabPreset.PrepareGoodLab(context, appSetting, new TU.Preset.LabPreset.Data
            {
                TotalTopic = 3,
                TotalSubTopic = 3,
                GoodScore = 10,
                FairScore = 10
            });

            preset.Laboratory.EndDate = DateTime.Now.AddDays(-1);
            context.Update(preset.Laboratory);
            await context.SaveChangesAsync();
            context.Entry(preset.Laboratory).State = EntityState.Detached;

            var student = await StudentGenerator.Generate(context, appSetting);
            var studentLaboratory = await StudentLaboratoryGenerator.Generate(context, new EN.StudentLaboratory
            {
                LaboratoryId = preset.Laboratory.Id,
                StudentId = student.Id
            });

            // Add subtopic 1
            var studentSubTopic = await StudentSubTopicGenerator.Generate(context, new EN.StudentSubTopic
            {
                SubTopicId = preset.SubTopics[0].Id,
                StudentLaboratoryId = studentLaboratory.Id
            });

            var criterias = preset.Criterias.Where(c => c.SubTopicId == preset.SubTopics[0].Id).ToList();

            await StudentScoreGenerator.Generate(context, appSetting, new EN.StudentScore
            {
                StudentSubTopicId = studentSubTopic.Id,
                CriteriaId = criterias[0].Id,
                Score = criterias[0].Score
            });

            // Add subtopic 2 but createDate over laboratory endDate
            studentSubTopic = await StudentSubTopicGenerator.Generate(context, new EN.StudentSubTopic
            {
                SubTopicId = preset.SubTopics[0].Id,
                StudentLaboratoryId = studentLaboratory.Id
            });

            criterias = preset.Criterias.Where(c => c.SubTopicId == preset.SubTopics[0].Id).ToList();

            await StudentScoreGenerator.Generate(context, appSetting, new EN.StudentScore
            {
                StudentSubTopicId = studentSubTopic.Id,
                CriteriaId = criterias[1].Id,
                Score = criterias[1].Score,
                CreateDate = DateTimeOffset.Now
            });


            // Act
            var studentScores = await _student.GetScore(student.Id, preset.Laboratory.Id);
            var totalScore = _student.LabScoreSummary(studentScores, preset.Laboratory);

            // Assert
            Assert.Equal(0, totalScore);
        }

        [Fact]
        public async Task GetLabScoreSummaryWhenSomeScoreCreateDateOverTopicEndDate()
        {
            // Assign
            await TU.Preset.CommonPreset.Generate(context);
            var preset = await TU.Preset.LabPreset.PrepareGoodLab(context, appSetting, new TU.Preset.LabPreset.Data
            {
                TotalTopic = 2,
                TotalSubTopic = 1,
                GoodScore = 5,
                FairScore = 10
            });

            preset.Laboratory.EndDate = DateTime.Now.AddDays(1);
            context.Update(preset.Laboratory);

            preset.Topics[1].EndDate = DateTime.Now.AddDays(-1);
            context.Update(preset.Topics[1]);

            await context.SaveChangesAsync();
            context.Entry(preset.Laboratory).State = EntityState.Detached;
            context.Entry(preset.Topics[1]).State = EntityState.Detached;

            var student = await StudentGenerator.Generate(context, appSetting);
            var studentLaboratory = await StudentLaboratoryGenerator.Generate(context, new EN.StudentLaboratory
            {
                LaboratoryId = preset.Laboratory.Id,
                StudentId = student.Id
            });

            // Add subtopic 1
            var studentSubTopic = await StudentSubTopicGenerator.Generate(context, new EN.StudentSubTopic
            {
                SubTopicId = preset.SubTopics[0].Id,
                StudentLaboratoryId = studentLaboratory.Id
            });

            var criterias = preset.Criterias.Where(c => c.SubTopicId == preset.SubTopics[0].Id).ToList();

            await StudentScoreGenerator.Generate(context, appSetting, new EN.StudentScore
            {
                StudentSubTopicId = studentSubTopic.Id,
                CriteriaId = criterias[0].Id,
                Score = criterias[0].Score
            });

            // Add subtopic 2 but createDate over topic endDate
            studentSubTopic = await StudentSubTopicGenerator.Generate(context, new EN.StudentSubTopic
            {
                SubTopicId = preset.SubTopics[1].Id,
                StudentLaboratoryId = studentLaboratory.Id
            });

            criterias = preset.Criterias.Where(c => c.SubTopicId == preset.SubTopics[1].Id).ToList();

            await StudentScoreGenerator.Generate(context, appSetting, new EN.StudentScore
            {
                StudentSubTopicId = studentSubTopic.Id,
                CriteriaId = criterias[1].Id,
                Score = criterias[1].Score,
                CreateDate = DateTimeOffset.Now
            });

            // Act
            var studentScores = await _student.GetScore(student.Id, preset.Laboratory.Id);
            var totalScore = _student.LabScoreSummary(studentScores, preset.Laboratory);

            // Assert
            Assert.Equal(5, totalScore);
        }

        [Fact]
        public async Task GetLabScoreSummaryWhenScoreHasCritical()
        {
            // Assign
            var labPreset = await TU.Preset.LabPreset.PrepareEmptyLab(context, appSetting);
            var critical1 = await CriticalGenerator.Generate(context);
            var critical2 = await CriticalGenerator.Generate(context);
            var critical3 = await CriticalGenerator.Generate(context);

            //  Topic A
            var topicA = await labPreset.AddTopic();
            var subTopicA1 = await labPreset.AddSubTopic(topicA.Id, critical1.Id);
            var goodCriteriaA1 = await labPreset.AddCriteria(subTopicA1.Id, FN.Criteria.Type.Good.Id, statusId: null, 5);
            var fairCriteriaA1 = await labPreset.AddCriteria(subTopicA1.Id, FN.Criteria.Type.FairOrUnAcceptable.Id, statusId: null, 10);

            var subTopicA2 = await labPreset.AddSubTopic(topicA.Id, critical2.Id);
            var goodCriteriaA2 = await labPreset.AddCriteria(subTopicA2.Id, FN.Criteria.Type.Good.Id, statusId: null, 4);
            var fairCriteriaA2 = await labPreset.AddCriteria(subTopicA2.Id, FN.Criteria.Type.FairOrUnAcceptable.Id, statusId: null, 9);

            var subTopicA3 = await labPreset.AddSubTopic(topicA.Id, critical2.Id);
            var goodCriteriaA3 = await labPreset.AddCriteria(subTopicA2.Id, FN.Criteria.Type.Good.Id, statusId: null, 6);
            var fairCriteriaA3 = await labPreset.AddCriteria(subTopicA2.Id, FN.Criteria.Type.FairOrUnAcceptable.Id, statusId: null, 8);

            var subTopicA4 = await labPreset.AddSubTopic(topicA.Id, critical3.Id);
            var goodCriteriaA4 = await labPreset.AddCriteria(subTopicA2.Id, FN.Criteria.Type.Good.Id, statusId: null, 7);
            var fairCriteriaA4 = await labPreset.AddCriteria(subTopicA2.Id, FN.Criteria.Type.FairOrUnAcceptable.Id, statusId: null, 0);

            // Student detail
            var instructorX = await InstructorGenerator.Generate(context, appSetting);
            var instructorY = await InstructorGenerator.Generate(context, appSetting);
            var studentPreset = await TU.Preset.StudentPreset.PrepareEmptyStudent(context, appSetting);
            var studentLab = await StudentLaboratoryGenerator.Generate(context, new EN.StudentLaboratory
            {
                StudentId = studentPreset.Student.Id,
                LaboratoryId = labPreset.Laboratory.Id,
            });

            var studentSubTopicA1 = await studentPreset.AddSubTopic(studentLab.Id, subTopicA1.Id);
            var studentScoreA1 = await studentPreset.AddScore(studentSubTopicA1.Id, instructorX.Id,
                                isCritical: false, goodCriteriaA1.Id, statusId: null, 5);

            var studentSubTopicA2_1 = await studentPreset.AddSubTopic(studentLab.Id, subTopicA2.Id);
            var studentScoreA2_1 = await studentPreset.AddScore(studentSubTopicA2_1.Id, instructorX.Id,
                                isCritical: false, fairCriteriaA2.Id, statusId: null, 9);

            var studentSubTopicA2_2 = await studentPreset.AddSubTopic(studentLab.Id, subTopicA2.Id);
            var studentScoreA2_2 = await studentPreset.AddScore(studentSubTopicA2_2.Id, instructorY.Id,
                                isCritical: true);

            var studentSubTopicA3_1 = await studentPreset.AddSubTopic(studentLab.Id, subTopicA3.Id);
            var studentScoreA3_1 = await studentPreset.AddScore(studentSubTopicA3_1.Id, instructorX.Id,
                                isCritical: false, goodCriteriaA3.Id, statusId: null, 6);

            var studentSubTopicA3_2 = await studentPreset.AddSubTopic(studentLab.Id, subTopicA3.Id);
            var studentScoreA3_2 = await studentPreset.AddScore(studentSubTopicA3_2.Id, instructorY.Id,
                                isCritical: true);

            var studentSubTopicA4 = await studentPreset.AddSubTopic(studentLab.Id, subTopicA4.Id);
            var studentScoreA4 = await studentPreset.AddScore(studentSubTopicA4.Id, instructorX.Id,
                                isCritical: false, fairCriteriaA4.Id, statusId: null, 0);

            // Act
            var studentScores = await _student.GetScore(studentPreset.Student.Id, labPreset.Laboratory.Id);
            var totalScore = _student.LabScoreSummary(studentScores, labPreset.Laboratory);

            // Assert
            Assert.Equal(5, totalScore);

        }

        [Fact]
        public async Task GetLabScoreNullShouldReturnNullNotException()
        {
            // Assign
            await TU.Preset.CommonPreset.Generate(context);
            var preset = await TU.Preset.LabPreset.PrepareGoodLab(context, appSetting, new TU.Preset.LabPreset.Data
            {
                TotalTopic = 1,
                TotalSubTopic = 1,
                GoodScore = 1,
                FairScore = 0
            });

            var student = await StudentGenerator.Generate(context, appSetting);

            // Act
            var studentScores = await _student.GetScore(student.Id, preset.Laboratory.Id);

            // Assert
            Assert.Null(studentScores);
        }

        [Fact]
        public async Task GetMinusPoint()
        {
            //Assign
            await TU.Preset.CommonPreset.Generate(context);
            var course = await CourseGenerator.Generate(context);
            var student = await StudentGenerator.Generate(context, appSetting, new EN.Student
            {
                CourseId = course.Id
            });

            var presetLab1 = await TU.Preset.LabPreset.PrepareGoodLabWithSomeStudentScore(
                context, appSetting, course, student, new TU.Preset.LabPreset.Data()
            );

            var minusPointLab1 = await StudentMinusPointGenerator.Generate(context, appSetting, new EN.StudentMinusPoint
            {
                StudentLaboratoryId = presetLab1.StudentLaboratory.Id,
                Score = 1,
                CreateDate = DateTime.Now.AddDays(-1)
            });

            var presetLab2 = await TU.Preset.LabPreset.PrepareGoodLabWithSomeStudentScore(
                context, appSetting, course, student, new TU.Preset.LabPreset.Data()
            );

            var minusPointLab2 = await StudentMinusPointGenerator.Generate(context, appSetting, new EN.StudentMinusPoint
            {
                StudentLaboratoryId = presetLab2.StudentLaboratory.Id,
                Score = 10,
                CreateDate = DateTime.Now.AddDays(1)
            });

            var presetLab3 = await TU.Preset.LabPreset.PrepareGoodLabWithSomeStudentScore(
                context, appSetting, course, student, new TU.Preset.LabPreset.Data()
            );

            var minusPointLab3 = await StudentMinusPointGenerator.Generate(context, appSetting, new EN.StudentMinusPoint
            {
                StudentLaboratoryId = presetLab1.StudentLaboratory.Id,
                Score = 100,
                CreateDate = DateTime.Now
            });

            var minusPointLabList = new List<EN.StudentMinusPoint> { minusPointLab2, minusPointLab3, minusPointLab1 };

            // Act
            var minusPoints = await _student.GetMinusPoint(student.Id);

            // Assert
            Assert.Equal(3, minusPoints.Count);

            for (var i = 0; i < minusPoints.Count; i++)
            {
                var minusPoint = minusPoints[i];
                Assert.Equal(minusPointLabList[i].Id, minusPoint.Id);
                Assert.Equal(minusPointLabList[i].Score, minusPoint.Score);
                Assert.Equal(minusPointLabList[i].StudentLaboratoryId, minusPoint.StudentLaboratoryId);
                Assert.Equal(minusPointLabList[i].InstructorId, minusPoint.InstructorId);
                Assert.Equal(minusPointLabList[i].MinusPointId, minusPoint.MinusPointId);
                Assert.Equal(minusPointLabList[i].Remark, minusPoint.Remark);
            }
        }

        [Fact]
        public async Task GetMinusPointWithNotActiveStatus()
        {
            //Assign
            await TU.Preset.CommonPreset.Generate(context);
            var course = await CourseGenerator.Generate(context);
            var student = await StudentGenerator.Generate(context, appSetting, new EN.Student
            {
                CourseId = course.Id
            });

            var presetLab1 = await TU.Preset.LabPreset.PrepareGoodLabWithSomeStudentScore(
                context, appSetting, course, student, new TU.Preset.LabPreset.Data()
            );

            var minusPointLab1 = await StudentMinusPointGenerator.Generate(context, appSetting, new EN.StudentMinusPoint
            {
                StudentLaboratoryId = presetLab1.StudentLaboratory.Id,
                Score = 1,
                CreateDate = DateTime.Now.AddDays(-1)
            });

            var presetLab2 = await TU.Preset.LabPreset.PrepareGoodLabWithSomeStudentScore(
                context, appSetting, course, student, new TU.Preset.LabPreset.Data()
            );

            var minusPointLab2 = await StudentMinusPointGenerator.Generate(context, appSetting, new EN.StudentMinusPoint
            {
                StudentLaboratoryId = presetLab2.StudentLaboratory.Id,
                Score = 10,
                CreateDate = DateTime.Now.AddDays(1),
                StatusId = UT.Status.Inactive.Id
            });

            var minusPointLabList = new List<EN.StudentMinusPoint> { minusPointLab1 };

            // Act
            var minusPoints = await _student.GetMinusPoint(student.Id);

            // Assert
            Assert.Single(minusPoints);

            for (var i = 0; i < minusPoints.Count; i++)
            {
                var minusPoint = minusPoints[i];
                Assert.Equal(minusPointLabList[i].Id, minusPoint.Id);
                Assert.Equal(minusPointLabList[i].Score, minusPoint.Score);
                Assert.Equal(minusPointLabList[i].StudentLaboratoryId, minusPoint.StudentLaboratoryId);
                Assert.Equal(minusPointLabList[i].InstructorId, minusPoint.InstructorId);
                Assert.Equal(minusPointLabList[i].MinusPointId, minusPoint.MinusPointId);
                Assert.Equal(minusPointLabList[i].Remark, minusPoint.Remark);
            }
        }

        [Fact]
        public async Task GetMinusPointWhenStudentLabIsNull_returnEmptyArray_NotException()
        {
            //Assign
            await TU.Preset.CommonPreset.Generate(context);
            var course = await CourseGenerator.Generate(context);
            var student = await StudentGenerator.Generate(context, appSetting, new EN.Student
            {
                CourseId = course.Id
            });

            // Act
            var studentLabMinusPoint = await _student.GetMinusPoint(student.Id, laboratoryId: 1);

            // Assert
            Assert.NotNull(studentLabMinusPoint);
            Assert.Equal(new List<EN.StudentMinusPoint>(), studentLabMinusPoint);

        }

        [Fact]
        public async Task GetStudentLabDetail()
        {
            // Assign
            var labPreset = await TU.Preset.LabPreset.PrepareEmptyLab(context, appSetting);
            var critical1 = await CriticalGenerator.Generate(context);
            var critical2 = await CriticalGenerator.Generate(context);

            //  Topic A
            var topicA = await labPreset.AddTopic();
            var subTopicA1 = await labPreset.AddSubTopic(topicA.Id, critical2.Id);
            var goodCriteriaA1 = await labPreset.AddCriteria(subTopicA1.Id, FN.Criteria.Type.Good.Id, 
                                        statusId: null, score: 10);
            var fairCriteriaA1 = await labPreset.AddCriteria(subTopicA1.Id, FN.Criteria.Type.FairOrUnAcceptable.Id);

            var subTopicA2 = await labPreset.AddSubTopic(topicA.Id, critical1.Id);
            var goodCriteriaA2 = await labPreset.AddCriteria(subTopicA2.Id, FN.Criteria.Type.Good.Id,
                                        statusId: null, score: 8);
            var fairCriteriaA2 = await labPreset.AddCriteria(subTopicA2.Id, FN.Criteria.Type.FairOrUnAcceptable.Id);

            // Topic B
            var topicB = await labPreset.AddTopic();
            var subTopicB1 = await labPreset.AddSubTopic(topicB.Id, critical2.Id);
            var goodCriteriaB1 = await labPreset.AddCriteria(subTopicB1.Id, FN.Criteria.Type.Good.Id,
                                        statusId: null, score: 20);
            var fairCriteriaB1 = await labPreset.AddCriteria(subTopicB1.Id, FN.Criteria.Type.FairOrUnAcceptable.Id);

            var subTopicB2 = await labPreset.AddSubTopic(topicB.Id, CriticalId: null, statusId: null, isLabel: true);

            // Student detail
            var instructorX = await InstructorGenerator.Generate(context, appSetting);
            var instructorY = await InstructorGenerator.Generate(context, appSetting);
            var studentPreset = await TU.Preset.StudentPreset.PrepareEmptyStudent(context, appSetting);
            var studentLab = await StudentLaboratoryGenerator.Generate(context, new EN.StudentLaboratory
            {
                StudentId = studentPreset.Student.Id,
                LaboratoryId = labPreset.Laboratory.Id,
                Image = faker.Name.FirstName(),
                Material = faker.Name.FirstName(),
                KindOfWork = faker.Name.FirstName(),
                Tooth = faker.Name.FirstName(),
            });
            var studentSubTopicA2 = await studentPreset.AddSubTopic(studentLab.Id, subTopicA2.Id);
            var studentScoreA2 = await studentPreset.AddScore(studentSubTopicA2.Id, instructorX.Id,
                                isCritical: false, goodCriteriaA2.Id, statusId: null, goodCriteriaA2.Score);

            var studentSubTopicB1 = await studentPreset.AddSubTopic(studentLab.Id, subTopicB1.Id, selfAssessment: faker.Random.Number(0, 10));
            var studentScoreB1 = await studentPreset.AddScore(studentSubTopicB1.Id, instructorY.Id,
                                isCritical: false, fairCriteriaB1.Id, statusId: null, goodCriteriaB1.Score);

            // Act
            var studentLabDetail = (await _student.GetLabDetail(studentPreset.Student.Id, labPreset.Laboratory.Id)).Object;

            // Assert
            Assert.Equal(studentLab.Id, studentLabDetail.Id);
            Assert.Equal(studentLab.Image, studentLabDetail.Image);
            Assert.Equal(studentLab.KindOfWork, studentLabDetail.KindOfWork);
            Assert.Equal(studentLab.Material, studentLabDetail.Material);
            Assert.Equal(studentLab.Tooth, studentLabDetail.Tooth);
            Assert.Null(studentLabDetail.InstructorName);

            Assert.Equal(2, studentLabDetail.Topics.Count);

            // Topic A
            var topic = studentLabDetail.Topics[0];
            Assert.Equal(goodCriteriaA2.Score, topic.Score);
            Assert.Equal(goodCriteriaA1.Score + goodCriteriaA2.Score, topic.TotalScore);
            Assert.Equal(topicA.Id, topic.Id);
            Assert.Equal(topicA.Name, topic.Name);
            Assert.Equal(topicA.Description, topic.Description);

            Assert.Equal(2, topic.SubTopics.Count);

            // SubTopic A1
            var subTopic = topic.SubTopics[0];
            Assert.Equal(subTopicA1.Id, subTopic.Id);
            Assert.Equal(subTopicA1.IsLabel, subTopic.IsLabel);
            Assert.Equal(subTopicA1.Name, subTopic.Name);
            Assert.Null(subTopic.SelfAssessment);
            Assert.Equal(subTopicA1.Description, subTopic.Description);

            Assert.Equal(2, subTopic.Criterias.Count);

            var criteria = subTopic.Criterias[0];
            Assert.Equal(goodCriteriaA1.Id, criteria.Id);
            Assert.Equal(goodCriteriaA1.Name, criteria.Name);
            Assert.Equal(goodCriteriaA1.CriteriaTypeId, criteria.CriteriaTypeId);
            Assert.Null(criteria.InstructorName);

            criteria = subTopic.Criterias[1];
            Assert.Equal(fairCriteriaA1.Id, criteria.Id);
            Assert.Equal(fairCriteriaA1.Name, criteria.Name);
            Assert.Equal(fairCriteriaA1.CriteriaTypeId, criteria.CriteriaTypeId);
            Assert.Null(criteria.InstructorName);

            // SubTopic A2
            subTopic = topic.SubTopics[1];
            Assert.Equal(subTopicA2.Id, subTopic.Id);
            Assert.Equal(subTopicA2.IsLabel, subTopic.IsLabel);
            Assert.Equal(subTopicA2.Name, subTopic.Name);
            Assert.Null(subTopic.SelfAssessment);
            Assert.Equal(subTopicA2.Description, subTopic.Description);

            Assert.Equal(2, subTopic.Criterias.Count);

            criteria = subTopic.Criterias[0];
            Assert.Equal(goodCriteriaA2.Id, criteria.Id);
            Assert.Equal(goodCriteriaA2.Name, criteria.Name);
            Assert.Equal(goodCriteriaA2.CriteriaTypeId, criteria.CriteriaTypeId);
            var signature = UT.Parse.ToSignature(instructorX.Firstname, instructorX.Lastname);
            Assert.Equal(signature, criteria.InstructorName);

            criteria = subTopic.Criterias[1];
            Assert.Equal(fairCriteriaA2.Id, criteria.Id);
            Assert.Equal(fairCriteriaA2.Name, criteria.Name);
            Assert.Equal(fairCriteriaA2.CriteriaTypeId, criteria.CriteriaTypeId);
            Assert.Null(criteria.InstructorName);

            // Topic B
            topic = studentLabDetail.Topics[1];
            Assert.Equal(goodCriteriaB1.Score, topic.Score);
            Assert.Equal(goodCriteriaB1.Score, topic.TotalScore);
            Assert.Equal(topicB.Id, topic.Id);
            Assert.Equal(topicB.Name, topic.Name);
            Assert.Equal(topicB.Description, topic.Description);

            Assert.Equal(2, topic.SubTopics.Count);

            // SubTopic B1
            subTopic = topic.SubTopics[0];
            Assert.Equal(subTopicB1.Id, subTopic.Id);
            Assert.Equal(subTopicB1.IsLabel, subTopic.IsLabel);
            Assert.Equal(subTopicB1.Name, subTopic.Name);
            Assert.Equal(studentSubTopicB1.SelfAssessment, subTopic.SelfAssessment);
            Assert.Equal(subTopicB1.Description, subTopic.Description);

            Assert.Equal(2, subTopic.Criterias.Count);

            criteria = subTopic.Criterias[0];
            Assert.Equal(goodCriteriaB1.Id, criteria.Id);
            Assert.Equal(goodCriteriaB1.Name, criteria.Name);
            Assert.Equal(goodCriteriaB1.CriteriaTypeId, criteria.CriteriaTypeId);
            Assert.Null(criteria.InstructorName);

            criteria = subTopic.Criterias[1];
            Assert.Equal(fairCriteriaB1.Id, criteria.Id);
            Assert.Equal(fairCriteriaB1.Name, criteria.Name);
            Assert.Equal(fairCriteriaB1.CriteriaTypeId, criteria.CriteriaTypeId);
            signature = UT.Parse.ToSignature(instructorY.Firstname, instructorY.Lastname);
            Assert.Equal(signature, criteria.InstructorName);

            // SubTopic B2
            subTopic = topic.SubTopics[1];
            Assert.Equal(subTopicB2.Id, subTopic.Id);
            Assert.Equal(subTopicB2.IsLabel, subTopic.IsLabel);
            Assert.Equal(subTopicB2.Name, subTopic.Name);
            Assert.Null(subTopic.SelfAssessment);
            Assert.Equal(subTopicB2.Description, subTopic.Description);

            Assert.Empty(subTopic.Criterias);
        }

        [Fact]
        public async Task GetStudentLabDetailWhenNoAnyStudentData()
        {
            // Assign
            var labPreset = await TU.Preset.LabPreset.PrepareEmptyLab(context, appSetting);
            var critical1 = await CriticalGenerator.Generate(context);
            var critical2 = await CriticalGenerator.Generate(context);

            //  Topic A
            var topicA = await labPreset.AddTopic();
            var subTopicA1 = await labPreset.AddSubTopic(topicA.Id, critical2.Id);
            var goodCriteriaA1 = await labPreset.AddCriteria(subTopicA1.Id, FN.Criteria.Type.Good.Id);
            var fairCriteriaA1 = await labPreset.AddCriteria(subTopicA1.Id, FN.Criteria.Type.FairOrUnAcceptable.Id);

            var subTopicA2 = await labPreset.AddSubTopic(topicA.Id, critical1.Id);
            var goodCriteriaA2 = await labPreset.AddCriteria(subTopicA2.Id, FN.Criteria.Type.Good.Id);
            var fairCriteriaA2 = await labPreset.AddCriteria(subTopicA2.Id, FN.Criteria.Type.FairOrUnAcceptable.Id);

            // Student detail        
            var studentPreset = await TU.Preset.StudentPreset.PrepareEmptyStudent(context, appSetting);
            var instructorX = await InstructorGenerator.Generate(context, appSetting);

            // Act
            var studentLabDetail = (await _student.GetLabDetail(studentPreset.Student.Id, labPreset.Laboratory.Id)).Object;

            // Assert
            Assert.Null(studentLabDetail.Id);
            Assert.Null(studentLabDetail.Image);
            Assert.Null(studentLabDetail.KindOfWork);
            Assert.Null(studentLabDetail.Material);
            Assert.Null(studentLabDetail.Tooth);

            Assert.Single(studentLabDetail.Topics);

            // Topic A
            var topic = studentLabDetail.Topics[0];
            Assert.Equal(topicA.Id, topic.Id);
            Assert.Equal(topicA.Name, topic.Name);
            Assert.Equal(topicA.Description, topic.Description);

            Assert.Equal(2, topic.SubTopics.Count);

            // SubTopic A1
            var subTopic = topic.SubTopics[0];
            Assert.Equal(subTopicA1.Id, subTopic.Id);
            Assert.Equal(subTopicA1.IsLabel, subTopic.IsLabel);
            Assert.Equal(subTopicA1.Name, subTopic.Name);
            Assert.Null(subTopic.SelfAssessment);
            Assert.Equal(subTopicA1.Description, subTopic.Description);

            Assert.Equal(2, subTopic.Criterias.Count);

            var criteria = subTopic.Criterias[0];
            Assert.Equal(goodCriteriaA1.Id, criteria.Id);
            Assert.Equal(goodCriteriaA1.Name, criteria.Name);
            Assert.Equal(goodCriteriaA1.CriteriaTypeId, criteria.CriteriaTypeId);
            Assert.Null(criteria.InstructorName);

            criteria = subTopic.Criterias[1];
            Assert.Equal(fairCriteriaA1.Id, criteria.Id);
            Assert.Equal(fairCriteriaA1.Name, criteria.Name);
            Assert.Equal(fairCriteriaA1.CriteriaTypeId, criteria.CriteriaTypeId);
            Assert.Null(criteria.InstructorName);

            // SubTopic A2
            subTopic = topic.SubTopics[1];
            Assert.Equal(subTopicA2.Id, subTopic.Id);
            Assert.Equal(subTopicA2.IsLabel, subTopic.IsLabel);
            Assert.Equal(subTopicA2.Name, subTopic.Name);
            Assert.Null(subTopic.SelfAssessment);
            Assert.Equal(subTopicA2.Description, subTopic.Description);

            Assert.Equal(2, subTopic.Criterias.Count);

            criteria = subTopic.Criterias[0];
            Assert.Equal(goodCriteriaA2.Id, criteria.Id);
            Assert.Equal(goodCriteriaA2.Name, criteria.Name);
            Assert.Equal(goodCriteriaA2.CriteriaTypeId, criteria.CriteriaTypeId);
            Assert.Null(criteria.InstructorName);

            criteria = subTopic.Criterias[1];
            Assert.Equal(fairCriteriaA2.Id, criteria.Id);
            Assert.Equal(fairCriteriaA2.Name, criteria.Name);
            Assert.Equal(fairCriteriaA2.CriteriaTypeId, criteria.CriteriaTypeId);
            Assert.Null(criteria.InstructorName);
        }

        [Fact]
        public async Task GetStudentLabDetailWhenSubTopicIsLabel()
        {
            // Assign
            var labPreset = await TU.Preset.LabPreset.PrepareEmptyLab(context, appSetting);

            //  Topic A
            var topicA = await labPreset.AddTopic();
            var subTopicA = await labPreset.AddSubTopic(topicA.Id, CriticalId: null, statusId: null, isLabel: true);

            var studentPreset = await TU.Preset.StudentPreset.PrepareEmptyStudent(context, appSetting);

            // Act
            var studentLabDetail = (await _student.GetLabDetail(studentPreset.Student.Id, labPreset.Laboratory.Id)).Object;

            // Assert
            Assert.Single(studentLabDetail.Topics);

            // Topic A
            var topic = studentLabDetail.Topics[0];
            Assert.Equal(topicA.Id, topic.Id);
            Assert.Equal(topicA.Name, topic.Name);
            Assert.Equal(topicA.Description, topic.Description);

            Assert.Single(topic.SubTopics);

            // SubTopic A
            var subTopic = topic.SubTopics[0];
            Assert.Equal(subTopicA.Id, subTopic.Id);
            Assert.Equal(subTopicA.IsLabel, subTopic.IsLabel);
            Assert.Equal(subTopicA.Name, subTopic.Name);
            Assert.Null(subTopic.SelfAssessment);
            Assert.Equal(subTopicA.Description, subTopic.Description);

            Assert.Empty(subTopic.Criterias);
        }

        [Fact]
        public async Task GetStudentLabDetailWhenThereIsSomeTopicNotActive()
        {
            // Assign
            var labPreset = await TU.Preset.LabPreset.PrepareEmptyLab(context, appSetting);
            var critical1 = await CriticalGenerator.Generate(context);
            var critical2 = await CriticalGenerator.Generate(context);

            //  Topic A
            var topicA = await labPreset.AddTopic();
            var subTopicA1 = await labPreset.AddSubTopic(topicA.Id, critical2.Id);
            var goodCriteriaA1 = await labPreset.AddCriteria(subTopicA1.Id, FN.Criteria.Type.Good.Id);
            var fairCriteriaA1 = await labPreset.AddCriteria(subTopicA1.Id, FN.Criteria.Type.FairOrUnAcceptable.Id);

            var subTopicA2 = await labPreset.AddSubTopic(topicA.Id, critical1.Id);
            var goodCriteriaA2 = await labPreset.AddCriteria(subTopicA2.Id, FN.Criteria.Type.Good.Id);
            var fairCriteriaA2 = await labPreset.AddCriteria(subTopicA2.Id, FN.Criteria.Type.FairOrUnAcceptable.Id);

            // Topic B
            var topicB = await labPreset.AddTopic(statusId: UT.Status.Inactive.Id);
            var subTopicB1 = await labPreset.AddSubTopic(topicB.Id, critical2.Id);
            var goodCriteriaB1 = await labPreset.AddCriteria(subTopicB1.Id, FN.Criteria.Type.Good.Id);
            var fairCriteriaB1 = await labPreset.AddCriteria(subTopicB1.Id, FN.Criteria.Type.FairOrUnAcceptable.Id);

            // Topic C
            var topicC = await labPreset.AddTopic();
            var subTopicC1 = await labPreset.AddSubTopic(topicC.Id, critical2.Id);
            var goodCriteriaC1 = await labPreset.AddCriteria(subTopicC1.Id, FN.Criteria.Type.Good.Id);
            var fairCriteriaC1 = await labPreset.AddCriteria(subTopicC1.Id, FN.Criteria.Type.FairOrUnAcceptable.Id);


            // Student detail        
            var studentPreset = await TU.Preset.StudentPreset.PrepareEmptyStudent(context, appSetting);

            // Act
            var studentLabDetail = (await _student.GetLabDetail(studentPreset.Student.Id, labPreset.Laboratory.Id)).Object;

            // Assert
            Assert.Equal(2, studentLabDetail.Topics.Count);

            // Topic A
            var topic = studentLabDetail.Topics[0];
            Assert.Equal(topicA.Id, topic.Id);
            Assert.Equal(topicA.Name, topic.Name);
            Assert.Equal(topicA.Description, topic.Description);

            Assert.Equal(2, topic.SubTopics.Count);

            // Topic C
            topic = studentLabDetail.Topics[1];
            Assert.Equal(topicC.Id, topic.Id);
            Assert.Equal(topicC.Name, topic.Name);
            Assert.Equal(topicC.Description, topic.Description);

            Assert.Single(topic.SubTopics);
        }

        [Fact]
        public async Task GetStudentLabDetailWhenThereIsSomeSubTopicNotActive()
        {
            // Assign
            var labPreset = await TU.Preset.LabPreset.PrepareEmptyLab(context, appSetting);
            var critical1 = await CriticalGenerator.Generate(context);
            var critical2 = await CriticalGenerator.Generate(context);

            //  Topic A
            var topicA = await labPreset.AddTopic();
            var subTopicA1 = await labPreset.AddSubTopic(topicA.Id, critical2.Id, statusId: UT.Status.Inactive.Id);
            var goodCriteriaA1 = await labPreset.AddCriteria(subTopicA1.Id, FN.Criteria.Type.Good.Id);
            var fairCriteriaA1 = await labPreset.AddCriteria(subTopicA1.Id, FN.Criteria.Type.FairOrUnAcceptable.Id);

            var subTopicA2 = await labPreset.AddSubTopic(topicA.Id, critical1.Id);
            var goodCriteriaA2 = await labPreset.AddCriteria(subTopicA2.Id, FN.Criteria.Type.Good.Id);
            var fairCriteriaA2 = await labPreset.AddCriteria(subTopicA2.Id, FN.Criteria.Type.FairOrUnAcceptable.Id);

            var subTopicA3 = await labPreset.AddSubTopic(topicA.Id, critical1.Id);
            var goodCriteriaA3 = await labPreset.AddCriteria(subTopicA3.Id, FN.Criteria.Type.Good.Id);
            var fairCriteriaA3 = await labPreset.AddCriteria(subTopicA3.Id, FN.Criteria.Type.FairOrUnAcceptable.Id);

            // Student detail        
            var studentPreset = await TU.Preset.StudentPreset.PrepareEmptyStudent(context, appSetting);

            // Act
            var studentLabDetail = (await _student.GetLabDetail(studentPreset.Student.Id, labPreset.Laboratory.Id)).Object;

            // Assert
            Assert.Single(studentLabDetail.Topics);

            // Topic A
            var topic = studentLabDetail.Topics[0];
            Assert.Equal(2, topic.SubTopics.Count);

            // SubTopic A2
            var subTopic = topic.SubTopics[0];
            Assert.Equal(subTopicA2.Id, subTopic.Id);
            Assert.Equal(subTopicA2.IsLabel, subTopic.IsLabel);
            Assert.Equal(subTopicA2.Name, subTopic.Name);
            Assert.Null(subTopic.SelfAssessment);
            Assert.Equal(subTopicA2.Description, subTopic.Description);

            Assert.Equal(2, subTopic.Criterias.Count);

            // SubTopic A3
            subTopic = topic.SubTopics[1];
            Assert.Equal(subTopicA3.Id, subTopic.Id);
            Assert.Equal(subTopicA3.IsLabel, subTopic.IsLabel);
            Assert.Equal(subTopicA3.Name, subTopic.Name);
            Assert.Null(subTopic.SelfAssessment);
            Assert.Equal(subTopicA3.Description, subTopic.Description);

            Assert.Equal(2, subTopic.Criterias.Count);
        }

        [Fact]
        public async Task GetStudentLabDetailWhenThereIsSomeCriteriaNotActive()
        {
            // Assign
            var labPreset = await TU.Preset.LabPreset.PrepareEmptyLab(context, appSetting);
            var critical1 = await CriticalGenerator.Generate(context);
            var critical2 = await CriticalGenerator.Generate(context);

            //  Topic A
            var topicA = await labPreset.AddTopic();
            var subTopicA1 = await labPreset.AddSubTopic(topicA.Id, critical2.Id);
            var goodCriteriaA1 = await labPreset.AddCriteria(subTopicA1.Id, FN.Criteria.Type.Good.Id);
            var fairCriteriaA1 = await labPreset.AddCriteria(subTopicA1.Id, FN.Criteria.Type.FairOrUnAcceptable.Id,
                                        statusId: UT.Status.Inactive.Id);
            var fairCriteriaA12 = await labPreset.AddCriteria(subTopicA1.Id, FN.Criteria.Type.FairOrUnAcceptable.Id);

            // Student detail        
            var studentPreset = await TU.Preset.StudentPreset.PrepareEmptyStudent(context, appSetting);

            // Act
            var studentLabDetail = (await _student.GetLabDetail(studentPreset.Student.Id, labPreset.Laboratory.Id)).Object;

            // Assert
            Assert.Single(studentLabDetail.Topics);

            // Topic A
            var topic = studentLabDetail.Topics[0];
            Assert.Single(topic.SubTopics);

            // SubTopic A
            var subTopic = topic.SubTopics[0];
            Assert.Equal(2, subTopic.Criterias.Count);

            var criteria = subTopic.Criterias[0];
            Assert.Equal(goodCriteriaA1.Id, criteria.Id);
            Assert.Equal(goodCriteriaA1.Name, criteria.Name);
            Assert.Equal(goodCriteriaA1.CriteriaTypeId, criteria.CriteriaTypeId);
            Assert.Null(criteria.InstructorName);

            criteria = subTopic.Criterias[1];
            Assert.Equal(fairCriteriaA12.Id, criteria.Id);
            Assert.Equal(fairCriteriaA12.Name, criteria.Name);
            Assert.Equal(fairCriteriaA12.CriteriaTypeId, criteria.CriteriaTypeId);
            Assert.Null(criteria.InstructorName);
        }

        [Fact]
        public async Task CheckCacheGetStudentLabDetail()
        {
            var labPreset = await TU.Preset.LabPreset.PrepareEmptyLab(context, appSetting);
            var critical1 = await CriticalGenerator.Generate(context);
            var critical2 = await CriticalGenerator.Generate(context);

            //  Topic A
            var topicA = await labPreset.AddTopic();
            var subTopicA1 = await labPreset.AddSubTopic(topicA.Id, critical2.Id);
            var goodCriteriaA1 = await labPreset.AddCriteria(subTopicA1.Id, FN.Criteria.Type.Good.Id);
            var fairCriteriaA1 = await labPreset.AddCriteria(subTopicA1.Id, FN.Criteria.Type.FairOrUnAcceptable.Id);

            var subTopicA2 = await labPreset.AddSubTopic(topicA.Id, critical1.Id);
            var goodCriteriaA2 = await labPreset.AddCriteria(subTopicA2.Id, FN.Criteria.Type.Good.Id);
            var fairCriteriaA2 = await labPreset.AddCriteria(subTopicA2.Id, FN.Criteria.Type.FairOrUnAcceptable.Id);

            // Topic B
            var topicB = await labPreset.AddTopic();
            var subTopicB1 = await labPreset.AddSubTopic(topicB.Id, critical2.Id);
            var goodCriteriaB1 = await labPreset.AddCriteria(subTopicB1.Id, FN.Criteria.Type.Good.Id);
            var fairCriteriaB1 = await labPreset.AddCriteria(subTopicB1.Id, FN.Criteria.Type.FairOrUnAcceptable.Id);

            var subTopicB2 = await labPreset.AddSubTopic(topicB.Id, CriticalId: null, statusId: null, isLabel: true);

            // Student detail
            var instructorX = await InstructorGenerator.Generate(context, appSetting);
            var instructorY = await InstructorGenerator.Generate(context, appSetting);
            var studentPreset = await TU.Preset.StudentPreset.PrepareEmptyStudent(context, appSetting);
            var studentLab = await StudentLaboratoryGenerator.Generate(context, new EN.StudentLaboratory
            {
                StudentId = studentPreset.Student.Id,
                LaboratoryId = labPreset.Laboratory.Id,
                Image = faker.Name.FirstName(),
                Material = faker.Name.FirstName(),
                KindOfWork = faker.Name.FirstName(),
                Tooth = faker.Name.FirstName(),
            });
            var studentSubTopicA2 = await studentPreset.AddSubTopic(studentLab.Id, subTopicA2.Id);
            var studentScoreA2 = await studentPreset.AddScore(studentSubTopicA2.Id, instructorX.Id,
                                isCritical: false, goodCriteriaA2.Id);

            var studentSubTopicB1 = await studentPreset.AddSubTopic(studentLab.Id, subTopicB1.Id, selfAssessment: faker.Random.Number(0, 10));
            var studentScoreB1 = await studentPreset.AddScore(studentSubTopicB1.Id, instructorY.Id,
                                isCritical: false, fairCriteriaB1.Id);

            // Act
            var cacheKey = UT.CacheKey.Student.LaboratoryDetail(studentPreset.Student.Id, labPreset.Laboratory.Id);
            var cacheBefore = UT.CacheManager.Get<StudentLaboratoryDetailResponse>(cacheKey);
            var studentLabDetail = (await _student.GetLabDetail(studentPreset.Student.Id, labPreset.Laboratory.Id)).Object;
            var cacheAfter = UT.CacheManager.Get<StudentLaboratoryDetailResponse>(cacheKey);

            // Assert
            Assert.Null(cacheBefore);
            Assert.Equal(studentLabDetail, cacheAfter);
        }

        [Fact]
        public async Task GetExperience()
        {
            //Arrange
            var labPreset = await TU.Preset.LabPreset.PrepareEmptyLab(context, appSetting);
            var experienceA = labPreset.Experience;
            experienceA.Sequence = 2;
            context.Update(experienceA);
            await context.SaveChangesAsync();
            context.Entry(experienceA).State = EntityState.Detached;

            var experienceB = await ExperienceGenerator.Generate(context, new EN.Experience { Sequence = 1 });

            var topicA = await labPreset.AddTopic(statusId: null, experienceA.Id);
            var topicB = await labPreset.AddTopic(statusId: null, experienceB.Id);

            var subTopicA = await labPreset.AddSubTopic(topicA.Id);
            var subTopicB = await labPreset.AddSubTopic(topicB.Id);

            var student = new EN.Student { CourseId = labPreset.Laboratory.CourseId };
            var studentPreset = await TU.Preset.StudentPreset.PrepareEmptyStudent(context, appSetting, student);
            var studentLab = await studentPreset.AddLab(labPreset.Laboratory.Id);
            var studentSubTopicA = await studentPreset.AddSubTopic(studentLab.Id, subTopicA.Id);
            var instructor = await InstructorGenerator.Generate(context, appSetting);
            var studentScore = await studentPreset.AddScore(studentSubTopicA.Id, instructor.Id);

            //Act
            var experiences = (await _student.GetExperience(studentPreset.Student.Id)).Object;

            //Assert
            Assert.Equal(2, experiences.Count);

            var experience = experiences[0];
            Assert.Equal(experienceB.Id, experience.Id);
            Assert.Equal(experienceB.Name, experience.Name);
            Assert.Equal(experienceB.Description, experience.Description);
            Assert.False(experience.IsAchieve);

            experience = experiences[1];
            Assert.Equal(experienceA.Id, experience.Id);
            Assert.Equal(experienceA.Name, experience.Name);
            Assert.Equal(experienceA.Description, experience.Description);
            Assert.True(experience.IsAchieve);
        }

        [Fact]
        public async Task GetExperienceWhenNoAnyExperience_returnEmpty()
        {
            //Arrange
            var laboratory = await LaboratoryGenerator.Generate(context);
            var student = await StudentGenerator.Generate(context, appSetting);

            //Act
            var experiences = (await _student.GetExperience(student.Id)).Object;

            //Assert
            Assert.Empty(experiences);
        }

        [Fact]
        public async Task GetExperienceWhenNoTopicInsideIsAchieve_false()
        {

            //Arrange
            var labPreset = await TU.Preset.LabPreset.PrepareEmptyLab(context, appSetting);
            var student = await StudentGenerator.Generate(context, appSetting);

            //Act
            var experiences = (await _student.GetExperience(student.Id)).Object;

            //Assert
            Assert.Single(experiences);
            var experience = experiences[0];

            Assert.Equal(labPreset.Experience.Id, experience.Id);
            Assert.Equal(labPreset.Experience.Name, experience.Name);
            Assert.Equal(labPreset.Experience.Description, experience.Description);
            Assert.False(experience.IsAchieve);
        }

        [Fact]
        public async Task GetExperienceWhenOnlyNotActiveTopics_false()
        {

            //Arrange
            var labPreset = await TU.Preset.LabPreset.PrepareEmptyLab(context, appSetting);
            var experienceA = labPreset.Experience;
            experienceA.Sequence = 2;
            context.Update(experienceA);
            await context.SaveChangesAsync();
            context.Entry(experienceA).State = EntityState.Detached;

            var topicA = await labPreset.AddTopic(UT.Status.Inactive.Id, experienceA.Id);
            var topicB = await labPreset.AddTopic(UT.Status.Inactive.Id, experienceA.Id);

            var studentPreset = await TU.Preset.StudentPreset.PrepareEmptyStudent(context, appSetting);

            //Act
            var experiences = (await _student.GetExperience(studentPreset.Student.Id)).Object;

            //Assert
            Assert.Single(experiences);
            var experience = experiences[0];

            Assert.Equal(labPreset.Experience.Id, experience.Id);
            Assert.Equal(labPreset.Experience.Name, experience.Name);
            Assert.Equal(labPreset.Experience.Description, experience.Description);
            Assert.False(experience.IsAchieve);
        }

        [Theory]
        [InlineData(UT.Status.Inactive.Id, UT.Status.Inactive.Id, UT.Status.Inactive.Id, true)]
        [InlineData(UT.Status.Inactive.Id, UT.Status.Inactive.Id, UT.Status.Active.Id, false)]
        [InlineData(UT.Status.Inactive.Id, UT.Status.Active.Id, UT.Status.Active.Id, false)]
        [InlineData(UT.Status.Active.Id, UT.Status.Active.Id, UT.Status.Active.Id, false)]
        public async Task IsOnlyNotActiveTopic(int statusAId, int statusBId, int StatusCId, bool expectResult)
        {
            //Arrange
            var topicA = await TopicGenerator.Generate(context, new EN.Topic { StatusId = statusAId });
            var topicB = await TopicGenerator.Generate(context, new EN.Topic { StatusId = statusBId });
            var topicC = await TopicGenerator.Generate(context, new EN.Topic { StatusId = StatusCId });

            var topicList = new List<EN.Topic> { topicA, topicB, topicC };

            //Act
            var result = _student.IsOnlyNotActiveTopic(topicList);

            //Assert
            Assert.Equal(expectResult, result);
        }

        [Fact]
        public async Task GetExperienceWhenSomeTopicNotAchieve_false()
        {
            //Arrange
            var labPreset = await TU.Preset.LabPreset.PrepareEmptyLab(context, appSetting);
            var experienceA = labPreset.Experience;
            experienceA.Sequence = 2;
            context.Update(experienceA);
            await context.SaveChangesAsync();
            context.Entry(experienceA).State = EntityState.Detached;

            var topicA = await labPreset.AddTopic(statusId: null, experienceA.Id);
            var topicB = await labPreset.AddTopic(statusId: null, experienceA.Id);

            var subTopicA = await labPreset.AddSubTopic(topicA.Id);
            var subTopicB = await labPreset.AddSubTopic(topicB.Id);

            var student = new EN.Student { CourseId = labPreset.Laboratory.CourseId };
            var studentPreset = await TU.Preset.StudentPreset.PrepareEmptyStudent(context, appSetting, student);
            var studentLab = await studentPreset.AddLab(labPreset.Laboratory.Id);
            var studentSubTopicA = await studentPreset.AddSubTopic(studentLab.Id, subTopicA.Id);
            var instructor = await InstructorGenerator.Generate(context, appSetting);
            var studentScore = await studentPreset.AddScore(studentSubTopicA.Id, instructor.Id);

            //Act
            var experiences = (await _student.GetExperience(studentPreset.Student.Id)).Object;

            //Assert
            Assert.Single(experiences);

            var experience = experiences[0];
            Assert.Equal(experienceA.Id, experience.Id);
            Assert.Equal(experienceA.Name, experience.Name);
            Assert.Equal(experienceA.Description, experience.Description);
            Assert.False(experience.IsAchieve);
        }

        [Fact]
        public async Task CheckCacheGetExperience()
        {
            //Arrange
            var labPreset = await TU.Preset.LabPreset.PrepareEmptyLab(context, appSetting);
            var experienceA = labPreset.Experience;
            experienceA.Sequence = 2;
            context.Update(experienceA);
            await context.SaveChangesAsync();
            context.Entry(experienceA).State = EntityState.Detached;

            var topicA = await labPreset.AddTopic(statusId: null, experienceA.Id);
            var topicB = await labPreset.AddTopic(statusId: null, experienceA.Id);

            var subTopicA = await labPreset.AddSubTopic(topicA.Id);
            var subTopicB = await labPreset.AddSubTopic(topicB.Id);

            var studentPreset = await TU.Preset.StudentPreset.PrepareEmptyStudent(context, appSetting);
            var studentLab = await studentPreset.AddLab(labPreset.Laboratory.Id);
            var studentSubTopicA = await studentPreset.AddSubTopic(studentLab.Id, subTopicA.Id);
            var instructor = await InstructorGenerator.Generate(context, appSetting);
            var studentScore = await studentPreset.AddScore(studentSubTopicA.Id, instructor.Id);

            //Act
            var cacheKey = UT.CacheKey.Student.Experience(studentPreset.Student.Id);
            var cacheBefore = UT.CacheManager.Get<List<StudentExperienceResponse>>(cacheKey);
            var experiences = (await _student.GetExperience(studentPreset.Student.Id)).Object;
            var cacheAfter = UT.CacheManager.Get<List<StudentExperienceResponse>>(cacheKey);

            //Assert
            Assert.Null(cacheBefore);
            Assert.Equal(experiences, cacheAfter);
        }

        [Fact]
        public async Task IsDoneAllSubTopic_true()
        {
            // Arrange
            var subTopicA = await SubTopicGenerator.Generate(context);
            var subTopicB = await SubTopicGenerator.Generate(context, new EN.SubTopic { StatusId = UT.Status.Inactive.Id });
            var subTopicC = await SubTopicGenerator.Generate(context);

            var student = await StudentGenerator.Generate(context, appSetting);
            var studentLaboratory = await StudentLaboratoryGenerator.Generate(context, new EN.StudentLaboratory
            {
                StudentId = student.Id
            });

            // Add subtopic A
            var studentSubTopicA = await StudentSubTopicGenerator.Generate(context, new EN.StudentSubTopic
            {
                SubTopicId = subTopicA.Id,
                StudentLaboratoryId = studentLaboratory.Id
            });

            await StudentScoreGenerator.Generate(context, appSetting, new EN.StudentScore
            {
                StudentSubTopicId = studentSubTopicA.Id,
            });


            // Add subtopic B
            var studentSubTopicB = await StudentSubTopicGenerator.Generate(context, new EN.StudentSubTopic
            {
                SubTopicId = subTopicB.Id,
                StudentLaboratoryId = studentLaboratory.Id
            });

            await StudentScoreGenerator.Generate(context, appSetting, new EN.StudentScore
            {
                StudentSubTopicId = studentSubTopicB.Id,
            });


            // Add subtopic C
            var studentSubTopicC = await StudentSubTopicGenerator.Generate(context, new EN.StudentSubTopic
            {
                SubTopicId = subTopicC.Id,
                StudentLaboratoryId = studentLaboratory.Id
            });

            await StudentScoreGenerator.Generate(context, appSetting, new EN.StudentScore
            {
                StudentSubTopicId = studentSubTopicC.Id,
            });

            // Act
            var isAllDone = await _student.IsDoneAll(student.Id);

            // Assert
            Assert.True(isAllDone);
        }

        [Fact]
        public async Task IsDoneAllSubTopic_false()
        {
            // Arrange
            var subTopicA = await SubTopicGenerator.Generate(context);
            var subTopicB = await SubTopicGenerator.Generate(context, new EN.SubTopic { StatusId = UT.Status.Inactive.Id });
            var subTopicC = await SubTopicGenerator.Generate(context);

            var student = await StudentGenerator.Generate(context, appSetting);
            var studentLaboratory = await StudentLaboratoryGenerator.Generate(context, new EN.StudentLaboratory
            {
                StudentId = student.Id
            });

            // Add subtopic A
            var studentSubTopicA = await StudentSubTopicGenerator.Generate(context, new EN.StudentSubTopic
            {
                SubTopicId = subTopicA.Id,
                StudentLaboratoryId = studentLaboratory.Id
            });

            await StudentScoreGenerator.Generate(context, appSetting, new EN.StudentScore
            {
                StudentSubTopicId = studentSubTopicA.Id,
            });


            // Add subtopic B
            var studentSubTopicB = await StudentSubTopicGenerator.Generate(context, new EN.StudentSubTopic
            {
                SubTopicId = subTopicB.Id,
                StudentLaboratoryId = studentLaboratory.Id
            });

            await StudentScoreGenerator.Generate(context, appSetting, new EN.StudentScore
            {
                StudentSubTopicId = studentSubTopicB.Id,
            });


            // Act
            var isAllDone = await _student.IsDoneAll(student.Id);

            // Assert
            Assert.False(isAllDone);
        }

        [Fact]
        public async Task IsDoneAllSubTopicHasStudentSubTopicStatusInActive()
        {
            // Arrange
            var subTopicA = await SubTopicGenerator.Generate(context);
            var subTopicC = await SubTopicGenerator.Generate(context);

            var student = await StudentGenerator.Generate(context, appSetting);
            var studentLaboratory = await StudentLaboratoryGenerator.Generate(context, new EN.StudentLaboratory
            {
                StudentId = student.Id
            });

            // Add subtopic A
            var studentSubTopicA = await StudentSubTopicGenerator.Generate(context, new EN.StudentSubTopic
            {
                SubTopicId = subTopicA.Id,
                StudentLaboratoryId = studentLaboratory.Id
            });

            await StudentScoreGenerator.Generate(context, appSetting, new EN.StudentScore
            {
                StudentSubTopicId = studentSubTopicA.Id,
            });

            // Add subtopic C_1
            var studentSubTopicC_1 = await StudentSubTopicGenerator.Generate(context, new EN.StudentSubTopic
            {
                SubTopicId = subTopicC.Id,
                StudentLaboratoryId = studentLaboratory.Id
            });

            await StudentScoreGenerator.Generate(context, appSetting, new EN.StudentScore
            {
                StudentSubTopicId = studentSubTopicC_1.Id,
                StatusId = UT.Status.Inactive.Id
            });

            // Act
            var isAllDone = await _student.IsDoneAll(student.Id);

            // Assert
            Assert.False(isAllDone);
        }

        [Fact]
        public async Task CheckCacheIsDoneAllSubTopic()
        {
            // Arrange
            var subTopicA = await SubTopicGenerator.Generate(context);
            var subTopicC = await SubTopicGenerator.Generate(context);

            var student = await StudentGenerator.Generate(context, appSetting);
            var studentLaboratory = await StudentLaboratoryGenerator.Generate(context, new EN.StudentLaboratory
            {
                StudentId = student.Id
            });

            // Add subtopic A
            var studentSubTopicA = await StudentSubTopicGenerator.Generate(context, new EN.StudentSubTopic
            {
                SubTopicId = subTopicA.Id,
                StudentLaboratoryId = studentLaboratory.Id
            });

            await StudentScoreGenerator.Generate(context, appSetting, new EN.StudentScore
            {
                StudentSubTopicId = studentSubTopicA.Id,
            });

            // Add subtopic C_1
            var studentSubTopicC_1 = await StudentSubTopicGenerator.Generate(context, new EN.StudentSubTopic
            {
                SubTopicId = subTopicC.Id,
                StudentLaboratoryId = studentLaboratory.Id
            });

            await StudentScoreGenerator.Generate(context, appSetting, new EN.StudentScore
            {
                StudentSubTopicId = studentSubTopicC_1.Id,
                StatusId = UT.Status.Inactive.Id
            });

            // Act
            var cacheKey = UT.CacheKey.Student.IsDoneAll(student.Id);
            var cacheBefore = UT.CacheManager.Get<bool?>(cacheKey);
            var isAllDone = await _student.IsDoneAll(student.Id);
            var cacheAfter = UT.CacheManager.Get<bool?>(cacheKey);

            // Assert
            Assert.Null(cacheBefore);
            Assert.Equal(isAllDone, cacheAfter);
        }

        [Fact]
        public async Task AddMinusPoint()
        {
            //Assign
            await TU.Preset.CommonPreset.Generate(context);
            var MaxMinusPointPerTime = await ConfigGenerator.Generate(context, new EN.Config
            {
                Id = 2,
                Name = "MaxMinusPointPerTime",
                Value = "10"
            });
            var course = await CourseGenerator.Generate(context);
            var instructor = await InstructorGenerator.Generate(context, appSetting);
            var student = await StudentGenerator.Generate(context, appSetting, new EN.Student
            {
                CourseId = course.Id
            });

            var presetLab = await TU.Preset.LabPreset.PrepareGoodLabWithSomeStudentScore(
                context, appSetting, course, student, new TU.Preset.LabPreset.Data()
            );

            var minusPoint = await MinusPointGenerator.Generate(context);

            var studentLab = await _laboratory.GetStudentLab(presetLab.Laboratory.Id, student.Id);
            var studentLabDetail = await _student.GetLabDetail(student.Id, presetLab.Laboratory.Id);

            var request = new MinusPointAddRequest
            {
                LaboratoryId = presetLab.Laboratory.Id,
                MinusPointId = minusPoint.Id,
                Score = faker.Random.Int(0, 10),
                Remark = faker.Name.FirstName()
            };

            // Act
            var cacheStudentLabKey = UT.CacheKey.Laboratory.Student(presetLab.Laboratory.Id, student.Id);
            var cacheStudentLabDetailKey = UT.CacheKey.Student.LaboratoryDetail(student.Id, presetLab.Laboratory.Id);

            var cacheStudentLabBefore = UT.CacheManager.Get<EN.StudentLaboratory>(cacheStudentLabKey);
            var cacheStudentLabDetailBefore = UT.CacheManager.Get<StudentLaboratoryDetailResponse>(cacheStudentLabDetailKey);
            var response = await _student.AddMinusPoint(student.Id, instructor.Id, request);
            var cacheStudentLabAfter = UT.CacheManager.Get<EN.StudentLaboratory>(cacheStudentLabKey);
            var cacheStudentLabDetailAfter = UT.CacheManager.Get<StudentLaboratoryDetailResponse>(cacheStudentLabDetailKey);
            var studentMinusPoints = await context.StudentMinusPoints.Select(s => s).AsNoTracking().ToListAsync();

            // Assert
            Assert.Equal(HttpStatusCode.Created, response.StatusCode);
            Assert.Single(studentMinusPoints);
            var studentMinustPoint = studentMinusPoints[0];

            Assert.Equal(1, studentMinustPoint.Id);
            Assert.Equal(instructor.Id, studentMinustPoint.InstructorId);
            Assert.Equal(minusPoint.Id, studentMinustPoint.MinusPointId);
            Assert.Equal(presetLab.StudentLaboratory.Id, studentMinustPoint.StudentLaboratoryId);
            Assert.Equal(UT.Status.Active.Id, studentMinustPoint.StatusId);
            Assert.Equal(request.Score, studentMinustPoint.Score);
            Assert.Equal(request.Remark, studentMinustPoint.Remark);

            Assert.NotNull(cacheStudentLabBefore);
            Assert.Equal(studentLabDetail.Object, cacheStudentLabDetailBefore);
            Assert.Null(cacheStudentLabAfter);
            Assert.Null(cacheStudentLabDetailAfter);
        }

        [Fact]
        public async Task AddMinusPointWhenStudentLabNotExistingThenShouldCreateNewStudentLab()
        {
            //Assign
            await TU.Preset.CommonPreset.Generate(context);
            var MaxMinusPointPerTime = await ConfigGenerator.Generate(context, new EN.Config
            {
                Id = 2,
                Name = "MaxMinusPointPerTime",
                Value = "10"
            });
            var course = await CourseGenerator.Generate(context);
            var instructor = await InstructorGenerator.Generate(context, appSetting);
            var student = await StudentGenerator.Generate(context, appSetting, new EN.Student
            {
                CourseId = course.Id
            });

            var presetLab = await TU.Preset.LabPreset.PrepareGoodLab(context, appSetting, new TU.Preset.LabPreset.Data
            {
                TotalTopic = 1,
                TotalSubTopic = 1,
                GoodScore = 1,
                FairScore = 1
            });

            var minusPoint = await MinusPointGenerator.Generate(context);

            var studentLab = await _laboratory.GetStudentLab(presetLab.Laboratory.Id, student.Id);
            var studentLabDetail = await _student.GetLabDetail(student.Id, presetLab.Laboratory.Id);

            var request = new MinusPointAddRequest
            {
                LaboratoryId = presetLab.Laboratory.Id,
                MinusPointId = minusPoint.Id,
                Score = faker.Random.Int(0, 10),
                Remark = faker.Name.FirstName()
            };

            // Act
            var cacheStudentLabKey = UT.CacheKey.Laboratory.Student(presetLab.Laboratory.Id, student.Id);
            var cacheStudentLabDetailKey = UT.CacheKey.Student.LaboratoryDetail(student.Id, presetLab.Laboratory.Id);

            var cacheStudentLabBefore = UT.CacheManager.Get<EN.StudentLaboratory>(cacheStudentLabKey);
            var cacheStudentLabDetailBefore = UT.CacheManager.Get<StudentLaboratoryDetailResponse>(cacheStudentLabDetailKey);
            var response = await _student.AddMinusPoint(student.Id, instructor.Id, request);
            var cacheStudentLabAfter = UT.CacheManager.Get<EN.StudentLaboratory>(cacheStudentLabKey);
            var cacheStudentLabDetailAfter = UT.CacheManager.Get<StudentLaboratoryDetailResponse>(cacheStudentLabDetailKey);
            var studentMinusPoints = await context.StudentMinusPoints.Select(s => s).AsNoTracking().ToListAsync();

            // Assert
            Assert.Equal(HttpStatusCode.Created, response.StatusCode);
            Assert.Single(studentMinusPoints);
            var studentMinustPoint = studentMinusPoints[0];

            Assert.Equal(1, studentMinustPoint.Id);
            Assert.Equal(instructor.Id, studentMinustPoint.InstructorId);
            Assert.Equal(minusPoint.Id, studentMinustPoint.MinusPointId);
            Assert.Equal(1, studentMinustPoint.StudentLaboratoryId);
            Assert.Equal(UT.Status.Active.Id, studentMinustPoint.StatusId);
            Assert.Equal(request.Score, studentMinustPoint.Score);
            Assert.Equal(request.Remark, studentMinustPoint.Remark);

            Assert.Null(cacheStudentLabBefore);
            Assert.Equal(studentLabDetail.Object, cacheStudentLabDetailBefore);
            Assert.Null(cacheStudentLabAfter);
            Assert.Null(cacheStudentLabDetailAfter);
        }

        [Fact]
        public async Task SelfAssessmentCreate()
        {
            // Arrange
            await TU.Preset.CommonPreset.Generate(context);

            var course = await CourseGenerator.Generate(context);
            var instructor = await InstructorGenerator.Generate(context, appSetting);
            var student = await StudentGenerator.Generate(context, appSetting, new EN.Student
            {
                CourseId = course.Id
            });

            var labPreset = await TU.Preset.LabPreset.PrepareGoodLab(context, appSetting, new TU.Preset.LabPreset.Data
            {
                TotalTopic = 1,
                TotalSubTopic = 1,
                GoodScore = 1,
                FairScore = 1
            });

            var studentLaboratory = await StudentLaboratoryGenerator.Generate(context, new EN.StudentLaboratory
            {
                LaboratoryId = labPreset.Laboratory.Id,
                StudentId = student.Id
            });

            var request = new SelfAssessmentRequest
            {
                LaboratoryId = labPreset.Laboratory.Id,
                SelfAssessment = faker.Random.Int(0, 10),
                SubTopicId = labPreset.SubTopics[0].Id
            };

            // Act
            var result = await _student.CreateOrUpdateSelfAssessment(student.Id, request);
            var studentSubTopics = await context.StudentSubTopics.Select(ss => ss).AsNoTracking().ToListAsync();

            // Assert
            Assert.Equal(HttpStatusCode.Created, result.StatusCode);
            Assert.Single(studentSubTopics);

            var studentSubTopic = studentSubTopics[0];
            Assert.Equal(1, studentSubTopic.Id);
            Assert.Equal(studentLaboratory.Id, studentSubTopic.StudentLaboratoryId);
            Assert.Equal(labPreset.SubTopics[0].Id, studentSubTopic.SubTopicId);
            Assert.Equal(UT.Status.Active.Id, studentSubTopic.StatusId);
            Assert.Equal(request.SelfAssessment, studentSubTopic.SelfAssessment);
        }

        [Fact]
        public async Task SelfAssessmentUpdate()
        {
            // Arrange
            await TU.Preset.CommonPreset.Generate(context);

            var course = await CourseGenerator.Generate(context);
            var instructor = await InstructorGenerator.Generate(context, appSetting);
            var student = await StudentGenerator.Generate(context, appSetting, new EN.Student
            {
                CourseId = course.Id
            });

            var labPreset = await TU.Preset.LabPreset.PrepareGoodLab(context, appSetting, new TU.Preset.LabPreset.Data
            {
                TotalTopic = 1,
                TotalSubTopic = 1,
                GoodScore = 1,
                FairScore = 1
            });

            var studentLaboratory = await StudentLaboratoryGenerator.Generate(context, new EN.StudentLaboratory
            {
                LaboratoryId = labPreset.Laboratory.Id,
                StudentId = student.Id
            });

            var studentSubTopic = await StudentSubTopicGenerator.Generate(context, new EN.StudentSubTopic
            {
                StudentLaboratoryId = studentLaboratory.Id,
                SelfAssessment = 5,
                SubTopicId = labPreset.SubTopics[0].Id,
                StatusId = UT.Status.Active.Id,
            });

            var request = new SelfAssessmentRequest
            {
                LaboratoryId = labPreset.Laboratory.Id,
                SelfAssessment = 7,
                SubTopicId = labPreset.SubTopics[0].Id
            };

            // Act
            var result = await _student.CreateOrUpdateSelfAssessment(student.Id, request);
            var studentSubTopicDbs = await context.StudentSubTopics.Select(ss => ss).AsNoTracking().ToListAsync();

            // Assert
            Assert.Equal(HttpStatusCode.OK, result.StatusCode);
            Assert.Single(studentSubTopicDbs);

            var studentSubTopicDb = studentSubTopicDbs[0];
            Assert.Equal(studentSubTopic.Id, studentSubTopicDb.Id);
            Assert.Equal(studentSubTopic.StudentLaboratoryId, studentSubTopicDb.StudentLaboratoryId);
            Assert.Equal(studentSubTopic.SubTopicId, studentSubTopicDb.SubTopicId);
            Assert.Equal(studentSubTopic.StatusId, studentSubTopicDb.StatusId);
            Assert.Equal(request.SelfAssessment, studentSubTopicDb.SelfAssessment);
        }

        [Fact]
        public async Task SelfAssessmentCreateIfStudentLabNotExistingThenCreateStudentLabToo()
        {
            // Arrange
            await TU.Preset.CommonPreset.Generate(context);

            var course = await CourseGenerator.Generate(context);
            var instructor = await InstructorGenerator.Generate(context, appSetting);
            var student = await StudentGenerator.Generate(context, appSetting, new EN.Student
            {
                CourseId = course.Id
            });

            var labPreset = await TU.Preset.LabPreset.PrepareGoodLab(context, appSetting, new TU.Preset.LabPreset.Data
            {
                TotalTopic = 1,
                TotalSubTopic = 1,
                GoodScore = 1,
                FairScore = 1
            });

            var request = new SelfAssessmentRequest
            {
                LaboratoryId = labPreset.Laboratory.Id,
                SelfAssessment = faker.Random.Int(0, 10),
                SubTopicId = labPreset.SubTopics[0].Id
            };

            // Act
            var result = await _student.CreateOrUpdateSelfAssessment(student.Id, request);
            var studentLaboratories = await context.StudentLaboratories.Select(sl => sl).AsNoTracking().ToListAsync();
            var studentSubTopics = await context.StudentSubTopics.Select(ss => ss).AsNoTracking().ToListAsync();

            // Assert
            Assert.Equal(HttpStatusCode.Created, result.StatusCode);
            Assert.Single(studentLaboratories);
            Assert.Single(studentSubTopics);

            var studentLaboratory = studentLaboratories[0];
            Assert.Equal(1, studentLaboratory.Id);
            Assert.Equal(labPreset.Laboratory.Id, studentLaboratory.LaboratoryId);
            Assert.Equal(student.Id, studentLaboratory.StudentId);
            Assert.Equal(UT.Status.Active.Id, studentLaboratory.StatusId);

            var studentSubTopic = studentSubTopics[0];
            Assert.Equal(1, studentSubTopic.Id);
            Assert.Equal(studentLaboratory.Id, studentSubTopic.StudentLaboratoryId);
            Assert.Equal(labPreset.SubTopics[0].Id, studentSubTopic.SubTopicId);
            Assert.Equal(UT.Status.Active.Id, studentSubTopic.StatusId);
            Assert.Equal(request.SelfAssessment, studentSubTopic.SelfAssessment);
        }

        [Fact]
        public async Task SelfAssessmentCreateOrUpdateWhenStudentNotExisting_NotFound()
        {
            // Arrange
            await TU.Preset.CommonPreset.Generate(context);

            var course = await CourseGenerator.Generate(context);

            var labPreset = await TU.Preset.LabPreset.PrepareGoodLab(context, appSetting, new TU.Preset.LabPreset.Data
            {
                TotalTopic = 1,
                TotalSubTopic = 1,
                GoodScore = 1,
                FairScore = 1
            });

            var request = new SelfAssessmentRequest
            {
                LaboratoryId = labPreset.Laboratory.Id,
                SelfAssessment = faker.Random.Int(0, 10),
                SubTopicId = labPreset.SubTopics[0].Id
            };

            // Act
            var result = await _student.CreateOrUpdateSelfAssessment(100, request);

            // Assert
            Assert.Equal(HttpStatusCode.NotFound, result.StatusCode);
        }

        [Fact]
        public async Task SelfAssessmentCreateOrUpdateWhenLaboratoryNotExisting_NotFound()
        {
            // Arrange
            await TU.Preset.CommonPreset.Generate(context);

            var course = await CourseGenerator.Generate(context);
            var instructor = await InstructorGenerator.Generate(context, appSetting);
            var student = await StudentGenerator.Generate(context, appSetting, new EN.Student
            {
                CourseId = course.Id
            });

            var request = new SelfAssessmentRequest
            {
                LaboratoryId = 1000,
                SelfAssessment = faker.Random.Int(0, 10),
                SubTopicId = 1000
            };

            // Act
            var result = await _student.CreateOrUpdateSelfAssessment(student.Id, request);

            // Assert
            Assert.Equal(HttpStatusCode.NotFound, result.StatusCode);
        }

        [Fact]
        public async Task SelfAssessmentCreateOrUpdateWhenSubTopicNotExisting_NotFound()
        {
            // Arrange
            await TU.Preset.CommonPreset.Generate(context);

            var course = await CourseGenerator.Generate(context);
            var instructor = await InstructorGenerator.Generate(context, appSetting);
            var student = await StudentGenerator.Generate(context, appSetting, new EN.Student
            {
                CourseId = course.Id
            });

            var laboratory = await LaboratoryGenerator.Generate(context, new EN.Laboratory
            {
                EndDate = DateTime.Now.AddDays(1)
            });
                
            var studentLaboratory = await StudentLaboratoryGenerator.Generate(context, new EN.StudentLaboratory
            {
                LaboratoryId = laboratory.Id,
                StudentId = student.Id
            });

            var request = new SelfAssessmentRequest
            {
                LaboratoryId = laboratory.Id,
                SelfAssessment = faker.Random.Int(0, 10),
                SubTopicId = 1000
            };

            // Act
            var result = await _student.CreateOrUpdateSelfAssessment(student.Id, request);

            // Assert
            Assert.Equal(HttpStatusCode.NotFound, result.StatusCode);
        }

        [Fact]
        public async Task SelfAssessmentCreateOrUpdateSuccessThenClearCache()
        {
            // Arrange
            await TU.Preset.CommonPreset.Generate(context);

            var course = await CourseGenerator.Generate(context);
            var instructor = await InstructorGenerator.Generate(context, appSetting);
            var student = await StudentGenerator.Generate(context, appSetting, new EN.Student
            {
                CourseId = course.Id
            });

            var labPreset = await TU.Preset.LabPreset.PrepareGoodLab(context, appSetting, new TU.Preset.LabPreset.Data
            {
                TotalTopic = 1,
                TotalSubTopic = 1,
                GoodScore = 1,
                FairScore = 1
            });

            var studentLaboratory = await StudentLaboratoryGenerator.Generate(context, new EN.StudentLaboratory
            {
                LaboratoryId = labPreset.Laboratory.Id,
                StudentId = student.Id
            });

            var request = new SelfAssessmentRequest
            {
                LaboratoryId = labPreset.Laboratory.Id,
                SelfAssessment = faker.Random.Int(0, 10),
                SubTopicId = labPreset.SubTopics[0].Id
            };

            var studentLab = await _laboratory.GetStudentLab(labPreset.Laboratory.Id, student.Id);
            var studentLabDetail = await _student.GetLabDetail(student.Id, labPreset.Laboratory.Id);

            // Act
            var cacheStudentLabKey = UT.CacheKey.Laboratory.Student(labPreset.Laboratory.Id, student.Id);
            var cacheStudentLabDetailKey = UT.CacheKey.Student.LaboratoryDetail(student.Id, labPreset.Laboratory.Id);

            var cacheStudentLabBefore = UT.CacheManager.Get<EN.StudentLaboratory>(cacheStudentLabKey);
            var cacheStudentLabDetailBefore = UT.CacheManager.Get<StudentLaboratoryDetailResponse>(cacheStudentLabDetailKey);
            var result = await _student.CreateOrUpdateSelfAssessment(student.Id, request);
            var cacheStudentLabAfter = UT.CacheManager.Get<EN.StudentLaboratory>(cacheStudentLabKey);
            var cacheStudentLabDetailAfter = UT.CacheManager.Get<StudentLaboratoryDetailResponse>(cacheStudentLabDetailKey);

            // Assert
            Assert.Equal(HttpStatusCode.Created, result.StatusCode);

            Assert.NotNull(cacheStudentLabBefore);
            Assert.Equal(studentLabDetail.Object, cacheStudentLabDetailBefore);
            Assert.Null(cacheStudentLabAfter);
            Assert.Null(cacheStudentLabDetailAfter);
        }

        [Fact]
        public async Task LabScopeCreate()
        {           
            // Arrange
            await TU.Preset.CommonPreset.Generate(context);

            var course = await CourseGenerator.Generate(context);
            var instructor = await InstructorGenerator.Generate(context, appSetting);
            var student = await StudentGenerator.Generate(context, appSetting, new EN.Student
            {
                CourseId = course.Id
            });

            var labPreset = await TU.Preset.LabPreset.PrepareGoodLab(context, appSetting, new TU.Preset.LabPreset.Data
            {
                TotalTopic = 1,
                TotalSubTopic = 1,
                GoodScore = 1,
                FairScore = 1
            });

            var request = new LabScopeRequest
            {
                LaboratoryId = labPreset.Laboratory.Id,
                KindOfWork = faker.Name.FirstName(),
                Material = faker.Name.FirstName(),
                Tooth = faker.Name.FirstName(),
            };

            // Act
            var result = await _student.CreateOrUpdateLabScope(student.Id, request);
            var studentLabs = await context.StudentLaboratories.Select(sl => sl).AsNoTracking().ToListAsync();

            // Assert
            Assert.Equal(HttpStatusCode.Created, result.StatusCode);
            Assert.Single(studentLabs);

            var studentLab = studentLabs[0];
            Assert.Equal(1, studentLab.Id);
            Assert.Equal(student.Id, studentLab.StudentId);
            Assert.Equal(labPreset.Laboratory.Id, studentLab.LaboratoryId);
            Assert.Equal(UT.Status.Active.Id, studentLab.StatusId);
            Assert.Equal(request.KindOfWork, studentLab.KindOfWork);
            Assert.Equal(request.Material, studentLab.Material);
            Assert.Equal(request.Tooth, studentLab.Tooth);
        }

        [Fact]
        public async Task LabScopeUpdate()
        {
            // Arrange
            await TU.Preset.CommonPreset.Generate(context);

            var course = await CourseGenerator.Generate(context);
            var instructor = await InstructorGenerator.Generate(context, appSetting);
            var student = await StudentGenerator.Generate(context, appSetting, new EN.Student
            {
                CourseId = course.Id
            });

            var labPreset = await TU.Preset.LabPreset.PrepareGoodLab(context, appSetting, new TU.Preset.LabPreset.Data
            {
                TotalTopic = 1,
                TotalSubTopic = 1,
                GoodScore = 1,
                FairScore = 1
            });

            var studentLaboratory = await StudentLaboratoryGenerator.Generate(context, new EN.StudentLaboratory
            {
                LaboratoryId = labPreset.Laboratory.Id,
                StudentId = student.Id
            });

            var request = new LabScopeRequest
            {
                LaboratoryId = labPreset.Laboratory.Id,
                KindOfWork = faker.Name.FirstName(),
                Material = faker.Name.FirstName(),
                Tooth = faker.Name.FirstName(),
            };

            // Act
            var result = await _student.CreateOrUpdateLabScope(student.Id, request);
            var studentLabs = await context.StudentLaboratories.Select(sl => sl).AsNoTracking().ToListAsync();

            // Assert
            Assert.Equal(HttpStatusCode.OK, result.StatusCode);
            Assert.Single(studentLabs);

            var studentLab = studentLabs[0];
            Assert.Equal(studentLaboratory.Id, studentLab.Id);
            Assert.Equal(studentLaboratory.StudentId, studentLab.StudentId);
            Assert.Equal(studentLaboratory.LaboratoryId, studentLab.LaboratoryId);
            Assert.Equal(studentLaboratory.StatusId, studentLab.StatusId);
            Assert.Equal(request.KindOfWork, studentLab.KindOfWork);
            Assert.Equal(request.Material, studentLab.Material);
            Assert.Equal(request.Tooth, studentLab.Tooth);
        }

        [Fact]
        public async Task LabScopeCreateOrUpdateWhenStudentNotExisting_NotFound()
        {
            // Arrange
            await TU.Preset.CommonPreset.Generate(context);

            var course = await CourseGenerator.Generate(context);
            var instructor = await InstructorGenerator.Generate(context, appSetting);

            var labPreset = await TU.Preset.LabPreset.PrepareGoodLab(context, appSetting, new TU.Preset.LabPreset.Data
            {
                TotalTopic = 1,
                TotalSubTopic = 1,
                GoodScore = 1,
                FairScore = 1
            });

            var request = new LabScopeRequest
            {
                LaboratoryId = labPreset.Laboratory.Id,
                KindOfWork = faker.Name.FirstName(),
                Material = faker.Name.FirstName(),
                Tooth = faker.Name.FirstName(),
            };

            // Act
            var result = await _student.CreateOrUpdateLabScope(1000, request);

            // Assert
            Assert.Equal(HttpStatusCode.NotFound, result.StatusCode);
        }

        [Fact]
        public async Task LabScopeCreateOrUpdateWhenLabNotExising_NotFound()
        {
            // Arrange
            await TU.Preset.CommonPreset.Generate(context);

            var course = await CourseGenerator.Generate(context);
            var instructor = await InstructorGenerator.Generate(context, appSetting);
            var student = await StudentGenerator.Generate(context, appSetting, new EN.Student
            {
                CourseId = course.Id
            });

            var request = new LabScopeRequest
            {
                LaboratoryId = 1000,
                KindOfWork = faker.Name.FirstName(),
                Material = faker.Name.FirstName(),
                Tooth = faker.Name.FirstName(),
            };

            // Act
            var result = await _student.CreateOrUpdateLabScope(student.Id, request);

            // Assert
            Assert.Equal(HttpStatusCode.NotFound, result.StatusCode);
        }

        [Fact]
        public async Task LabScopeCreateOrUpdateSuccessThenClearCache()
        {
            // Arrange
            await TU.Preset.CommonPreset.Generate(context);

            var course = await CourseGenerator.Generate(context);
            var instructor = await InstructorGenerator.Generate(context, appSetting);
            var student = await StudentGenerator.Generate(context, appSetting, new EN.Student
            {
                CourseId = course.Id
            });

            var labPreset = await TU.Preset.LabPreset.PrepareGoodLab(context, appSetting, new TU.Preset.LabPreset.Data
            {
                TotalTopic = 1,
                TotalSubTopic = 1,
                GoodScore = 1,
                FairScore = 1
            });

            var studentLaboratory = await StudentLaboratoryGenerator.Generate(context, new EN.StudentLaboratory
            {
                LaboratoryId = labPreset.Laboratory.Id,
                StudentId = student.Id
            });

            var request = new LabScopeRequest
            {
                LaboratoryId = labPreset.Laboratory.Id,
                KindOfWork = faker.Name.FirstName(),
                Material = faker.Name.FirstName(),
                Tooth = faker.Name.FirstName(),
            };

            var studentLab = await _laboratory.GetStudentLab(labPreset.Laboratory.Id, student.Id);
            var studentLabDetail = await _student.GetLabDetail(student.Id, labPreset.Laboratory.Id);

            // Act
            var cacheStudentLabKey = UT.CacheKey.Laboratory.Student(labPreset.Laboratory.Id, student.Id);
            var cacheStudentLabDetailKey = UT.CacheKey.Student.LaboratoryDetail(student.Id, labPreset.Laboratory.Id);

            var cacheStudentLabBefore = UT.CacheManager.Get<EN.StudentLaboratory>(cacheStudentLabKey);
            var cacheStudentLabDetailBefore = UT.CacheManager.Get<StudentLaboratoryDetailResponse>(cacheStudentLabDetailKey);
            var result = await _student.CreateOrUpdateLabScope(student.Id, request);
            var cacheStudentLabAfter = UT.CacheManager.Get<EN.StudentLaboratory>(cacheStudentLabKey);
            var cacheStudentLabDetailAfter = UT.CacheManager.Get<StudentLaboratoryDetailResponse>(cacheStudentLabDetailKey);

            // Assert
            Assert.Equal(HttpStatusCode.OK, result.StatusCode);

            Assert.NotNull(cacheStudentLabBefore);
            Assert.Equal(studentLabDetail.Object, cacheStudentLabDetailBefore);
            Assert.Null(cacheStudentLabAfter);
            Assert.Null(cacheStudentLabDetailAfter);
        }

        [Fact]
        public async Task SubmitScore()
        {
            // Arrange
            await TU.Preset.CommonPreset.Generate(context);

            var course = await CourseGenerator.Generate(context);
            var instructor = await InstructorGenerator.Generate(context, appSetting);
            var student = await StudentGenerator.Generate(context, appSetting, new EN.Student
            {
                CourseId = course.Id
            });

            var labPreset = await TU.Preset.LabPreset.PrepareGoodLab(context, appSetting, new TU.Preset.LabPreset.Data
            {
                TotalTopic = 1,
                TotalSubTopic = 1,
                GoodScore = 10,
                FairScore = 5
            });

            var studentLaboratory = await StudentLaboratoryGenerator.Generate(context, new EN.StudentLaboratory
            {
                LaboratoryId = labPreset.Laboratory.Id,
                StudentId = student.Id
            });

            var studentSubTopic = await StudentSubTopicGenerator.Generate(context, new EN.StudentSubTopic
            {
                StudentLaboratoryId = studentLaboratory.Id,
                SubTopicId = labPreset.SubTopics[0].Id,
                StatusId = UT.Status.Active.Id,
            });

            var request = new SubmitScoreRequest
            {
                LaboratoryId = labPreset.Laboratory.Id,
                SubTopicId = labPreset.SubTopics[0].Id,
                CriteriaId = labPreset.Criterias[1].Id,
                Remark = faker.Name.FirstName(),
            };

            // Act
            var response = await _student.SubmitScore(student.Id, instructor.Id, request);
            var studentLabs = await context.StudentLaboratories.Select(sl => sl).AsNoTracking().ToArrayAsync();
            var studentSubTopics = await context.StudentSubTopics.Select(ss => ss).AsNoTracking().ToArrayAsync();
            var studentScores = await context.StudentScores.Select(ss => ss).AsNoTracking().ToListAsync();

            // Assert
            Assert.Equal(HttpStatusCode.Created, response.StatusCode);
            Assert.Single(studentLabs);
            Assert.Single(studentSubTopics);
            Assert.Single(studentScores);

            var studentScore = studentScores[0];
            Assert.Equal(1, studentScore.Id);
            Assert.Equal(instructor.Id, studentScore.InstructorId);
            Assert.Equal(studentSubTopic.Id, studentScore.StudentSubTopicId);
            Assert.Equal(labPreset.Criterias[1].Id, studentScore.CriteriaId);
            Assert.False(studentScore.IsCritical);
            Assert.Equal(labPreset.Criterias[1].Score, studentScore.Score);
            Assert.Equal(UT.Status.Active.Id, studentScore.StatusId);
        }

        [Fact]
        public async Task SubmitScoreWhenStudentSubTopicNotExistingThenCreateStudentSubTopicToo()
        {
            // Arrange
            await TU.Preset.CommonPreset.Generate(context);

            var course = await CourseGenerator.Generate(context);
            var instructor = await InstructorGenerator.Generate(context, appSetting);
            var student = await StudentGenerator.Generate(context, appSetting, new EN.Student
            {
                CourseId = course.Id
            });

            var labPreset = await TU.Preset.LabPreset.PrepareGoodLab(context, appSetting, new TU.Preset.LabPreset.Data
            {
                TotalTopic = 1,
                TotalSubTopic = 1,
                GoodScore = 10,
                FairScore = 5
            });

            var studentLaboratory = await StudentLaboratoryGenerator.Generate(context, new EN.StudentLaboratory
            {
                LaboratoryId = labPreset.Laboratory.Id,
                StudentId = student.Id
            });

            var request = new SubmitScoreRequest
            {
                LaboratoryId = labPreset.Laboratory.Id,
                SubTopicId = labPreset.SubTopics[0].Id,
                CriteriaId = labPreset.Criterias[1].Id,
                Remark = faker.Name.FirstName(),
            };

            // Act
            var response = await _student.SubmitScore(student.Id, instructor.Id, request);
            var studentLabs = await context.StudentLaboratories.Select(sl => sl).AsNoTracking().ToArrayAsync();
            var studentSubTopics = await context.StudentSubTopics.Select(ss => ss).AsNoTracking().ToArrayAsync();
            var studentScores = await context.StudentScores.Select(ss => ss).AsNoTracking().ToListAsync();

            // Assert
            Assert.Equal(HttpStatusCode.Created, response.StatusCode);
            Assert.Single(studentLabs);
            Assert.Single(studentSubTopics);

            var studentSubTopic = studentSubTopics[0];
            Assert.Equal(1, studentSubTopic.Id);
            Assert.Equal(studentLaboratory.Id, studentSubTopic.StudentLaboratoryId);
            Assert.Equal(labPreset.SubTopics[0].Id, studentSubTopic.SubTopicId);
            Assert.Equal(UT.Status.Active.Id, studentSubTopic.StatusId);
            
            Assert.Single(studentScores);

            var studentScore = studentScores[0];
            Assert.Equal(1, studentScore.Id);
            Assert.Equal(instructor.Id, studentScore.InstructorId);
            Assert.Equal(studentSubTopic.Id, studentScore.StudentSubTopicId);
            Assert.Equal(labPreset.Criterias[1].Id, studentScore.CriteriaId);
            Assert.False(studentScore.IsCritical);
            Assert.Equal(labPreset.Criterias[1].Score, studentScore.Score);
            Assert.Equal(UT.Status.Active.Id, studentScore.StatusId);
        }

        [Fact]
        public async Task SubmitScoreWhenStudentLabNotExistingThenCreateStudentLabAndSubTopic()
        {
            // Arrange
            await TU.Preset.CommonPreset.Generate(context);

            var course = await CourseGenerator.Generate(context);
            var instructor = await InstructorGenerator.Generate(context, appSetting);
            var student = await StudentGenerator.Generate(context, appSetting, new EN.Student
            {
                CourseId = course.Id
            });

            var labPreset = await TU.Preset.LabPreset.PrepareGoodLab(context, appSetting, new TU.Preset.LabPreset.Data
            {
                TotalTopic = 1,
                TotalSubTopic = 1,
                GoodScore = 10,
                FairScore = 5
            });

            var request = new SubmitScoreRequest
            {
                LaboratoryId = labPreset.Laboratory.Id,
                SubTopicId = labPreset.SubTopics[0].Id,
                CriteriaId = labPreset.Criterias[1].Id,
                Remark = faker.Name.FirstName(),
            };

            // Act
            var response = await _student.SubmitScore(student.Id, instructor.Id, request);
            var studentLabs = await context.StudentLaboratories.Select(sl => sl).AsNoTracking().ToArrayAsync();
            var studentSubTopics = await context.StudentSubTopics.Select(ss => ss).AsNoTracking().ToArrayAsync();
            var studentScores = await context.StudentScores.Select(ss => ss).AsNoTracking().ToListAsync();

            // Assert
            Assert.Equal(HttpStatusCode.Created, response.StatusCode);
            Assert.Single(studentLabs);

            var studentLaboratory = studentLabs[0];
            Assert.Equal(1, studentLaboratory.Id);
            Assert.Equal(labPreset.Laboratory.Id, studentLaboratory.LaboratoryId);
            Assert.Equal(student.Id, studentLaboratory.StudentId);
            Assert.Equal(UT.Status.Active.Id, studentLaboratory.StatusId);            

            Assert.Single(studentSubTopics);

            var studentSubTopic = studentSubTopics[0];
            Assert.Equal(1, studentSubTopic.Id);
            Assert.Equal(studentLaboratory.Id, studentSubTopic.StudentLaboratoryId);
            Assert.Equal(labPreset.SubTopics[0].Id, studentSubTopic.SubTopicId);
            Assert.Equal(UT.Status.Active.Id, studentSubTopic.StatusId);

            Assert.Single(studentScores);

            var studentScore = studentScores[0];
            Assert.Equal(1, studentScore.Id);
            Assert.Equal(instructor.Id, studentScore.InstructorId);
            Assert.Equal(studentSubTopic.Id, studentScore.StudentSubTopicId);
            Assert.Equal(labPreset.Criterias[1].Id, studentScore.CriteriaId);
            Assert.False(studentScore.IsCritical);
            Assert.Equal(labPreset.Criterias[1].Score, studentScore.Score);
            Assert.Equal(UT.Status.Active.Id, studentScore.StatusId);
        }

        [Fact]
        public async Task SubmitScoreWhenStudentNotExisting_NotFound()
        {
            // Arrange
            await TU.Preset.CommonPreset.Generate(context);

            var course = await CourseGenerator.Generate(context);
            var instructor = await InstructorGenerator.Generate(context, appSetting);
            var student = await StudentGenerator.Generate(context, appSetting, new EN.Student
            {
                CourseId = course.Id
            });

            var labPreset = await TU.Preset.LabPreset.PrepareGoodLab(context, appSetting, new TU.Preset.LabPreset.Data
            {
                TotalTopic = 1,
                TotalSubTopic = 1,
                GoodScore = 10,
                FairScore = 5
            });

            var request = new SubmitScoreRequest
            {
                LaboratoryId = labPreset.Laboratory.Id,
                SubTopicId = labPreset.SubTopics[0].Id,
                CriteriaId = labPreset.Criterias[1].Id,
                Remark = faker.Name.FirstName(),
            };

            // Act
            var response = await _student.SubmitScore(100, instructor.Id, request);
      
            // Assert
            Assert.Equal(HttpStatusCode.NotFound, response.StatusCode);
        }

        [Fact]
        public async Task SubmitScoreWhenInstructorNotExisting_NotFound()
        {
            // Arrange
            await TU.Preset.CommonPreset.Generate(context);

            var course = await CourseGenerator.Generate(context);
            var instructor = await InstructorGenerator.Generate(context, appSetting);
            var student = await StudentGenerator.Generate(context, appSetting, new EN.Student
            {
                CourseId = course.Id
            });

            var labPreset = await TU.Preset.LabPreset.PrepareGoodLab(context, appSetting, new TU.Preset.LabPreset.Data
            {
                TotalTopic = 1,
                TotalSubTopic = 1,
                GoodScore = 10,
                FairScore = 5
            });

            var request = new SubmitScoreRequest
            {
                LaboratoryId = labPreset.Laboratory.Id,
                SubTopicId = labPreset.SubTopics[0].Id,
                CriteriaId = labPreset.Criterias[1].Id,
                Remark = faker.Name.FirstName(),
            };

            // Act
            var response = await _student.SubmitScore(student.Id, 100, request);

            // Assert
            Assert.Equal(HttpStatusCode.NotFound, response.StatusCode);
        }

        [Fact]
        public async Task SubmitScoreWhenLaboratoryNotExisting_NotFound()
        {
            // Arrange
            await TU.Preset.CommonPreset.Generate(context);

            var course = await CourseGenerator.Generate(context);
            var instructor = await InstructorGenerator.Generate(context, appSetting);
            var student = await StudentGenerator.Generate(context, appSetting, new EN.Student
            {
                CourseId = course.Id
            });

            var labPreset = await TU.Preset.LabPreset.PrepareGoodLab(context, appSetting, new TU.Preset.LabPreset.Data
            {
                TotalTopic = 1,
                TotalSubTopic = 1,
                GoodScore = 10,
                FairScore = 5
            });

            var request = new SubmitScoreRequest
            {
                LaboratoryId = 100,
                SubTopicId = labPreset.SubTopics[0].Id,
                CriteriaId = labPreset.Criterias[1].Id,
                Remark = faker.Name.FirstName(),
            };

            // Act
            var response = await _student.SubmitScore(student.Id, instructor.Id, request);

            // Assert
            Assert.Equal(HttpStatusCode.NotFound, response.StatusCode);
        }

        [Fact]
        public async Task SubmitScoreWhenSubTopicNotBelongToLab_NotFound()
        {
            // Arrange
            await TU.Preset.CommonPreset.Generate(context);

            var course = await CourseGenerator.Generate(context);
            var instructor = await InstructorGenerator.Generate(context, appSetting);
            var student = await StudentGenerator.Generate(context, appSetting, new EN.Student
            {
                CourseId = course.Id
            });

            var labPreset = await TU.Preset.LabPreset.PrepareGoodLab(context, appSetting, new TU.Preset.LabPreset.Data
            {
                TotalTopic = 1,
                TotalSubTopic = 1,
                GoodScore = 10,
                FairScore = 5
            });

            var request = new SubmitScoreRequest
            {
                LaboratoryId = labPreset.Laboratory.Id,
                SubTopicId = 100,
                CriteriaId = labPreset.Criterias[1].Id,
                Remark = faker.Name.FirstName(),
            };

            // Act
            var response = await _student.SubmitScore(student.Id, instructor.Id, request);

            // Assert
            Assert.Equal(HttpStatusCode.NotFound, response.StatusCode);
        }

        [Fact]
        public async Task SubmitScoreWhenCriteriaNotBelongToSubTopicAndLab_NotFound()
        {
            // Arrange
            await TU.Preset.CommonPreset.Generate(context);

            var course = await CourseGenerator.Generate(context);
            var instructor = await InstructorGenerator.Generate(context, appSetting);
            var student = await StudentGenerator.Generate(context, appSetting, new EN.Student
            {
                CourseId = course.Id
            });

            var labPreset = await TU.Preset.LabPreset.PrepareGoodLab(context, appSetting, new TU.Preset.LabPreset.Data
            {
                TotalTopic = 1,
                TotalSubTopic = 1,
                GoodScore = 10,
                FairScore = 5
            });

            var request = new SubmitScoreRequest
            {
                LaboratoryId = labPreset.Laboratory.Id,
                SubTopicId = labPreset.SubTopics[0].Id,
                CriteriaId = 100,
                Remark = faker.Name.FirstName(),
            };

            // Act
            var response = await _student.SubmitScore(student.Id, instructor.Id, request);

            // Assert
            Assert.Equal(HttpStatusCode.NotFound, response.StatusCode);
        }

        [Fact]
        public async Task SubmitScoreSuccessThenClearCache()
        {
            // Arrange
            await TU.Preset.CommonPreset.Generate(context);

            var course = await CourseGenerator.Generate(context);
            var instructor = await InstructorGenerator.Generate(context, appSetting);
            var student = await StudentGenerator.Generate(context, appSetting, new EN.Student
            {
                CourseId = course.Id
            });

            var labPreset = await TU.Preset.LabPreset.PrepareGoodLab(context, appSetting, new TU.Preset.LabPreset.Data
            {
                TotalTopic = 1,
                TotalSubTopic = 1,
                GoodScore = 10,
                FairScore = 5
            });

            var studentLaboratory = await StudentLaboratoryGenerator.Generate(context, new EN.StudentLaboratory
            {
                LaboratoryId = labPreset.Laboratory.Id,
                StudentId = student.Id
            });

            var studentSubTopic = await StudentSubTopicGenerator.Generate(context, new EN.StudentSubTopic
            {
                StudentLaboratoryId = studentLaboratory.Id,
                SubTopicId = labPreset.SubTopics[0].Id,
                StatusId = UT.Status.Active.Id,
            });
            var request = new SubmitScoreRequest
            {
                LaboratoryId = labPreset.Laboratory.Id,
                SubTopicId = labPreset.SubTopics[0].Id,
                CriteriaId = labPreset.Criterias[1].Id,
                Remark = faker.Name.FirstName(),
            };

            var studentLab = await _laboratory.GetStudentLab(labPreset.Laboratory.Id, student.Id);
            var studentLabDetail = await _student.GetLabDetail(student.Id, labPreset.Laboratory.Id);

            // Act
            var cacheStudentLabKey = UT.CacheKey.Laboratory.Student(labPreset.Laboratory.Id, student.Id);
            var cacheStudentLabDetailKey = UT.CacheKey.Student.LaboratoryDetail(student.Id, labPreset.Laboratory.Id);
            var cacheExperienceKey = UT.CacheKey.Student.Experience(student.Id);

            UT.CacheManager.Set(cacheExperienceKey, new List<StudentExperienceResponse>());

            var cacheStudentLabBefore = UT.CacheManager.Get<EN.StudentLaboratory>(cacheStudentLabKey);
            var cacheStudentLabDetailBefore = UT.CacheManager.Get<StudentLaboratoryDetailResponse>(cacheStudentLabDetailKey);
            var cacheExperienceBefore = UT.CacheManager.Get<List<StudentExperienceResponse>>(cacheExperienceKey);
            var result = await _student.SubmitScore(student.Id, instructor.Id, request);
            var cacheStudentLabAfter = UT.CacheManager.Get<EN.StudentLaboratory>(cacheStudentLabKey);
            var cacheStudentLabDetailAfter = UT.CacheManager.Get<StudentLaboratoryDetailResponse>(cacheStudentLabDetailKey);
            var cacheExperienceAfter = UT.CacheManager.Get<List<StudentExperienceResponse>>(cacheExperienceKey);


            // Assert
            Assert.Equal(HttpStatusCode.Created, result.StatusCode);

            Assert.NotNull(cacheStudentLabBefore);
            Assert.Equal(studentLabDetail.Object, cacheStudentLabDetailBefore);
            Assert.NotNull(cacheExperienceBefore);
            Assert.Null(cacheStudentLabAfter);
            Assert.Null(cacheStudentLabDetailAfter);
            Assert.Null(cacheExperienceAfter);
        }

        [Fact]
        public async Task SubmitScoreAndAlreadyExistingAndItIsCritical_ReturnConflict()
        {
            // Arrange
            await TU.Preset.CommonPreset.Generate(context);

            var course = await CourseGenerator.Generate(context);
            var instructor = await InstructorGenerator.Generate(context, appSetting);
            var student = await StudentGenerator.Generate(context, appSetting, new EN.Student
            {
                CourseId = course.Id
            });

            var labPreset = await TU.Preset.LabPreset.PrepareGoodLab(context, appSetting, new TU.Preset.LabPreset.Data
            {
                TotalTopic = 1,
                TotalSubTopic = 1,
                GoodScore = 10,
                FairScore = 5
            });

            var studentLaboratory = await StudentLaboratoryGenerator.Generate(context, new EN.StudentLaboratory
            {
                LaboratoryId = labPreset.Laboratory.Id,
                StudentId = student.Id
            });

            var studentSubTopic = await StudentSubTopicGenerator.Generate(context, new EN.StudentSubTopic
            {
                StudentLaboratoryId = studentLaboratory.Id,
                SubTopicId = labPreset.SubTopics[0].Id,
                StatusId = UT.Status.Active.Id,
            });

            var studentScore = await StudentScoreGenerator.Generate(context, appSetting, new EN.StudentScore
            {
                InstructorId = instructor.Id,
                StudentSubTopicId = studentSubTopic.Id,
                IsCritical = true,
                StatusId = UT.Status.Active.Id,
                Score = 0 
            });

            var request = new SubmitScoreRequest
            {
                LaboratoryId = labPreset.Laboratory.Id,
                SubTopicId = labPreset.SubTopics[0].Id,
                CriteriaId = labPreset.Criterias[1].Id,
                Remark = faker.Name.FirstName(),
            };

            var studentLab = await _laboratory.GetStudentLab(labPreset.Laboratory.Id, student.Id);
            var studentLabDetail = await _student.GetLabDetail(student.Id, labPreset.Laboratory.Id);

            // Act
            var result = await _student.SubmitScore(student.Id, instructor.Id, request);
            var notUpdatedStudentScore = await context.StudentScores.FindAsync(studentScore.Id);

            // Assert
            Assert.Equal(HttpStatusCode.Conflict, result.StatusCode);
            Assert.Equal(studentScore.Id, notUpdatedStudentScore.Id);
            Assert.Equal(studentScore.StatusId, notUpdatedStudentScore.StatusId);
        }

        [Fact]
        public async Task SubmitScoreAndAlreadyExistingSameCriteria_InactiveExisting_returnCreated()
        {
            // Arrange
            await TU.Preset.CommonPreset.Generate(context);

            var course = await CourseGenerator.Generate(context);
            var instructor = await InstructorGenerator.Generate(context, appSetting);
            var student = await StudentGenerator.Generate(context, appSetting, new EN.Student
            {
                CourseId = course.Id
            });

            var labPreset = await TU.Preset.LabPreset.PrepareGoodLab(context, appSetting, new TU.Preset.LabPreset.Data
            {
                TotalTopic = 1,
                TotalSubTopic = 1,
                GoodScore = 10,
                FairScore = 5
            });

            var studentLaboratory = await StudentLaboratoryGenerator.Generate(context, new EN.StudentLaboratory
            {
                LaboratoryId = labPreset.Laboratory.Id,
                StudentId = student.Id
            });

            var studentSubTopic = await StudentSubTopicGenerator.Generate(context, new EN.StudentSubTopic
            {
                StudentLaboratoryId = studentLaboratory.Id,
                SubTopicId = labPreset.SubTopics[0].Id,
                StatusId = UT.Status.Active.Id,
            });

            var studentScore = await StudentScoreGenerator.Generate(context, appSetting, new EN.StudentScore
            {
                InstructorId = instructor.Id,
                StudentSubTopicId = studentSubTopic.Id,
                IsCritical = false,
                StatusId = UT.Status.Active.Id,
                CriteriaId = labPreset.Criterias[0].Id,
                Score = 0
            });

            var request = new SubmitScoreRequest
            {
                LaboratoryId = labPreset.Laboratory.Id,
                SubTopicId = labPreset.SubTopics[0].Id,
                CriteriaId = labPreset.Criterias[0].Id,
                Remark = faker.Name.FirstName(),
            };

            var studentLab = await _laboratory.GetStudentLab(labPreset.Laboratory.Id, student.Id);
            var studentLabDetail = await _student.GetLabDetail(student.Id, labPreset.Laboratory.Id);

            // Act
            var result = await _student.SubmitScore(student.Id, instructor.Id, request);
            var updatedStudentScore = await context.StudentScores.FindAsync(studentScore.Id);
            var allStudentScore = (await context.StudentScores.Where(ss => ss.StatusId == UT.Status.Active.Id)
                                                            .ToListAsync()).Count;

            // Assert
            Assert.Equal(HttpStatusCode.Created, result.StatusCode);
            Assert.Equal(studentScore.Id, updatedStudentScore.Id);
            Assert.Equal(UT.Status.Inactive.Id, updatedStudentScore.StatusId);
            Assert.Equal(0, allStudentScore);
        }

        [Fact]
        public async Task SubmitScoreAndAlreadyExistingDiffCriteria_InactiveExisting_createNew_returnCreated()
        {
            // Arrange
            await TU.Preset.CommonPreset.Generate(context);

            var course = await CourseGenerator.Generate(context);
            var instructor = await InstructorGenerator.Generate(context, appSetting);
            var student = await StudentGenerator.Generate(context, appSetting, new EN.Student
            {
                CourseId = course.Id
            });

            var labPreset = await TU.Preset.LabPreset.PrepareGoodLab(context, appSetting, new TU.Preset.LabPreset.Data
            {
                TotalTopic = 1,
                TotalSubTopic = 1,
                GoodScore = 10,
                FairScore = 5
            });

            var studentLaboratory = await StudentLaboratoryGenerator.Generate(context, new EN.StudentLaboratory
            {
                LaboratoryId = labPreset.Laboratory.Id,
                StudentId = student.Id
            });

            var studentSubTopic = await StudentSubTopicGenerator.Generate(context, new EN.StudentSubTopic
            {
                StudentLaboratoryId = studentLaboratory.Id,
                SubTopicId = labPreset.SubTopics[0].Id,
                StatusId = UT.Status.Active.Id,
            });

            var studentScore = await StudentScoreGenerator.Generate(context, appSetting, new EN.StudentScore
            {
                InstructorId = instructor.Id,
                StudentSubTopicId = studentSubTopic.Id,
                IsCritical = false,
                StatusId = UT.Status.Active.Id,
                CriteriaId = labPreset.Criterias[0].Id,
                Score = 0
            });

            var request = new SubmitScoreRequest
            {
                LaboratoryId = labPreset.Laboratory.Id,
                SubTopicId = labPreset.SubTopics[0].Id,
                CriteriaId = labPreset.Criterias[1].Id,
                Remark = faker.Name.FirstName(),
            };

            var studentLab = await _laboratory.GetStudentLab(labPreset.Laboratory.Id, student.Id);
            var studentLabDetail = await _student.GetLabDetail(student.Id, labPreset.Laboratory.Id);

            // Act
            var result = await _student.SubmitScore(student.Id, instructor.Id, request);
            var updatedStudentScore = await context.StudentScores.FindAsync(studentScore.Id);
            var newStudentScore = await context.StudentScores.Where(ss => ss.StatusId == UT.Status.Active.Id)
                                                            .FirstOrDefaultAsync();


            // Assert
            Assert.Equal(HttpStatusCode.Created, result.StatusCode);
            Assert.Equal(studentScore.Id, updatedStudentScore.Id);
            Assert.Equal(UT.Status.Inactive.Id, updatedStudentScore.StatusId);

            Assert.Equal(instructor.Id, newStudentScore.InstructorId);
            Assert.Equal(studentSubTopic.Id, newStudentScore.StudentSubTopicId);
            Assert.Equal(labPreset.Criterias[1].Id, newStudentScore.CriteriaId);
            Assert.Equal(request.Remark, newStudentScore.Remark);
            Assert.False(newStudentScore.IsCritical);
        }

        [Fact]
        public async Task SubmitScoreWhenOverLabEndDate_Forbidden()
        {
            // Arrange
            await TU.Preset.CommonPreset.Generate(context);

            var course = await CourseGenerator.Generate(context);
            var instructor = await InstructorGenerator.Generate(context, appSetting);
            var student = await StudentGenerator.Generate(context, appSetting, new EN.Student
            {
                CourseId = course.Id
            });

            var labPreset = await TU.Preset.LabPreset.PrepareGoodLab(context, appSetting, new TU.Preset.LabPreset.Data
            {
                TotalTopic = 1,
                TotalSubTopic = 1,
                GoodScore = 10,
                FairScore = 5
            });

            labPreset.Laboratory.EndDate = DateTime.Now.AddDays(-1);
            context.Update(labPreset.Laboratory);
            await context.SaveChangesAsync();
            context.Entry(labPreset.Laboratory).State = EntityState.Detached;

            var studentLaboratory = await StudentLaboratoryGenerator.Generate(context, new EN.StudentLaboratory
            {
                LaboratoryId = labPreset.Laboratory.Id,
                StudentId = student.Id
            });

            var studentSubTopic = await StudentSubTopicGenerator.Generate(context, new EN.StudentSubTopic
            {
                StudentLaboratoryId = studentLaboratory.Id,
                SubTopicId = labPreset.SubTopics[0].Id,
                StatusId = UT.Status.Active.Id,
            });

            var request = new SubmitScoreRequest
            {
                LaboratoryId = labPreset.Laboratory.Id,
                SubTopicId = labPreset.SubTopics[0].Id,
                CriteriaId = labPreset.Criterias[1].Id,
                Remark = faker.Name.FirstName(),
            };

            // Act
            var response = await _student.SubmitScore(student.Id, instructor.Id, request);

            // Assert
            Assert.Equal(HttpStatusCode.Forbidden, response.StatusCode);
        }

        [Fact]
        public async Task SubmitScoreWhenOverTopicEndDate_Forbidden()
        {
            // Arrange
            await TU.Preset.CommonPreset.Generate(context);

            var course = await CourseGenerator.Generate(context);
            var instructor = await InstructorGenerator.Generate(context, appSetting);
            var student = await StudentGenerator.Generate(context, appSetting, new EN.Student
            {
                CourseId = course.Id
            });

            var labPreset = await TU.Preset.LabPreset.PrepareGoodLab(context, appSetting, new TU.Preset.LabPreset.Data
            {
                TotalTopic = 1,
                TotalSubTopic = 1,
                GoodScore = 10,
                FairScore = 5
            });

            labPreset.Topics[0].EndDate = DateTime.Now.AddDays(-1);
            context.Update(labPreset.Topics[0]);
            await context.SaveChangesAsync();
            context.Entry(labPreset.Topics[0]).State = EntityState.Detached;

            var studentLaboratory = await StudentLaboratoryGenerator.Generate(context, new EN.StudentLaboratory
            {
                LaboratoryId = labPreset.Laboratory.Id,
                StudentId = student.Id
            });

            var studentSubTopic = await StudentSubTopicGenerator.Generate(context, new EN.StudentSubTopic
            {
                StudentLaboratoryId = studentLaboratory.Id,
                SubTopicId = labPreset.SubTopics[0].Id,
                StatusId = UT.Status.Active.Id,
            });

            var request = new SubmitScoreRequest
            {
                LaboratoryId = labPreset.Laboratory.Id,
                SubTopicId = labPreset.SubTopics[0].Id,
                CriteriaId = labPreset.Criterias[1].Id,
                Remark = faker.Name.FirstName(),
            };

            // Act
            var response = await _student.SubmitScore(student.Id, instructor.Id, request);

            // Assert
            Assert.Equal(HttpStatusCode.Forbidden, response.StatusCode);
        }

        [Fact]
        public async Task SubmitCritical()
        {
            // Arrange
            await TU.Preset.CommonPreset.Generate(context);

            var course = await CourseGenerator.Generate(context);
            var instructor = await InstructorGenerator.Generate(context, appSetting);
            var student = await StudentGenerator.Generate(context, appSetting, new EN.Student
            {
                CourseId = course.Id
            });

            var labPreset = await TU.Preset.LabPreset.PrepareGoodLab(context, appSetting, new TU.Preset.LabPreset.Data
            {
                TotalTopic = 1,
                TotalSubTopic = 1,
                GoodScore = 10,
                FairScore = 5
            });

            var studentLaboratory = await StudentLaboratoryGenerator.Generate(context, new EN.StudentLaboratory
            {
                LaboratoryId = labPreset.Laboratory.Id,
                StudentId = student.Id
            });

            var studentSubTopic = await StudentSubTopicGenerator.Generate(context, new EN.StudentSubTopic
            {
                StudentLaboratoryId = studentLaboratory.Id,
                SubTopicId = labPreset.SubTopics[0].Id,
                StatusId = UT.Status.Active.Id,
            });

            var request = new SubmitCriticalRequest
            {
                LaboratoryId = labPreset.Laboratory.Id,
                SubTopicId = labPreset.SubTopics[0].Id,
                Remark = faker.Name.FirstName(),
            };

            // Act
            var response = await _student.SubmitCritical(student.Id, instructor.Id, request);
            var studentLabs = await context.StudentLaboratories.Select(sl => sl).AsNoTracking().ToArrayAsync();
            var studentSubTopics = await context.StudentSubTopics.Select(ss => ss).AsNoTracking().ToArrayAsync();
            var studentScores = await context.StudentScores.Select(ss => ss).AsNoTracking().ToListAsync();

            // Assert
            Assert.Equal(HttpStatusCode.Created, response.StatusCode);
            Assert.Single(studentLabs);
            Assert.Single(studentSubTopics);
            Assert.Single(studentScores);

            var studentScore = studentScores[0];
            Assert.Equal(1, studentScore.Id);
            Assert.Equal(instructor.Id, studentScore.InstructorId);
            Assert.Equal(studentSubTopic.Id, studentScore.StudentSubTopicId);
            Assert.Null(studentScore.CriteriaId);
            Assert.True(studentScore.IsCritical);
            Assert.Equal(0, studentScore.Score);
            Assert.Equal(UT.Status.Active.Id, studentScore.StatusId);
        }

        [Fact]
        public async Task SubmitCriticalInvolveOtherSubTopic()
        {
            // Arrange
            await TU.Preset.CommonPreset.Generate(context);

            var course = await CourseGenerator.Generate(context);
            var instructor = await InstructorGenerator.Generate(context, appSetting);
            var student = await StudentGenerator.Generate(context, appSetting, new EN.Student
            {
                CourseId = course.Id
            });

            var labPreset = await TU.Preset.LabPreset.PrepareEmptyLab(context, appSetting);
            var critical = await CriticalGenerator.Generate(context);

            var topic = await labPreset.AddTopic();
            var subTopicA = await labPreset.AddSubTopic(topic.Id, critical.Id);          
            var subTopicB = await labPreset.AddSubTopic(topic.Id, critical.Id);

            var studentLaboratory = await StudentLaboratoryGenerator.Generate(context, new EN.StudentLaboratory
            {
                LaboratoryId = labPreset.Laboratory.Id,
                StudentId = student.Id
            });

            var studentSubTopicA = await StudentSubTopicGenerator.Generate(context, new EN.StudentSubTopic
            {
                StudentLaboratoryId = studentLaboratory.Id,
                SubTopicId = subTopicA.Id,
                StatusId = UT.Status.Active.Id,
            });

            var studentSubTopicB = await StudentSubTopicGenerator.Generate(context, new EN.StudentSubTopic
            {
                StudentLaboratoryId = studentLaboratory.Id,
                SubTopicId = subTopicB.Id,
                StatusId = UT.Status.Active.Id,
            });

            var request = new SubmitCriticalRequest
            {
                LaboratoryId = labPreset.Laboratory.Id,
                SubTopicId = subTopicB.Id,
                Remark = faker.Name.FirstName(),
            };

            // Act
            var response = await _student.SubmitCritical(student.Id, instructor.Id, request);
            var studentLabs = await context.StudentLaboratories.Select(sl => sl).AsNoTracking().ToArrayAsync();
            var studentSubTopics = await context.StudentSubTopics.Select(ss => ss).AsNoTracking().ToArrayAsync();
            var studentScores = await context.StudentScores.Select(ss => ss).AsNoTracking().ToListAsync();

            // Assert
            Assert.Equal(HttpStatusCode.Created, response.StatusCode);
            Assert.Single(studentLabs);
            Assert.Equal(2, studentSubTopics.Count());
            Assert.Equal(2, studentScores.Count());

            var studentScore = studentScores[0];
            Assert.Equal(1, studentScore.Id);
            Assert.Equal(instructor.Id, studentScore.InstructorId);
            Assert.Equal(studentSubTopics[0].Id, studentScore.StudentSubTopicId);
            Assert.Null(studentScore.CriteriaId);
            Assert.True(studentScore.IsCritical);
            Assert.Equal(0, studentScore.Score);
            Assert.Equal(UT.Status.Active.Id, studentScore.StatusId);

            studentScore = studentScores[1];
            Assert.Equal(2, studentScore.Id);
            Assert.Equal(instructor.Id, studentScore.InstructorId);
            Assert.Equal(studentSubTopics[1].Id, studentScore.StudentSubTopicId);
            Assert.Null(studentScore.CriteriaId);
            Assert.True(studentScore.IsCritical);
            Assert.Equal(0, studentScore.Score);
            Assert.Equal(UT.Status.Active.Id, studentScore.StatusId);
        }

        [Fact]
        public async Task SubmitCriticalInvolveOtherSubTopicWhenSomeStudentSubTopicNotExistingThenCreate()
        {
            // Arrange
            await TU.Preset.CommonPreset.Generate(context);

            var course = await CourseGenerator.Generate(context);
            var instructor = await InstructorGenerator.Generate(context, appSetting);
            var student = await StudentGenerator.Generate(context, appSetting, new EN.Student
            {
                CourseId = course.Id
            });

            var labPreset = await TU.Preset.LabPreset.PrepareEmptyLab(context, appSetting);
            var critical = await CriticalGenerator.Generate(context);

            var topic = await labPreset.AddTopic();
            var subTopicA = await labPreset.AddSubTopic(topic.Id, critical.Id);
            var subTopicB = await labPreset.AddSubTopic(topic.Id, critical.Id);

            var studentLaboratory = await StudentLaboratoryGenerator.Generate(context, new EN.StudentLaboratory
            {
                LaboratoryId = labPreset.Laboratory.Id,
                StudentId = student.Id
            });

            var studentSubTopicA = await StudentSubTopicGenerator.Generate(context, new EN.StudentSubTopic
            {
                StudentLaboratoryId = studentLaboratory.Id,
                SubTopicId = subTopicA.Id,
                StatusId = UT.Status.Active.Id,
            });

            var request = new SubmitCriticalRequest
            {
                LaboratoryId = labPreset.Laboratory.Id,
                SubTopicId = subTopicB.Id,
                Remark = faker.Name.FirstName(),
            };

            // Act
            var response = await _student.SubmitCritical(student.Id, instructor.Id, request);
            var studentLabs = await context.StudentLaboratories.Select(sl => sl).AsNoTracking().ToArrayAsync();
            var studentSubTopics = await context.StudentSubTopics.Select(ss => ss).AsNoTracking().ToArrayAsync();
            var studentScores = await context.StudentScores.Select(ss => ss).AsNoTracking().ToListAsync();

            // Assert
            Assert.Equal(HttpStatusCode.Created, response.StatusCode);
            Assert.Single(studentLabs);
            Assert.Equal(2, studentSubTopics.Count());

            var studentSubTopic = studentSubTopics[1];
            Assert.Equal(2, studentSubTopic.Id);
            Assert.Equal(studentLaboratory.Id, studentSubTopic.StudentLaboratoryId);
            Assert.Equal(subTopicB.Id, studentSubTopic.SubTopicId);
            Assert.Equal(UT.Status.Active.Id, studentSubTopic.StatusId);

            Assert.Equal(2, studentScores.Count());

            var studentScore = studentScores[0];
            Assert.Equal(1, studentScore.Id);
            Assert.Equal(instructor.Id, studentScore.InstructorId);
            Assert.Equal(studentSubTopics[0].Id, studentScore.StudentSubTopicId);
            Assert.Null(studentScore.CriteriaId);
            Assert.True(studentScore.IsCritical);
            Assert.Equal(0, studentScore.Score);
            Assert.Equal(UT.Status.Active.Id, studentScore.StatusId);

            studentScore = studentScores[1];
            Assert.Equal(2, studentScore.Id);
            Assert.Equal(instructor.Id, studentScore.InstructorId);
            Assert.Equal(studentSubTopics[1].Id, studentScore.StudentSubTopicId);
            Assert.Null(studentScore.CriteriaId);
            Assert.True(studentScore.IsCritical);
            Assert.Equal(0, studentScore.Score);
            Assert.Equal(UT.Status.Active.Id, studentScore.StatusId);
        }
    
        [Fact]
        public async Task SubmitCriticalInvolveOtherSubTopicWhenStudentLabNotExistingThenCreate()
        {
            // Arrange
            await TU.Preset.CommonPreset.Generate(context);

            var course = await CourseGenerator.Generate(context);
            var instructor = await InstructorGenerator.Generate(context, appSetting);
            var student = await StudentGenerator.Generate(context, appSetting, new EN.Student
            {
                CourseId = course.Id
            });

            var labPreset = await TU.Preset.LabPreset.PrepareEmptyLab(context, appSetting);
            var critical = await CriticalGenerator.Generate(context);

            var topic = await labPreset.AddTopic();
            var subTopicA = await labPreset.AddSubTopic(topic.Id, critical.Id);
            var subTopicB = await labPreset.AddSubTopic(topic.Id, critical.Id);

            var request = new SubmitCriticalRequest
            {
                LaboratoryId = labPreset.Laboratory.Id,
                SubTopicId = subTopicB.Id,
                Remark = faker.Name.FirstName(),
            };

            // Act
            var response = await _student.SubmitCritical(student.Id, instructor.Id, request);
            var studentLabs = await context.StudentLaboratories.Select(sl => sl).AsNoTracking().ToArrayAsync();
            var studentSubTopics = await context.StudentSubTopics.Select(ss => ss).AsNoTracking().ToArrayAsync();
            var studentScores = await context.StudentScores.Select(ss => ss).AsNoTracking().ToListAsync();

            // Assert
            Assert.Equal(HttpStatusCode.Created, response.StatusCode);
            Assert.Single(studentLabs);

            var studentLaboratory = studentLabs[0];
            Assert.Equal(1, studentLaboratory.Id);
            Assert.Equal(labPreset.Laboratory.Id, studentLaboratory.LaboratoryId);
            Assert.Equal(student.Id, studentLaboratory.StudentId);
            Assert.Equal(UT.Status.Active.Id, studentLaboratory.StatusId);

            Assert.Equal(2, studentSubTopics.Count());
            var studentSubTopic = studentSubTopics[0];
            Assert.Equal(1, studentSubTopic.Id);
            Assert.Equal(studentLaboratory.Id, studentSubTopic.StudentLaboratoryId);
            Assert.Equal(subTopicA.Id, studentSubTopic.SubTopicId);
            Assert.Equal(UT.Status.Active.Id, studentSubTopic.StatusId);

            studentSubTopic = studentSubTopics[1];
            Assert.Equal(2, studentSubTopic.Id);
            Assert.Equal(studentLaboratory.Id, studentSubTopic.StudentLaboratoryId);
            Assert.Equal(subTopicB.Id, studentSubTopic.SubTopicId);
            Assert.Equal(UT.Status.Active.Id, studentSubTopic.StatusId);

            Assert.Equal(2, studentScores.Count());

            var studentScore = studentScores[0];
            Assert.Equal(1, studentScore.Id);
            Assert.Equal(instructor.Id, studentScore.InstructorId);
            Assert.Equal(studentSubTopics[0].Id, studentScore.StudentSubTopicId);
            Assert.Null(studentScore.CriteriaId);
            Assert.True(studentScore.IsCritical);
            Assert.Equal(0, studentScore.Score);
            Assert.Equal(UT.Status.Active.Id, studentScore.StatusId);

            studentScore = studentScores[1];
            Assert.Equal(2, studentScore.Id);
            Assert.Equal(instructor.Id, studentScore.InstructorId);
            Assert.Equal(studentSubTopics[1].Id, studentScore.StudentSubTopicId);
            Assert.Null(studentScore.CriteriaId);
            Assert.True(studentScore.IsCritical);
            Assert.Equal(0, studentScore.Score);
            Assert.Equal(UT.Status.Active.Id, studentScore.StatusId);
        }

        [Fact]
        public async Task SubmitCriticalWhenStudentNotExisting_NotFound()
        {
            // Arrange
            await TU.Preset.CommonPreset.Generate(context);

            var course = await CourseGenerator.Generate(context);
            var instructor = await InstructorGenerator.Generate(context, appSetting);
            var student = await StudentGenerator.Generate(context, appSetting, new EN.Student
            {
                CourseId = course.Id
            });

            var labPreset = await TU.Preset.LabPreset.PrepareGoodLab(context, appSetting, new TU.Preset.LabPreset.Data
            {
                TotalTopic = 1,
                TotalSubTopic = 1,
                GoodScore = 10,
                FairScore = 5
            });

            var studentLaboratory = await StudentLaboratoryGenerator.Generate(context, new EN.StudentLaboratory
            {
                LaboratoryId = labPreset.Laboratory.Id,
                StudentId = student.Id
            });

            var studentSubTopic = await StudentSubTopicGenerator.Generate(context, new EN.StudentSubTopic
            {
                StudentLaboratoryId = studentLaboratory.Id,
                SubTopicId = labPreset.SubTopics[0].Id,
                StatusId = UT.Status.Active.Id,
            });

            var request = new SubmitCriticalRequest
            {
                LaboratoryId = labPreset.Laboratory.Id,
                SubTopicId = labPreset.SubTopics[0].Id,
                Remark = faker.Name.FirstName(),
            };

            // Act
            var response = await _student.SubmitCritical(100, instructor.Id, request);       

            // Assert
            Assert.Equal(HttpStatusCode.NotFound, response.StatusCode);
        }

        [Fact]
        public async Task SubmitCriticalWhenInstructorNotExisting_NotFound()
        {
            // Arrange
            await TU.Preset.CommonPreset.Generate(context);

            var course = await CourseGenerator.Generate(context);
            var instructor = await InstructorGenerator.Generate(context, appSetting);
            var student = await StudentGenerator.Generate(context, appSetting, new EN.Student
            {
                CourseId = course.Id
            });

            var labPreset = await TU.Preset.LabPreset.PrepareGoodLab(context, appSetting, new TU.Preset.LabPreset.Data
            {
                TotalTopic = 1,
                TotalSubTopic = 1,
                GoodScore = 10,
                FairScore = 5
            });

            var studentLaboratory = await StudentLaboratoryGenerator.Generate(context, new EN.StudentLaboratory
            {
                LaboratoryId = labPreset.Laboratory.Id,
                StudentId = student.Id
            });

            var studentSubTopic = await StudentSubTopicGenerator.Generate(context, new EN.StudentSubTopic
            {
                StudentLaboratoryId = studentLaboratory.Id,
                SubTopicId = labPreset.SubTopics[0].Id,
                StatusId = UT.Status.Active.Id,
            });

            var request = new SubmitCriticalRequest
            {
                LaboratoryId = labPreset.Laboratory.Id,
                SubTopicId = labPreset.SubTopics[0].Id,
                Remark = faker.Name.FirstName(),
            };

            // Act
            var response = await _student.SubmitCritical(student.Id, 100, request);

            // Assert
            Assert.Equal(HttpStatusCode.NotFound, response.StatusCode);
        }

        [Fact]
        public async Task SubmitCriticalWhenLaboratoryNotExisting_NotFound()
        {
            // Arrange
            await TU.Preset.CommonPreset.Generate(context);

            var course = await CourseGenerator.Generate(context);
            var instructor = await InstructorGenerator.Generate(context, appSetting);
            var student = await StudentGenerator.Generate(context, appSetting, new EN.Student
            {
                CourseId = course.Id
            });

            var labPreset = await TU.Preset.LabPreset.PrepareGoodLab(context, appSetting, new TU.Preset.LabPreset.Data
            {
                TotalTopic = 1,
                TotalSubTopic = 1,
                GoodScore = 10,
                FairScore = 5
            });

            var studentLaboratory = await StudentLaboratoryGenerator.Generate(context, new EN.StudentLaboratory
            {
                LaboratoryId = labPreset.Laboratory.Id,
                StudentId = student.Id
            });

            var studentSubTopic = await StudentSubTopicGenerator.Generate(context, new EN.StudentSubTopic
            {
                StudentLaboratoryId = studentLaboratory.Id,
                SubTopicId = labPreset.SubTopics[0].Id,
                StatusId = UT.Status.Active.Id,
            });

            var request = new SubmitCriticalRequest
            {
                LaboratoryId = 100,
                SubTopicId = labPreset.SubTopics[0].Id,
                Remark = faker.Name.FirstName(),
            };

            // Act
            var response = await _student.SubmitCritical(student.Id, instructor.Id, request);

            // Assert
            Assert.Equal(HttpStatusCode.NotFound, response.StatusCode);
        }

        [Fact]
        public async Task SubmitCriticalWhenSubTopicNotExisting_NotFound()
        {
            // Arrange
            await TU.Preset.CommonPreset.Generate(context);

            var course = await CourseGenerator.Generate(context);
            var instructor = await InstructorGenerator.Generate(context, appSetting);
            var student = await StudentGenerator.Generate(context, appSetting, new EN.Student
            {
                CourseId = course.Id
            });

            var labPreset = await TU.Preset.LabPreset.PrepareGoodLab(context, appSetting, new TU.Preset.LabPreset.Data
            {
                TotalTopic = 1,
                TotalSubTopic = 1,
                GoodScore = 10,
                FairScore = 5
            });

            var studentLaboratory = await StudentLaboratoryGenerator.Generate(context, new EN.StudentLaboratory
            {
                LaboratoryId = labPreset.Laboratory.Id,
                StudentId = student.Id
            });

            var studentSubTopic = await StudentSubTopicGenerator.Generate(context, new EN.StudentSubTopic
            {
                StudentLaboratoryId = studentLaboratory.Id,
                SubTopicId = labPreset.SubTopics[0].Id,
                StatusId = UT.Status.Active.Id,
            });

            var request = new SubmitCriticalRequest
            {
                LaboratoryId = labPreset.Laboratory.Id,
                SubTopicId = 100,
                Remark = faker.Name.FirstName(),
            };

            // Act
            var response = await _student.SubmitCritical(student.Id, instructor.Id, request);

            // Assert
            Assert.Equal(HttpStatusCode.NotFound, response.StatusCode);
        }

        [Fact]
        public async Task SubmitCriticalSuccessThenCacheClear()
        {
            // Arrange
            await TU.Preset.CommonPreset.Generate(context);

            var course = await CourseGenerator.Generate(context);
            var instructor = await InstructorGenerator.Generate(context, appSetting);
            var student = await StudentGenerator.Generate(context, appSetting, new EN.Student
            {
                CourseId = course.Id
            });

            var labPreset = await TU.Preset.LabPreset.PrepareGoodLab(context, appSetting, new TU.Preset.LabPreset.Data
            {
                TotalTopic = 1,
                TotalSubTopic = 1,
                GoodScore = 10,
                FairScore = 5
            });

            var studentLaboratory = await StudentLaboratoryGenerator.Generate(context, new EN.StudentLaboratory
            {
                LaboratoryId = labPreset.Laboratory.Id,
                StudentId = student.Id
            });

            var studentSubTopic = await StudentSubTopicGenerator.Generate(context, new EN.StudentSubTopic
            {
                StudentLaboratoryId = studentLaboratory.Id,
                SubTopicId = labPreset.SubTopics[0].Id,
                StatusId = UT.Status.Active.Id,
            });

            var request = new SubmitCriticalRequest
            {
                LaboratoryId = labPreset.Laboratory.Id,
                SubTopicId = labPreset.SubTopics[0].Id,
                Remark = faker.Name.FirstName(),
            };

            var studentLab = await _laboratory.GetStudentLab(labPreset.Laboratory.Id, student.Id);
            var studentLabDetail = await _student.GetLabDetail(student.Id, labPreset.Laboratory.Id);

            // Act
            var cacheStudentLabKey = UT.CacheKey.Laboratory.Student(labPreset.Laboratory.Id, student.Id);
            var cacheStudentLabDetailKey = UT.CacheKey.Student.LaboratoryDetail(student.Id, labPreset.Laboratory.Id);
            var cacheExperienceKey = UT.CacheKey.Student.Experience(student.Id);

            UT.CacheManager.Set(cacheExperienceKey, new List<StudentExperienceResponse>());

            var cacheStudentLabBefore = UT.CacheManager.Get<EN.StudentLaboratory>(cacheStudentLabKey);
            var cacheStudentLabDetailBefore = UT.CacheManager.Get<StudentLaboratoryDetailResponse>(cacheStudentLabDetailKey);
            var cacheExperienceBefore = UT.CacheManager.Get<List<StudentExperienceResponse>>(cacheExperienceKey);
            var result = await _student.SubmitCritical(student.Id, instructor.Id, request);
            var cacheStudentLabAfter = UT.CacheManager.Get<EN.StudentLaboratory>(cacheStudentLabKey);
            var cacheStudentLabDetailAfter = UT.CacheManager.Get<StudentLaboratoryDetailResponse>(cacheStudentLabDetailKey);
            var cacheExperienceAfter = UT.CacheManager.Get<List<StudentExperienceResponse>>(cacheExperienceKey);

            // Assert
            Assert.Equal(HttpStatusCode.Created, result.StatusCode);

            Assert.NotNull(cacheStudentLabBefore);
            Assert.Equal(studentLabDetail.Object, cacheStudentLabDetailBefore);
            Assert.NotNull(cacheExperienceBefore);
            Assert.Null(cacheStudentLabAfter);
            Assert.Null(cacheStudentLabDetailAfter);
            Assert.Null(cacheExperienceAfter);
        }

        [Fact]
        public async Task SubmitCriticalWhenOverLabEndDate_Forbidden()
        {
            // Arrange
            await TU.Preset.CommonPreset.Generate(context);

            var course = await CourseGenerator.Generate(context);
            var instructor = await InstructorGenerator.Generate(context, appSetting);
            var student = await StudentGenerator.Generate(context, appSetting, new EN.Student
            {
                CourseId = course.Id
            });

            var labPreset = await TU.Preset.LabPreset.PrepareGoodLab(context, appSetting, new TU.Preset.LabPreset.Data
            {
                TotalTopic = 1,
                TotalSubTopic = 1,
                GoodScore = 10,
                FairScore = 5
            });

            labPreset.Laboratory.EndDate = DateTime.Now.AddDays(-1);
            context.Update(labPreset.Laboratory);
            await context.SaveChangesAsync();
            context.Entry(labPreset.Laboratory).State = EntityState.Detached;

            var studentLaboratory = await StudentLaboratoryGenerator.Generate(context, new EN.StudentLaboratory
            {
                LaboratoryId = labPreset.Laboratory.Id,
                StudentId = student.Id
            });

            var studentSubTopic = await StudentSubTopicGenerator.Generate(context, new EN.StudentSubTopic
            {
                StudentLaboratoryId = studentLaboratory.Id,
                SubTopicId = labPreset.SubTopics[0].Id,
                StatusId = UT.Status.Active.Id,
            });

            var request = new SubmitCriticalRequest
            {
                LaboratoryId = labPreset.Laboratory.Id,
                SubTopicId = labPreset.SubTopics[0].Id,
                Remark = faker.Name.FirstName(),
            };

            // Act
            var response = await _student.SubmitCritical(student.Id, instructor.Id, request);

            // Assert
            Assert.Equal(HttpStatusCode.Forbidden, response.StatusCode);
        }

        [Fact]
        public async Task SubmitCriticalWhenOverTopicEndDate_Forbidden()
        {
            // Arrange
            await TU.Preset.CommonPreset.Generate(context);

            var course = await CourseGenerator.Generate(context);
            var instructor = await InstructorGenerator.Generate(context, appSetting);
            var student = await StudentGenerator.Generate(context, appSetting, new EN.Student
            {
                CourseId = course.Id
            });

            var labPreset = await TU.Preset.LabPreset.PrepareGoodLab(context, appSetting, new TU.Preset.LabPreset.Data
            {
                TotalTopic = 1,
                TotalSubTopic = 1,
                GoodScore = 10,
                FairScore = 5
            });

            labPreset.Topics[0].EndDate = DateTime.Now.AddDays(-1);
            context.Update(labPreset.Topics[0]);
            await context.SaveChangesAsync();
            context.Entry(labPreset.Topics[0]).State = EntityState.Detached;

            var studentLaboratory = await StudentLaboratoryGenerator.Generate(context, new EN.StudentLaboratory
            {
                LaboratoryId = labPreset.Laboratory.Id,
                StudentId = student.Id
            });

            var studentSubTopic = await StudentSubTopicGenerator.Generate(context, new EN.StudentSubTopic
            {
                StudentLaboratoryId = studentLaboratory.Id,
                SubTopicId = labPreset.SubTopics[0].Id,
                StatusId = UT.Status.Active.Id,
            });

            var request = new SubmitCriticalRequest
            {
                LaboratoryId = labPreset.Laboratory.Id,
                SubTopicId = labPreset.SubTopics[0].Id,
                Remark = faker.Name.FirstName(),
            };

            // Act
            var response = await _student.SubmitCritical(student.Id, instructor.Id, request);

            // Assert
            Assert.Equal(HttpStatusCode.Forbidden, response.StatusCode);
        }

        [Fact]
        public async Task CancelCritical()
        {
            // Arrange
            await TU.Preset.CommonPreset.Generate(context);

            var course = await CourseGenerator.Generate(context);
            var instructor = await InstructorGenerator.Generate(context, appSetting);
            var student = await StudentGenerator.Generate(context, appSetting, new EN.Student
            {
                CourseId = course.Id
            });

            var labPreset = await TU.Preset.LabPreset.PrepareGoodLab(context, appSetting, new TU.Preset.LabPreset.Data
            {
                TotalTopic = 1,
                TotalSubTopic = 1,
                GoodScore = 10,
                FairScore = 5
            });

            var studentLaboratory = await StudentLaboratoryGenerator.Generate(context, new EN.StudentLaboratory
            {
                LaboratoryId = labPreset.Laboratory.Id,
                StudentId = student.Id
            });

            var studentSubTopic = await StudentSubTopicGenerator.Generate(context, new EN.StudentSubTopic
            {
                StudentLaboratoryId = studentLaboratory.Id,
                SubTopicId = labPreset.SubTopics[0].Id,
                StatusId = UT.Status.Active.Id,
            });

            var studentScore = await StudentScoreGenerator.Generate(context, appSetting, new EN.StudentScore
            {
                InstructorId = instructor.Id,
                StudentSubTopicId = studentSubTopic.Id,
                IsCritical = false,
                StatusId = UT.Status.Active.Id,
                CriteriaId = labPreset.Criterias[0].Id,
                Score = 0
            });

            var critical1 = await StudentScoreGenerator.Generate(context, appSetting, new EN.StudentScore
            {
                InstructorId = instructor.Id,
                StudentSubTopicId = studentSubTopic.Id,
                IsCritical = true,
                StatusId = UT.Status.Active.Id,
                CriteriaId = labPreset.Criterias[0].Id,
                Score = 0
            });

            var critical2 = await StudentScoreGenerator.Generate(context, appSetting, new EN.StudentScore
            {
                InstructorId = instructor.Id,
                StudentSubTopicId = studentSubTopic.Id,
                IsCritical = true,
                StatusId = UT.Status.Active.Id,
                CriteriaId = labPreset.Criterias[0].Id,
                Score = 0
            });

            var request = new SubmitCriticalRequest
            {
                LaboratoryId = labPreset.Laboratory.Id,
                SubTopicId = labPreset.SubTopics[0].Id,
                Remark = faker.Name.FirstName(),
            };

            //Act
            var result = await _student.CancelCritical(student.Id, request);
            var allStudentScores = await context.StudentScores.OrderBy(ss => ss.Id).ToListAsync();

            //Assert
            Assert.Equal(HttpStatusCode.OK, result.StatusCode);
            Assert.Equal(3, allStudentScores.Count());

            var ss = allStudentScores[0];
            Assert.Equal(studentScore.Id, ss.Id);
            Assert.Equal(studentScore.IsCritical, ss.IsCritical);
            Assert.Equal(studentScore.StatusId, ss.StatusId);

            ss = allStudentScores[1];
            Assert.Equal(critical1.Id, ss.Id);
            Assert.Equal(critical1.IsCritical, ss.IsCritical);
            Assert.Equal(UT.Status.Inactive.Id, ss.StatusId);

            ss = allStudentScores[2];
            Assert.Equal(critical2.Id, ss.Id);
            Assert.Equal(critical2.IsCritical, ss.IsCritical);
            Assert.Equal(UT.Status.Inactive.Id, ss.StatusId);
        }

        [Fact]
        public async Task CancelCriticalWhenStudentNotExisting_NotFound()
        {
            // Arrange
            await TU.Preset.CommonPreset.Generate(context);

            var course = await CourseGenerator.Generate(context);
            var instructor = await InstructorGenerator.Generate(context, appSetting);
            var student = await StudentGenerator.Generate(context, appSetting, new EN.Student
            {
                CourseId = course.Id
            });

            var labPreset = await TU.Preset.LabPreset.PrepareGoodLab(context, appSetting, new TU.Preset.LabPreset.Data
            {
                TotalTopic = 1,
                TotalSubTopic = 1,
                GoodScore = 10,
                FairScore = 5
            });

            var studentLaboratory = await StudentLaboratoryGenerator.Generate(context, new EN.StudentLaboratory
            {
                LaboratoryId = labPreset.Laboratory.Id,
                StudentId = student.Id
            });

            var studentSubTopic = await StudentSubTopicGenerator.Generate(context, new EN.StudentSubTopic
            {
                StudentLaboratoryId = studentLaboratory.Id,
                SubTopicId = labPreset.SubTopics[0].Id,
                StatusId = UT.Status.Active.Id,
            });

            var studentScore = await StudentScoreGenerator.Generate(context, appSetting, new EN.StudentScore
            {
                InstructorId = instructor.Id,
                StudentSubTopicId = studentSubTopic.Id,
                IsCritical = false,
                StatusId = UT.Status.Active.Id,
                CriteriaId = labPreset.Criterias[0].Id,
                Score = 0
            });

            var critical1 = await StudentScoreGenerator.Generate(context, appSetting, new EN.StudentScore
            {
                InstructorId = instructor.Id,
                StudentSubTopicId = studentSubTopic.Id,
                IsCritical = true,
                StatusId = UT.Status.Active.Id,
                CriteriaId = labPreset.Criterias[0].Id,
                Score = 0
            });

            var request = new SubmitCriticalRequest
            {
                LaboratoryId = labPreset.Laboratory.Id,
                SubTopicId = labPreset.SubTopics[0].Id,
                Remark = faker.Name.FirstName(),
            };

            //Act
            var result = await _student.CancelCritical(9999, request);

            //Assert
            Assert.Equal(HttpStatusCode.NotFound, result.StatusCode);
        }

        [Fact]
        public async Task CancelCriticalWhenLaboratoryNotExisting_NotFound()
        {
            // Arrange
            await TU.Preset.CommonPreset.Generate(context);

            var course = await CourseGenerator.Generate(context);
            var instructor = await InstructorGenerator.Generate(context, appSetting);
            var student = await StudentGenerator.Generate(context, appSetting, new EN.Student
            {
                CourseId = course.Id
            });

            var labPreset = await TU.Preset.LabPreset.PrepareGoodLab(context, appSetting, new TU.Preset.LabPreset.Data
            {
                TotalTopic = 1,
                TotalSubTopic = 1,
                GoodScore = 10,
                FairScore = 5
            });

            var studentLaboratory = await StudentLaboratoryGenerator.Generate(context, new EN.StudentLaboratory
            {
                LaboratoryId = labPreset.Laboratory.Id,
                StudentId = student.Id
            });

            var studentSubTopic = await StudentSubTopicGenerator.Generate(context, new EN.StudentSubTopic
            {
                StudentLaboratoryId = studentLaboratory.Id,
                SubTopicId = labPreset.SubTopics[0].Id,
                StatusId = UT.Status.Active.Id,
            });

            var studentScore = await StudentScoreGenerator.Generate(context, appSetting, new EN.StudentScore
            {
                InstructorId = instructor.Id,
                StudentSubTopicId = studentSubTopic.Id,
                IsCritical = false,
                StatusId = UT.Status.Active.Id,
                CriteriaId = labPreset.Criterias[0].Id,
                Score = 0
            });

            var critical1 = await StudentScoreGenerator.Generate(context, appSetting, new EN.StudentScore
            {
                InstructorId = instructor.Id,
                StudentSubTopicId = studentSubTopic.Id,
                IsCritical = true,
                StatusId = UT.Status.Active.Id,
                CriteriaId = labPreset.Criterias[0].Id,
                Score = 0
            });

            var request = new SubmitCriticalRequest
            {
                LaboratoryId = 9999,
                SubTopicId = labPreset.SubTopics[0].Id,
                Remark = faker.Name.FirstName(),
            };

            //Act
            var result = await _student.CancelCritical(student.Id, request);

            //Assert
            Assert.Equal(HttpStatusCode.NotFound, result.StatusCode);
        }

        public async Task CancelCriticalWhenSubTopicNotExisting_NotFound()
        {
            // Arrange
            await TU.Preset.CommonPreset.Generate(context);

            var course = await CourseGenerator.Generate(context);
            var instructor = await InstructorGenerator.Generate(context, appSetting);
            var student = await StudentGenerator.Generate(context, appSetting, new EN.Student
            {
                CourseId = course.Id
            });

            var labPreset = await TU.Preset.LabPreset.PrepareGoodLab(context, appSetting, new TU.Preset.LabPreset.Data
            {
                TotalTopic = 1,
                TotalSubTopic = 1,
                GoodScore = 10,
                FairScore = 5
            });

            var studentLaboratory = await StudentLaboratoryGenerator.Generate(context, new EN.StudentLaboratory
            {
                LaboratoryId = labPreset.Laboratory.Id,
                StudentId = student.Id
            });

            var studentSubTopic = await StudentSubTopicGenerator.Generate(context, new EN.StudentSubTopic
            {
                StudentLaboratoryId = studentLaboratory.Id,
                SubTopicId = labPreset.SubTopics[0].Id,
                StatusId = UT.Status.Active.Id,
            });

            var studentScore = await StudentScoreGenerator.Generate(context, appSetting, new EN.StudentScore
            {
                InstructorId = instructor.Id,
                StudentSubTopicId = studentSubTopic.Id,
                IsCritical = false,
                StatusId = UT.Status.Active.Id,
                CriteriaId = labPreset.Criterias[0].Id,
                Score = 0
            });

            var critical1 = await StudentScoreGenerator.Generate(context, appSetting, new EN.StudentScore
            {
                InstructorId = instructor.Id,
                StudentSubTopicId = studentSubTopic.Id,
                IsCritical = true,
                StatusId = UT.Status.Active.Id,
                CriteriaId = labPreset.Criterias[0].Id,
                Score = 0
            });

            var request = new SubmitCriticalRequest
            {
                LaboratoryId = labPreset.Laboratory.Id,
                SubTopicId = 9999,
                Remark = faker.Name.FirstName(),
            };

            //Act
            var result = await _student.CancelCritical(student.Id, request);

            //Assert
            Assert.Equal(HttpStatusCode.NotFound, result.StatusCode);
        }

        [Fact]
        public async Task CancelCriticaWhenOverLabEndDate_Forbidden()
        {
            // Arrange
            await TU.Preset.CommonPreset.Generate(context);

            var course = await CourseGenerator.Generate(context);
            var instructor = await InstructorGenerator.Generate(context, appSetting);
            var student = await StudentGenerator.Generate(context, appSetting, new EN.Student
            {
                CourseId = course.Id
            });

            var labPreset = await TU.Preset.LabPreset.PrepareGoodLab(context, appSetting, new TU.Preset.LabPreset.Data
            {
                TotalTopic = 1,
                TotalSubTopic = 1,
                GoodScore = 10,
                FairScore = 5
            });

            labPreset.Laboratory.EndDate = DateTime.Now.AddDays(-1);
            context.Update(labPreset.Laboratory);
            await context.SaveChangesAsync();
            context.Entry(labPreset.Laboratory).State = EntityState.Detached;

            var studentLaboratory = await StudentLaboratoryGenerator.Generate(context, new EN.StudentLaboratory
            {
                LaboratoryId = labPreset.Laboratory.Id,
                StudentId = student.Id
            });

            var studentSubTopic = await StudentSubTopicGenerator.Generate(context, new EN.StudentSubTopic
            {
                StudentLaboratoryId = studentLaboratory.Id,
                SubTopicId = labPreset.SubTopics[0].Id,
                StatusId = UT.Status.Active.Id,
            });

            var studentScore = await StudentScoreGenerator.Generate(context, appSetting, new EN.StudentScore
            {
                InstructorId = instructor.Id,
                StudentSubTopicId = studentSubTopic.Id,
                IsCritical = false,
                StatusId = UT.Status.Active.Id,
                CriteriaId = labPreset.Criterias[0].Id,
                Score = 0
            });

            var critical1 = await StudentScoreGenerator.Generate(context, appSetting, new EN.StudentScore
            {
                InstructorId = instructor.Id,
                StudentSubTopicId = studentSubTopic.Id,
                IsCritical = true,
                StatusId = UT.Status.Active.Id,
                CriteriaId = labPreset.Criterias[0].Id,
                Score = 0
            });

            var request = new SubmitCriticalRequest
            {
                LaboratoryId = labPreset.Laboratory.Id,
                SubTopicId = labPreset.SubTopics[0].Id,
                Remark = faker.Name.FirstName(),
            };

            //Act
            var result = await _student.CancelCritical(student.Id, request);

            //Assert
            Assert.Equal(HttpStatusCode.Forbidden, result.StatusCode);
        }

        [Fact]
        public async Task CancelCriticaWhenOverTopicEndDate_Forbidden()
        {
            // Arrange
            await TU.Preset.CommonPreset.Generate(context);

            var course = await CourseGenerator.Generate(context);
            var instructor = await InstructorGenerator.Generate(context, appSetting);
            var student = await StudentGenerator.Generate(context, appSetting, new EN.Student
            {
                CourseId = course.Id
            });

            var labPreset = await TU.Preset.LabPreset.PrepareGoodLab(context, appSetting, new TU.Preset.LabPreset.Data
            {
                TotalTopic = 1,
                TotalSubTopic = 1,
                GoodScore = 10,
                FairScore = 5
            });

            labPreset.Topics[0].EndDate = DateTime.Now.AddDays(-1);
            context.Update(labPreset.Topics[0]);
            await context.SaveChangesAsync();
            context.Entry(labPreset.Topics[0]).State = EntityState.Detached;

            var studentLaboratory = await StudentLaboratoryGenerator.Generate(context, new EN.StudentLaboratory
            {
                LaboratoryId = labPreset.Laboratory.Id,
                StudentId = student.Id
            });

            var studentSubTopic = await StudentSubTopicGenerator.Generate(context, new EN.StudentSubTopic
            {
                StudentLaboratoryId = studentLaboratory.Id,
                SubTopicId = labPreset.SubTopics[0].Id,
                StatusId = UT.Status.Active.Id,
            });

            var studentScore = await StudentScoreGenerator.Generate(context, appSetting, new EN.StudentScore
            {
                InstructorId = instructor.Id,
                StudentSubTopicId = studentSubTopic.Id,
                IsCritical = false,
                StatusId = UT.Status.Active.Id,
                CriteriaId = labPreset.Criterias[0].Id,
                Score = 0
            });

            var critical1 = await StudentScoreGenerator.Generate(context, appSetting, new EN.StudentScore
            {
                InstructorId = instructor.Id,
                StudentSubTopicId = studentSubTopic.Id,
                IsCritical = true,
                StatusId = UT.Status.Active.Id,
                CriteriaId = labPreset.Criterias[0].Id,
                Score = 0
            });

            var request = new SubmitCriticalRequest
            {
                LaboratoryId = labPreset.Laboratory.Id,
                SubTopicId = labPreset.SubTopics[0].Id,
                Remark = faker.Name.FirstName(),
            };

            //Act
            var result = await _student.CancelCritical(student.Id, request);

            //Assert
            Assert.Equal(HttpStatusCode.Forbidden, result.StatusCode);
        }

        [Fact]
        public async Task CancelCriticalCacheClearCheck()
        {
            // Arrange
            await TU.Preset.CommonPreset.Generate(context);

            var course = await CourseGenerator.Generate(context);
            var instructor = await InstructorGenerator.Generate(context, appSetting);
            var student = await StudentGenerator.Generate(context, appSetting, new EN.Student
            {
                CourseId = course.Id
            });

            var labPreset = await TU.Preset.LabPreset.PrepareGoodLab(context, appSetting, new TU.Preset.LabPreset.Data
            {
                TotalTopic = 1,
                TotalSubTopic = 1,
                GoodScore = 10,
                FairScore = 5
            });

            var studentLaboratory = await StudentLaboratoryGenerator.Generate(context, new EN.StudentLaboratory
            {
                LaboratoryId = labPreset.Laboratory.Id,
                StudentId = student.Id
            });

            var studentSubTopic = await StudentSubTopicGenerator.Generate(context, new EN.StudentSubTopic
            {
                StudentLaboratoryId = studentLaboratory.Id,
                SubTopicId = labPreset.SubTopics[0].Id,
                StatusId = UT.Status.Active.Id,
            });

            var studentScore = await StudentScoreGenerator.Generate(context, appSetting, new EN.StudentScore
            {
                InstructorId = instructor.Id,
                StudentSubTopicId = studentSubTopic.Id,
                IsCritical = false,
                StatusId = UT.Status.Active.Id,
                CriteriaId = labPreset.Criterias[0].Id,
                Score = 0
            });

            var critical1 = await StudentScoreGenerator.Generate(context, appSetting, new EN.StudentScore
            {
                InstructorId = instructor.Id,
                StudentSubTopicId = studentSubTopic.Id,
                IsCritical = true,
                StatusId = UT.Status.Active.Id,
                CriteriaId = labPreset.Criterias[0].Id,
                Score = 0
            });

            var request = new SubmitCriticalRequest
            {
                LaboratoryId = labPreset.Laboratory.Id,
                SubTopicId = labPreset.SubTopics[0].Id,
                Remark = faker.Name.FirstName(),
            };

            var studentLabDetail = await _student.GetLabDetail(student.Id, labPreset.Laboratory.Id);

            //Act
            var cacheStudentLabKey = UT.CacheKey.Laboratory.Student(labPreset.Laboratory.Id, student.Id);
            var cacheStudentLabDetailKey = UT.CacheKey.Student.LaboratoryDetail(student.Id, labPreset.Laboratory.Id);
            var cacheExperienceKey = UT.CacheKey.Student.Experience(student.Id);

            UT.CacheManager.Set(cacheExperienceKey, new List<StudentExperienceResponse>());

            var cacheStudentLabBefore = UT.CacheManager.Get<EN.StudentLaboratory>(cacheStudentLabKey);
            var cacheStudentLabDetailBefore = UT.CacheManager.Get<StudentLaboratoryDetailResponse>(cacheStudentLabDetailKey);
            var cacheExperienceBefore = UT.CacheManager.Get<List<StudentExperienceResponse>>(cacheExperienceKey);
            var result = await _student.SubmitCritical(student.Id, instructor.Id, request);
            var cacheStudentLabAfter = UT.CacheManager.Get<EN.StudentLaboratory>(cacheStudentLabKey);
            var cacheStudentLabDetailAfter = UT.CacheManager.Get<StudentLaboratoryDetailResponse>(cacheStudentLabDetailKey);
            var cacheExperienceAfter = UT.CacheManager.Get<List<StudentExperienceResponse>>(cacheExperienceKey);

            // Assert
            Assert.Equal(HttpStatusCode.Created, result.StatusCode);

            Assert.NotNull(cacheStudentLabBefore);
            Assert.Equal(studentLabDetail.Object, cacheStudentLabDetailBefore);
            Assert.NotNull(cacheExperienceBefore);
            Assert.Null(cacheStudentLabAfter);
            Assert.Null(cacheStudentLabDetailAfter);
            Assert.Null(cacheExperienceAfter);
        }

        [Fact]
        public async Task GetStudentLabImage()
        {
            // Arrange
            var course = await CourseGenerator.Generate(context);
            var instructor = await InstructorGenerator.Generate(context, appSetting);
            var student = await StudentGenerator.Generate(context, appSetting, new EN.Student
            {
                CourseId = course.Id
            });

            var laboratory = await LaboratoryGenerator.Generate(context, new EN.Laboratory
            {
                CourseId = course.Id
            });

            var labImageA = await LaboratoryImageGenerator.Generate(context, new EN.LaboratoryImage
            {
                LaboratoryId = laboratory.Id,
                Name = faker.Name.FirstName(),
                Description = faker.Name.FirstName(),
                Sequence = 2
            });

            var labImageB = await LaboratoryImageGenerator.Generate(context, new EN.LaboratoryImage
            {
                LaboratoryId = laboratory.Id,
                Name = faker.Name.FirstName(),
                Description = faker.Name.FirstName(),
                Sequence = 1
            });

            var studentLabImage1 = await StudentLaboratoryImageGenerator.Generate(context, new EN.StudentLaboratoryImage
            {
                LaboratoryImageId = labImageB.Id,
                Image = faker.Name.FirstName(),
                Remark = faker.Name.FirstName(),
                StudentId = student.Id,
            });

            var studentLabImage2 = await StudentLaboratoryImageGenerator.Generate(context, new EN.StudentLaboratoryImage
            {
                LaboratoryImageId = labImageB.Id,
                Image = faker.Name.FirstName(),
                Remark = faker.Name.FirstName(),
                StudentId = student.Id,
            });

            // Act
            var response = await _student.GetLabImage(student.Id, laboratory.Id);
            var labImages = response.Object;

            // Assert
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
            Assert.Equal(2, labImages.Count);

            var labImage = labImages[0];
            Assert.Equal(labImageB.Id, labImage.Id);
            Assert.Equal(labImageB.Name, labImage.Name);
            Assert.Equal(labImageB.Description, labImage.Description);
            Assert.Equal(2, labImage.StudentLaboratoryImages.Count);

            var studentLabImageResponses = labImage.StudentLaboratoryImages[0];
            Assert.Equal(studentLabImage1.Id, studentLabImageResponses.Id);
            Assert.Equal(studentLabImage1.Image, studentLabImageResponses.Image);
            Assert.Equal(studentLabImage1.Remark, studentLabImageResponses.Remark);
            Assert.Null(studentLabImageResponses.InstructorName);

            studentLabImageResponses = labImage.StudentLaboratoryImages[1];
            Assert.Equal(studentLabImage2.Id, studentLabImageResponses.Id);
            Assert.Equal(studentLabImage2.Image, studentLabImageResponses.Image);
            Assert.Equal(studentLabImage2.Remark, studentLabImageResponses.Remark);
            Assert.Null(studentLabImageResponses.InstructorName);

            labImage = labImages[1];
            Assert.Equal(labImageA.Id, labImage.Id);
            Assert.Equal(labImageA.Name, labImage.Name);
            Assert.Equal(labImageA.Description, labImage.Description);
            Assert.Empty(labImage.StudentLaboratoryImages);
        }

        [Fact]
        public async Task GetStudentLabImageWhenStudentNotExisting_NotFound()
        {
            // Arrange
            var course = await CourseGenerator.Generate(context);
            var instructor = await InstructorGenerator.Generate(context, appSetting);
            var student = await StudentGenerator.Generate(context, appSetting, new EN.Student
            {
                CourseId = course.Id
            });

            var laboratory = await LaboratoryGenerator.Generate(context, new EN.Laboratory
            {
                CourseId = course.Id
            });

            var labImageA = await LaboratoryImageGenerator.Generate(context, new EN.LaboratoryImage
            {
                LaboratoryId = laboratory.Id,
                Name = faker.Name.FirstName(),
                Description = faker.Name.FirstName(),
                Sequence = 2
            });

            var labImageB = await LaboratoryImageGenerator.Generate(context, new EN.LaboratoryImage
            {
                LaboratoryId = laboratory.Id,
                Name = faker.Name.FirstName(),
                Description = faker.Name.FirstName(),
                Sequence = 1
            });

            var studentLabImage1 = await StudentLaboratoryImageGenerator.Generate(context, new EN.StudentLaboratoryImage
            {
                LaboratoryImageId = labImageB.Id,
                Image = faker.Name.FirstName(),
                Remark = faker.Name.FirstName(),
                StudentId = student.Id,
            });

            // Act
            var response = await _student.GetLabImage(100, laboratory.Id);

            // Assert
            Assert.Equal(HttpStatusCode.NotFound, response.StatusCode);
        }

        [Fact]
        public async Task GetStudentLabImageWhenLaboratoryNotExisting_NotFound()
        {
            // Arrange
            var course = await CourseGenerator.Generate(context);
            var instructor = await InstructorGenerator.Generate(context, appSetting);
            var student = await StudentGenerator.Generate(context, appSetting, new EN.Student
            {
                CourseId = course.Id
            });

            var laboratory = await LaboratoryGenerator.Generate(context, new EN.Laboratory
            {
                CourseId = course.Id
            });

            var labImageA = await LaboratoryImageGenerator.Generate(context, new EN.LaboratoryImage
            {
                LaboratoryId = laboratory.Id,
                Name = faker.Name.FirstName(),
                Description = faker.Name.FirstName(),
                Sequence = 2
            });

            var labImageB = await LaboratoryImageGenerator.Generate(context, new EN.LaboratoryImage
            {
                LaboratoryId = laboratory.Id,
                Name = faker.Name.FirstName(),
                Description = faker.Name.FirstName(),
                Sequence = 1
            });

            var studentLabImage1 = await StudentLaboratoryImageGenerator.Generate(context, new EN.StudentLaboratoryImage
            {
                LaboratoryImageId = labImageB.Id,
                Image = faker.Name.FirstName(),
                Remark = faker.Name.FirstName(),
                StudentId = student.Id,
            });

            var studentLabImage2 = await StudentLaboratoryImageGenerator.Generate(context, new EN.StudentLaboratoryImage
            {
                LaboratoryImageId = labImageB.Id,
                Image = faker.Name.FirstName(),
                Remark = faker.Name.FirstName(),
                StudentId = student.Id,
            });

            // Act
            var response = await _student.GetLabImage(student.Id, 100);

            // Assert
            Assert.Equal(HttpStatusCode.NotFound, response.StatusCode);
        }

        [Fact]
        public async Task GetStudentLabImageWhenLaboratoryImageNotExisting_OKEmpty()
        {
            // Arrange
            var course = await CourseGenerator.Generate(context);
            var instructor = await InstructorGenerator.Generate(context, appSetting);
            var student = await StudentGenerator.Generate(context, appSetting, new EN.Student
            {
                CourseId = course.Id
            });

            var laboratory = await LaboratoryGenerator.Generate(context, new EN.Laboratory
            {
                CourseId = course.Id
            });


            // Act
            var response = await _student.GetLabImage(student.Id, laboratory.Id);
            var labImages = response.Object;

            // Assert
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
            Assert.Empty(labImages);
        }

        [Fact]
        public async Task CheckCacheGetStudentLabImage()
        {
            // Arrange
            var course = await CourseGenerator.Generate(context);
            var instructor = await InstructorGenerator.Generate(context, appSetting);
            var student = await StudentGenerator.Generate(context, appSetting, new EN.Student
            {
                CourseId = course.Id
            });

            var laboratory = await LaboratoryGenerator.Generate(context, new EN.Laboratory
            {
                CourseId = course.Id
            });

            var labImageA = await LaboratoryImageGenerator.Generate(context, new EN.LaboratoryImage
            {
                LaboratoryId = laboratory.Id,
                Name = faker.Name.FirstName(),
                Description = faker.Name.FirstName(),
                Sequence = 2
            });

            var labImageB = await LaboratoryImageGenerator.Generate(context, new EN.LaboratoryImage
            {
                LaboratoryId = laboratory.Id,
                Name = faker.Name.FirstName(),
                Description = faker.Name.FirstName(),
                Sequence = 1
            });

            var studentLabImage1 = await StudentLaboratoryImageGenerator.Generate(context, new EN.StudentLaboratoryImage
            {
                LaboratoryImageId = labImageB.Id,
                Image = faker.Name.FirstName(),
                Remark = faker.Name.FirstName(),
                StudentId = student.Id,
            });

            var studentLabImage2 = await StudentLaboratoryImageGenerator.Generate(context, new EN.StudentLaboratoryImage
            {
                LaboratoryImageId = labImageB.Id,
                Image = faker.Name.FirstName(),
                Remark = faker.Name.FirstName(),
                StudentId = student.Id,
            });

            // Act
            var cacheKey = UT.CacheKey.Student.LaboratoryImage(student.Id, laboratory.Id);

            var cacheBefore = UT.CacheManager.Get<List<LaboratoryImageResponse>>(cacheKey);
            var response = await _student.GetLabImage(student.Id, laboratory.Id);
            var labImages = response.Object;
            var cacheAfter = UT.CacheManager.Get<List<LaboratoryImageResponse>>(cacheKey);

            // Assert
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
            Assert.Null(cacheBefore);
            Assert.Equal(labImages, cacheAfter);
        }

        [Fact]
        public async Task ConfirmStudentLabImageAndGet()
        {
            // Arrange
            var course = await CourseGenerator.Generate(context);
            var instructor = await InstructorGenerator.Generate(context, appSetting);
            var student = await StudentGenerator.Generate(context, appSetting, new EN.Student
            {
                CourseId = course.Id
            });

            var laboratory = await LaboratoryGenerator.Generate(context, new EN.Laboratory
            {
                CourseId = course.Id
            });

            var labImageA = await LaboratoryImageGenerator.Generate(context, new EN.LaboratoryImage
            {
                LaboratoryId = laboratory.Id,
                Name = faker.Name.FirstName(),
                Description = faker.Name.FirstName(),
                Sequence = 2
            });

            var studentLabImage = await StudentLaboratoryImageGenerator.Generate(context, new EN.StudentLaboratoryImage
            {
                LaboratoryImageId = labImageA.Id,
                Image = faker.Name.FirstName(),
                Remark = faker.Name.FirstName(),
                StudentId = student.Id,
            });

            //Act
            var confrimResponse = await _student.ConfirmLabImage(instructor.Id, laboratory.Id, studentLabImage.Id);
            var getResponse = await _student.GetLabImage(student.Id, laboratory.Id);
            var labImages = getResponse.Object;

            // Assert
            Assert.Equal(HttpStatusCode.OK, confrimResponse.StatusCode);
            Assert.Equal(HttpStatusCode.OK, getResponse.StatusCode);
            Assert.Single(labImages);

            var labImage = labImages[0];
            Assert.Equal(labImageA.Id, labImage.Id);
            Assert.Equal(labImageA.Name, labImage.Name);
            Assert.Equal(labImageA.Description, labImage.Description);

            Assert.Single(labImage.StudentLaboratoryImages);
            var studentLabImageResponse = labImage.StudentLaboratoryImages[0];
            Assert.Equal(studentLabImage.Id, studentLabImageResponse.Id);
            Assert.Equal(studentLabImage.Image, studentLabImageResponse.Image);
            Assert.Equal(studentLabImage.Remark, studentLabImageResponse.Remark);
            Assert.Equal($"{instructor.Firstname} {instructor.Lastname}", studentLabImageResponse.InstructorName);
        }

        [Fact]
        public async Task CancelConfirmStudentLabImageAndGet()
        {
            // Arrange
            var course = await CourseGenerator.Generate(context);
            var instructor = await InstructorGenerator.Generate(context, appSetting);
            var student = await StudentGenerator.Generate(context, appSetting, new EN.Student
            {
                CourseId = course.Id
            });

            var laboratory = await LaboratoryGenerator.Generate(context, new EN.Laboratory
            {
                CourseId = course.Id
            });

            var labImageA = await LaboratoryImageGenerator.Generate(context, new EN.LaboratoryImage
            {
                LaboratoryId = laboratory.Id,
                Name = faker.Name.FirstName(),
                Description = faker.Name.FirstName(),
                Sequence = 2
            });

            var studentLabImage = await StudentLaboratoryImageGenerator.Generate(context, new EN.StudentLaboratoryImage
            {
                LaboratoryImageId = labImageA.Id,
                Image = faker.Name.FirstName(),
                Remark = faker.Name.FirstName(),
                StudentId = student.Id,
                InstructorConfirmId = instructor.Id
            });

            //Act
            var confrimResponse = await _student.ConfirmLabImage(instructor.Id, laboratory.Id, studentLabImage.Id);
            var getResponse = await _student.GetLabImage(student.Id, laboratory.Id);
            var labImages = getResponse.Object;

            // Assert
            Assert.Equal(HttpStatusCode.OK, confrimResponse.StatusCode);
            Assert.Equal(HttpStatusCode.OK, getResponse.StatusCode);
            Assert.Single(labImages);

            var labImage = labImages[0];
            Assert.Equal(labImageA.Id, labImage.Id);
            Assert.Equal(labImageA.Name, labImage.Name);
            Assert.Equal(labImageA.Description, labImage.Description);

            Assert.Single(labImage.StudentLaboratoryImages);
            var studentLabImageResponse = labImage.StudentLaboratoryImages[0];
            Assert.Equal(studentLabImage.Id, studentLabImageResponse.Id);
            Assert.Equal(studentLabImage.Image, studentLabImageResponse.Image);
            Assert.Equal(studentLabImage.Remark, studentLabImageResponse.Remark);
            Assert.Null(studentLabImageResponse.InstructorName);
        }

        [Fact]
        public async Task CheckRemoveCacheWhenInstructorConfirmLabImage()
        {
            // Arrange
            var course = await CourseGenerator.Generate(context);
            var instructor = await InstructorGenerator.Generate(context, appSetting);
            var student = await StudentGenerator.Generate(context, appSetting, new EN.Student
            {
                CourseId = course.Id
            });

            var laboratory = await LaboratoryGenerator.Generate(context, new EN.Laboratory
            {
                CourseId = course.Id
            });

            var labImageA = await LaboratoryImageGenerator.Generate(context, new EN.LaboratoryImage
            {
                LaboratoryId = laboratory.Id,
                Name = faker.Name.FirstName(),
                Description = faker.Name.FirstName(),
                Sequence = 2
            });

            var studentLabImage = await StudentLaboratoryImageGenerator.Generate(context, new EN.StudentLaboratoryImage
            {
                LaboratoryImageId = labImageA.Id,
                Image = faker.Name.FirstName(),
                Remark = faker.Name.FirstName(),
                StudentId = student.Id,
            });

            //Act
            var cacheKey = UT.CacheKey.Student.LaboratoryImage(student.Id, laboratory.Id);
            UT.CacheManager.Set(cacheKey, new object());

            var cacheBefore = UT.CacheManager.Get<object>(cacheKey);
            var confrimResponse = await _student.ConfirmLabImage(instructor.Id, laboratory.Id, studentLabImage.Id);
            var cacheAfter = UT.CacheManager.Get<object>(cacheKey);

            //Assert
            Assert.NotNull(cacheBefore);
            Assert.Null(cacheAfter);
        }

        [Fact]
        public async Task SaveLabscopeImageWhenNoStudentLab_CreateNewStudentLab()
        {
            // Arrange
            await TU.Preset.CommonPreset.Generate(context);

            var course = await CourseGenerator.Generate(context);
            var instructor = await InstructorGenerator.Generate(context, appSetting);
            var student = await StudentGenerator.Generate(context, appSetting, new EN.Student
            {
                CourseId = course.Id
            });

            var labPreset = await TU.Preset.LabPreset.PrepareGoodLab(context, appSetting, new TU.Preset.LabPreset.Data
            {
                TotalTopic = 1,
                TotalSubTopic = 1,
                GoodScore = 1,
                FairScore = 1
            });

            var url = faker.Name.FirstName();

            // Act
            await _student.SaveLabScopeImage(student.Id, labPreset.Laboratory.Id, url);
            var studentLabs = await context.StudentLaboratories.Select(sl => sl).ToListAsync();

            // Assert
            Assert.Single(studentLabs);
            var studentLab = studentLabs[0];
            Assert.Equal(student.Id, studentLab.StudentId);
            Assert.Equal(labPreset.Laboratory.Id, studentLab.LaboratoryId);
            Assert.Equal(UT.Status.Active.Id, studentLab.StatusId);
            Assert.Null(studentLab.KindOfWork);
            Assert.Null(studentLab.Material);
            Assert.Null(studentLab.Tooth);
            Assert.Equal(url, studentLab.Image);
        }

        [Fact]
        public async Task SaveLabscopeImageWhenStudentLabExist_UpdateStudentLab()
        {
            // Arrange
            await TU.Preset.CommonPreset.Generate(context);

            var course = await CourseGenerator.Generate(context);
            var instructor = await InstructorGenerator.Generate(context, appSetting);
            var student = await StudentGenerator.Generate(context, appSetting, new EN.Student
            {
                CourseId = course.Id
            });

            var labPreset = await TU.Preset.LabPreset.PrepareGoodLab(context, appSetting, new TU.Preset.LabPreset.Data
            {
                TotalTopic = 1,
                TotalSubTopic = 1,
                GoodScore = 1,
                FairScore = 1
            });

            var studentLaboratory = await StudentLaboratoryGenerator.Generate(context, new EN.StudentLaboratory
            {
                LaboratoryId = labPreset.Laboratory.Id,
                StudentId = student.Id,
                KindOfWork = faker.Name.FirstName(),
                Material = faker.Name.FirstName(),
                Tooth = faker.Name.FirstName(),
                
            });

            var url = faker.Name.FirstName();

            // Act
            await _student.SaveLabScopeImage(student.Id, labPreset.Laboratory.Id, url);
            var studentLabs = await context.StudentLaboratories.Select(sl => sl).ToListAsync();

            // Assert
            Assert.Single(studentLabs);
            var studentLab = studentLabs[0];
            Assert.Equal(studentLaboratory.StudentId, studentLab.StudentId);
            Assert.Equal(studentLaboratory.LaboratoryId, studentLab.LaboratoryId);
            Assert.Equal(studentLaboratory.StatusId, studentLab.StatusId);
            Assert.Equal(studentLaboratory.KindOfWork, studentLab.KindOfWork);
            Assert.Equal(studentLaboratory.Material, studentLab.Material);
            Assert.Equal(studentLaboratory.Tooth, studentLab.Tooth);
            Assert.Equal(url, studentLab.Image);
        }

        [Fact]
        public async Task CheckCacheClearWhenSabLabscopeImage()
        {
            // Arrange
            await TU.Preset.CommonPreset.Generate(context);

            var course = await CourseGenerator.Generate(context);
            var instructor = await InstructorGenerator.Generate(context, appSetting);
            var student = await StudentGenerator.Generate(context, appSetting, new EN.Student
            {
                CourseId = course.Id
            });

            var labPreset = await TU.Preset.LabPreset.PrepareGoodLab(context, appSetting, new TU.Preset.LabPreset.Data
            {
                TotalTopic = 1,
                TotalSubTopic = 1,
                GoodScore = 1,
                FairScore = 1
            });

            var url = faker.Name.FirstName();

            var cacheLabKey = UT.CacheKey.Laboratory.Student(labPreset.Laboratory.Id, student.Id);
            var cacheStudentKey = UT.CacheKey.Student.LaboratoryDetail(student.Id, labPreset.Laboratory.Id);
            UT.CacheManager.Set(cacheLabKey, new object());
            UT.CacheManager.Set(cacheStudentKey, new object());

            // Act
            var cacheLabBefore = UT.CacheManager.Get<object>(cacheLabKey);
            var cacheStudentBefore = UT.CacheManager.Get<object>(cacheStudentKey);
            await _student.SaveLabScopeImage(student.Id, labPreset.Laboratory.Id, url);
            var cacheLabAfter = UT.CacheManager.Get<object>(cacheLabKey);
            var cacheStudentAfter = UT.CacheManager.Get<object>(cacheStudentKey);

            // Assert
            Assert.NotNull(cacheLabBefore);
            Assert.NotNull(cacheStudentBefore);
            Assert.Null(cacheLabAfter);
            Assert.Null(cacheStudentAfter);
        }

        [Theory]
        [InlineData("abd123", "Abd123",true)]
        [InlineData("abd123", "ABD123",true)]
        [InlineData("abd123", "abD123",true)]
        [InlineData("abd123", "ab123",false)]
        public async Task IsUsernameExisting(string inputUsername, string checkUsername, bool expectResult)
        {
            // Arrange
            var student = await StudentGenerator.Generate(context, appSetting, new EN.Student
            {
                Username = inputUsername,
            });

            // Act
            var result = await _student.IsUsernameExisting(checkUsername);

            // Assert
            Assert.Equal(result, expectResult);

        }

        [Fact]
        public async Task ConfirmLabScopeImageAndGet()
        {
            // Arrange
            var course = await CourseGenerator.Generate(context);
            var instructor = await InstructorGenerator.Generate(context, appSetting);
            var student = await StudentGenerator.Generate(context, appSetting, new EN.Student
            {
                CourseId = course.Id
            });

            var laboratory = await LaboratoryGenerator.Generate(context, new EN.Laboratory
            {
                CourseId = course.Id
            });

            var studentLab = await StudentLaboratoryGenerator.Generate(context, new EN.StudentLaboratory
            {
                StudentId = student.Id,
                LaboratoryId = laboratory.Id,
                KindOfWork = faker.Name.FirstName(),
                Material = faker.Name.FirstName(),
                Tooth = faker.Name.FirstName(),
                Image = faker.Name.FirstName(),
            });

            // Act
            var response = await _student.ConfirmLabScopeImage(student.Id, instructor.Id, laboratory.Id);
            var studentLabDetail = (await _student.GetLabDetail(student.Id, laboratory.Id)).Object;

            // Assert
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
            Assert.Equal(studentLab.Id, studentLabDetail.Id);
            Assert.Equal(studentLab.Image, studentLabDetail.Image);
            Assert.Equal(studentLab.KindOfWork, studentLabDetail.KindOfWork);
            Assert.Equal(studentLab.Material, studentLabDetail.Material);
            Assert.Equal(studentLab.Tooth, studentLabDetail.Tooth);
            Assert.Equal($"{instructor.Firstname} {instructor.Lastname}", studentLabDetail.InstructorName);            
        }

        [Fact]
        public async Task CancelConfirmLabScopeImageAndGet()
        {
            // Arrange
            var course = await CourseGenerator.Generate(context);
            var instructor = await InstructorGenerator.Generate(context, appSetting);
            var student = await StudentGenerator.Generate(context, appSetting, new EN.Student
            {
                CourseId = course.Id
            });

            var laboratory = await LaboratoryGenerator.Generate(context, new EN.Laboratory
            {
                CourseId = course.Id
            });

            var studentLab = await StudentLaboratoryGenerator.Generate(context, new EN.StudentLaboratory
            {
                StudentId = student.Id,
                LaboratoryId = laboratory.Id,
                KindOfWork = faker.Name.FirstName(),
                Material = faker.Name.FirstName(),
                Tooth = faker.Name.FirstName(),
                Image = faker.Name.FirstName(),
                InstructorConfirmId = instructor.Id
            });

            // Act
            var response = await _student.ConfirmLabScopeImage(student.Id, instructor.Id, laboratory.Id);
            var studentLabDetail = (await _student.GetLabDetail(student.Id, laboratory.Id)).Object;

            // Assert
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
            Assert.Equal(studentLab.Id, studentLabDetail.Id);
            Assert.Equal(studentLab.Image, studentLabDetail.Image);
            Assert.Equal(studentLab.KindOfWork, studentLabDetail.KindOfWork);
            Assert.Equal(studentLab.Material, studentLabDetail.Material);
            Assert.Equal(studentLab.Tooth, studentLabDetail.Tooth);
            Assert.Null(studentLabDetail.InstructorName);
        }

        [Fact]
        public async Task CheckCacheRemoveConfirmLabScopeImage()
        {
            // Arrange
            var course = await CourseGenerator.Generate(context);
            var instructor = await InstructorGenerator.Generate(context, appSetting);
            var student = await StudentGenerator.Generate(context, appSetting, new EN.Student
            {
                CourseId = course.Id
            });

            var laboratory = await LaboratoryGenerator.Generate(context, new EN.Laboratory
            {
                CourseId = course.Id
            });

            var studentLab = await StudentLaboratoryGenerator.Generate(context, new EN.StudentLaboratory
            {
                StudentId = student.Id,
                LaboratoryId = laboratory.Id,
                KindOfWork = faker.Name.FirstName(),
                Material = faker.Name.FirstName(),
                Tooth = faker.Name.FirstName(),
                Image = faker.Name.FirstName(),
                InstructorConfirmId = instructor.Id
            });

            // Act
            var cacheKey = UT.CacheKey.Student.LaboratoryDetail(student.Id, laboratory.Id);
            UT.CacheManager.Set(cacheKey, new object());

            var cacheBefore = UT.CacheManager.Get<object>(cacheKey);
            var response = await _student.ConfirmLabScopeImage(student.Id, instructor.Id, laboratory.Id);
            var cacheAfter = UT.CacheManager.Get<object>(cacheKey);

            // Assert
            Assert.NotNull(cacheBefore);
            Assert.Null(cacheAfter);
        }
    }
}
