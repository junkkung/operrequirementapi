﻿using Database;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using UT = Utilities;
using EN = Database.Entities;
using FN = Models.Functions;
using Database;
using Microsoft.EntityFrameworkCore;

namespace _Test.Utilities.Generator
{
    public class CriteriaTypeGenerator
    {
        public static async Task Generate(OperRequirementContext context)
        {
            var criteriaTypes = new List<EN.CriteriaType>();
            foreach (var data in FN.Criteria.Type.CriteriaTypeData)
            {
                criteriaTypes.Add(new EN.CriteriaType { Name = data.Key });
            }

            await context.AddRangeAsync(criteriaTypes);
            await context.SaveChangesAsync();
            foreach(var type in criteriaTypes)
                context.Entry(type).State = EntityState.Detached;

        }
    }
}
