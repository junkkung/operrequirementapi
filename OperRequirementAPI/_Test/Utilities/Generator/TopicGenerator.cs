﻿using System;
using System.Collections.Generic;
using System.Text;
using FN = Models.Functions;
using EN = Database.Entities;
using UT = Utilities;
using Database;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using Bogus;

namespace _Test.Utilities.Generator
{
    public class TopicGenerator
    {
        public static async Task<EN.Topic> Generate(OperRequirementContext context, EN.Topic topic = null)
        {
            var faker = new Faker();
            if (topic is null) topic = new EN.Topic();
            topic.Name = ValueGenerator.Generate(topic.Name, faker.Name.FirstName());
            topic.Description = ValueGenerator.Generate(topic.Description, faker.Name.FirstName());
            topic.StatusId = ValueGenerator.Generate(topic.StatusId, UT.Status.Active.Id);
            topic.LaboratoryId = topic.LaboratoryId == 0 ? (await LaboratoryGenerator.Generate(context)).Id : topic.LaboratoryId;
            topic.Sequence = ValueGenerator.Generate(topic.Sequence, 1);
            topic.ExperienceId = topic.ExperienceId == 0 ? (await ExperienceGenerator.Generate(context)).Id :
                                    topic.ExperienceId;

            await context.AddAsync(topic);
            await context.SaveChangesAsync();
            context.Entry(topic).State = EntityState.Detached;

            return topic;
        }

        public static async Task<List<EN.Topic>> Generate(OperRequirementContext context, int laboratoryId, int experienceId, int total)
        {
            var topics = new List<EN.Topic>();
            for (var i = 0; i < total; i++)
                topics.Add(await Generate(context, new EN.Topic
                {
                    LaboratoryId = laboratoryId,
                    ExperienceId = experienceId
                }));

            return topics;
        }
    }
}
