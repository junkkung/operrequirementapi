﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using UT = Utilities;
using EN = Database.Entities;
using FN = Models.Functions;
using Database;
using Microsoft.EntityFrameworkCore;
using Bogus;
using Microsoft.Extensions.Configuration;

namespace _Test.Utilities.Generator
{
    public class StudentScoreGenerator
    {
        public static async Task<EN.StudentScore> Generate(OperRequirementContext context, IConfiguration appSetting, EN.StudentScore studentScore)
        {
            studentScore.InstructorId = studentScore.InstructorId == 0 ? (await InstructorGenerator.Generate(context, appSetting)).Id :
                                            studentScore.InstructorId;
            studentScore.StatusId = ValueGenerator.Generate(studentScore.StatusId, UT.Status.Active.Id);
            studentScore.CriteriaId = ValueGenerator.ValueOrNull(studentScore.CriteriaId);

            await context.AddRangeAsync(studentScore);
            await context.SaveChangesAsync();
            context.Entry(studentScore).State = EntityState.Detached;

            return studentScore;
        }
    }
}
