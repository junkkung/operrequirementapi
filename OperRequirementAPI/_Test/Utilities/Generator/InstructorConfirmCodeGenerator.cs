﻿using Bogus;
using Database;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using EN = Database.Entities;

namespace _Test.Utilities.Generator
{
    public class InstructorConfirmCodeGenerator
    {
        public static async Task<EN.InstructorConfirmCode> Generate (OperRequirementContext context, EN.InstructorConfirmCode code = null)
        {
            var faker = new Faker();
            if (code is null) code = new EN.InstructorConfirmCode();
            code.Code = ValueGenerator.Generate(code.Code, faker.Random.String(10));
            await context.AddAsync(code);
            await context.SaveChangesAsync();
            context.Entry(code).State = EntityState.Detached;

            return code;
        }
    }
}
