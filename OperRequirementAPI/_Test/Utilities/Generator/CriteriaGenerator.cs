﻿using Bogus;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using FN = Models.Functions;
using EN = Database.Entities;
using UT = Utilities;
using Database;
using Microsoft.EntityFrameworkCore;

namespace _Test.Utilities.Generator
{
    public class CriteriaGenerator
    {
        public static async Task<EN.Criteria> Generate(OperRequirementContext context, EN.Criteria criteria = null)
        {
            var faker = new Faker();
            if (criteria is null) criteria = new EN.Criteria();
            criteria.Name = ValueGenerator.Generate(criteria.Name, faker.Name.FirstName());
            criteria.CriteriaTypeId = ValueGenerator.Generate(criteria.CriteriaTypeId, FN.Criteria.Type.Good.Id);
            criteria.StatusId = ValueGenerator.Generate(criteria.StatusId, UT.Status.Active.Id);
            criteria.SubTopicId = criteria.SubTopicId != 0 ? criteria.SubTopicId : (await SubTopicGenerator.Generate(context)).Id;

            await context.AddAsync(criteria);
            await context.SaveChangesAsync();
            context.Entry(criteria).State = EntityState.Detached;

            return criteria;
        }
    }
}
