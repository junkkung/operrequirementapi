﻿using System;
using System.Collections.Generic;
using System.Text;
using EN = Database.Entities;
using UT = Utilities;
using Database;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using Bogus;

namespace _Test.Utilities.Generator
{
    public class ConfigGenerator
    {
        public static async Task<EN.Config> Generate(OperRequirementContext context, EN.Config config)
        {
            var faker = new Faker();
            if (config is null) config = new EN.Config();
            config.Name = ValueGenerator.Generate(config.Name, faker.Name.FindName());
            config.Description = ValueGenerator.Generate(config.Name, faker.Name.FindName());
            config.Unit = ValueGenerator.Generate(config.Name, faker.Name.FindName());

            await context.AddAsync(config);
            await context.SaveChangesAsync();
            context.Entry(config).State = EntityState.Detached;

            return config;
        }
    }
}
