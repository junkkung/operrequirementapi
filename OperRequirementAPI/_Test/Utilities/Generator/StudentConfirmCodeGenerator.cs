﻿using Bogus;
using Database;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using EN = Database.Entities;


namespace _Test.Utilities.Generator
{
    public class StudentConfirmCodeGenerator
    {
        public static async Task<EN.StudentConfrimCode> Generate(OperRequirementContext context, EN.StudentConfrimCode code = null)
        {
            var faker = new Faker();
            if (code is null) code = new EN.StudentConfrimCode();
            code.Code = ValueGenerator.Generate(code.Code, faker.Random.String(10));
            code.CourseId = code.CourseId != 0 ? code.CourseId : (await CourseGenerator.Generate(context)).Id;
            await context.AddAsync(code);
            await context.SaveChangesAsync();
            context.Entry(code).State = EntityState.Detached;

            return code;
        }
    }
}
