﻿using Bogus;
using Database;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using EN = Database.Entities;
using UT = Utilities;

namespace _Test.Utilities.Generator
{
    public class InstructorGenerator
    {
        public static async Task<EN.Instructor> Generate (OperRequirementContext context, IConfiguration appSetting, EN.Instructor instructor = null)
        {
            var faker = new Faker();
            if (instructor is null) instructor = new EN.Instructor();
            instructor.Firstname = ValueGenerator.Generate(instructor.Firstname, faker.Name.FirstName());
            instructor.Lastname = ValueGenerator.Generate(instructor.Lastname, faker.Name.LastName());
            instructor.Username = ValueGenerator.Generate(instructor.Username, faker.Internet.UserName()).ToLower();
            instructor.Password = ValueGenerator.Generate(
                UT.Crypto.EncryptPassword(appSetting, instructor.Username, instructor.Password), 
                UT.Crypto.EncryptPassword(appSetting, instructor.Username, faker.Internet.Password()));
            instructor.Image = ValueGenerator.ValueOrNull(instructor.Image);
            instructor.Email = ValueGenerator.Generate(instructor.Email, faker.Internet.Email());
            instructor.Telephone = ValueGenerator.Generate(instructor.Telephone, faker.Phone.PhoneNumber());
            instructor.StatusId = ValueGenerator.Generate(instructor.StatusId, UT.Status.Active.Id);
            instructor.IsAdmin = ValueGenerator.ValueOrNull(instructor.IsAdmin);
            instructor.RefreshToken = ValueGenerator.ValueOrNull(instructor.RefreshToken);

            await context.AddAsync(instructor);
            await context.SaveChangesAsync();
            context.Entry(instructor).State = EntityState.Detached;

            return instructor;
        }
    }
}
