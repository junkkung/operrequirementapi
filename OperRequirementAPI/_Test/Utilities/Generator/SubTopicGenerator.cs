﻿using System;
using System.Collections.Generic;
using System.Text;
using FN = Models.Functions;
using EN = Database.Entities;
using UT = Utilities;
using Database;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using Bogus;

namespace _Test.Utilities.Generator
{
    public class SubTopicGenerator
    {
        public static async Task<EN.SubTopic> Generate (OperRequirementContext context, EN.SubTopic subTopic = null)
        {
            var faker = new Faker();
            if (subTopic is null) subTopic = new EN.SubTopic();
            subTopic.Name = ValueGenerator.Generate(subTopic.Name, faker.Name.FirstName());
            subTopic.Description = ValueGenerator.Generate(subTopic.Description, faker.Name.FirstName());
            subTopic.StatusId = ValueGenerator.Generate(subTopic.StatusId, UT.Status.Active.Id);
            subTopic.TopicId = subTopic.TopicId == 0 ? (await TopicGenerator.Generate(context)).Id : subTopic.TopicId;
            subTopic.Sequence = ValueGenerator.Generate(subTopic.Sequence, 1);
            subTopic.CriticalId = subTopic.CriticalId == 0 ? (await CriticalGenerator.Generate(context)).Id :
                                    subTopic.CriticalId;

            await context.AddAsync(subTopic);
            await context.SaveChangesAsync();
            context.Entry(subTopic).State = EntityState.Detached;

            return subTopic;
        }

        public static async Task<List<EN.SubTopic>> Generate(OperRequirementContext context, int topicId, int total)
        {
            var subTopics = new List<EN.SubTopic>();
            for (var i = 0; i < total; i++)
                subTopics.Add(await Generate(context, new EN.SubTopic
                {
                    TopicId = topicId
                }));

            return subTopics;
        }
    }
}
