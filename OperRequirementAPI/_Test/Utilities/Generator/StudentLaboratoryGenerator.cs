﻿using System;
using System.Collections.Generic;
using System.Text;
using FN = Models.Functions;
using EN = Database.Entities;
using UT = Utilities;
using Database;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using Bogus;

namespace _Test.Utilities.Generator
{
    public class StudentLaboratoryGenerator
    {
        public static async Task<EN.StudentLaboratory> Generate (OperRequirementContext context, EN.StudentLaboratory studentLaboratory)
        {
            studentLaboratory.StatusId = ValueGenerator.Generate(studentLaboratory.StatusId, UT.Status.Active.Id);

            await context.AddAsync(studentLaboratory);
            await context.SaveChangesAsync();
            context.Entry(studentLaboratory).State = EntityState.Detached;

            return studentLaboratory;
        }
    }
}
