﻿using System;
using System.Collections.Generic;
using System.Text;
using FN = Models.Functions;
using EN = Database.Entities;
using UT = Utilities;
using Database;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using Bogus;

namespace _Test.Utilities.Generator
{
    public class LaboratoryGenerator
    {
        public static async Task<EN.Laboratory> Generate(OperRequirementContext context, EN.Laboratory laboratory = null)
        {
            var faker = new Faker();
            if (laboratory is null) laboratory = new EN.Laboratory();
            laboratory.Name = ValueGenerator.Generate(laboratory.Name, faker.Name.FirstName());
            laboratory.Description = ValueGenerator.Generate(laboratory.Description, faker.Name.FirstName());
            laboratory.StatusId = ValueGenerator.Generate(laboratory.StatusId, UT.Status.Active.Id);
            laboratory.Sequence = ValueGenerator.Generate(laboratory.Sequence, 1);
            laboratory.CourseId = laboratory.CourseId == 0 ? (await CourseGenerator.Generate(context)).Id : laboratory.CourseId;

            await context.AddAsync(laboratory);
            await context.SaveChangesAsync();
            context.Entry(laboratory).State = EntityState.Detached;

            return laboratory;
        }
    }
}
