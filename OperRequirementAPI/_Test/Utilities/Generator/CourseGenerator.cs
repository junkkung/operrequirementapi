﻿using Bogus;
using Database;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using EN = Database.Entities;
using UT = Utilities;

namespace _Test.Utilities.Generator
{
    public class CourseGenerator
    {
        public static async Task<EN.Course> Generate (OperRequirementContext context, EN.Course course = null)
        {
            var faker = new Faker();
            if (course is null) course = new EN.Course();
            course.Name = ValueGenerator.Generate(course.Name, faker.Name.FirstName());
            course.SemesterYear = ValueGenerator.Generate(course.SemesterYear, faker.Random.Number(2000, 2010));
            course.StartDate = ValueGenerator.Generate(course.StartDate, DateTime.Now);
            course.EndDate = ValueGenerator.Generate(course.EndDate, DateTime.Now.AddMinutes(1));
            course.StatusId = ValueGenerator.Generate(course.StatusId, UT.Status.Active.Id);

            await context.AddAsync(course);
            await context.SaveChangesAsync();
            context.Entry(course).State = EntityState.Detached;

            return course;
        }
    }
}
