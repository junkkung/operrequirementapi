﻿using System;
using System.Collections.Generic;
using System.Text;
using FN = Models.Functions;
using EN = Database.Entities;
using UT = Utilities;
using Database;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using Bogus;

namespace _Test.Utilities.Generator
{
    public class ExperienceGenerator
    {
        public static async Task<EN.Experience> Generate(OperRequirementContext context, EN.Experience experience = null)
        {
            var faker = new Faker();
            if (experience is null) experience = new EN.Experience();
            experience.Name = ValueGenerator.Generate(experience.Name, faker.Name.FirstName());
            experience.Description = ValueGenerator.Generate(experience.Description, faker.Name.FirstName());
            experience.StatusId = ValueGenerator.Generate(experience.StatusId, UT.Status.Active.Id);

            await context.AddAsync(experience);
            await context.SaveChangesAsync();
            context.Entry(experience).State = EntityState.Detached;

            return experience;
        }
    }
}
