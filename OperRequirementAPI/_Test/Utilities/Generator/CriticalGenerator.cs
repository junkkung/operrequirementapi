﻿using System;
using System.Collections.Generic;
using System.Text;
using FN = Models.Functions;
using EN = Database.Entities;
using UT = Utilities;
using Database;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using Bogus;

namespace _Test.Utilities.Generator
{
    public class CriticalGenerator
    {
        public static async Task<EN.Critical> Generate(OperRequirementContext context, EN.Critical critical = null)
        {
            var faker = new Faker();
            if (critical is null) critical = new EN.Critical();
            critical.Name = ValueGenerator.Generate(critical.Name, faker.Name.FirstName());
            critical.StatusId = ValueGenerator.Generate(critical.StatusId, UT.Status.Active.Id);

            await context.AddAsync(critical);
            await context.SaveChangesAsync();
            context.Entry(critical).State = EntityState.Detached;

            return critical;
        }
    }
}
