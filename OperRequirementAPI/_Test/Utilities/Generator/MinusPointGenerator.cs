﻿using System;
using System.Collections.Generic;
using System.Text;
using FN = Models.Functions;
using EN = Database.Entities;
using UT = Utilities;
using Database;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using Bogus;

namespace _Test.Utilities.Generator
{
    public class MinusPointGenerator
    {
        public static async Task<EN.MinusPoint> Generate (OperRequirementContext context, EN.MinusPoint minusPoint = null)
        {
            var faker = new Faker();
            if (minusPoint is null) minusPoint = new EN.MinusPoint();
            minusPoint.Name = ValueGenerator.Generate(minusPoint.Name, faker.Name.FirstName());
            minusPoint.Description = ValueGenerator.Generate(minusPoint.Description, faker.Name.FirstName());
            minusPoint.StatusId = ValueGenerator.Generate(minusPoint.StatusId, UT.Status.Active.Id);

            await context.AddAsync(minusPoint);
            await context.SaveChangesAsync();
            context.Entry(minusPoint).State = EntityState.Detached;

            return minusPoint;
        }
    }
}
