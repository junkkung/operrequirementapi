﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using UT = Utilities;
using EN = Database.Entities;
using FN = Models.Functions;
using Database;
using Microsoft.EntityFrameworkCore;
using Bogus;

namespace _Test.Utilities.Generator
{
    public class StudentLaboratoryImageGenerator
    {
        public static async Task<EN.StudentLaboratoryImage> Generate (OperRequirementContext context, EN.StudentLaboratoryImage studentLaboratoryImage)
        {
            studentLaboratoryImage.StatusId = ValueGenerator.Generate(studentLaboratoryImage.StatusId, UT.Status.Active.Id);

            await context.AddRangeAsync(studentLaboratoryImage);
            await context.SaveChangesAsync();
            context.Entry(studentLaboratoryImage).State = EntityState.Detached;

            return studentLaboratoryImage;
        }
    }
}
