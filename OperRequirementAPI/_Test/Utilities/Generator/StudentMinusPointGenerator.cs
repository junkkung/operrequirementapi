﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using UT = Utilities;
using EN = Database.Entities;
using FN = Models.Functions;
using Database;
using Microsoft.EntityFrameworkCore;
using Bogus;
using Microsoft.Extensions.Configuration;

namespace _Test.Utilities.Generator
{
    public class StudentMinusPointGenerator
    {
        public static async Task<EN.StudentMinusPoint> Generate (OperRequirementContext context, IConfiguration appSetting, EN.StudentMinusPoint studentMinusPoint)
        {
            studentMinusPoint.MinusPointId = studentMinusPoint.MinusPointId == 0 ? (await MinusPointGenerator.Generate(context)).Id :
                                                studentMinusPoint.MinusPointId;
            studentMinusPoint.InstructorId = studentMinusPoint.InstructorId == 0 ? (await InstructorGenerator.Generate(context, appSetting)).Id :
                                                studentMinusPoint.InstructorId;
            studentMinusPoint.StatusId = ValueGenerator.Generate(studentMinusPoint.StatusId, UT.Status.Active.Id);

            await context.AddRangeAsync(studentMinusPoint);
            await context.SaveChangesAsync();
            context.Entry(studentMinusPoint).State = EntityState.Detached;

            return studentMinusPoint;
        }
    }
}
