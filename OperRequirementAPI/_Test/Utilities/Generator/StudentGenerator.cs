﻿using Bogus;
using Database;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using EN = Database.Entities;
using UT = Utilities;

namespace _Test.Utilities.Generator
{
    public class StudentGenerator
    {
        public static async Task<EN.Student> Generate(OperRequirementContext context, IConfiguration appSetting, EN.Student student = null)
        {
            var faker = new Faker();
            if (student is null) student = new EN.Student();
            student.Firstname = ValueGenerator.Generate(student.Firstname, faker.Name.FirstName());
            student.Lastname = ValueGenerator.Generate(student.Lastname, faker.Name.LastName());
            student.Username = ValueGenerator.Generate(student.Username, faker.Internet.UserName()).ToLower();
            student.Password = ValueGenerator.Generate(
                UT.Crypto.EncryptPassword(appSetting, student.Username, student.Password),
                UT.Crypto.EncryptPassword(appSetting, student.Username, faker.Internet.Password()));
            student.Image = ValueGenerator.ValueOrNull(student.Image);
            student.Email = ValueGenerator.Generate(student.Email, faker.Internet.Email());
            student.Telephone = ValueGenerator.Generate(student.Telephone, faker.Phone.PhoneNumber());
            student.CourseId = student.CourseId != 0 ? student.CourseId : (await CourseGenerator.Generate(context)).Id;
            student.UniversityIdNumber = ValueGenerator.Generate(student.UniversityIdNumber, faker.Name.FirstName());
            student.Number = ValueGenerator.Generate(student.Number, faker.Random.Number(1, 1000));
            student.StatusId = ValueGenerator.Generate(student.StatusId, UT.Status.Active.Id);
            student.RefreshToken = ValueGenerator.ValueOrNull(student.RefreshToken);
            student.Group = ValueGenerator.Generate(student.Group, faker.Random.Int(0, 99));

            await context.AddAsync(student);
            await context.SaveChangesAsync();
            context.Entry(student).State = EntityState.Detached;

            return student;
        }
    }
}
