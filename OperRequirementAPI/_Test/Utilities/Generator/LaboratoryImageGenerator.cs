﻿using System;
using System.Collections.Generic;
using System.Text;
using FN = Models.Functions;
using EN = Database.Entities;
using UT = Utilities;
using Database;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using Bogus;

namespace _Test.Utilities.Generator
{
    public class LaboratoryImageGenerator
    {
        public static async Task<EN.LaboratoryImage> Generate(OperRequirementContext context, EN.LaboratoryImage labImage = null)
        {
            var faker = new Faker();
            if (labImage is null) labImage = new EN.LaboratoryImage();
            labImage.Name = ValueGenerator.Generate(labImage.Name, faker.Name.FirstName());
            labImage.LaboratoryId = labImage.LaboratoryId == 0 ? (await LaboratoryGenerator.Generate(context)).Id :
                                        labImage.LaboratoryId;
            labImage.StatusId = ValueGenerator.Generate(labImage.StatusId, UT.Status.Active.Id);

            await context.AddAsync(labImage);
            await context.SaveChangesAsync();
            context.Entry(labImage).State = EntityState.Detached;

            return labImage;
        }
    }
}
