﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using UT = Utilities;
using EN = Database.Entities;
using Database;
using Microsoft.EntityFrameworkCore;

namespace _Test.Utilities.Generator
{
    public class StatusGenerator
    {
        public static async Task Generate(OperRequirementContext context)
        {
            var statuses = new List<EN.Status>();
            foreach(var data in UT.Status.statusData)
            {
                statuses.Add(new EN.Status { Name = data.Key });                
            }

            await context.AddRangeAsync(statuses);
            await context.SaveChangesAsync();
            foreach (var status in statuses)
                context.Entry(status).State = EntityState.Detached;

        }
    }
}
