﻿using System;
using System.Collections.Generic;
using System.Text;
using FN = Models.Functions;
using EN = Database.Entities;
using UT = Utilities;
using Database;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using Bogus;

namespace _Test.Utilities.Generator
{
    public class StudentSubTopicGenerator
    {
        public static async Task<EN.StudentSubTopic> Generate(OperRequirementContext context, EN.StudentSubTopic studentSubTopic)
        {
            studentSubTopic.StatusId = ValueGenerator.Generate(studentSubTopic.StatusId, UT.Status.Active.Id);

            await context.AddAsync(studentSubTopic);
            await context.SaveChangesAsync();
            context.Entry(studentSubTopic).State = EntityState.Detached;

            return studentSubTopic;
        }
    }
}
