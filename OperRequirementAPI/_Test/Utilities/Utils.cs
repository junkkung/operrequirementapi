﻿using Database;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Text;
using FN = Models.Functions;

namespace _Test.Utilities
{
    public class Utils
    {
        public static IConfiguration InitConfiguration()
        {
            var config = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json")
                .Build();

            return config;
        }

        public static OperRequirementContext GetMemoryContext(string dbName)
        {
            var serviceProvider = new ServiceCollection()
                                    .AddEntityFrameworkInMemoryDatabase()
                                    .BuildServiceProvider();

            var optionsBuilder = new DbContextOptionsBuilder<OperRequirementContext>();
            optionsBuilder.UseInMemoryDatabase(dbName)
                            .UseInternalServiceProvider(serviceProvider);
            var context = new OperRequirementContext(optionsBuilder.Options);

            return context;
        }

        public static OperRequirementContext GetDbContext(string dbName, IConfiguration appConfig)
        {
            var optionsBuilder = new DbContextOptionsBuilder<OperRequirementContext>();
            var connectionString = appConfig.GetConnectionString("DefaultConnection");
            optionsBuilder.UseSqlServer(connectionString.Replace("operrequirement", dbName));
            var context = new OperRequirementContext(optionsBuilder.Options);

            return context;
        }     

        public static DefaultHttpContext AddClaimPrinciple(int identifier, string role)
        {
            return new DefaultHttpContext()
            {
                User = new ClaimsPrincipal(new ClaimsIdentity(new Claim[]
                {
                    new Claim("identifier", identifier.ToString()),
                    new Claim(ClaimTypes.Role, role),
                }))
            };
        }

    }
}
