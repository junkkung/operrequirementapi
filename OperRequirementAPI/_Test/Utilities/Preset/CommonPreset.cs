﻿using _Test.Utilities.Generator;
using Database;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace _Test.Utilities.Preset
{
    public class CommonPreset
    {
        public static async Task Generate(OperRequirementContext context)
        {
            await StatusGenerator.Generate(context);
            await CriteriaTypeGenerator.Generate(context);
        }
    }
}
