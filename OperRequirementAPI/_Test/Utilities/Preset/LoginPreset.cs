﻿using _Test.Utilities.Generator;
using API.Controllers;
using Bogus;
using Database;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Models.Requests;
using Models.Responses;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using EN = Database.Entities;
using UT = Utilities;

namespace _Test.Utilities.Preset
{
    public class LoginPreset
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public string AccessToken { get; set; }
        public string RefreshToken { get; set; }
        public EN.Instructor Instructor { get; set; }
        public EN.Student Student { get; set; }

        public static async Task<LoginPreset> GenerateInstructor(OperRequirementContext context, IConfiguration appSetting, AccountController controller)
        {
            var faker = new Faker();
            var preset = new LoginPreset
            {
                Username = faker.Internet.UserName(),
                Password = faker.Internet.Password()
            };

            preset.Instructor = await InstructorGenerator.Generate(context, appSetting, new Database.Entities.Instructor
            {
                Username = preset.Username.ToLower(),
                Password = preset.Password
            });

            var loginRequest = new LoginRequest { Username = preset.Username, Password = preset.Password };

            // Act
            var loginResponse = await controller.LoginInstructor(loginRequest) as ObjectResult;
            var response = loginResponse.Value as LoginResponse;
            preset.AccessToken = response.AccessToken;
            preset.RefreshToken = response.RefreshToken;
            return preset;
        }

        public static async Task<LoginPreset> GenerateStudent(OperRequirementContext context, IConfiguration appSetting, AccountController controller)
        {
            var faker = new Faker();
            var preset = new LoginPreset
            {
                Username = faker.Internet.UserName(),
                Password = faker.Internet.Password()
            };

            preset.Student = await StudentGenerator.Generate(context, appSetting, new Database.Entities.Student
            {
                Username = preset.Username,
                Password = preset.Password
            });

            var loginRequest = new LoginRequest { Username = preset.Username, Password = preset.Password };

            // Act
            var loginResponse = await controller.LoginStudent(loginRequest) as ObjectResult;
            var response = loginResponse.Value as LoginResponse;
            preset.AccessToken = response.AccessToken;
            preset.RefreshToken = response.RefreshToken;
            return preset;
        }

        public static void AddToken(AccountController controller, string token, int id, string role)
        {
            controller.ControllerContext.HttpContext = new DefaultHttpContext()
            {
                User = new ClaimsPrincipal(new ClaimsIdentity(new Claim[]
                {
                    new Claim("identifier", id.ToString()),
                    new Claim(ClaimTypes.Role, role),
                }))
            };
            controller.ControllerContext.HttpContext.Request.Headers.Add("Authorization", $"Bearer {token}");
        }
    }
}
