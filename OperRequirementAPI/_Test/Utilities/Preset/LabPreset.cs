﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using UT = Utilities;
using EN = Database.Entities;
using FN = Models.Functions;
using Database;
using Microsoft.EntityFrameworkCore;
using _Test.Utilities.Generator;
using Microsoft.Extensions.Configuration;
using System.Linq;

namespace _Test.Utilities.Preset
{
    public class LabPreset
    {
        private OperRequirementContext context;
        private IConfiguration appSetting;

        public EN.Laboratory Laboratory { get; set; }
        public EN.Experience Experience { get; set; }
        public List<EN.Experience> Experiences { get; set; }
        public List<EN.Topic> Topics { get; set; }
        public List<EN.SubTopic> SubTopics { get; set; }
        public List<EN.Criteria> Criterias { get; set; }
        public List<EN.Critical> Criticals { get; set; }
        public EN.StudentLaboratory StudentLaboratory { get; set; }
        public List<EN.StudentSubTopic> StudentSubTopics { get; set; }

        public struct Data
        {
            public int TotalTopic { get; set; }
            public int TotalSubTopic { get; set; }
            public int GoodScore { get; set; }
            public int FairScore { get; set; }
            public int TotalSubTopicAdd { get; set; }
        }

        public static async Task<LabPreset> PrepareGoodLab(OperRequirementContext context, IConfiguration appSetting, Data data, EN.Course course = null)
        {
            var preset = new LabPreset
            {
                Laboratory = await LaboratoryGenerator.Generate(context, course is null ? new EN.Laboratory
                { EndDate = DateTime.Now.AddDays(1) } : new EN.Laboratory
                { CourseId = course.Id, EndDate = DateTime.Now.AddDays(1) }),
                Experience = await ExperienceGenerator.Generate(context),
                Topics = new List<EN.Topic>(),
                SubTopics = new List<EN.SubTopic>(),
                Criterias = new List<EN.Criteria>(),
                Criticals = new List<EN.Critical>(),
            };

            //preset.Topics = await TopicGenerator.Generate(context, preset.Laboratory.Id, preset.Experience.Id, data.TotalTopic);
            preset.SubTopics = new List<EN.SubTopic>();
            for (var i = 0; i < data.TotalTopic; i++)
            {
                var topic = await TopicGenerator.Generate(context, new EN.Topic
                {
                    LaboratoryId = preset.Laboratory.Id,
                    ExperienceId = preset.Experience.Id,
                    EndDate = DateTime.Now.AddDays(1)
                });

                for (var j = 0; j < data.TotalSubTopic; j++)
                {
                    var subTopic = await SubTopicGenerator.Generate(context, new EN.SubTopic
                    {
                        TopicId = topic.Id
                    });

                    preset.Criterias.Add(await CriteriaGenerator.Generate(context, new EN.Criteria
                    {
                        SubTopicId = subTopic.Id,
                        CriteriaTypeId = FN.Criteria.Type.Good.Id,
                        Score = data.GoodScore
                    }));

                    preset.Criterias.Add(await CriteriaGenerator.Generate(context, new EN.Criteria
                    {
                        SubTopicId = subTopic.Id,
                        CriteriaTypeId = FN.Criteria.Type.FairOrUnAcceptable.Id,
                        Score = data.FairScore
                    }));

                    preset.SubTopics.Add(subTopic);
                }

                preset.Topics.Add(topic);
            }

            return preset;
        }

        public static async Task<LabPreset> PrepareGoodLabWithSomeStudentScore(OperRequirementContext context, IConfiguration appSetting,
                               EN.Course course, EN.Student student, LabPreset.Data data)
        {
            var preset = await PrepareGoodLab(context, appSetting, data, course);

            var studentLaboratory = await StudentLaboratoryGenerator.Generate(context, new EN.StudentLaboratory
            {
                LaboratoryId = preset.Laboratory.Id,
                StudentId = student.Id
            });

            preset.StudentLaboratory = studentLaboratory;
            preset.StudentSubTopics = new List<EN.StudentSubTopic>();
            var studentScores = new List<EN.StudentScore>();
            for (var i = 0; i < data.TotalSubTopicAdd; i++)
            {
                var studentSubTopic = await StudentSubTopicGenerator.Generate(context, new EN.StudentSubTopic
                {
                    SubTopicId = preset.SubTopics[i].Id,
                    StudentLaboratoryId = studentLaboratory.Id
                });

                var criterias = preset.Criterias.Where(c => c.SubTopicId == preset.SubTopics[i].Id).ToList();

                studentScores.Add(await StudentScoreGenerator.Generate(context, appSetting, new EN.StudentScore
                {
                    StudentSubTopicId = studentSubTopic.Id,
                    CriteriaId = criterias[0].Id,
                    Score = criterias[0].Score
                }));

                studentScores.Add(await StudentScoreGenerator.Generate(context, appSetting, new EN.StudentScore
                {
                    StudentSubTopicId = studentSubTopic.Id,
                    CriteriaId = criterias[1].Id,
                    Score = criterias[1].Score
                }));

                preset.StudentSubTopics.Add(studentSubTopic);
            }

            return preset;
        }

        public static async Task<LabPreset> PrepareEmptyLab(OperRequirementContext context, IConfiguration appSetting)
        {
            return new LabPreset
            {
                context = context,
                appSetting = appSetting,
                Laboratory = await LaboratoryGenerator.Generate(context, new EN.Laboratory 
                { EndDate = DateTime.Now.AddDays(1)}),
                Experience = await ExperienceGenerator.Generate(context),
                Topics = new List<EN.Topic>(),
                SubTopics = new List<EN.SubTopic>(),
                Criterias = new List<EN.Criteria>(),
                Criticals = new List<EN.Critical>(),
            };
        }

        public async Task<EN.Topic> AddTopic(int? statusId = null, int? experienceId = null)
        {
            var topic = await TopicGenerator.Generate(context, new EN.Topic
            {
                LaboratoryId = this.Laboratory.Id,
                ExperienceId = experienceId ?? this.Experience.Id,
                StatusId = statusId ?? UT.Status.Active.Id,
                EndDate = DateTime.Now.AddDays(1)
            });

            this.Topics.Add(topic);
            return topic;
        }

        public async Task<EN.SubTopic> AddSubTopic(int topicId, int? CriticalId = null, int? statusId = null, bool isLabel = false)
        {
            var subTopic =  await SubTopicGenerator.Generate(context, new EN.SubTopic
            {
                TopicId = topicId,
                CriticalId = CriticalId,
                StatusId = statusId ?? UT.Status.Active.Id,
                IsLabel = isLabel
            });

            this.SubTopics.Add(subTopic);
            return subTopic;
        }

        public async Task<EN.Criteria> AddCriteria(int subTopicId, int criteriaTypeId, int? statusId = null,
                                                int? score = null)
        {
            var criteria = await CriteriaGenerator.Generate(context, new EN.Criteria
            {
                SubTopicId = subTopicId,
                CriteriaTypeId = criteriaTypeId,
                StatusId = statusId ?? UT.Status.Active.Id,
                Score = score ?? 0
            });

            this.Criterias.Add(criteria);
            return criteria;
        }
    }
}
