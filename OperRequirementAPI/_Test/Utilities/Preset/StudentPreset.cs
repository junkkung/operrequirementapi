﻿using System;
using System.Collections.Generic;
using System.Text;
using UT = Utilities;
using EN = Database.Entities;
using FN = Models.Functions;
using Database;
using System.Threading.Tasks;
using _Test.Utilities.Generator;
using Microsoft.Extensions.Configuration;

namespace _Test.Utilities.Preset
{
    public class StudentPreset
    {
        private OperRequirementContext context;
        private IConfiguration appSetting;

        public EN.Student Student { get; set; }
        public List<EN.StudentLaboratory> StudentLaboratories { get; set; }
        public List<EN.StudentSubTopic> StudentSubTopics { get; set; }
        public List<EN.StudentScore> StudentScores { get; set; }

        public static async Task<StudentPreset> PrepareEmptyStudent(OperRequirementContext context, IConfiguration appSetting, EN.Student student = null)
        {
            return new StudentPreset
            {
                context = context,
                appSetting = appSetting,
                Student = await StudentGenerator.Generate(context, appSetting, student),
                StudentLaboratories = new List<EN.StudentLaboratory>(),
                StudentSubTopics = new List<EN.StudentSubTopic>(),
                StudentScores = new List<EN.StudentScore>()
            };
        }

        public async Task<EN.StudentLaboratory> AddLab(int laboratoryId, int? statusId = null)
        {
            var studentLab = await StudentLaboratoryGenerator.Generate(context, new EN.StudentLaboratory
            {
                StudentId = this.Student.Id,
                LaboratoryId = laboratoryId,
                StatusId = statusId ?? UT.Status.Active.Id
            });

            this.StudentLaboratories.Add(studentLab);
            return studentLab;
        }

        public async Task<EN.StudentSubTopic> AddSubTopic(int studentLabId, int subtopicId, int? selfAssessment = null, int? statusId = null)
        {
            var studentSubTopic = await StudentSubTopicGenerator.Generate(context, new EN.StudentSubTopic
            {
                StudentLaboratoryId = studentLabId,
                SubTopicId = subtopicId,
                StatusId = statusId ?? UT.Status.Active.Id,
                SelfAssessment = selfAssessment
            });

            this.StudentSubTopics.Add(studentSubTopic);
            return studentSubTopic;
        }

        public async Task<EN.StudentScore> AddScore(int studentSubTopicId, int instructorId, bool isCritical = false,
                                                int? criteriaId = null, int? statusId = null, int? score = null)
        {
            var studentScore = await StudentScoreGenerator.Generate(context, appSetting, new EN.StudentScore
            {
                StudentSubTopicId = studentSubTopicId,
                CriteriaId = criteriaId,
                IsCritical = isCritical,
                InstructorId = instructorId,
                StatusId = statusId ?? UT.Status.Active.Id,
                Score = score ?? 0
            });

            this.StudentScores.Add(studentScore);
            return studentScore;
        }
    }
}
