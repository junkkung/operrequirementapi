﻿using _Test.Utilities.Preset;
using API.Controllers;
using Database;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Text;
using FN = Models.Functions;
using UT = Utilities;

namespace _Test.Utilities
{
    public class NewController
    {
        public IConfiguration AppSetting { get; set; }

        public AccountController AccountController { get; set; }
        public CourseController CourseController { get; set; }
        public StudentController StudentController { get; set; }

        public FN.Account Account { get; set; }
        public FN.Criteria Criteria { get; set; }
        public FN.Course Course { get; set; }
        public FN.Config Config { get; set; }
        public FN.Experience Experience { get; set; }
        public FN.Instructor Instructor { get; set; }
        public FN.Laboratory Laboratory { get; set; }
        public FN.MinusPoint MinusPoint { get; set; }
        public FN.Student Student { get; set; }
        public FN.Topic Topic { get; set; }
        public FN.SubTopic SubTopic { get; set; }

        public UT.Token Token { get; set; }

        public NewController(OperRequirementContext context, UT.IToken token = null)
        {
            context.Database.EnsureCreated();
            UT.CacheManager.RemoveAll();
            this.AppSetting = Utils.InitConfiguration();
            this.Token = new UT.Token(this.AppSetting);

            this.Instructor = new FN.Instructor(context, this.AppSetting);
            this.Course = new FN.Course(context);
            this.Criteria = new FN.Criteria(context);
            this.Config = new FN.Config(context);
            this.Experience = new FN.Experience(context);
            this.Topic = new FN.Topic(context);
            this.SubTopic = new FN.SubTopic(context);
            this.MinusPoint = new FN.MinusPoint(context, this.Config);
            this.Laboratory = new FN.Laboratory(context, this.Topic, this.Criteria);
            this.Student = new FN.Student(context, this.AppSetting, this.Course, this.Laboratory, this.Config, this.Experience, this.SubTopic, this.MinusPoint);
            this.Account = new FN.Account(context, this.AppSetting, this.Instructor, this.Student);

            this.AccountController = new AccountController(context, this.AppSetting, this.Account, this.Instructor, this.Student);
            this.CourseController = new CourseController(this.Course, token is null ? this.Token : token, this.Student);
            this.StudentController = new StudentController(context, this.Student, this.AppSetting, token is null ? this.Token : token);          
        }
    }
}
