﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace _Test.Utilities
{
    public class ValueGenerator
    {
        public static string Generate(string input, string defaultValue)
        {
            return input ?? defaultValue;
        }

        public static int Generate(int input, int defaultValue)
        {
            return input == 0 ? defaultValue : input;
        }

        public static Int64 Generate(Int64 input, Int64 defaultValue)
        {
            return input == 0 ? defaultValue : input;
        }

        public static decimal Generate(decimal input, decimal defaultValue)
        {
            return input == 0 ? defaultValue : input;
        }

        public static DateTime Generate(DateTime input, DateTime defaultValue)
        {
            return input == DateTime.MinValue ? defaultValue : input;
        }

        public static int Generate(int? input, int defaultValue)
        {
            return input ?? defaultValue;
        }

        public static string ValueOrNull(string input)
        {
            return input is null ? "null" : $"'{input}'";
        }

        public static int? ValueOrNull(int? input)
        {
            return input is null ? null : input;
        }

        public static bool? ValueOrNull(bool? input)
        {
            return input is null ? null : input;
        }

        public static DateTime RandomDate()
        {
            var random = new Random();
            var thirtyMonth = new string[] { "4", "6", "9", "11" };
            var thirtyOneMonth = new string[] { "1", "3", "5", "7", "8", "10", "12" };
            var month = random.Next(1, 12);
            var date = 0;

            if (thirtyMonth.Contains(month.ToString())) date = random.Next(1, 30);
            else if (thirtyOneMonth.Contains(month.ToString())) date = random.Next(1, 31);
            else date = random.Next(1, 28);

            return new DateTime(DateTime.Today.Year, month, date);
        }

        public static string ValueOrNull(decimal? input)
        {
            return input is null ? "null" : $"{input}";
        }

        public static string ValueOrNull(DateTime input)
        {
            return input == DateTime.MinValue ? "null" : $"{input}";
        }

        public static string ValueOrNull(DateTime? input)
        {
            return input is null ? "null" : $"'{((DateTime)input).ToString("yyyy-MM-dd HH:mm:ss")}'";
        }
    }
}
