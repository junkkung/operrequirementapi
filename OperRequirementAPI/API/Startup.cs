﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Database;
using Microsoft.AspNetCore.Antiforgery;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Logging;
using Microsoft.IdentityModel.Tokens;
using Microsoft.Net.Http.Headers;
using NLog.Extensions.Logging;
using NLog.Web;
using FN = Models.Functions;
using UT = Utilities;

namespace API
{
    public class Startup
    {
        private IConfiguration Configuration { get; }
        private readonly ILogger<Startup> logger;

        public Startup(IConfiguration configuration, ILogger<Startup> logger)
        {
            Configuration = configuration;
            this.logger = logger;
        }

        private readonly string apiVersion = System.Reflection.Assembly.GetEntryAssembly().GetName().Version.ToString();
        private readonly string mobileWebOrigin = "mobileWebOrigin";

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc()
                    .AddRazorPagesOptions(options =>
                    {
                        //options.Conventions
                        //    .AddPageApplicationModelConvention("/StreamedSingleFileUploadDb",
                        //        model =>
                        //        {
                        //            model.Filters.Add(
                        //                new GenerateAntiforgeryTokenCookieAttribute());
                        //            model.Filters.Add(
                        //                new DisableFormValueModelBindingAttribute());
                        //        });
                        //options.Conventions
                        //    .AddPageApplicationModelConvention("/StreamedSingleFileUploadPhysical",
                        //        model =>
                        //        {
                        //            model.Filters.Add(
                        //                new GenerateAntiforgeryTokenCookieAttribute());
                        //            model.Filters.Add(
                        //                new DisableFormValueModelBindingAttribute());
                        //        });
                    })
                    .SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

            services.AddDbContext<OperRequirementContext>(options => options.UseMySQL(
                    Configuration.GetConnectionString(Configuration["SelectConnectionString"])
                )
            );

            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                    .AddJwtBearer(options =>
                    {
                        options.TokenValidationParameters = new TokenValidationParameters
                        {
                            ValidateIssuer = true,
                            ValidateAudience = true,
                            ValidateLifetime = true,
                            ValidateIssuerSigningKey = true,
                            ValidIssuer = Configuration["Jwt:Issuer"],
                            ValidAudiences = new List<string>
                            {
                                Configuration["Jwt:Instructor:Audience"],
                                Configuration["Jwt:Student:Audience"],
                            },
                            IssuerSigningKeys = new List<SymmetricSecurityKey>
                            {
                                new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Configuration["Jwt:Instructor:Key"])),
                                new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Configuration["Jwt:Student:Key"])),
                                new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Configuration["Jwt:Refresh:Key"])),
                                //new SymmetricSecurityKey(Encoding.UTF8.GetBytes("1234")),
                                //new SymmetricSecurityKey(Encoding.UTF8.GetBytes("5678")),
                            }
                        };
                    });

            services.AddMemoryCache();

            services.AddScoped<FN.IAccount, FN.Account>();
            services.AddScoped<FN.IInstructor, FN.Instructor>();

            services.AddCors(options =>
            {
                options.AddPolicy(name: mobileWebOrigin,
                builder =>
                {
                    //builder.WithOrigins("https://navyjone.com",
                    //            "http://localhost:3000",
                    //            "http://navyjone.com")
                            //.WithHeaders(HeaderNames.ContentType, "application/json")
                            //.WithHeaders("X-Requested-With", "XMLHttpRequest")
                            //.AllowAnyMethod();

                    //builder.AllowAnyOrigin()
                    //        .WithHeaders(HeaderNames.ContentType, "application/json")
                    //        .WithHeaders("X-Requested-With", "XMLHttpRequest")
                    //        .AllowAnyMethod();

                    builder.WithOrigins("https://navyjone.com",
                                //"https://localhost:3000",
                                //"http://localhost:3000",
                                "http://navyjone.com")
                        .AllowAnyHeader()
                        .AllowAnyMethod();
                });
            });

            services.AddScoped<FN.IAccount, FN.Account>();
            services.AddScoped<FN.IConfig, FN.Config>();
            services.AddScoped<FN.ICourse, FN.Course>();
            services.AddScoped<FN.ICriteria, FN.Criteria>();
            services.AddScoped<FN.IExperience, FN.Experience>();
            services.AddScoped<FN.IInstructor, FN.Instructor>();
            services.AddScoped<FN.ILaboratory, FN.Laboratory>();
            services.AddScoped<FN.IMinusPoint, FN.MinusPoint>();
            services.AddScoped<FN.IStudent, FN.Student>();
            services.AddScoped<FN.ISubTopic, FN.SubTopic>();
            services.AddScoped<FN.ITopic, FN.Topic>();
            services.AddScoped<UT.IToken, UT.Token>();
            services.AddScoped<FN.IImage, FN.Image>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, IAntiforgery antiforgery)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            //app.Run(async (context) =>
            //{
            //    await context.Response.WriteAsync("Hello World!");
            //});

            app.UseExceptionHandler(appBuilder =>
            {
                appBuilder.Run(async context =>
                {
                    var exceptionHandlerFeature = context.Features.Get<IExceptionHandlerFeature>();
                    if (exceptionHandlerFeature != null)
                    {
                        IdentityModelEventSource.ShowPII = true;
                        logger.LogError(500,
                                        exceptionHandlerFeature.Error,
                                        $"{exceptionHandlerFeature.Error.Source} {exceptionHandlerFeature.Error.Message} {exceptionHandlerFeature.Error.StackTrace}");
                    }

                    context.Response.StatusCode = 500;
                    await context.Response.Body.WriteAsync(Encoding.ASCII.GetBytes($"{exceptionHandlerFeature.Error.Source} {exceptionHandlerFeature.Error.Message}"));
                });
            });

            app.UseCors(mobileWebOrigin);

            app.UseAuthentication();
            app.UseHttpsRedirection();
            //app.UseMiddleware<APICore.Middleware.AdminChangeLog.AdminChangeLog>();
            app.UseMvc(routes =>
            {
                routes.MapRoute(
                   name: "default",
                   template: "{controller=Home}/{action=Index}/{id?}");
            });

            app.Use(next => context =>
            {
                string path = context.Request.Path.Value;

                if (
                    string.Equals(path, "/", StringComparison.OrdinalIgnoreCase) ||
                    string.Equals(path, "/index.html", StringComparison.OrdinalIgnoreCase))
                {
                    // The request token can be sent as a JavaScript-readable cookie, 
                    // and Angular uses it by default.
                    var tokens = antiforgery.GetAndStoreTokens(context);
                    context.Response.Cookies.Append("XSRF-TOKEN", tokens.RequestToken,
                        new CookieOptions() { HttpOnly = false });
                }

                return next(context);
            });
        }
    }
}
