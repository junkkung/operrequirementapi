﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Database;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Models.Functions;
using Models.Responses;
using Utilities;

namespace API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class MinusPointController : ControllerBase
    {
        private readonly OperRequirementContext context;
        private readonly IToken _token;
        private readonly IMinusPoint _minusPoint;

        public MinusPointController(OperRequirementContext context, IToken token, IMinusPoint minusPoint)
        {
            this.context = context;
            this._token = token;
            this._minusPoint = minusPoint;
        }

        [HttpGet]
        public async Task<IActionResult> Gets()
        {
            var response = new MinusPointResponse()
            {
                MinusPoints = await _minusPoint.Get(),
                MaxMinusPointPerTime = await _minusPoint.GetMaxMinusPointPerTime()
            };
        
            return Ok(response);
        }
    }
}