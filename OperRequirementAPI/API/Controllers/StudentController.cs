﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Database;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Models.Functions;
using Models.Requests;
using Models.Responses;
using Utilities;

namespace API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class StudentController : ControllerBase
    {
        private readonly OperRequirementContext context;
        private readonly IStudent _student;
        private readonly IToken _token;
        private readonly IConfiguration appSetting;

        public StudentController(OperRequirementContext context, IStudent student, IConfiguration appSetting, IToken token)
        {
            this.context = context;
            this._student = student;
            this._token = token;
            this.appSetting = appSetting;
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            if (!(_token.CanProcess(id, User)))
                return StatusCode((int)HttpStatusCode.Unauthorized, Http.Data("Identifier unacceptable"));

            var student = await _student.Get(id);
            var response = StudentResponse.Map(student);

            return Ok(response);
        }

        [HttpGet("{id}/labSummary")]
        public async Task<IActionResult> GetLabSummary(int id)
        {
            if (!(_token.CanProcess(id, User)))
                return StatusCode((int)HttpStatusCode.Unauthorized, Http.Data("Identifier unacceptable"));

            var response = await _student.SummaryScore(id);

            if (response.IsNotFound()) return NotFound(Http.Data("Student not found"));
            return Ok(response.Object);
        }

        [HttpGet("{id}/MinusPoint")]
        public async Task<IActionResult> GetMinusPoint(int id)
        {
            if (!(_token.CanProcess(id, User)))
                return StatusCode((int)HttpStatusCode.Unauthorized, Http.Data("Identifier unacceptable"));

            var minusPoints = await _student.GetMinusPoint(id);
            var responses = await StudentMinusPointResponse.Map(minusPoints);

            return Ok(responses);
        }

        [HttpGet("{id}/laboratory/{labId}")]
        public async Task<IActionResult> GetLabDetail(int id, int labId)
        {
            if (!(_token.CanProcess(id, User)))
                return StatusCode((int)HttpStatusCode.Unauthorized, Http.Data("Identifier unacceptable"));

            var response = await _student.GetLabDetail(id, labId);
            if (response.IsNotFound()) return NotFound(Http.Data("Student or laboratory not found"));
            return Ok(response.Object);
        }

        [HttpGet("{id}/experience")]
        public async Task<IActionResult> GetExperience(int id)
        {
            if (!(_token.CanProcess(id, User)))
                return StatusCode((int)HttpStatusCode.Unauthorized, Http.Data("Identifier unacceptable"));

            var response = await _student.GetExperience(id);
            if (response.IsNotFound()) return NotFound(Http.Data("Student not found"));
            return Ok(response.Object);
        }

        [HttpGet("{id}/isDoneAll")]
        public async Task<IActionResult> IsDoneAll(int id)
        {
            if (!(_token.CanProcess(id, User)))
                return StatusCode((int)HttpStatusCode.Unauthorized, Http.Data("Identifier unacceptable"));

            var response = await _student.IsDoneAll(id);
            return Ok(response);
        }

        [HttpPost("{id}/AddMinusPoint")]
        public async Task<IActionResult> AddMinusPoint(int id, MinusPointAddRequest request)
        {
            if (!(_token.IsInStructorRequest(User)))
                return StatusCode((int)HttpStatusCode.Unauthorized, Http.Data("Identifier unacceptable"));

            var response = await _student.AddMinusPoint(id, Http.GetIdentifier(User), request);
            if (response.IsNotFound()) return NotFound(Http.Data(response.Object));
            return Created("", response.Object);
        }

        [HttpPost("{id}/remove/minusPoint/{studentMinusPointId}")]
        public async Task<IActionResult> RemoveMinusPoint(int id, int studentMinusPointId)
        {
            if (!(_token.IsInStructorRequest(User)))
                return StatusCode((int)HttpStatusCode.Unauthorized, Http.Data("Identifier unacceptable"));

            var response = await _student.RemoveMinusPoint(id, Http.GetIdentifier(User), studentMinusPointId);
            if (response.IsNotFound()) return NotFound(Http.Data(response.Object));
            return Ok(Http.Data(response.Object));
        }

        [HttpPost("{id}/SelfAssessment")]
        public async Task<IActionResult> CreateOrUpdateSelfAssessment(int id, SelfAssessmentRequest request)
        {
            if (!(_token.IsStudentRequest(id, User)))
                return StatusCode((int)HttpStatusCode.Unauthorized, Http.Data("Identifier unacceptable"));

            var response = await _student.CreateOrUpdateSelfAssessment(id, request);
            if (response.IsNotFound()) return NotFound(Http.Data(response.Object));
            if (response.IsForbidden()) return StatusCode((int)HttpStatusCode.Forbidden, Http.Data(response.Object));
            if (response.IsCreated()) return Created("", response.Object);
            return Ok(response.Object);
        }

        [HttpPost("{id}/LaboratoryScope")]
        public async Task<IActionResult> CreateOrUpdateLabScope(int id, LabScopeRequest request)
        {
            if (!(_token.IsStudentRequest(id, User)))
                return StatusCode((int)HttpStatusCode.Unauthorized, Http.Data("Identifier unacceptable"));

            var response = await _student.CreateOrUpdateLabScope(id, request);
            if (response.IsNotFound()) return NotFound(Http.Data(response.Object));
            if (response.IsForbidden()) return StatusCode((int)HttpStatusCode.Forbidden, Http.Data(response.Object));
            if (response.IsCreated()) return Created("", response.Object);
            return Ok(response.Object);
        }

        [HttpPost("{id}/SubmitScore")]
        public async Task<IActionResult> SubmitScore(int id, SubmitScoreRequest request)
        {
            if (!(_token.IsInStructorRequest(User)))
                return StatusCode((int)HttpStatusCode.Unauthorized, Http.Data("Identifier unacceptable"));

            var instructorId = Http.GetIdentifier(User);
            var response = await _student.SubmitScore(id, instructorId, request);
            if (response.IsNotFound()) return NotFound(Http.Data(response.Object));
            if (response.IsConflict()) return Conflict(Http.Data(response.Object));
            if (response.IsForbidden()) return StatusCode((int)HttpStatusCode.Forbidden, Http.Data(response.Object));
            return Created("", response.Object);
        }

        [HttpPost("{id}/SubmitCritical")]
        public async Task<IActionResult> SubmitCritical(int id, SubmitCriticalRequest request)
        {
            if (!(_token.IsInStructorRequest(User)))
                return StatusCode((int)HttpStatusCode.Unauthorized, Http.Data("Identifier unacceptable"));

            var instructorId = Http.GetIdentifier(User);
            var response = await _student.SubmitCritical(id, instructorId, request);
            if (response.IsNotFound()) return NotFound(Http.Data(response.Object));
            if (response.IsForbidden()) return StatusCode((int)HttpStatusCode.Forbidden, Http.Data(response.Object));
            return Created("", response.Object);
        }

        [HttpGet("{id}/Laboratory/{laboratoryId}/Images")]
        public async Task<IActionResult> GetLabImages(int id, int laboratoryId)
        {
            if (!(_token.CanProcess(id, User)))
                return StatusCode((int)HttpStatusCode.Unauthorized, Http.Data("Identifier unacceptable"));

            var response = await _student.GetLabImage(id, laboratoryId);
            if (response.IsNotFound()) return NotFound(Http.Data("Student or laboratory not found"));
            return Ok(response.Object);
        }

        [HttpPost("{id}/CancelCritical")]
        public async Task<IActionResult> CancelCritical(int id, SubmitCriticalRequest request)
        {
            if (!(_token.IsInStructorRequest(User)))
                return StatusCode((int)HttpStatusCode.Unauthorized, Http.Data("Identifier unacceptable"));

            var response = await _student.CancelCritical(id, request);
            if (response.IsNotFound()) return NotFound(Http.Data(response.Object));
            if (response.IsForbidden()) return StatusCode((int)HttpStatusCode.Forbidden, Http.Data(response.Object));
            return Created("", response.Object);
        }

        [HttpGet("{id}/Laboratory/{laboratoryId}/tooth/confirm")]
        public async Task<IActionResult> GetToothConfirm(int id, int laboratoryId)
        {
            if (!(_token.CanProcess(id, User)))
                return StatusCode((int)HttpStatusCode.Unauthorized, Http.Data("Identifier unacceptable"));

            var response = await _student.GetToothConfirm(id, laboratoryId);
            return Ok(response.Object);
        }

        [HttpPost("{id}/Laboratory/{laboratoryId}/tooth/confirm/create")]
        public async Task<IActionResult> CreateToothConfirm(int id, int laboratoryId)
        {
            if (!(_token.CanProcess(id, User)))
                return StatusCode((int)HttpStatusCode.Unauthorized, Http.Data("Identifier unacceptable"));

            var response = await _student.CreateToothConfirm(id, laboratoryId);
            if (response.IsNotFound()) return NotFound(response.Object);
            if (response.IsForbidden()) return StatusCode((int)HttpStatusCode.Forbidden, Http.Data(response.Object));
            return Created("", Http.Data(response.Object));
        }

        [HttpPost("{id}/tooth/confirm/student/update")]
        public async Task<IActionResult> StudentToothConfirm(int id, StudentToothConfirmRequest request)
        {
            if (!(_token.CanProcess(id, User)))
                return StatusCode((int)HttpStatusCode.Unauthorized, Http.Data("Identifier unacceptable"));

            var response = await _student.StudentUpdateToothConfirm(id, request);
            if (response.IsNotFound()) return NotFound(Http.Data(response.Object));
            if (response.IsForbidden()) return StatusCode((int)HttpStatusCode.Forbidden, Http.Data(response.Object));
            return Ok(Http.Data(response.Object));
        }

        [HttpPost("{id}/tooth/confirm/instructor/update")]
        public async Task<IActionResult> InstructorToothConfirm(int id, InstuctorToothConfirmRequest request)
        {
            if (!(_token.IsInStructorRequest(User)))
                return StatusCode((int)HttpStatusCode.Unauthorized, Http.Data("Identifier unacceptable"));

            var instructorId = Http.GetIdentifier(User);
            var response = await _student.InstructorUpdateToothConfirm(id, request, instructorId);
            if (response.IsNotFound()) return NotFound(Http.Data(response.Object));
            if (response.IsForbidden()) return StatusCode((int)HttpStatusCode.Forbidden, Http.Data(response.Object));
            return Ok(Http.Data(response.Object));
        }

        [HttpPost("{id}/tooth/confirm/remove")]
        public async Task<IActionResult> RemoveToothConfirm(int id, StudentToothConfirmRemoveRequest request)
        {
            if (!(_token.CanProcess(id, User)))
                return StatusCode((int)HttpStatusCode.Unauthorized, Http.Data("Identifier unacceptable"));

            var response = await _student.RemoveToothConfirm(id, request);
            if (response.IsNotFound()) return NotFound(Http.Data(response.Object));
            if (response.IsForbidden()) return StatusCode((int)HttpStatusCode.Forbidden, Http.Data(response.Object));
            return Ok(Http.Data(response.Object));
        }

        [HttpGet("{id}/Laboratory/{laboratoryId}/tooth/approval")]
        public async Task<IActionResult> GetToothApproval(int id, int laboratoryId)
        {
            if (!(_token.CanProcess(id, User)))
                return StatusCode((int)HttpStatusCode.Unauthorized, Http.Data("Identifier unacceptable"));

            var response = await _student.GetToothApproval(id, laboratoryId);
            return Ok(response.Object);
        }

        [HttpPost("{id}/tooth/approve")]
        public async Task<IActionResult> ApproveTooth(int id, StudentToothApprovalRequest request)
        {
            if (!(_token.IsInStructorRequest(User)))
                return StatusCode((int)HttpStatusCode.Unauthorized, Http.Data("Identifier unacceptable"));

            var instructorId = Http.GetIdentifier(User);

            var response = await _student.CreateOrUpdateToothApproval(id, instructorId, request);
            if (response.IsNotFound()) return NotFound(Http.Data(response.Object));
            if (response.IsForbidden()) return StatusCode((int)HttpStatusCode.Forbidden, Http.Data(response.Object));
            return Ok(Http.Data(response.Object));
        }
    }
}