﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Database;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Models.Functions;
using Models.Requests;
using Utilities;

namespace API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AccountController : ControllerBase
    {
        private readonly OperRequirementContext context;
        private readonly IConfiguration appSetting;
        private readonly IAccount _account;
        private readonly IInstructor _instructor;
        private readonly IStudent _student;

        public AccountController(OperRequirementContext context, IConfiguration appSetting, IAccount account, IInstructor instructor, IStudent student)
        {
            this.context = context;
            this.appSetting = appSetting;
            this._account = account;
            this._instructor = instructor;
            this._student = student;
        }

        [HttpPost("login/instructor")]
        public async Task<IActionResult> LoginInstructor([FromBody] LoginRequest request)
        {
            _account.Username = request.Username;
            _account.Password = request.Password;

            var response = await _account.LoginInstructor();
            if (response.IsNotFound()) return NotFound(Http.Data("User not found"));
            if (response.IsBadRequest()) return BadRequest(Http.Data("Incorrect password"));
            return Ok(response.Object);
        }

        [HttpPost("signUp/instructor")]
        public async Task<IActionResult> SignUpInstructor([FromBody] SignUpInstructorRequest request)
        {
            var response = await _instructor.SignUp(request);
            if (response.IsConflict()) return Conflict(Http.Data(response.Object));
            if (response.IsBadRequest()) return BadRequest(Http.Data(response.Object));
            if (response.IsNotFound()) return NotFound(Http.Data(response.Object));

            return Created("", response.Object);
        }

        [HttpPut("refreshAccess/instructor")]
        [Authorize(Roles = TokenRole.Refresh)]
        public async Task<IActionResult> RefreshAccessInstructor()
        {
            _account.Identifier = Http.GetIdentifier(User);
            _account.RefreshToken = Http.GetToken(HttpContext);

            var response = await _account.RefreshInstructor();
            if (response.IsBadRequest()) return BadRequest();
            return Ok(response.Object);
        }

        [HttpPut("passwordChange/instructor")]
        public async Task<IActionResult> ChangePasswordInstructor([FromBody] ChangePasswordRequest request)
        {
            var response = await _account.ChangePasswordInstructor(request);

            if (response.IsNotFound()) return NotFound(Http.Data(response.Object));
            if (response.IsBadRequest()) return BadRequest(Http.Data(response.Object));

            return Ok(response.Object);
        }

        [HttpPost("login/student")]
        public async Task<IActionResult> LoginStudent([FromBody] LoginRequest request)
        {
            _account.Username = request.Username;
            _account.Password = request.Password;

            var response = await _account.LoginStudent();
            if (response.IsNotFound()) return NotFound(Http.Data("User not found"));
            if (response.IsBadRequest()) return BadRequest(Http.Data("Incorrect password"));
            return Ok(response.Object);
        }

        [HttpPost("signUp/student")]
        public async Task<IActionResult> SignUpStudent([FromBody] SignUpStudentRequest request)
        {
            var response = await _student.SignUp(request);
            if (response.IsConflict()) return Conflict(Http.Data(response.Object));
            if (response.IsBadRequest()) return BadRequest(Http.Data(response.Object));
            if (response.IsNotFound()) return NotFound(Http.Data(response.Object));

            return Created("", Http.Data(response.Object));
        }

        [HttpPut("refreshAccess/student")]
        [Authorize(Roles = TokenRole.Refresh)]
        public async Task<IActionResult> RefreshAccessStudent()
        {
            _account.Identifier = Http.GetIdentifier(User);
            _account.RefreshToken = Http.GetToken(HttpContext);

            var response = await _account.RefreshStudent();
            if (response.IsBadRequest()) return BadRequest();
            return Ok(response.Object);
        }

        [HttpPut("passwordChange/student")]
        public async Task<IActionResult> ChangePasswordStudent([FromBody] ChangePasswordRequest request)
        {
            var response = await _account.ChangePasswordStudent(request);

            if (response.IsNotFound()) return NotFound(Http.Data(response.Object));
            if (response.IsBadRequest()) return BadRequest(Http.Data(response.Object));

            return Ok(response.Object);
        }
    }
}