﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Database;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Models.Functions;
using Models.Requests;
using Utilities;

namespace API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class ImageController : ControllerBase
    {
        private readonly OperRequirementContext context;
        private readonly IStudent _student;
        private readonly IToken _token;
        private readonly ILaboratory _laboratory;
        private readonly IImage _image;
        private readonly ICourse _course;

        public ImageController(OperRequirementContext context, IStudent student,
                                IToken token, ILaboratory laboratory, IImage image,
                                ICourse course)
        {
            this.context = context;
            this._student = student;
            this._token = token;
            this._laboratory = laboratory;
            this._image = image;
            this._course = course;
        }

        [HttpPost("labScope")]
        public async Task<IActionResult> UploadLabScope([FromBody] LabScopeImageUploadRequest request)
        {
            if (!(_token.IsStudentRequest(request.StudentId, User)))
                return StatusCode((int)HttpStatusCode.Unauthorized, Http.Data("Identifier unacceptable"));

            var student = await _student.Get(request.StudentId);
            if (student is null) return NotFound(Http.Data("User not found"));

            var laboratory = await _laboratory.Get(request.LaboratoryId);
            if (laboratory is null) return NotFound(Http.Data("Laboratory not exist."));

            if (await _course.IsExpired(student.CourseId))
                return StatusCode((int)HttpStatusCode.Forbidden, Http.Data("Course already closed"));

            if (!_image.IsFileFormatAcceptable(request.Name))
                return StatusCode((int)HttpStatusCode.Forbidden, Http.Data("File's format unacceptable."));

            var location = $"students\\stu_{request.StudentId}\\lab_{request.LaboratoryId}";
            var url = _image.Save(request.Base64, location, request.Name);

            await _student.SaveLabScopeImage(request.StudentId, request.LaboratoryId, url);
            return Ok(url);
        }

        [HttpPost("profile")]
        public async Task<IActionResult> UploadProfile([FromBody] ProfileImageUploadRequest request)
        {
            if (!(_token.IsStudentRequest(request.StudentId, User)))
                return StatusCode((int)HttpStatusCode.Unauthorized, Http.Data("Identifier unacceptable"));

            var student = await _student.Get(request.StudentId);
            if (student is null) return NotFound(Http.Data("User not found"));

            if (await _course.IsExpired(student.CourseId))
                return StatusCode((int)HttpStatusCode.Forbidden, Http.Data("Course already closed"));

            if (!_image.IsFileFormatAcceptable(request.Name))
                return StatusCode((int)HttpStatusCode.Forbidden, Http.Data("File's format unacceptable."));

            var location = $"students\\stu_{request.StudentId}\\profile";
            var url = _image.Save(request.Base64, location, request.Name);

            student.Image = url;
            context.Update(student);
            await context.SaveChangesAsync();

            return Ok(url);
        }

        [HttpPost("labImage")]
        public async Task<IActionResult> UploadLabImage([FromBody] LabImageUploadRequest request)
        {
            if (!(_token.IsStudentRequest(request.StudentId, User)))
                return StatusCode((int)HttpStatusCode.Unauthorized, Http.Data("Identifier unacceptable"));

            var student = await _student.Get(request.StudentId);
            if (student is null) return NotFound(Http.Data("User not found"));

            if (await _course.IsExpired(student.CourseId))
                return StatusCode((int)HttpStatusCode.Forbidden, Http.Data("Course already closed"));

            var labImages = await _laboratory.GetAllLabImage();
            var labImageIds = labImages.Select(l => l.Id).ToList();

            if (!labImageIds.Contains(request.LaboratoryImageId))
                return NotFound(Http.Data("Laboratory image specified not exist"));

            if (!_image.IsFileFormatAcceptable(request.Name))
                return StatusCode((int)HttpStatusCode.Forbidden, Http.Data("File's format unacceptable."));

            var location = $"students\\stu_{request.StudentId}\\lab_{request.LaboratoryId}";
            var url = _image.Save(request.Base64, location, request.Name);

            await _student.SaveLabImage(request.StudentId, request.LaboratoryId, request.LaboratoryImageId, url);
            return Ok(url);
        }

        [HttpPost("confirm/labImage")]
        public async Task<IActionResult> ConfirmLabImage([FromBody] LabImageConfirmRequest request)
        {
            if (!(_token.IsInStructorRequest(User)))
                return StatusCode((int)HttpStatusCode.Unauthorized, Http.Data("Identifier unacceptable"));

            var instructorId = Http.GetIdentifier(User);

            var response = await _student.ConfirmLabImage(instructorId, request.LaboratoryId, request.StudentLaboratoryImageId);
            if (response.IsForbidden()) return StatusCode((int)HttpStatusCode.Forbidden, Http.Data(response.Object));
            if (response.IsNotFound()) return NotFound(Http.Data(response.Object));

            return Ok(Http.Data(response.Object));
        }

        [HttpPost("confirm/labScope")]
        public async Task<IActionResult> ConfirmLabScope([FromBody] LabScopeConfirmRequest request)
        {
            if (!(_token.IsInStructorRequest(User)))
                return StatusCode((int)HttpStatusCode.Unauthorized, Http.Data("Identifier unacceptable"));

            if (await _laboratory.IsExpired(request.LaboratoryId))
                return StatusCode((int)HttpStatusCode.Forbidden, Http.Data("Laboratory already closed"));

            var instructorId = Http.GetIdentifier(User);

            var response = await _student.ConfirmLabScopeImage(request.StudentId, instructorId, request.LaboratoryId);
            if (response.IsForbidden()) return StatusCode((int)HttpStatusCode.Forbidden, Http.Data(response.Object));
            if (response.IsNotFound()) return NotFound(Http.Data(response.Object));

            return Ok(Http.Data(response.Object));
        }
    }
}