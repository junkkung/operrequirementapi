﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Database;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Models.Requests;
using Utilities;
using EN = Database.Entities;

namespace API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    //[Authorize]
    public class CodeController : ControllerBase
    {
        private readonly OperRequirementContext context;

        public CodeController(OperRequirementContext context)
        {
            this.context = context;
        }

        [HttpPost("instructor")]
        //[Authorize(Roles = TokenRole.Instructor)]
        public async Task<IActionResult> CreateInstrutorCode([FromBody] CodeCreateRequest request)
        {
            var codes = CodeGenerator.Generate(request.Prefix, request.Range, request.Total);
            var confirmCodes = new List<EN.InstructorConfirmCode>();

            foreach (var code in codes)
                confirmCodes.Add(new EN.InstructorConfirmCode { Code = code });

            await context.AddRangeAsync(confirmCodes);
            await context.SaveChangesAsync();

            return Created("Instructor Confirmation code created.", "");
        }

        [HttpPost("student")]
        //[Authorize(Roles = TokenRole.Instructor)]
        public async Task<IActionResult> CreateStudentCode([FromBody] CodeCreateStudentRequest request)
        {
            var codes = CodeGenerator.Generate(request.Prefix, request.Range, request.Total);
            var confirmCodes = new List<EN.StudentConfrimCode>();

            foreach (var code in codes)
                confirmCodes.Add(new EN.StudentConfrimCode { Code = code, CourseId = request.CourseId });

            await context.AddRangeAsync(confirmCodes);
            await context.SaveChangesAsync();

            return Created("Student Confirmation code created.", "");
        }
    }
}