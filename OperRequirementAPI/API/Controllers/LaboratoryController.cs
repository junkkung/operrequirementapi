﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Database;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Models.Functions;
using Models.Responses;
using Utilities;

namespace API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class LaboratoryController : ControllerBase
    {
        private readonly OperRequirementContext context;
        private readonly IToken _token;
        private readonly ILaboratory _laboratory;

        public LaboratoryController(OperRequirementContext context, IToken token, ILaboratory laboratory)
        {
            this.context = context;
            this._token = token;
            this._laboratory = laboratory;
        }

        [HttpGet]
        public async Task<IActionResult> Gets (int courseId)
        {
            var responses = new List<LaboratoryResponse>();
            var laboratories = await _laboratory.GetByCourseId(courseId);
            if (laboratories is null) return Ok();
            foreach (var laboratory in laboratories)
                responses.Add(LaboratoryResponse.Map(laboratory));
            return Ok(responses);
        }
    }
}