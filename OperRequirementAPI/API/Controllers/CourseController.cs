﻿using Database;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Models.Functions;
using Models.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Utilities;

namespace API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class CourseController : ControllerBase
    {
        private readonly ICourse _course;
        private readonly IToken _token;
        private readonly IStudent _student;

        public CourseController(ICourse course, IToken token, IStudent student)
        {
            this._course = course;
            this._token = token;
            this._student = student;
        }

        [HttpGet]
        public async Task<IActionResult> GetAll ()
        {
            var courses = await _course.Get();
            var responses = CourseResponse.Map(courses);

            return Ok(responses);
        }


        [HttpGet("{id}/Students")]
        public async Task<IActionResult> GetStudent(int id)
        {
            if (!(_token.IsInStructorRequest(User)))
                return StatusCode((int)HttpStatusCode.Unauthorized, "Identifier unacceptable");
            var students = await _student.GetByCourseId(id);
            var responses = _student.GroupByStudentGroup(students);

            return Ok(responses);
        }
    }
}
