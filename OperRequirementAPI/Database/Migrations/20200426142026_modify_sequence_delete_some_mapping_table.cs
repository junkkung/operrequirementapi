﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Database.Migrations
{
    public partial class modify_sequence_delete_some_mapping_table : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql($"ALTER TABLE `critical` DROP FOREIGN KEY `FK__critical_criteriaTypeId__criteria_type_id`; ");

            migrationBuilder.DropTable(
                name: "critical_sub_topic");

            migrationBuilder.DropTable(
                name: "experience_topic");

            migrationBuilder.Sql($"ALTER TABLE `critical` DROP INDEX `IX_critical_criteriaTypeId`; ");

            migrationBuilder.DropColumn(
                name: "totalScore",
                table: "laboratory");

            migrationBuilder.DropColumn(
                name: "criteriaTypeId",
                table: "critical");

            migrationBuilder.DropColumn(
                name: "sequence",
                table: "course_laboratory");

            migrationBuilder.AddColumn<int>(
                name: "ExperienceId",
                table: "topic",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "sequence",
                table: "topic",
                type: "int(5)",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "sequence",
                table: "sub_topic",
                type: "int(5)",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AlterColumn<string>(
                name: "image",
                table: "student_laboratory_image",
                maxLength: 255,
                nullable: false,
                oldClrType: typeof(string),
                oldMaxLength: 255,
                oldNullable: true);

            migrationBuilder.AddColumn<int>(
                name: "sequence",
                table: "laboratory",
                type: "int(5)",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "sequence",
                table: "experience",
                type: "int(5)",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_topic_ExperienceId",
                table: "topic",
                column: "ExperienceId");

            migrationBuilder.AddForeignKey(
                name: "FK__topic_experienceId__experience_id",
                table: "topic",
                column: "ExperienceId",
                principalTable: "experience",
                principalColumn: "id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql($"ALTER TABLE `topic` DROP FOREIGN KEY `FK__topic_experienceId__experience_id`; ");

            migrationBuilder.Sql($"ALTER TABLE `topic` DROP INDEX `IX_topic_ExperienceId`; ");

            migrationBuilder.DropColumn(
                name: "ExperienceId",
                table: "topic");

            migrationBuilder.DropColumn(
                name: "sequence",
                table: "topic");

            migrationBuilder.DropColumn(
                name: "sequence",
                table: "sub_topic");

            migrationBuilder.DropColumn(
                name: "sequence",
                table: "laboratory");

            migrationBuilder.DropColumn(
                name: "sequence",
                table: "experience");

            migrationBuilder.AlterColumn<string>(
                name: "image",
                table: "student_laboratory_image",
                maxLength: 255,
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 255);

            migrationBuilder.AddColumn<int>(
                name: "totalScore",
                table: "laboratory",
                type: "int(10)",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "criteriaTypeId",
                table: "critical",
                type: "int(10)",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "sequence",
                table: "course_laboratory",
                type: "int(4)",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateTable(
                name: "critical_sub_topic",
                columns: table => new
                {
                    criticalId = table.Column<int>(type: "int(10)", nullable: false),
                    subTopicId = table.Column<int>(type: "int(10)", nullable: false),
                    sequence = table.Column<int>(type: "int(4)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_critical_sub_topic", x => new { x.criticalId, x.subTopicId });
                    table.ForeignKey(
                        name: "FK__critical_sub_topic_criticalId__critical_id",
                        column: x => x.criticalId,
                        principalTable: "critical",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK__critical_sub_topic_subTopicId__sub_topic_id",
                        column: x => x.subTopicId,
                        principalTable: "sub_topic",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "experience_topic",
                columns: table => new
                {
                    experienceId = table.Column<int>(type: "int(10)", nullable: false),
                    topicId = table.Column<int>(type: "int(10)", nullable: false),
                    sequence = table.Column<int>(type: "int(4)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_experience_topic", x => new { x.experienceId, x.topicId });
                    table.ForeignKey(
                        name: "FK__experience_topic_experienceId__experience_id",
                        column: x => x.experienceId,
                        principalTable: "experience",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK__experience_topic_topicId__topic_id",
                        column: x => x.topicId,
                        principalTable: "topic",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_critical_criteriaTypeId",
                table: "critical",
                column: "criteriaTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_critical_sub_topic_subTopicId",
                table: "critical_sub_topic",
                column: "subTopicId");

            migrationBuilder.CreateIndex(
                name: "IX_experience_topic_topicId",
                table: "experience_topic",
                column: "topicId");

            migrationBuilder.AddForeignKey(
                name: "FK__critical_criteriaTypeId__criteria_type_id",
                table: "critical",
                column: "criteriaTypeId",
                principalTable: "criteria_type",
                principalColumn: "id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
