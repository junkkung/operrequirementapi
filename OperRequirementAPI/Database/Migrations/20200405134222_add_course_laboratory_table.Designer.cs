﻿// <auto-generated />
using System;
using Database;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace Database.Migrations
{
    [DbContext(typeof(OperRequirementContext))]
    [Migration("20200405134222_add_course_laboratory_table")]
    partial class add_course_laboratory_table
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "2.2.6-servicing-10079");

            modelBuilder.Entity("Database.Entities.Course", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("id")
                        .HasColumnType("int(10)");

                    b.Property<DateTimeOffset>("CreateDate")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("createDate")
                        .HasDefaultValueSql("CURRENT_TIMESTAMP");

                    b.Property<DateTime>("EndDate")
                        .HasColumnName("endDate");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasColumnName("name")
                        .HasMaxLength(255);

                    b.Property<int>("SemesterYear")
                        .HasColumnName("semesterYear")
                        .HasColumnType("int(10)");

                    b.Property<DateTime>("StartDate")
                        .HasColumnName("startDate");

                    b.Property<int>("StatusId")
                        .HasColumnName("statusId")
                        .HasColumnType("int(10)");

                    b.Property<DateTimeOffset>("UpdateDate")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("updateDate")
                        .HasDefaultValueSql("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP");

                    b.HasKey("Id");

                    b.HasIndex("Id")
                        .HasName("ID__course__id");

                    b.HasIndex("StatusId");

                    b.ToTable("course");
                });

            modelBuilder.Entity("Database.Entities.CourseLaboratory", b =>
                {
                    b.Property<int>("CourseId")
                        .HasColumnName("courseId")
                        .HasColumnType("int(10)");

                    b.Property<int>("LaboratoryId")
                        .HasColumnName("laboratoryId")
                        .HasColumnType("int(10)");

                    b.Property<int>("Sequence")
                        .HasColumnName("sequence")
                        .HasColumnType("int(4)");

                    b.HasKey("CourseId", "LaboratoryId");

                    b.HasIndex("LaboratoryId");

                    b.ToTable("course_laboratory");
                });

            modelBuilder.Entity("Database.Entities.Criteria", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("id")
                        .HasColumnType("int(10)");

                    b.Property<DateTimeOffset>("CreateDate")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("createDate")
                        .HasDefaultValueSql("CURRENT_TIMESTAMP");

                    b.Property<int>("CriteriaTypeId")
                        .HasColumnName("criteriaTypeId")
                        .HasColumnType("int(10)");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasColumnName("name")
                        .HasColumnType("text");

                    b.Property<int>("Score")
                        .HasColumnName("score")
                        .HasColumnType("int(10)");

                    b.Property<int>("StatusId")
                        .HasColumnName("statusId")
                        .HasColumnType("int(10)");

                    b.Property<DateTimeOffset>("UpdateDate")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("updateDate")
                        .HasDefaultValueSql("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP");

                    b.HasKey("Id");

                    b.HasIndex("CriteriaTypeId");

                    b.HasIndex("Id")
                        .HasName("ID__criteria__id");

                    b.HasIndex("StatusId");

                    b.ToTable("criteria");
                });

            modelBuilder.Entity("Database.Entities.CriteriaType", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("id")
                        .HasColumnType("int(10)");

                    b.Property<DateTimeOffset>("CreateDate")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("createDate")
                        .HasDefaultValueSql("CURRENT_TIMESTAMP");

                    b.Property<string>("Description")
                        .HasColumnName("description")
                        .HasMaxLength(255);

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasColumnName("name")
                        .HasMaxLength(255);

                    b.Property<int>("StatusId")
                        .HasColumnName("statusId")
                        .HasColumnType("int(10)");

                    b.Property<DateTimeOffset>("UpdateDate")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("updateDate")
                        .HasDefaultValueSql("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP");

                    b.HasKey("Id");

                    b.HasIndex("Id")
                        .HasName("ID__criteria_type__id");

                    b.HasIndex("StatusId");

                    b.ToTable("criteria_type");
                });

            modelBuilder.Entity("Database.Entities.Critical", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("id")
                        .HasColumnType("int(10)");

                    b.Property<DateTimeOffset>("CreateDate")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("createDate")
                        .HasDefaultValueSql("CURRENT_TIMESTAMP");

                    b.Property<int>("CriteriaTypeId")
                        .HasColumnName("criteriaTypeId")
                        .HasColumnType("int(10)");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasColumnName("name")
                        .HasColumnType("text");

                    b.Property<int>("StatusId")
                        .HasColumnName("statusId")
                        .HasColumnType("int(10)");

                    b.Property<DateTimeOffset>("UpdateDate")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("updateDate")
                        .HasDefaultValueSql("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP");

                    b.HasKey("Id");

                    b.HasIndex("CriteriaTypeId");

                    b.HasIndex("Id")
                        .HasName("ID__critical__id");

                    b.HasIndex("StatusId");

                    b.ToTable("critical");
                });

            modelBuilder.Entity("Database.Entities.Experience", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("id")
                        .HasColumnType("int(10)");

                    b.Property<DateTimeOffset>("CreateDate")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("createDate")
                        .HasDefaultValueSql("CURRENT_TIMESTAMP");

                    b.Property<string>("Description")
                        .HasColumnName("description")
                        .HasMaxLength(255);

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasColumnName("name")
                        .HasMaxLength(255);

                    b.Property<int>("StatusId")
                        .HasColumnName("statusId")
                        .HasColumnType("int(10)");

                    b.Property<DateTimeOffset>("UpdateDate")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("updateDate")
                        .HasDefaultValueSql("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP");

                    b.HasKey("Id");

                    b.HasIndex("Id")
                        .HasName("ID__experience__id");

                    b.HasIndex("StatusId");

                    b.ToTable("experience");
                });

            modelBuilder.Entity("Database.Entities.Instructor", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("id")
                        .HasColumnType("int(10)");

                    b.Property<DateTimeOffset>("CreateDate")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("createDate")
                        .HasDefaultValueSql("CURRENT_TIMESTAMP");

                    b.Property<string>("Email")
                        .IsRequired()
                        .HasColumnName("email")
                        .HasMaxLength(255);

                    b.Property<string>("Firstname")
                        .IsRequired()
                        .HasColumnName("firstname")
                        .HasMaxLength(255);

                    b.Property<string>("Image")
                        .HasColumnName("image")
                        .HasMaxLength(255);

                    b.Property<string>("Lastname")
                        .IsRequired()
                        .HasColumnName("lastname")
                        .HasMaxLength(255);

                    b.Property<string>("Password")
                        .IsRequired()
                        .HasColumnName("password")
                        .HasMaxLength(255);

                    b.Property<int>("StatusId")
                        .HasColumnName("statusId")
                        .HasColumnType("int(10)");

                    b.Property<string>("Telephone")
                        .HasColumnName("telephone")
                        .HasMaxLength(255);

                    b.Property<DateTimeOffset>("UpdateDate")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("updateDate")
                        .HasDefaultValueSql("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP");

                    b.Property<string>("Username")
                        .IsRequired()
                        .HasColumnName("username")
                        .HasMaxLength(255);

                    b.HasKey("Id");

                    b.HasIndex("Id")
                        .HasName("ID__instructor__id");

                    b.HasIndex("StatusId");

                    b.ToTable("instructor");
                });

            modelBuilder.Entity("Database.Entities.InstructorConfirmCode", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("id")
                        .HasColumnType("int(10)");

                    b.Property<string>("Code")
                        .IsRequired()
                        .HasColumnName("code")
                        .HasMaxLength(255);

                    b.HasKey("Id");

                    b.HasIndex("Id")
                        .HasName("ID__instructor_confirm_code__id");

                    b.ToTable("instructor_confirm_code");
                });

            modelBuilder.Entity("Database.Entities.Laboratory", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("id")
                        .HasColumnType("int(10)");

                    b.Property<DateTimeOffset>("CreateDate")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("createDate")
                        .HasDefaultValueSql("CURRENT_TIMESTAMP");

                    b.Property<string>("Description")
                        .HasColumnName("description")
                        .HasMaxLength(255);

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasColumnName("name")
                        .HasMaxLength(255);

                    b.Property<int>("StatusId")
                        .HasColumnName("statusId")
                        .HasColumnType("int(10)");

                    b.Property<int>("TotalScore")
                        .HasColumnName("totalScore")
                        .HasColumnType("int(10)");

                    b.Property<DateTimeOffset>("UpdateDate")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("updateDate")
                        .HasDefaultValueSql("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP");

                    b.HasKey("Id");

                    b.HasIndex("Id")
                        .HasName("ID__laboratory__id");

                    b.HasIndex("StatusId");

                    b.ToTable("laboratory");
                });

            modelBuilder.Entity("Database.Entities.Status", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("id")
                        .HasColumnType("int(10)");

                    b.Property<string>("Description")
                        .HasColumnName("description")
                        .HasMaxLength(255);

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasColumnName("name")
                        .HasMaxLength(255);

                    b.HasKey("Id");

                    b.HasIndex("Id")
                        .HasName("ID__status__id");

                    b.ToTable("status");
                });

            modelBuilder.Entity("Database.Entities.Student", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("id")
                        .HasColumnType("int(10)");

                    b.Property<int>("CourseId")
                        .HasColumnName("courseId")
                        .HasColumnType("int(10)");

                    b.Property<DateTimeOffset>("CreateDate")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("createDate")
                        .HasDefaultValueSql("CURRENT_TIMESTAMP");

                    b.Property<string>("Email")
                        .IsRequired()
                        .HasColumnName("email")
                        .HasMaxLength(255);

                    b.Property<string>("Firstname")
                        .IsRequired()
                        .HasColumnName("firstname")
                        .HasMaxLength(255);

                    b.Property<string>("Image")
                        .IsRequired()
                        .HasColumnName("image")
                        .HasMaxLength(255);

                    b.Property<string>("Lastname")
                        .IsRequired()
                        .HasColumnName("lastname")
                        .HasMaxLength(255);

                    b.Property<int>("Number")
                        .HasColumnName("number")
                        .HasColumnType("int(10)");

                    b.Property<string>("Password")
                        .IsRequired()
                        .HasColumnName("password")
                        .HasMaxLength(255);

                    b.Property<int>("StatusId")
                        .HasColumnName("statusId")
                        .HasColumnType("int(10)");

                    b.Property<int>("StudentGroupId")
                        .HasColumnName("studentGroupId")
                        .HasColumnType("int(10)");

                    b.Property<string>("Telephone")
                        .HasColumnName("telephone")
                        .HasMaxLength(255);

                    b.Property<string>("UniversityIdNumber")
                        .IsRequired()
                        .HasColumnName("universityIdNumber")
                        .HasMaxLength(255);

                    b.Property<DateTimeOffset>("UpdateDate")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("updateDate")
                        .HasDefaultValueSql("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP");

                    b.Property<string>("Username")
                        .IsRequired()
                        .HasColumnName("username")
                        .HasMaxLength(255);

                    b.HasKey("Id");

                    b.HasIndex("CourseId");

                    b.HasIndex("Id")
                        .HasName("ID__student__id");

                    b.HasIndex("StatusId");

                    b.HasIndex("StudentGroupId");

                    b.ToTable("student");
                });

            modelBuilder.Entity("Database.Entities.StudentConfrimCode", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("id")
                        .HasColumnType("int(10)");

                    b.Property<string>("Code")
                        .IsRequired()
                        .HasColumnName("code")
                        .HasMaxLength(255);

                    b.Property<int>("CourseId")
                        .HasColumnName("courseId")
                        .HasColumnType("int(10)");

                    b.HasKey("Id");

                    b.HasIndex("CourseId");

                    b.HasIndex("Id")
                        .HasName("ID__student_confirm_code__id");

                    b.ToTable("student_confirm_code");
                });

            modelBuilder.Entity("Database.Entities.StudentGroup", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("id")
                        .HasColumnType("int(10)");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasColumnName("name")
                        .HasMaxLength(255);

                    b.HasKey("Id");

                    b.HasIndex("Id")
                        .HasName("ID__student_group__id");

                    b.ToTable("student_group");
                });

            modelBuilder.Entity("Database.Entities.SubTopic", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("id")
                        .HasColumnType("int(10)");

                    b.Property<DateTimeOffset>("CreateDate")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("createDate")
                        .HasDefaultValueSql("CURRENT_TIMESTAMP");

                    b.Property<string>("Description")
                        .HasColumnName("description")
                        .HasMaxLength(255);

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasColumnName("name")
                        .HasMaxLength(255);

                    b.Property<int>("StatusId")
                        .HasColumnName("statusId")
                        .HasColumnType("int(10)");

                    b.Property<DateTimeOffset>("UpdateDate")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("updateDate")
                        .HasDefaultValueSql("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP");

                    b.HasKey("Id");

                    b.HasIndex("Id")
                        .HasName("ID__sub_topic__id");

                    b.HasIndex("StatusId");

                    b.ToTable("sub_topic");
                });

            modelBuilder.Entity("Database.Entities.Topic", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("id")
                        .HasColumnType("int(10)");

                    b.Property<DateTimeOffset>("CreateDate")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("createDate")
                        .HasDefaultValueSql("CURRENT_TIMESTAMP");

                    b.Property<string>("Description")
                        .HasColumnName("description")
                        .HasMaxLength(255);

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasColumnName("name")
                        .HasMaxLength(255);

                    b.Property<int>("StatusId")
                        .HasColumnName("statusId")
                        .HasColumnType("int(10)");

                    b.Property<DateTimeOffset>("UpdateDate")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("updateDate")
                        .HasDefaultValueSql("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP");

                    b.HasKey("Id");

                    b.HasIndex("Id")
                        .HasName("ID__topic__id");

                    b.HasIndex("StatusId");

                    b.ToTable("topic");
                });

            modelBuilder.Entity("Database.Entities.Course", b =>
                {
                    b.HasOne("Database.Entities.Status", "Status")
                        .WithMany("Courses")
                        .HasForeignKey("StatusId")
                        .HasConstraintName("FK__course_statusId__status_id")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Database.Entities.CourseLaboratory", b =>
                {
                    b.HasOne("Database.Entities.Course", "Course")
                        .WithMany("CourseLaboratories")
                        .HasForeignKey("CourseId")
                        .HasConstraintName("FK__course_laboratory_courseId__course_id")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Database.Entities.Laboratory", "Laboratory")
                        .WithMany("CourseLaboratories")
                        .HasForeignKey("LaboratoryId")
                        .HasConstraintName("FK__course_laboratory_labId__laboratory_id")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Database.Entities.Criteria", b =>
                {
                    b.HasOne("Database.Entities.CriteriaType", "CriteriaType")
                        .WithMany("Criterias")
                        .HasForeignKey("CriteriaTypeId")
                        .HasConstraintName("FK__criteria_criteriaTypeId__criteria_type_id")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Database.Entities.Status", "Status")
                        .WithMany("Criterias")
                        .HasForeignKey("StatusId")
                        .HasConstraintName("FK__criteria_statusId__status_id")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Database.Entities.CriteriaType", b =>
                {
                    b.HasOne("Database.Entities.Status", "Status")
                        .WithMany("CriteriaTypes")
                        .HasForeignKey("StatusId")
                        .HasConstraintName("FK__criteria_type_statusId__status_id")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Database.Entities.Critical", b =>
                {
                    b.HasOne("Database.Entities.CriteriaType", "CriteriaType")
                        .WithMany("Criticals")
                        .HasForeignKey("CriteriaTypeId")
                        .HasConstraintName("FK__critical_criteriaTypeId__criteria_type_id")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Database.Entities.Status", "Status")
                        .WithMany("Criticals")
                        .HasForeignKey("StatusId")
                        .HasConstraintName("FK__critical_statusId__status_id")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Database.Entities.Experience", b =>
                {
                    b.HasOne("Database.Entities.Status", "Status")
                        .WithMany("Experiences")
                        .HasForeignKey("StatusId")
                        .HasConstraintName("FK__experience_statusId__status_id")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Database.Entities.Instructor", b =>
                {
                    b.HasOne("Database.Entities.Status", "Status")
                        .WithMany("Instructors")
                        .HasForeignKey("StatusId")
                        .HasConstraintName("FK__instructor_statusId__status_id")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Database.Entities.Laboratory", b =>
                {
                    b.HasOne("Database.Entities.Status", "Status")
                        .WithMany("Laboratories")
                        .HasForeignKey("StatusId")
                        .HasConstraintName("FK__laboratory_statusId__status_id")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Database.Entities.Student", b =>
                {
                    b.HasOne("Database.Entities.Course", "Course")
                        .WithMany("Students")
                        .HasForeignKey("CourseId")
                        .HasConstraintName("FK__student_courseId__course_id")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Database.Entities.Status", "Status")
                        .WithMany("Students")
                        .HasForeignKey("StatusId")
                        .HasConstraintName("FK__student_statusId__status_id")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Database.Entities.StudentGroup", "StudentGroup")
                        .WithMany("Students")
                        .HasForeignKey("StudentGroupId")
                        .HasConstraintName("FK__student_studentGroupId__student_group_id")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Database.Entities.StudentConfrimCode", b =>
                {
                    b.HasOne("Database.Entities.Course", "Course")
                        .WithMany("StudentConfrimCodes")
                        .HasForeignKey("CourseId")
                        .HasConstraintName("FK__student_confirm_code_courseId__course_id")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Database.Entities.SubTopic", b =>
                {
                    b.HasOne("Database.Entities.Status", "Status")
                        .WithMany("SubTopics")
                        .HasForeignKey("StatusId")
                        .HasConstraintName("FK__sub_topic_statusId__status_id")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Database.Entities.Topic", b =>
                {
                    b.HasOne("Database.Entities.Status", "Status")
                        .WithMany("Topics")
                        .HasForeignKey("StatusId")
                        .HasConstraintName("FK__topic_statusId__status_id")
                        .OnDelete(DeleteBehavior.Cascade);
                });
#pragma warning restore 612, 618
        }
    }
}
