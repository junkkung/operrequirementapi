﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Database.Migrations
{
    public partial class rename_clv : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "student_laboratory_clv_teeth_approval");

            migrationBuilder.DropTable(
                name: "student_laboratory_clv_teeth_confirm");

            migrationBuilder.CreateTable(
                name: "student_laboratory_teeth_approval",
                columns: table => new
                {
                    id = table.Column<int>(type: "int(10)", nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    studentId = table.Column<int>(type: "int(10)", nullable: false),
                    isApproved = table.Column<short>(type: "bit(1)", nullable: true),
                    comment = table.Column<string>(maxLength: 255, nullable: true),
                    statusId = table.Column<int>(type: "int(10)", nullable: false),
                    instructorId = table.Column<int>(type: "int(10)", nullable: true),
                    laboratoryId = table.Column<int>(type: "int(10)", nullable: false),
                    createDate = table.Column<DateTimeOffset>(nullable: false, defaultValueSql: "CURRENT_TIMESTAMP"),
                    updateDate = table.Column<DateTimeOffset>(nullable: false, defaultValueSql: "CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_student_laboratory_teeth_approval", x => x.id);
                    table.ForeignKey(
                        name: "FK__student_lab_app__instId__instructor_id",
                        column: x => x.instructorId,
                        principalTable: "instructor",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK__student_lab_app__labId__lab_id",
                        column: x => x.laboratoryId,
                        principalTable: "laboratory",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK__student_lab_app__statusId__status_id",
                        column: x => x.statusId,
                        principalTable: "status",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK__student_lab_app__studentId__student_id",
                        column: x => x.studentId,
                        principalTable: "student",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "student_laboratory_teeth_confirm",
                columns: table => new
                {
                    id = table.Column<int>(type: "int(10)", nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    studentId = table.Column<int>(type: "int(10)", nullable: false),
                    teethNo = table.Column<string>(maxLength: 255, nullable: true),
                    canUse = table.Column<short>(type: "bit(1)", nullable: false, defaultValue: (short)0),
                    canNotUse = table.Column<short>(type: "bit(1)", nullable: false, defaultValue: (short)0),
                    useInLab = table.Column<short>(type: "bit(1)", nullable: false, defaultValue: (short)0),
                    useInControl = table.Column<short>(type: "bit(1)", nullable: false, defaultValue: (short)0),
                    comment = table.Column<string>(maxLength: 255, nullable: true),
                    statusId = table.Column<int>(type: "int(10)", nullable: false),
                    InstructorId = table.Column<int>(nullable: true),
                    laboratoryId = table.Column<int>(type: "int(10)", nullable: false),
                    createDate = table.Column<DateTimeOffset>(nullable: false, defaultValueSql: "CURRENT_TIMESTAMP"),
                    updateDate = table.Column<DateTimeOffset>(nullable: false, defaultValueSql: "CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_student_laboratory_teeth_confirm", x => x.id);
                    table.ForeignKey(
                        name: "FK__student_lab_con__instId__instructor_id",
                        column: x => x.InstructorId,
                        principalTable: "instructor",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK__student_lab_con__labId__lab_id",
                        column: x => x.laboratoryId,
                        principalTable: "laboratory",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK__student_lab_con__statusId__status_id",
                        column: x => x.statusId,
                        principalTable: "status",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK__student_lab_con__studentId__student_id",
                        column: x => x.studentId,
                        principalTable: "student",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "ID__sub_topic__id",
                table: "student_laboratory_teeth_approval",
                column: "id");

            migrationBuilder.CreateIndex(
                name: "IX_student_laboratory_teeth_approval_instructorId",
                table: "student_laboratory_teeth_approval",
                column: "instructorId");

            migrationBuilder.CreateIndex(
                name: "IX_student_laboratory_teeth_approval_laboratoryId",
                table: "student_laboratory_teeth_approval",
                column: "laboratoryId");

            migrationBuilder.CreateIndex(
                name: "IX_student_laboratory_teeth_approval_statusId",
                table: "student_laboratory_teeth_approval",
                column: "statusId");

            migrationBuilder.CreateIndex(
                name: "IX_student_laboratory_teeth_approval_studentId",
                table: "student_laboratory_teeth_approval",
                column: "studentId");

            migrationBuilder.CreateIndex(
                name: "ID__sub_topic__id",
                table: "student_laboratory_teeth_confirm",
                column: "id");

            migrationBuilder.CreateIndex(
                name: "IX_student_laboratory_teeth_confirm_InstructorId",
                table: "student_laboratory_teeth_confirm",
                column: "InstructorId");

            migrationBuilder.CreateIndex(
                name: "IX_student_laboratory_teeth_confirm_laboratoryId",
                table: "student_laboratory_teeth_confirm",
                column: "laboratoryId");

            migrationBuilder.CreateIndex(
                name: "IX_student_laboratory_teeth_confirm_statusId",
                table: "student_laboratory_teeth_confirm",
                column: "statusId");

            migrationBuilder.CreateIndex(
                name: "IX_student_laboratory_teeth_confirm_studentId",
                table: "student_laboratory_teeth_confirm",
                column: "studentId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "student_laboratory_teeth_approval");

            migrationBuilder.DropTable(
                name: "student_laboratory_teeth_confirm");

            migrationBuilder.CreateTable(
                name: "student_laboratory_clv_teeth_approval",
                columns: table => new
                {
                    id = table.Column<int>(type: "int(10)", nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    comment = table.Column<string>(maxLength: 255, nullable: true),
                    createDate = table.Column<DateTimeOffset>(nullable: false, defaultValueSql: "CURRENT_TIMESTAMP"),
                    instructorId = table.Column<int>(type: "int(10)", nullable: true),
                    isApproved = table.Column<short>(type: "bit(1)", nullable: true),
                    laboratoryId = table.Column<int>(type: "int(10)", nullable: false),
                    statusId = table.Column<int>(type: "int(10)", nullable: false),
                    studentId = table.Column<int>(type: "int(10)", nullable: false),
                    updateDate = table.Column<DateTimeOffset>(nullable: false, defaultValueSql: "CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_student_laboratory_clv_teeth_approval", x => x.id);
                    table.ForeignKey(
                        name: "FK__student_lab_clv_app__instId__instructor_id",
                        column: x => x.instructorId,
                        principalTable: "instructor",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK__student_lab_clv_app__labId__lab_id",
                        column: x => x.laboratoryId,
                        principalTable: "laboratory",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK__student_lab_clv_app__statusId__status_id",
                        column: x => x.statusId,
                        principalTable: "status",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK__student_lab_clv_app__studentId__student_id",
                        column: x => x.studentId,
                        principalTable: "student",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "student_laboratory_clv_teeth_confirm",
                columns: table => new
                {
                    id = table.Column<int>(type: "int(10)", nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    canNotUse = table.Column<short>(type: "bit(1)", nullable: false, defaultValue: (short)0),
                    canUse = table.Column<short>(type: "bit(1)", nullable: false, defaultValue: (short)0),
                    comment = table.Column<string>(maxLength: 255, nullable: true),
                    createDate = table.Column<DateTimeOffset>(nullable: false, defaultValueSql: "CURRENT_TIMESTAMP"),
                    InstructorId = table.Column<int>(nullable: true),
                    laboratoryId = table.Column<int>(type: "int(10)", nullable: false),
                    statusId = table.Column<int>(type: "int(10)", nullable: false),
                    studentId = table.Column<int>(type: "int(10)", nullable: false),
                    teethNo = table.Column<string>(maxLength: 255, nullable: true),
                    updateDate = table.Column<DateTimeOffset>(nullable: false, defaultValueSql: "CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"),
                    useInControl = table.Column<short>(type: "bit(1)", nullable: false, defaultValue: (short)0),
                    useInLab = table.Column<short>(type: "bit(1)", nullable: false, defaultValue: (short)0)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_student_laboratory_clv_teeth_confirm", x => x.id);
                    table.ForeignKey(
                        name: "FK__student_lab_clv_con__instId__instructor_id",
                        column: x => x.InstructorId,
                        principalTable: "instructor",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK__student_lab_clv_con__labId__lab_id",
                        column: x => x.laboratoryId,
                        principalTable: "laboratory",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK__student_lab_clv_con__statusId__status_id",
                        column: x => x.statusId,
                        principalTable: "status",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK__student_lab_clv_con__studentId__student_id",
                        column: x => x.studentId,
                        principalTable: "student",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "ID__sub_topic__id",
                table: "student_laboratory_clv_teeth_approval",
                column: "id");

            migrationBuilder.CreateIndex(
                name: "IX_student_laboratory_clv_teeth_approval_instructorId",
                table: "student_laboratory_clv_teeth_approval",
                column: "instructorId");

            migrationBuilder.CreateIndex(
                name: "IX_student_laboratory_clv_teeth_approval_laboratoryId",
                table: "student_laboratory_clv_teeth_approval",
                column: "laboratoryId");

            migrationBuilder.CreateIndex(
                name: "IX_student_laboratory_clv_teeth_approval_statusId",
                table: "student_laboratory_clv_teeth_approval",
                column: "statusId");

            migrationBuilder.CreateIndex(
                name: "IX_student_laboratory_clv_teeth_approval_studentId",
                table: "student_laboratory_clv_teeth_approval",
                column: "studentId");

            migrationBuilder.CreateIndex(
                name: "ID__sub_topic__id",
                table: "student_laboratory_clv_teeth_confirm",
                column: "id");

            migrationBuilder.CreateIndex(
                name: "IX_student_laboratory_clv_teeth_confirm_InstructorId",
                table: "student_laboratory_clv_teeth_confirm",
                column: "InstructorId");

            migrationBuilder.CreateIndex(
                name: "IX_student_laboratory_clv_teeth_confirm_laboratoryId",
                table: "student_laboratory_clv_teeth_confirm",
                column: "laboratoryId");

            migrationBuilder.CreateIndex(
                name: "IX_student_laboratory_clv_teeth_confirm_statusId",
                table: "student_laboratory_clv_teeth_confirm",
                column: "statusId");

            migrationBuilder.CreateIndex(
                name: "IX_student_laboratory_clv_teeth_confirm_studentId",
                table: "student_laboratory_clv_teeth_confirm",
                column: "studentId");
        }
    }
}
