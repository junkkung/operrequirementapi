﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Database.Migrations
{
    public partial class change_column_suptopicId_to_subTopicId_at_criteria_table : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {

            migrationBuilder.Sql($"ALTER TABLE `criteria` DROP FOREIGN KEY `FK__criteria_subTopicId__sub_topic_id`; ");

            migrationBuilder.Sql($"ALTER TABLE `criteria` DROP INDEX `IX_criteria_supTopicId`; ");

            migrationBuilder.Sql($"ALTER TABLE `criteria` CHANGE `supTopicId` `subTopicId` INT(10) NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_criteria_subTopicId",
                table: "criteria",
                column: "subTopicId");

            migrationBuilder.AddForeignKey(
                name: "FK__criteria_subTopicId__sub_topic_id",
                table: "criteria",
                column: "subTopicId",
                principalTable: "sub_topic",
                principalColumn: "id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

            migrationBuilder.Sql($"ALTER TABLE `criteria` DROP FOREIGN KEY `FK__criteria_subTopicId__sub_topic_id`; ");

            migrationBuilder.Sql($"ALTER TABLE `criteria` DROP INDEX `IX_criteria_subTopicId`; ");

            migrationBuilder.Sql($"ALTER TABLE `criteria` CHANGE `subTopicId` `supTopicId` INT(10) NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_criteria_supTopicId",
                table: "criteria",
                column: "supTopicId");

            migrationBuilder.AddForeignKey(
                name: "FK__criteria_subTopicId__sub_topic_id",
                table: "criteria",
                column: "supTopicId",
                principalTable: "sub_topic",
                principalColumn: "id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
