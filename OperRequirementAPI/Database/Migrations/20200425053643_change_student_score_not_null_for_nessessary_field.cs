﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Database.Migrations
{
    public partial class change_student_score_not_null_for_nessessary_field : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql($"ALTER TABLE `student_score` DROP FOREIGN KEY `FK__student_score_criteriaId__criteria_id`; ");

            migrationBuilder.AlterColumn<int>(
                name: "score",
                table: "student_score",
                type: "int(10)",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "int(10)",
                oldNullable: true);

            migrationBuilder.AlterColumn<short>(
                name: "isCritical",
                table: "student_score",
                type: "bit(1)",
                nullable: false,
                oldClrType: typeof(short),
                oldType: "bit(1)",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "criteriaId",
                table: "student_score",
                type: "int(10)",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "int(10)",
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK__student_score_criteriaId__criteria_id",
                table: "student_score",
                column: "criteriaId",
                principalTable: "criteria",
                principalColumn: "id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql($"ALTER TABLE `student_score` DROP FOREIGN KEY `FK__student_score_criteriaId__criteria_id`; ");

            migrationBuilder.AlterColumn<int>(
                name: "score",
                table: "student_score",
                type: "int(10)",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int(10)");

            migrationBuilder.AlterColumn<short>(
                name: "isCritical",
                table: "student_score",
                type: "bit(1)",
                nullable: true,
                oldClrType: typeof(short),
                oldType: "bit(1)");

            migrationBuilder.AlterColumn<int>(
                name: "criteriaId",
                table: "student_score",
                type: "int(10)",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int(10)");

            migrationBuilder.AddForeignKey(
                name: "FK__student_score_criteriaId__criteria_id",
                table: "student_score",
                column: "criteriaId",
                principalTable: "criteria",
                principalColumn: "id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
