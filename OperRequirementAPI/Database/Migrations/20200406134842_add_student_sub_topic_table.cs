﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Database.Migrations
{
    public partial class add_student_sub_topic_table : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "student_sub_topic",
                columns: table => new
                {
                    id = table.Column<int>(type: "int(10)", nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    studentLaboratoryId = table.Column<int>(type: "int(10)", nullable: false),
                    subTopicId = table.Column<int>(type: "int(10)", nullable: false),
                    selfAssessment = table.Column<int>(type: "int(10)", nullable: true),
                    createDate = table.Column<DateTimeOffset>(nullable: false, defaultValueSql: "CURRENT_TIMESTAMP"),
                    updateDate = table.Column<DateTimeOffset>(nullable: false, defaultValueSql: "CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"),
                    statusId = table.Column<int>(type: "int(10)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_student_sub_topic", x => x.id);
                    table.ForeignKey(
                        name: "FK__student_sub_topic_statusId__status_id",
                        column: x => x.statusId,
                        principalTable: "status",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK__student_sub_topic_stulabId__stu_laboratory_id",
                        column: x => x.studentLaboratoryId,
                        principalTable: "student_laboratory",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK__student_sub_topic_subTopicId__sub_topic_id",
                        column: x => x.subTopicId,
                        principalTable: "sub_topic",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "ID__student_sub_topic__id",
                table: "student_sub_topic",
                column: "id");

            migrationBuilder.CreateIndex(
                name: "IX_student_sub_topic_statusId",
                table: "student_sub_topic",
                column: "statusId");

            migrationBuilder.CreateIndex(
                name: "IX_student_sub_topic_studentLaboratoryId",
                table: "student_sub_topic",
                column: "studentLaboratoryId");

            migrationBuilder.CreateIndex(
                name: "IX_student_sub_topic_subTopicId",
                table: "student_sub_topic",
                column: "subTopicId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "student_sub_topic");
        }
    }
}
