﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Database.Migrations
{
    public partial class add_student_minus_point_table : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "student_minus_point",
                columns: table => new
                {
                    id = table.Column<int>(type: "int(10)", nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    minusPointId = table.Column<int>(type: "int(10)", nullable: false),
                    studentLaboratoryId = table.Column<int>(type: "int(10)", nullable: false),
                    instructorId = table.Column<int>(type: "int(10)", nullable: false),
                    score = table.Column<int>(type: "int(10)", nullable: false),
                    remark = table.Column<string>(maxLength: 255, nullable: true),
                    createDate = table.Column<DateTimeOffset>(nullable: false, defaultValueSql: "CURRENT_TIMESTAMP"),
                    updateDate = table.Column<DateTimeOffset>(nullable: false, defaultValueSql: "CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"),
                    statusId = table.Column<int>(type: "int(10)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_student_minus_point", x => x.id);
                    table.ForeignKey(
                        name: "FK__student_minus_p_instructorId__instructor_id",
                        column: x => x.instructorId,
                        principalTable: "instructor",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK__student_minus_p_minusPointId__minus_point_id",
                        column: x => x.minusPointId,
                        principalTable: "minus_point",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK__student_minus_p_statusId__status_id",
                        column: x => x.statusId,
                        principalTable: "status",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK__student_minus_p_stuLabId__stu_lab_id",
                        column: x => x.studentLaboratoryId,
                        principalTable: "student_laboratory",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "ID__sub_topic__id",
                table: "student_minus_point",
                column: "id");

            migrationBuilder.CreateIndex(
                name: "IX_student_minus_point_instructorId",
                table: "student_minus_point",
                column: "instructorId");

            migrationBuilder.CreateIndex(
                name: "IX_student_minus_point_minusPointId",
                table: "student_minus_point",
                column: "minusPointId");

            migrationBuilder.CreateIndex(
                name: "IX_student_minus_point_statusId",
                table: "student_minus_point",
                column: "statusId");

            migrationBuilder.CreateIndex(
                name: "IX_student_minus_point_studentLaboratoryId",
                table: "student_minus_point",
                column: "studentLaboratoryId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "student_minus_point");
        }
    }
}
