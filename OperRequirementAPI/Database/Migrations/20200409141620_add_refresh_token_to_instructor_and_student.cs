﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Database.Migrations
{
    public partial class add_refresh_token_to_instructor_and_student : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "refreshToken",
                table: "student",
                maxLength: 255,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "refreshToken",
                table: "instructor",
                maxLength: 255,
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "refreshToken",
                table: "student");

            migrationBuilder.DropColumn(
                name: "refreshToken",
                table: "instructor");
        }
    }
}
