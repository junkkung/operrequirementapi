﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Database.Migrations
{
    public partial class student_image_can_be_null : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            //migrationBuilder.AlterColumn<string>(
            //    name: "image",
            //    table: "student",
            //    maxLength: 255,
            //    nullable: true,
            //    oldClrType: typeof(string),
            //    oldMaxLength: 255);

            migrationBuilder.Sql($"ALTER TABLE `student` CHANGE `image` `image` varchar(255);");

        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            //migrationBuilder.AlterColumn<string>(
            //    name: "image",
            //    table: "student",
            //    maxLength: 255,
            //    nullable: false,
            //    oldClrType: typeof(string),
            //    oldMaxLength: 255,
            //    oldNullable: true);

            migrationBuilder.Sql($"ALTER TABLE `student` CHANGE `image` `image` varchar(255) NOT NULL;");

        }
    }
}
