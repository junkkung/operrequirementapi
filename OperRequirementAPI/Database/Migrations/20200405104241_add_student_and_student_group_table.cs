﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Database.Migrations
{
    public partial class add_student_and_student_group_table : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "student_group",
                columns: table => new
                {
                    id = table.Column<int>(type: "int(10)", nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    name = table.Column<string>(maxLength: 255, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_student_group", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "student",
                columns: table => new
                {
                    id = table.Column<int>(type: "int(10)", nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    firstname = table.Column<string>(maxLength: 255, nullable: false),
                    lastname = table.Column<string>(maxLength: 255, nullable: false),
                    username = table.Column<string>(maxLength: 255, nullable: false),
                    password = table.Column<string>(maxLength: 255, nullable: false),
                    image = table.Column<string>(maxLength: 255, nullable: false),
                    email = table.Column<string>(maxLength: 255, nullable: false),
                    telephone = table.Column<string>(maxLength: 255, nullable: true),
                    courseId = table.Column<int>(type: "int(10)", nullable: false),
                    universityIdNumber = table.Column<string>(maxLength: 255, nullable: false),
                    number = table.Column<int>(type: "int(10)", nullable: false),
                    studentGroupId = table.Column<int>(type: "int(10)", nullable: false),
                    createDate = table.Column<DateTimeOffset>(nullable: false, defaultValueSql: "CURRENT_TIMESTAMP"),
                    updateDate = table.Column<DateTimeOffset>(nullable: false, defaultValueSql: "CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"),
                    statusId = table.Column<int>(type: "int(10)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_student", x => x.id);
                    table.ForeignKey(
                        name: "FK__student_courseId__course_id",
                        column: x => x.courseId,
                        principalTable: "course",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK__student_statusId__status_id",
                        column: x => x.statusId,
                        principalTable: "status",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK__student_studentGroupId__student_group_id",
                        column: x => x.studentGroupId,
                        principalTable: "student_group",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_student_courseId",
                table: "student",
                column: "courseId");

            migrationBuilder.CreateIndex(
                name: "ID__student__id",
                table: "student",
                column: "id");

            migrationBuilder.CreateIndex(
                name: "IX_student_statusId",
                table: "student",
                column: "statusId");

            migrationBuilder.CreateIndex(
                name: "IX_student_studentGroupId",
                table: "student",
                column: "studentGroupId");

            migrationBuilder.CreateIndex(
                name: "ID__student_group__id",
                table: "student_group",
                column: "id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "student");

            migrationBuilder.DropTable(
                name: "student_group");
        }
    }
}
