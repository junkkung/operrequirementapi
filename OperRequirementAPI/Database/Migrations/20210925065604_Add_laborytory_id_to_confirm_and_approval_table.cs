﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Database.Migrations
{
    public partial class Add_laborytory_id_to_confirm_and_approval_table : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "laboratoryId",
                table: "student_laboratory_clv_teeth_confirm",
                type: "int(10)",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "laboratoryId",
                table: "student_laboratory_clv_teeth_approval",
                type: "int(10)",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_student_laboratory_clv_teeth_confirm_laboratoryId",
                table: "student_laboratory_clv_teeth_confirm",
                column: "laboratoryId");

            migrationBuilder.CreateIndex(
                name: "IX_student_laboratory_clv_teeth_approval_laboratoryId",
                table: "student_laboratory_clv_teeth_approval",
                column: "laboratoryId");

            migrationBuilder.AddForeignKey(
                name: "FK__student_lab_clv_app__labId__lab_id",
                table: "student_laboratory_clv_teeth_approval",
                column: "laboratoryId",
                principalTable: "laboratory",
                principalColumn: "id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK__student_lab_clv_con__labId__lab_id",
                table: "student_laboratory_clv_teeth_confirm",
                column: "laboratoryId",
                principalTable: "laboratory",
                principalColumn: "id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK__student_lab_clv_app__labId__lab_id",
                table: "student_laboratory_clv_teeth_approval");

            migrationBuilder.DropForeignKey(
                name: "FK__student_lab_clv_con__labId__lab_id",
                table: "student_laboratory_clv_teeth_confirm");

            migrationBuilder.DropIndex(
                name: "IX_student_laboratory_clv_teeth_confirm_laboratoryId",
                table: "student_laboratory_clv_teeth_confirm");

            migrationBuilder.DropIndex(
                name: "IX_student_laboratory_clv_teeth_approval_laboratoryId",
                table: "student_laboratory_clv_teeth_approval");

            migrationBuilder.DropColumn(
                name: "laboratoryId",
                table: "student_laboratory_clv_teeth_confirm");

            migrationBuilder.DropColumn(
                name: "laboratoryId",
                table: "student_laboratory_clv_teeth_approval");
        }
    }
}
