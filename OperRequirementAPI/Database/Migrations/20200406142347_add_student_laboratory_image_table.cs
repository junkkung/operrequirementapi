﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Database.Migrations
{
    public partial class add_student_laboratory_image_table : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "student_laboratory_image",
                columns: table => new
                {
                    id = table.Column<int>(type: "int(10)", nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    studentLaboratoryId = table.Column<int>(type: "int(10)", nullable: false),
                    image = table.Column<string>(maxLength: 255, nullable: true),
                    remark = table.Column<string>(maxLength: 255, nullable: true),
                    createDate = table.Column<DateTimeOffset>(nullable: false, defaultValueSql: "CURRENT_TIMESTAMP"),
                    updateDate = table.Column<DateTimeOffset>(nullable: false, defaultValueSql: "CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"),
                    statusId = table.Column<int>(type: "int(10)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_student_laboratory_image", x => x.id);
                    table.ForeignKey(
                        name: "FK__student_lab_Image_statusId__status_id",
                        column: x => x.statusId,
                        principalTable: "status",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK__student_lab_Image_stuLabId__stu_lab_id",
                        column: x => x.studentLaboratoryId,
                        principalTable: "student_laboratory",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "ID__sub_topic__id",
                table: "student_laboratory_image",
                column: "id");

            migrationBuilder.CreateIndex(
                name: "IX_student_laboratory_image_statusId",
                table: "student_laboratory_image",
                column: "statusId");

            migrationBuilder.CreateIndex(
                name: "IX_student_laboratory_image_studentLaboratoryId",
                table: "student_laboratory_image",
                column: "studentLaboratoryId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "student_laboratory_image");
        }
    }
}
