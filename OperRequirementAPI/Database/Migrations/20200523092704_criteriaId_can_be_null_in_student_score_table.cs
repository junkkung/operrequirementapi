﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Database.Migrations
{
    public partial class criteriaId_can_be_null_in_student_score_table : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql($"ALTER TABLE `student_score` DROP FOREIGN KEY `FK__student_score_criteriaId__criteria_id`; ");

            migrationBuilder.AlterColumn<short>(
                name: "isCritical",
                table: "student_score",
                type: "bit(1)",
                nullable: false,
                defaultValue: (short)0,
                oldClrType: typeof(short),
                oldType: "bit(1)");

            migrationBuilder.AlterColumn<int>(
                name: "criteriaId",
                table: "student_score",
                type: "int(10)",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int(10)");

            migrationBuilder.Sql($"ALTER TABLE student_score MODIFY `criteriaId` int(10);");

            migrationBuilder.AddForeignKey(
                name: "FK__student_score_criteriaId__criteria_id",
                table: "student_score",
                column: "criteriaId",
                principalTable: "criteria",
                principalColumn: "id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql($"ALTER TABLE `student_score` DROP FOREIGN KEY `FK__student_score_criteriaId__criteria_id`; ");

            migrationBuilder.AlterColumn<short>(
                name: "isCritical",
                table: "student_score",
                type: "bit(1)",
                nullable: false,
                oldClrType: typeof(short),
                oldType: "bit(1)",
                oldDefaultValue: (short)0);

            migrationBuilder.AlterColumn<int>(
                name: "criteriaId",
                table: "student_score",
                type: "int(10)",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "int(10)",
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK__student_score_criteriaId__criteria_id",
                table: "student_score",
                column: "criteriaId",
                principalTable: "criteria",
                principalColumn: "id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
