﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Database.Migrations
{
    public partial class change_column_type_teeth_no : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            //migrationBuilder.RenameColumn(
            //    name: "teethNo",
            //    table: "student_laboratory_clv_teeth_confirm",
            //    newName: "teetNo");

            //migrationBuilder.AlterColumn<string>(
            //    name: "teetNo",
            //    table: "student_laboratory_clv_teeth_confirm",
            //    maxLength: 255,
            //    nullable: true,
            //    oldClrType: typeof(int),
            //    oldType: "int(10)");

            migrationBuilder.Sql($"ALTER TABLE `student_laboratory_clv_teeth_confirm` CHANGE `teethNo` `teethNo` varchar(255);");

        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql($"ALTER TABLE `student_laboratory_clv_teeth_confirm` CHANGE `teethNo` `teethNo` int(10) null;");

            //migrationBuilder.RenameColumn(
            //    name: "teetNo",
            //    table: "student_laboratory_clv_teeth_confirm",
            //    newName: "teethNo");

            //migrationBuilder.AlterColumn<int>(
            //    name: "teethNo",
            //    table: "student_laboratory_clv_teeth_confirm",
            //    type: "int(10)",
            //    nullable: false,
            //    oldClrType: typeof(string),
            //    oldMaxLength: 255,
            //    oldNullable: true);
        }
    }
}
