﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Database.Migrations
{
    public partial class delete_sub_topic_group_table : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql($"ALTER TABLE `sub_topic` DROP FOREIGN KEY `FK_sub_topic_sub_topic_group_subTopicGroupId`; ");

            migrationBuilder.DropTable(
                name: "sub_topic_group");

            migrationBuilder.Sql($"ALTER TABLE `sub_topic` DROP INDEX `IX_sub_topic_subTopicGroupId`; ");

            migrationBuilder.DropColumn(
                name: "subTopicGroupId",
                table: "sub_topic");

            migrationBuilder.AddColumn<short>(
                name: "isLabel",
                table: "sub_topic",
                type: "bit(1)",
                nullable: false,
                defaultValue: (short)0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "isLabel",
                table: "sub_topic");

            migrationBuilder.AddColumn<int>(
                name: "subTopicGroupId",
                table: "sub_topic",
                type: "int(10)",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "sub_topic_group",
                columns: table => new
                {
                    id = table.Column<int>(type: "int(10)", nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    createDate = table.Column<DateTimeOffset>(nullable: false, defaultValueSql: "CURRENT_TIMESTAMP"),
                    description = table.Column<string>(maxLength: 255, nullable: true),
                    name = table.Column<string>(maxLength: 255, nullable: false),
                    statusId = table.Column<int>(type: "int(10)", nullable: false),
                    updateDate = table.Column<DateTimeOffset>(nullable: false, defaultValueSql: "CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_sub_topic_group", x => x.id);
                    table.ForeignKey(
                        name: "FK__sub_topic_group_statusId__status_id",
                        column: x => x.statusId,
                        principalTable: "status",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_sub_topic_subTopicGroupId",
                table: "sub_topic",
                column: "subTopicGroupId");

            migrationBuilder.CreateIndex(
                name: "IX__sub_topic_group__id",
                table: "sub_topic_group",
                column: "id");

            migrationBuilder.CreateIndex(
                name: "IX_sub_topic_group_statusId",
                table: "sub_topic_group",
                column: "statusId");

            migrationBuilder.AddForeignKey(
                name: "FK_sub_topic_sub_topic_group_subTopicGroupId",
                table: "sub_topic",
                column: "subTopicGroupId",
                principalTable: "sub_topic_group",
                principalColumn: "id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
