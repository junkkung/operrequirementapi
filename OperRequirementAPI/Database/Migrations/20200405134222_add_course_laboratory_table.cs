﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Database.Migrations
{
    public partial class add_course_laboratory_table : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "course_laboratory",
                columns: table => new
                {
                    courseId = table.Column<int>(type: "int(10)", nullable: false),
                    laboratoryId = table.Column<int>(type: "int(10)", nullable: false),
                    sequence = table.Column<int>(type: "int(4)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_course_laboratory", x => new { x.courseId, x.laboratoryId });
                    table.ForeignKey(
                        name: "FK__course_laboratory_courseId__course_id",
                        column: x => x.courseId,
                        principalTable: "course",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK__course_laboratory_labId__laboratory_id",
                        column: x => x.laboratoryId,
                        principalTable: "laboratory",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_course_laboratory_laboratoryId",
                table: "course_laboratory",
                column: "laboratoryId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "course_laboratory");
        }
    }
}
