﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Database.Migrations
{
    public partial class change_table_student_lab_image_reference_to_student_and_lab_image_table : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql($"ALTER TABLE `student_laboratory_image` DROP FOREIGN KEY `FK__student_lab_Image_stuLabId__stu_lab_id`; ");

            migrationBuilder.Sql($"ALTER TABLE `student_laboratory_image` CHANGE `studentLaboratoryId` `studentId` INT(10) NOT NULL");

            migrationBuilder.Sql($"ALTER TABLE `student_laboratory_image` DROP INDEX `IX_student_laboratory_image_studentLaboratoryId`; ");

            migrationBuilder.CreateIndex(
                name: "IX_student_laboratory_image_studentId",
                table: "student_laboratory_image",
                column: "studentId");

            migrationBuilder.AddColumn<int>(
                name: "laboratoryImageId",
                table: "student_laboratory_image",
                type: "int(10)",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_student_laboratory_image_laboratoryImageId",
                table: "student_laboratory_image",
                column: "laboratoryImageId");

            migrationBuilder.AddForeignKey(
                name: "FK__student_lab_Image_labImgId__lab_img_id",
                table: "student_laboratory_image",
                column: "laboratoryImageId",
                principalTable: "laboratory_image",
                principalColumn: "id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK__student_lab_Image_studentId__student_id",
                table: "student_laboratory_image",
                column: "studentId",
                principalTable: "student",
                principalColumn: "id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql($"ALTER TABLE `student_laboratory_image` DROP FOREIGN KEY `FK__student_lab_Image_labImgId__lab_img_id`; ");

            migrationBuilder.Sql($"ALTER TABLE `student_laboratory_image` DROP FOREIGN KEY `FK__student_lab_Image_studentId__student_id`; ");

            migrationBuilder.Sql($"ALTER TABLE `student_laboratory_image` DROP INDEX `IX_student_laboratory_image_laboratoryImageId`; ");

            migrationBuilder.DropColumn(
                name: "laboratoryImageId",
                table: "student_laboratory_image");

            migrationBuilder.Sql($"ALTER TABLE `student_laboratory_image` CHANGE `studentId` `studentLaboratoryId` INT(10) NOT NULL");

            migrationBuilder.Sql($"ALTER TABLE `student_laboratory_image` DROP INDEX `IX_student_laboratory_image_studentId`; ");

            migrationBuilder.CreateIndex(
                name: "IX_student_laboratory_image_studentLaboratoryId",
                table: "student_laboratory_image",
                column: "studentLaboratoryId");

            migrationBuilder.AddForeignKey(
                name: "FK__student_lab_Image_stuLabId__stu_lab_id",
                table: "student_laboratory_image",
                column: "studentLaboratoryId",
                principalTable: "student_laboratory",
                principalColumn: "id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
