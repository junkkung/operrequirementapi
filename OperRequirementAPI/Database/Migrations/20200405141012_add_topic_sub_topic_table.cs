﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Database.Migrations
{
    public partial class add_topic_sub_topic_table : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "topic_sub_topic",
                columns: table => new
                {
                    topicId = table.Column<int>(type: "int(10)", nullable: false),
                    subTopicId = table.Column<int>(type: "int(10)", nullable: false),
                    sequence = table.Column<int>(type: "int(4)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_topic_sub_topic", x => new { x.topicId, x.subTopicId });
                    table.ForeignKey(
                        name: "FK__topic_sub_topic_subTopicId__sub_topic_id",
                        column: x => x.subTopicId,
                        principalTable: "sub_topic",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK__topic_sub_topic_topicId__topic_id",
                        column: x => x.topicId,
                        principalTable: "topic",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_topic_sub_topic_subTopicId",
                table: "topic_sub_topic",
                column: "subTopicId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "topic_sub_topic");
        }
    }
}
