﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Database.Migrations
{
    public partial class change_table_criticalCriteria_to_criticalSubTopic : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "critical_criteria");

            migrationBuilder.CreateTable(
                name: "critical_sub_topic",
                columns: table => new
                {
                    criticalId = table.Column<int>(type: "int(10)", nullable: false),
                    subTopicId = table.Column<int>(type: "int(10)", nullable: false),
                    sequence = table.Column<int>(type: "int(4)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_critical_sub_topic", x => new { x.criticalId, x.subTopicId });
                    table.ForeignKey(
                        name: "FK__critical_sub_topic_criticalId__critical_id",
                        column: x => x.criticalId,
                        principalTable: "critical",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK__critical_sub_topic_subTopicId__sub_topic_id",
                        column: x => x.subTopicId,
                        principalTable: "sub_topic",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_critical_sub_topic_subTopicId",
                table: "critical_sub_topic",
                column: "subTopicId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "critical_sub_topic");

            migrationBuilder.CreateTable(
                name: "critical_criteria",
                columns: table => new
                {
                    criticalId = table.Column<int>(type: "int(10)", nullable: false),
                    criteriaId = table.Column<int>(type: "int(10)", nullable: false),
                    sequence = table.Column<int>(type: "int(4)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_critical_criteria", x => new { x.criticalId, x.criteriaId });
                    table.ForeignKey(
                        name: "FK__critical_criteria_criteriaId__criteria_id",
                        column: x => x.criteriaId,
                        principalTable: "criteria",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK__critical_criteria_criticalId__critical_id",
                        column: x => x.criticalId,
                        principalTable: "critical",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_critical_criteria_criteriaId",
                table: "critical_criteria",
                column: "criteriaId");
        }
    }
}
