﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Database.Migrations
{
    public partial class Add_Table_student_lab_clv_confirm : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "student_laboratory_clv_teeth_confirm",
                columns: table => new
                {
                    id = table.Column<int>(type: "int(10)", nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    studentId = table.Column<int>(type: "int(10)", nullable: false),
                    teethNo = table.Column<int>(type: "int(10)", nullable: false),
                    canUse = table.Column<short>(type: "bit(1)", nullable: false, defaultValue: (short)0),
                    canNotUse = table.Column<short>(type: "bit(1)", nullable: false, defaultValue: (short)0),
                    useInLab = table.Column<short>(type: "bit(1)", nullable: false, defaultValue: (short)0),
                    useInControl = table.Column<short>(type: "bit(1)", nullable: false, defaultValue: (short)0),
                    comment = table.Column<string>(maxLength: 255, nullable: true),
                    statusId = table.Column<int>(type: "int(10)", nullable: false),
                    createDate = table.Column<DateTimeOffset>(nullable: false, defaultValueSql: "CURRENT_TIMESTAMP"),
                    updateDate = table.Column<DateTimeOffset>(nullable: false, defaultValueSql: "CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_student_laboratory_clv_teeth_confirm", x => x.id);
                    table.ForeignKey(
                        name: "FK__student_lab_clv_con__statusId__status_id",
                        column: x => x.statusId,
                        principalTable: "status",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK__student_lab_clv_con__studentId__student_id",
                        column: x => x.studentId,
                        principalTable: "student",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "ID__sub_topic__id",
                table: "student_laboratory_clv_teeth_confirm",
                column: "id");

            migrationBuilder.CreateIndex(
                name: "IX_student_laboratory_clv_teeth_confirm_statusId",
                table: "student_laboratory_clv_teeth_confirm",
                column: "statusId");

            migrationBuilder.CreateIndex(
                name: "IX_student_laboratory_clv_teeth_confirm_studentId",
                table: "student_laboratory_clv_teeth_confirm",
                column: "studentId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "student_laboratory_clv_teeth_confirm");
        }
    }
}
