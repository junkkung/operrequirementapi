﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Database.Migrations
{
    public partial class add_sub_topic_criteria_table : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "sub_topic_criteria",
                columns: table => new
                {
                    subTopicId = table.Column<int>(type: "int(10)", nullable: false),
                    criteriaId = table.Column<int>(type: "int(10)", nullable: false),
                    sequence = table.Column<int>(type: "int(4)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_sub_topic_criteria", x => new { x.subTopicId, x.criteriaId });
                    table.ForeignKey(
                        name: "FK__sub_topic_criteria_criteriaId__criteria_id",
                        column: x => x.criteriaId,
                        principalTable: "criteria",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK__sub_topic_criteria_subTopicId__sub_topic_id",
                        column: x => x.subTopicId,
                        principalTable: "sub_topic",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_sub_topic_criteria_criteriaId",
                table: "sub_topic_criteria",
                column: "criteriaId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "sub_topic_criteria");
        }
    }
}
