﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Database.Migrations
{
    public partial class create_foreignkey_student_confirm_code_course : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "IX_student_confirm_code_courseId",
                table: "student_confirm_code",
                column: "courseId");

            migrationBuilder.AddForeignKey(
                name: "FK__student_confirm_code_courseId__course_id",
                table: "student_confirm_code",
                column: "courseId",
                principalTable: "course",
                principalColumn: "id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK__student_confirm_code_courseId__course_id",
                table: "student_confirm_code");

            migrationBuilder.DropIndex(
                name: "IX_student_confirm_code_courseId",
                table: "student_confirm_code");
        }
    }
}
