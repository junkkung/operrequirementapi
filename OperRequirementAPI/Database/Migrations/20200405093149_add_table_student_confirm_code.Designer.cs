﻿// <auto-generated />
using Database;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace Database.Migrations
{
    [DbContext(typeof(OperRequirementContext))]
    [Migration("20200405093149_add_table_student_confirm_code")]
    partial class add_table_student_confirm_code
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "2.2.6-servicing-10079");

            modelBuilder.Entity("Database.Entities.InstructorConfirmCode", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("id")
                        .HasColumnType("int(10)");

                    b.Property<string>("Code")
                        .IsRequired()
                        .HasColumnName("code")
                        .HasMaxLength(255);

                    b.HasKey("Id");

                    b.ToTable("instructor_confirm_code");
                });

            modelBuilder.Entity("Database.Entities.StudentConfrimCode", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("id")
                        .HasColumnType("int(10)");

                    b.Property<string>("Code")
                        .IsRequired()
                        .HasColumnName("code")
                        .HasMaxLength(255);

                    b.Property<int>("CourseId")
                        .HasColumnName("courseId")
                        .HasColumnType("int(10)");

                    b.HasKey("Id");

                    b.ToTable("student_confirm_code");
                });
#pragma warning restore 612, 618
        }
    }
}
