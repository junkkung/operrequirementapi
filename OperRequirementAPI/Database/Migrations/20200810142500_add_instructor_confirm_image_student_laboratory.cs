﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Database.Migrations
{
    public partial class add_instructor_confirm_image_student_laboratory : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "InstuctorConfirmId",
                table: "student_laboratory",
                type: "int(10)",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_student_laboratory_InstuctorConfirmId",
                table: "student_laboratory",
                column: "InstuctorConfirmId");

            migrationBuilder.AddForeignKey(
                name: "FK__student_laboratory_instConfirmId__instructor_id",
                table: "student_laboratory",
                column: "InstuctorConfirmId",
                principalTable: "instructor",
                principalColumn: "id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK__student_laboratory_instConfirmId__instructor_id",
                table: "student_laboratory");

            migrationBuilder.DropIndex(
                name: "IX_student_laboratory_InstuctorConfirmId",
                table: "student_laboratory");

            migrationBuilder.DropColumn(
                name: "InstuctorConfirmId",
                table: "student_laboratory");
        }
    }
}
