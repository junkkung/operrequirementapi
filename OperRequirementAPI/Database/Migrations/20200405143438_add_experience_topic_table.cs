﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Database.Migrations
{
    public partial class add_experience_topic_table : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "experience_topic",
                columns: table => new
                {
                    experienceId = table.Column<int>(type: "int(10)", nullable: false),
                    topicId = table.Column<int>(type: "int(10)", nullable: false),
                    sequence = table.Column<int>(type: "int(4)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_experience_topic", x => new { x.experienceId, x.topicId });
                    table.ForeignKey(
                        name: "FK__experience_topic_experienceId__experience_id",
                        column: x => x.experienceId,
                        principalTable: "experience",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK__experience_topic_topicId__topic_id",
                        column: x => x.topicId,
                        principalTable: "topic",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_experience_topic_topicId",
                table: "experience_topic",
                column: "topicId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "experience_topic");
        }
    }
}
