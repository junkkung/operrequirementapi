﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Database.Migrations
{
    public partial class add_subTopicGroup_table : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "subTopicGroupId",
                table: "sub_topic",
                type: "int(10)",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "sub_topic_group",
                columns: table => new
                {
                    id = table.Column<int>(type: "int(10)", nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    name = table.Column<string>(maxLength: 255, nullable: false),
                    description = table.Column<string>(maxLength: 255, nullable: true),
                    createDate = table.Column<DateTimeOffset>(nullable: false, defaultValueSql: "CURRENT_TIMESTAMP"),
                    updateDate = table.Column<DateTimeOffset>(nullable: false, defaultValueSql: "CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"),
                    statusId = table.Column<int>(type: "int(10)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_sub_topic_group", x => x.id);
                    table.ForeignKey(
                        name: "FK__sub_topic_group_statusId__status_id",
                        column: x => x.statusId,
                        principalTable: "status",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_sub_topic_subTopicGroupId",
                table: "sub_topic",
                column: "subTopicGroupId");

            migrationBuilder.CreateIndex(
                name: "IX__sub_topic_group__id",
                table: "sub_topic_group",
                column: "id");

            migrationBuilder.CreateIndex(
                name: "IX_sub_topic_group_statusId",
                table: "sub_topic_group",
                column: "statusId");

            migrationBuilder.AddForeignKey(
                name: "FK_sub_topic_sub_topic_group_subTopicGroupId",
                table: "sub_topic",
                column: "subTopicGroupId",
                principalTable: "sub_topic_group",
                principalColumn: "id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_sub_topic_sub_topic_group_subTopicGroupId",
                table: "sub_topic");

            migrationBuilder.DropTable(
                name: "sub_topic_group");

            migrationBuilder.DropIndex(
                name: "IX_sub_topic_subTopicGroupId",
                table: "sub_topic");

            migrationBuilder.DropColumn(
                name: "subTopicGroupId",
                table: "sub_topic");
        }
    }
}
