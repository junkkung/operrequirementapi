﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Database.Migrations
{
    public partial class add_student_score_table : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "student_score",
                columns: table => new
                {
                    id = table.Column<int>(type: "int(10)", nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    studentSubTopicId = table.Column<int>(type: "int(10)", nullable: false),
                    instructorId = table.Column<int>(type: "int(10)", nullable: false),
                    criteriaId = table.Column<int>(type: "int(10)", nullable: true),
                    isCritical = table.Column<short>(type: "bit(1)", nullable: true),
                    score = table.Column<int>(type: "int(10)", nullable: true),
                    remark = table.Column<string>(maxLength: 255, nullable: true),
                    createDate = table.Column<DateTimeOffset>(nullable: false, defaultValueSql: "CURRENT_TIMESTAMP"),
                    updateDate = table.Column<DateTimeOffset>(nullable: false, defaultValueSql: "CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"),
                    statusId = table.Column<int>(type: "int(10)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_student_score", x => x.id);
                    table.ForeignKey(
                        name: "FK__student_score_criteriaId__criteria_id",
                        column: x => x.criteriaId,
                        principalTable: "criteria",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK__student_score_instructorId__instructor_id",
                        column: x => x.instructorId,
                        principalTable: "instructor",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK__student_score_statusId__status_id",
                        column: x => x.statusId,
                        principalTable: "status",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK__student_score_stuSubTopicId__stu_sub_topic_id",
                        column: x => x.studentSubTopicId,
                        principalTable: "student_sub_topic",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_student_score_criteriaId",
                table: "student_score",
                column: "criteriaId");

            migrationBuilder.CreateIndex(
                name: "ID__student_score__id",
                table: "student_score",
                column: "id");

            migrationBuilder.CreateIndex(
                name: "IX_student_score_instructorId",
                table: "student_score",
                column: "instructorId");

            migrationBuilder.CreateIndex(
                name: "IX_student_score_statusId",
                table: "student_score",
                column: "statusId");

            migrationBuilder.CreateIndex(
                name: "IX_student_score_studentSubTopicId",
                table: "student_score",
                column: "studentSubTopicId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "student_score");
        }
    }
}
