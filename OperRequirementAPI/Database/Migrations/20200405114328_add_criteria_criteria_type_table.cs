﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Database.Migrations
{
    public partial class add_criteria_criteria_type_table : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "criteria_type",
                columns: table => new
                {
                    id = table.Column<int>(type: "int(10)", nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    name = table.Column<string>(maxLength: 255, nullable: false),
                    description = table.Column<string>(maxLength: 255, nullable: true),
                    createDate = table.Column<DateTimeOffset>(nullable: false, defaultValueSql: "CURRENT_TIMESTAMP"),
                    updateDate = table.Column<DateTimeOffset>(nullable: false, defaultValueSql: "CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"),
                    statusId = table.Column<int>(type: "int(10)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_criteria_type", x => x.id);
                    table.ForeignKey(
                        name: "FK__criteria_type_statusId__status_id",
                        column: x => x.statusId,
                        principalTable: "status",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "criteria",
                columns: table => new
                {
                    id = table.Column<int>(type: "int(10)", nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    name = table.Column<string>(type: "text", nullable: false),
                    criteriaTypeId = table.Column<int>(type: "int(10)", nullable: false),
                    score = table.Column<int>(type: "int(10)", nullable: false),
                    createDate = table.Column<DateTimeOffset>(nullable: false, defaultValueSql: "CURRENT_TIMESTAMP"),
                    updateDate = table.Column<DateTimeOffset>(nullable: false, defaultValueSql: "CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"),
                    statusId = table.Column<int>(type: "int(10)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_criteria", x => x.id);
                    table.ForeignKey(
                        name: "FK__criteria_criteriaTypeId__criteria_type_id",
                        column: x => x.criteriaTypeId,
                        principalTable: "criteria_type",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK__criteria_statusId__status_id",
                        column: x => x.statusId,
                        principalTable: "status",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_criteria_criteriaTypeId",
                table: "criteria",
                column: "criteriaTypeId");

            migrationBuilder.CreateIndex(
                name: "ID__criteria__id",
                table: "criteria",
                column: "id");

            migrationBuilder.CreateIndex(
                name: "IX_criteria_statusId",
                table: "criteria",
                column: "statusId");

            migrationBuilder.CreateIndex(
                name: "ID__criteria_type__id",
                table: "criteria_type",
                column: "id");

            migrationBuilder.CreateIndex(
                name: "IX_criteria_type_statusId",
                table: "criteria_type",
                column: "statusId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "criteria");

            migrationBuilder.DropTable(
                name: "criteria_type");
        }
    }
}
