﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Database.Migrations
{
    public partial class change_refreshToken_column_type_in_instructor_table_to_be_text : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            //migrationBuilder.AlterColumn<string>(
            //    name: "refreshToken",
            //    table: "instructor",
            //    type: "text",
            //    nullable: true,
            //    oldClrType: typeof(string),
            //    oldMaxLength: 255,
            //    oldNullable: true);

            migrationBuilder.Sql($"ALTER TABLE `instructor` CHANGE `refreshToken` `refreshToken` text;");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            //migrationBuilder.AlterColumn<string>(
            //    name: "refreshToken",
            //    table: "instructor",
            //    maxLength: 255,
            //    nullable: true,
            //    oldClrType: typeof(string),
            //    oldType: "text",
            //    oldNullable: true);

            migrationBuilder.Sql($"ALTER TABLE `instructor` CHANGE `refreshToken` `refreshToken` VARCHAR(255);");
        }
    }
}
