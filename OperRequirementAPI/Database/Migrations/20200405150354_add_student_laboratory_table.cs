﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Database.Migrations
{
    public partial class add_student_laboratory_table : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "student_laboratory",
                columns: table => new
                {
                    id = table.Column<int>(type: "int(10)", nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    studentId = table.Column<int>(type: "int(10)", nullable: false),
                    laboratoryId = table.Column<int>(type: "int(10)", nullable: false),
                    tooth = table.Column<string>(maxLength: 255, nullable: true),
                    kindOfWork = table.Column<string>(maxLength: 255, nullable: true),
                    material = table.Column<string>(maxLength: 255, nullable: true),
                    createDate = table.Column<DateTimeOffset>(nullable: false, defaultValueSql: "CURRENT_TIMESTAMP"),
                    updateDate = table.Column<DateTimeOffset>(nullable: false, defaultValueSql: "CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"),
                    statusId = table.Column<int>(type: "int(10)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_student_laboratory", x => x.id);
                    table.ForeignKey(
                        name: "FK__student_laboratory_labId__laboratory_id",
                        column: x => x.laboratoryId,
                        principalTable: "laboratory",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK__student_laboratory_statusId__status_id",
                        column: x => x.statusId,
                        principalTable: "status",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK__student_laboratory_studentId__student_id",
                        column: x => x.studentId,
                        principalTable: "student",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "ID__sub_topic__id",
                table: "student_laboratory",
                column: "id");

            migrationBuilder.CreateIndex(
                name: "IX_student_laboratory_laboratoryId",
                table: "student_laboratory",
                column: "laboratoryId");

            migrationBuilder.CreateIndex(
                name: "IX_student_laboratory_statusId",
                table: "student_laboratory",
                column: "statusId");

            migrationBuilder.CreateIndex(
                name: "IX_student_laboratory_studentId",
                table: "student_laboratory",
                column: "studentId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "student_laboratory");
        }
    }
}
