﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Database.Migrations
{
    public partial class add_laboratory_topic_table : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "laboratory_topic",
                columns: table => new
                {
                    laboratoryId = table.Column<int>(type: "int(10)", nullable: false),
                    topicId = table.Column<int>(type: "int(10)", nullable: false),
                    sequence = table.Column<int>(type: "int(4)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_laboratory_topic", x => new { x.laboratoryId, x.topicId });
                    table.ForeignKey(
                        name: "FK__laboratory_topic_labId__laboratory_id",
                        column: x => x.laboratoryId,
                        principalTable: "laboratory",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK__laboratory_topic_topicId__topic_id",
                        column: x => x.topicId,
                        principalTable: "topic",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_laboratory_topic_topicId",
                table: "laboratory_topic",
                column: "topicId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "laboratory_topic");
        }
    }
}
