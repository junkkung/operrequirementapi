﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Database.Migrations
{
    public partial class change_refreshToken_column_type_in_student_table_to_be_text : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            //migrationBuilder.AlterColumn<string>(
            //    name: "refreshToken",
            //    table: "student",
            //    type: "text",
            //    nullable: true,
            //    oldClrType: typeof(string),
            //    oldMaxLength: 255,
            //    oldNullable: true);

            migrationBuilder.Sql($"ALTER TABLE `student` CHANGE `refreshToken` `refreshToken` text;");

        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            //migrationBuilder.AlterColumn<string>(
            //    name: "refreshToken",
            //    table: "student",
            //    maxLength: 255,
            //    nullable: true,
            //    oldClrType: typeof(string),
            //    oldType: "text",
            //    oldNullable: true);

            migrationBuilder.Sql($"ALTER TABLE `student` CHANGE `refreshToken` `refreshToken` VARCHAR(255);");

        }
    }
}
