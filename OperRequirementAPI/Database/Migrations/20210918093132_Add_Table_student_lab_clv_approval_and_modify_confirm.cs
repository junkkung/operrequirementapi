﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Database.Migrations
{
    public partial class Add_Table_student_lab_clv_approval_and_modify_confirm : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "InstructorId",
                table: "student_laboratory_clv_teeth_confirm",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "student_laboratory_clv_teeth_approval",
                columns: table => new
                {
                    id = table.Column<int>(type: "int(10)", nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    studentId = table.Column<int>(type: "int(10)", nullable: false),
                    isApproved = table.Column<short>(type: "bit(1)", nullable: true),
                    comment = table.Column<string>(maxLength: 255, nullable: true),
                    statusId = table.Column<int>(type: "int(10)", nullable: false),
                    instructorId = table.Column<int>(type: "int(10)", nullable: true),
                    createDate = table.Column<DateTimeOffset>(nullable: false, defaultValueSql: "CURRENT_TIMESTAMP"),
                    updateDate = table.Column<DateTimeOffset>(nullable: false, defaultValueSql: "CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_student_laboratory_clv_teeth_approval", x => x.id);
                    table.ForeignKey(
                        name: "FK__student_lab_clv_app__instId__instructor_id",
                        column: x => x.instructorId,
                        principalTable: "instructor",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK__student_lab_clv_app__statusId__status_id",
                        column: x => x.statusId,
                        principalTable: "status",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK__student_lab_clv_app__studentId__student_id",
                        column: x => x.studentId,
                        principalTable: "student",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_student_laboratory_clv_teeth_confirm_InstructorId",
                table: "student_laboratory_clv_teeth_confirm",
                column: "InstructorId");

            migrationBuilder.CreateIndex(
                name: "ID__sub_topic__id",
                table: "student_laboratory_clv_teeth_approval",
                column: "id");

            migrationBuilder.CreateIndex(
                name: "IX_student_laboratory_clv_teeth_approval_instructorId",
                table: "student_laboratory_clv_teeth_approval",
                column: "instructorId");

            migrationBuilder.CreateIndex(
                name: "IX_student_laboratory_clv_teeth_approval_statusId",
                table: "student_laboratory_clv_teeth_approval",
                column: "statusId");

            migrationBuilder.CreateIndex(
                name: "IX_student_laboratory_clv_teeth_approval_studentId",
                table: "student_laboratory_clv_teeth_approval",
                column: "studentId");

            migrationBuilder.AddForeignKey(
                name: "FK__student_lab_clv_con__instId__instructor_id",
                table: "student_laboratory_clv_teeth_confirm",
                column: "InstructorId",
                principalTable: "instructor",
                principalColumn: "id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK__student_lab_clv_con__instId__instructor_id",
                table: "student_laboratory_clv_teeth_confirm");

            migrationBuilder.DropTable(
                name: "student_laboratory_clv_teeth_approval");

            migrationBuilder.DropIndex(
                name: "IX_student_laboratory_clv_teeth_confirm_InstructorId",
                table: "student_laboratory_clv_teeth_confirm");

            migrationBuilder.DropColumn(
                name: "InstructorId",
                table: "student_laboratory_clv_teeth_confirm");
        }
    }
}
