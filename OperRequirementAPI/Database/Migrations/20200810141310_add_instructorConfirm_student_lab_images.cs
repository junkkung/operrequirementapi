﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Database.Migrations
{
    public partial class add_instructorConfirm_student_lab_images : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "InstuctorConfirmId",
                table: "student_laboratory_image",
                type: "int(10)",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_student_laboratory_image_InstuctorConfirmId",
                table: "student_laboratory_image",
                column: "InstuctorConfirmId");

            migrationBuilder.AddForeignKey(
                name: "FK__student_lab_Image_instConfirmId__instructor_id",
                table: "student_laboratory_image",
                column: "InstuctorConfirmId",
                principalTable: "instructor",
                principalColumn: "id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK__student_lab_Image_instConfirmId__instructor_id",
                table: "student_laboratory_image");

            migrationBuilder.DropIndex(
                name: "IX_student_laboratory_image_InstuctorConfirmId",
                table: "student_laboratory_image");

            migrationBuilder.DropColumn(
                name: "InstuctorConfirmId",
                table: "student_laboratory_image");
        }
    }
}
