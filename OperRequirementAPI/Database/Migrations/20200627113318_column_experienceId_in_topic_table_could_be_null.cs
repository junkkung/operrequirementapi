﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Database.Migrations
{
    public partial class column_experienceId_in_topic_table_could_be_null : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql($"ALTER TABLE `topic` DROP FOREIGN KEY `FK__topic_experienceId__experience_id`; ");

            migrationBuilder.Sql($"ALTER TABLE `topic` CHANGE `ExperienceId` `experienceId` INT(10)");

            migrationBuilder.Sql($"ALTER TABLE `topic` DROP INDEX `IX_topic_ExperienceId`; ");

            migrationBuilder.CreateIndex(
                name: "IX_topic_experienceId",
                table: "topic",
                column: "experienceId");

            migrationBuilder.AddForeignKey(
                name: "FK__topic_experienceId__experience_id",
                table: "topic",
                column: "experienceId",
                principalTable: "experience",
                principalColumn: "id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql($"ALTER TABLE `topic` DROP FOREIGN KEY `FK__topic_experienceId__experience_id`; ");

            migrationBuilder.Sql($"ALTER TABLE `topic` CHANGE `experienceId` `ExperienceId` INT(10)");

            migrationBuilder.Sql($"ALTER TABLE `topic` DROP INDEX `IX_topic_experienceId`; ");

            migrationBuilder.CreateIndex(
                name: "IX_topic_ExperienceId",
                table: "topic",
                column: "ExperienceId"); ;

            migrationBuilder.AddForeignKey(
                name: "FK__topic_experienceId__experience_id",
                table: "topic",
                column: "ExperienceId",
                principalTable: "experience",
                principalColumn: "id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
