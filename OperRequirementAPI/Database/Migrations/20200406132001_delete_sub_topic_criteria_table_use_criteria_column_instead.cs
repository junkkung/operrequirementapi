﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Database.Migrations
{
    public partial class delete_sub_topic_criteria_table_use_criteria_column_instead : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "sub_topic_criteria");

            migrationBuilder.AddColumn<int>(
                name: "supTopicId",
                table: "criteria",
                type: "int(10)",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_criteria_supTopicId",
                table: "criteria",
                column: "supTopicId");

            migrationBuilder.AddForeignKey(
                name: "FK__criteria_subTopicId__sub_topic_id",
                table: "criteria",
                column: "supTopicId",
                principalTable: "sub_topic",
                principalColumn: "id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK__criteria_subTopicId__sub_topic_id",
                table: "criteria");

            migrationBuilder.DropIndex(
                name: "IX_criteria_supTopicId",
                table: "criteria");

            migrationBuilder.DropColumn(
                name: "supTopicId",
                table: "criteria");

            migrationBuilder.CreateTable(
                name: "sub_topic_criteria",
                columns: table => new
                {
                    subTopicId = table.Column<int>(type: "int(10)", nullable: false),
                    criteriaId = table.Column<int>(type: "int(10)", nullable: false),
                    sequence = table.Column<int>(type: "int(4)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_sub_topic_criteria", x => new { x.subTopicId, x.criteriaId });
                    table.ForeignKey(
                        name: "FK__sub_topic_criteria_criteriaId__criteria_id",
                        column: x => x.criteriaId,
                        principalTable: "criteria",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK__sub_topic_criteria_subTopicId__sub_topic_id",
                        column: x => x.subTopicId,
                        principalTable: "sub_topic",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_sub_topic_criteria_criteriaId",
                table: "sub_topic_criteria",
                column: "criteriaId");
        }
    }
}
