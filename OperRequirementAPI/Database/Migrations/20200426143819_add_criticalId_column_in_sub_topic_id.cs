﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Database.Migrations
{
    public partial class add_criticalId_column_in_sub_topic_id : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "criticalId",
                table: "sub_topic",
                type: "int(10)",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_sub_topic_criticalId",
                table: "sub_topic",
                column: "criticalId");

            migrationBuilder.AddForeignKey(
                name: "FK__sub_topic_criticalId__critical_id",
                table: "sub_topic",
                column: "criticalId",
                principalTable: "critical",
                principalColumn: "id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK__sub_topic_criticalId__critical_id",
                table: "sub_topic");

            migrationBuilder.DropIndex(
                name: "IX_sub_topic_criticalId",
                table: "sub_topic");

            migrationBuilder.DropColumn(
                name: "criticalId",
                table: "sub_topic");
        }
    }
}
