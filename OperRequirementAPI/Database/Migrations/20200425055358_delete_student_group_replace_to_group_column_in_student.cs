﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Database.Migrations
{
    public partial class delete_student_group_replace_to_group_column_in_student : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql($"ALTER TABLE `student` DROP FOREIGN KEY `FK__student_studentGroupId__student_group_id`; ");

            migrationBuilder.DropTable(
                name: "student_group");

            migrationBuilder.Sql($"ALTER TABLE `student` DROP INDEX `IX_student_studentGroupId`; ");

            migrationBuilder.DropColumn(
                name: "studentGroupId",
                table: "student");

            migrationBuilder.AddColumn<int>(
                name: "group",
                table: "student",
                type: "int(2)",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "group",
                table: "student");

            migrationBuilder.AddColumn<int>(
                name: "studentGroupId",
                table: "student",
                type: "int(10)",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateTable(
                name: "student_group",
                columns: table => new
                {
                    id = table.Column<int>(type: "int(10)", nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    name = table.Column<string>(maxLength: 255, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_student_group", x => x.id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_student_studentGroupId",
                table: "student",
                column: "studentGroupId");

            migrationBuilder.CreateIndex(
                name: "ID__student_group__id",
                table: "student_group",
                column: "id");

            migrationBuilder.AddForeignKey(
                name: "FK__student_studentGroupId__student_group_id",
                table: "student",
                column: "studentGroupId",
                principalTable: "student_group",
                principalColumn: "id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
