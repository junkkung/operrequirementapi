﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Database.Migrations
{
    public partial class add_status_table_and_add_index_to_table : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "status",
                columns: table => new
                {
                    id = table.Column<int>(type: "int(10)", nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    name = table.Column<string>(maxLength: 255, nullable: true),
                    description = table.Column<string>(maxLength: 255, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_status", x => x.id);
                });

            migrationBuilder.CreateIndex(
                name: "ID__student_confirm_code__id",
                table: "student_confirm_code",
                column: "id");

            migrationBuilder.CreateIndex(
                name: "ID__instructor_confirm_code__id",
                table: "instructor_confirm_code",
                column: "id");

            migrationBuilder.CreateIndex(
                name: "ID__status__id",
                table: "status",
                column: "id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "status");

            migrationBuilder.DropIndex(
                name: "ID__student_confirm_code__id",
                table: "student_confirm_code");

            migrationBuilder.DropIndex(
                name: "ID__instructor_confirm_code__id",
                table: "instructor_confirm_code");
        }
    }
}
