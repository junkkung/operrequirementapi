﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Database.Entities
{
    public partial class SubTopic
    {
        public SubTopic()
        {
            Criterias = new HashSet<Criteria>();
            StudentSubTopics = new HashSet<StudentSubTopic>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTimeOffset CreateDate { get; set; }
        public DateTimeOffset UpdateDate { get; set; }
        public int StatusId { get; set; }
        public int TopicId { get; set; }
        public int Sequence { get; set; }
        public int? CriticalId { get; set; }
        public bool IsLabel { get; set; }

        public virtual Status Status { get; set; }
        public virtual Topic Topic { get; set; }
        public virtual Critical Critical { get; set; }
        public virtual ICollection<Criteria> Criterias { get; set; }
        public virtual ICollection<StudentSubTopic> StudentSubTopics { get; set; }
    }
}
