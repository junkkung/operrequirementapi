﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Database.Entities
{
    public partial class StudentLaboratoryTeethApproval
    {
        public int Id { get; set; }
        public int StudentId { get; set; }
        public bool? IsApproved { get; set; }
        public string Comment { get; set; }
        public int StatusId { get; set; }
        public int? InstructorId { get; set; }
        public int LaboratoryId { get; set; }
        public DateTimeOffset CreateDate { get; set; }
        public DateTimeOffset UpdateDate { get; set; }

        public Status Status { get; set; }
        public Student Student { get; set; }
        public Instructor Instructor { get; set; }
        public Laboratory Laboratory { get; set; }
    }
}
