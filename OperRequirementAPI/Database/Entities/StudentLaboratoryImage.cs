﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Database.Entities
{
    public partial class StudentLaboratoryImage
    {
        public int Id { get; set; }
        public int StudentId { get; set; }
        public int LaboratoryImageId { get; set; }
        public string Image { get; set; }
        public string Remark { get; set; }
        public DateTimeOffset CreateDate { get; set; }
        public DateTimeOffset UpdateDate { get; set; }
        public int StatusId { get; set; }
        public int? InstructorConfirmId { get; set; }

        public Status Status { get; set; }
        public Student Student { get; set; }
        public LaboratoryImage LaboratoryImage { get; set; }
        public Instructor Instructor { get; set; }
    }
}
