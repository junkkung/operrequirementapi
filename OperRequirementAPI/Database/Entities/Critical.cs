﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Database.Entities
{
    public partial class Critical
    {
        public Critical()
        {
            SubTopics = new HashSet<SubTopic>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public DateTimeOffset CreateDate { get; set; }
        public DateTimeOffset UpdateDate { get; set; }
        public int StatusId { get; set; }

        public virtual Status Status { get; set; }
        public virtual ICollection<SubTopic> SubTopics { get; set; }
    }
}
