﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Database.Entities
{
    public partial class Laboratory
    {
        public Laboratory()
        {
            Topics = new HashSet<Topic>();
            StudentLaboratories = new HashSet<StudentLaboratory>();
            LaboratoryImages = new HashSet<LaboratoryImage>();
            StudentLaboratoryTeethConfirms = new HashSet<StudentLaboratoryTeethConfirm>();
            StudentLaboratoryTeethApprovals = new HashSet<StudentLaboratoryTeethApproval>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTimeOffset CreateDate { get; set; }
        public DateTimeOffset UpdateDate { get; set; }
        public int StatusId { get; set; }
        public int Sequence { get; set; }
        public DateTime EndDate { get; set; }
        public int CourseId { get; set; }
        public bool HasToothConfirm { get; set; }

        public virtual Course Course { get; set; }
        public virtual Status Status { get; set; }
        public virtual ICollection<LaboratoryImage> LaboratoryImages { get; set; }
        public virtual ICollection<Topic> Topics { get; set; }
        public virtual ICollection<StudentLaboratory> StudentLaboratories { get; set; }
        public virtual ICollection<StudentLaboratoryTeethConfirm> StudentLaboratoryTeethConfirms { get; set; }
        public virtual ICollection<StudentLaboratoryTeethApproval> StudentLaboratoryTeethApprovals { get; set; }

    }
}
