﻿using System;
using System;
using System.Collections.Generic;
using System.Text;

namespace Database.Entities
{
    public partial class StudentLaboratory
    {
        public StudentLaboratory()
        {
            StudentMinusPoints = new HashSet<StudentMinusPoint>();
            StudentSubTopics = new HashSet<StudentSubTopic>();
        }

        public int Id { get; set; }
        public int StudentId { get; set; }
        public int LaboratoryId { get; set; }
        public string Tooth { get; set; }
        public string KindOfWork { get; set; }
        public string Material { get; set; }
        public DateTimeOffset CreateDate { get; set; }
        public DateTimeOffset UpdateDate { get; set; }
        public int StatusId { get; set; }
        public string Image { get; set; }
        public int? InstructorConfirmId { get; set; }

        public virtual Status Status { get; set; }
        public virtual Student Student { get; set; }
        public virtual Laboratory Laboratory { get; set; }
        public virtual Instructor Instructor { get; set; }

        public virtual ICollection<StudentMinusPoint> StudentMinusPoints { get; set; }
        public virtual ICollection<StudentSubTopic> StudentSubTopics { get; set; }

    }
}
