﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Database.Entities
{
    public partial class Config
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Value { get; set; }
        public string Unit { get; set; }
    }
}
