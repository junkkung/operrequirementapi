﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Database.Entities
{
    public partial class InstructorConfirmCode
    {
        public int Id { get; set; }
        public string Code { get; set; }
    }
}
