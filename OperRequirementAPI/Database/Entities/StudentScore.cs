﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Database.Entities
{
    public partial class StudentScore
    {
        public int Id { get; set; }
        public int StudentSubTopicId { get; set; }
        public int InstructorId { get; set; }
        public int? CriteriaId { get; set; }
        public bool IsCritical { get; set; }
        public int Score { get; set; }
        public string Remark { get; set; }
        public DateTimeOffset CreateDate { get; set; }
        public DateTimeOffset UpdateDate { get; set; }
        public int StatusId { get; set; }

        public Criteria Criteria { get; set; }
        public Instructor Instructor { get; set; }
        public Status Status { get; set; }
        public StudentSubTopic StudentSubTopic { get; set; }
    }
}
