﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Database.Entities
{
    public partial class Instructor
    {
        public Instructor()
        {
            StudentMinusPoints = new HashSet<StudentMinusPoint>();
            StudentLaboratories = new HashSet<StudentLaboratory>();
            StudentLaboratoryImages = new HashSet<StudentLaboratoryImage>();
            StudentScores = new HashSet<StudentScore>();
            StudentLaboratoryTeethApprovals = new HashSet<StudentLaboratoryTeethApproval>();
            StudentLaboratoryTeethConfirms = new HashSet<StudentLaboratoryTeethConfirm>();
        }

        public int Id { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string Image { get; set; }
        public string Email { get; set; }
        public string Telephone { get; set; }
        public DateTimeOffset CreateDate { get; set; }
        public DateTimeOffset UpdateDate { get; set; }
        public int StatusId { get; set; }   
        public bool? IsAdmin { get; set; }
        public string RefreshToken { get; set; }

        public virtual Status Status { get; set; }
        public virtual ICollection<StudentMinusPoint> StudentMinusPoints { get; set; }
        public virtual ICollection<StudentLaboratory> StudentLaboratories { get; set; }
        public virtual ICollection<StudentLaboratoryImage> StudentLaboratoryImages { get; set; }
        public virtual ICollection<StudentScore> StudentScores { get; set; }
        public virtual ICollection<StudentLaboratoryTeethApproval> StudentLaboratoryTeethApprovals { get; set; }
        public virtual ICollection<StudentLaboratoryTeethConfirm> StudentLaboratoryTeethConfirms { get; set; }
    }
}
