﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Database.Entities
{
    public partial class Student
    {
        public Student()
        {
            StudentLaboratoryImages = new HashSet<StudentLaboratoryImage>();
            StudentLaboratories = new HashSet<StudentLaboratory>();
            StudentLaboratoryTeethConfirms = new HashSet<StudentLaboratoryTeethConfirm>();
            StudentLaboratoryTeethApprovals = new HashSet<StudentLaboratoryTeethApproval>();
        }

        public int Id { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string Image { get; set; }
        public string Email { get; set; }
        public string Telephone { get; set; }
        public int CourseId { get; set; }
        public string UniversityIdNumber { get; set; }
        public int Number { get; set; }
        public int Group { get; set; }    
        public DateTimeOffset CreateDate { get; set; }
        public DateTimeOffset UpdateDate { get; set; }
        public int StatusId { get; set; }
        public string RefreshToken { get; set; }

        public virtual Course Course { get; set; }
        public virtual Status Status { get; set; }
        public virtual ICollection<StudentLaboratoryImage> StudentLaboratoryImages { get; set; }
        public virtual ICollection<StudentLaboratory> StudentLaboratories { get; set; }
        public virtual ICollection<StudentLaboratoryTeethConfirm> StudentLaboratoryTeethConfirms { get; set; }
        public virtual ICollection<StudentLaboratoryTeethApproval> StudentLaboratoryTeethApprovals { get; set; }

    }
}
