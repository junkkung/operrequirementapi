﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Database.Entities
{
    public partial class Criteria
    {
        public Criteria()
        {
            StudentScores = new HashSet<StudentScore>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public int CriteriaTypeId { get; set; }
        public int Score { get; set; }
        public DateTimeOffset CreateDate { get; set; }
        public DateTimeOffset UpdateDate { get; set; }
        public int StatusId { get; set; }
        public int SubTopicId { get; set; }

        public virtual CriteriaType CriteriaType { get; set; }
        public virtual SubTopic SubTopic { get; set; }
        public virtual Status Status { get; set; }

        public virtual ICollection<StudentScore> StudentScores { get; set; }
    }
}
