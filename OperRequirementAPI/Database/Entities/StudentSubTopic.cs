﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Database.Entities
{
    public partial class StudentSubTopic
    {
        public StudentSubTopic()
        {
            StudentScores = new HashSet<StudentScore>();
        }

        public int Id { get; set; }
        public int StudentLaboratoryId { get; set; }
        public int SubTopicId { get; set; }
        public int? SelfAssessment { get; set; }
        public DateTimeOffset CreateDate { get; set; }
        public DateTimeOffset UpdateDate { get; set; }
        public int StatusId { get; set; }

        public Status Status { get; set; }
        public StudentLaboratory StudentLaboratory { get; set; }
        public SubTopic SubTopic { get; set; }

        public virtual ICollection<StudentScore> StudentScores { get; set; }

    }
}
