﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Database.Entities
{
    public partial class MinusPoint
    {
        public MinusPoint()
        {
            StudentMinusPoints = new HashSet<StudentMinusPoint>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTimeOffset CreateDate { get; set; }
        public DateTimeOffset UpdateDate { get; set; }
        public int StatusId { get; set; }

        public virtual Status Status { get; set; }
        public virtual ICollection<StudentMinusPoint> StudentMinusPoints { get; set; }

    }
}
