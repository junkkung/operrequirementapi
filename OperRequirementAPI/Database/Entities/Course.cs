﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Database.Entities
{
    public partial class Course
    {
        public Course()
        {
            Laboratories = new HashSet<Laboratory>();
            Students = new HashSet<Student>();
            StudentConfrimCodes = new HashSet<StudentConfrimCode>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public int SemesterYear { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public DateTimeOffset CreateDate { get; set; }
        public DateTimeOffset UpdateDate { get; set; }
        public int StatusId { get; set; }


        public virtual Status Status { get; set; }

        public virtual ICollection<Laboratory> Laboratories { get; set; }
        public virtual ICollection<Student> Students { get; set; }
        public virtual ICollection<StudentConfrimCode> StudentConfrimCodes { get; set; }
    }
}
