﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Database.Entities
{
    public partial class StudentMinusPoint
    {
        public int Id { get; set; }
        public int MinusPointId { get; set; }
        public int StudentLaboratoryId { get; set; }
        public int InstructorId { get; set; }
        public int Score { get; set; }
        public string Remark { get; set; }
        public DateTimeOffset CreateDate { get; set; }
        public DateTimeOffset UpdateDate { get; set; }
        public int StatusId { get; set; }

        public virtual Instructor Instructor { get; set; }
        public virtual MinusPoint MinusPoint { get; set; }
        public virtual Status Status { get; set; }
        public virtual StudentLaboratory StudentLaboratory { get; set; }
    }
}
