﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Database.Entities
{
    public partial class StudentConfrimCode
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public int CourseId { get; set; }

        public virtual Course Course { get; set; }
    }
}
