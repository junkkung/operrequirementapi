﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Database.Entities
{
    public partial class Experience
    {
        public Experience()
        {
            Topics = new HashSet<Topic>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTimeOffset CreateDate { get; set; }
        public DateTimeOffset UpdateDate { get; set; }
        public int StatusId { get; set; }
        public int Sequence { get; set; }

        public virtual Status Status { get; set; }
        public virtual ICollection<Topic> Topics { get; set; }
    }
}
