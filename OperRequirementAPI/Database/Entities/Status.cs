﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Database.Entities
{
    public partial class Status
    {
        public Status()
        {
            Courses = new HashSet<Course>();
            Criterias = new HashSet<Criteria>();
            CriteriaTypes = new HashSet<CriteriaType>();
            Criticals = new HashSet<Critical>();
            Experiences = new HashSet<Experience>();
            Instructors = new HashSet<Instructor>();
            Laboratories = new HashSet<Laboratory>();
            LaboratoryImages = new HashSet<LaboratoryImage>();
            MinusPoints = new HashSet<MinusPoint>();
            Students = new HashSet<Student>();
            StudentLaboratories = new HashSet<StudentLaboratory>();
            StudentLaboratoryImages = new HashSet<StudentLaboratoryImage>();
            StudentMinusPoints = new HashSet<StudentMinusPoint>();
            StudentScores = new HashSet<StudentScore>();
            StudentSubTopics = new HashSet<StudentSubTopic>();
            SubTopics = new HashSet<SubTopic>();
            Topics = new HashSet<Topic>();
            StudentLaboratoryTeethConfirms = new HashSet<StudentLaboratoryTeethConfirm>();
            StudentLaboratoryTeethApprovals = new HashSet<StudentLaboratoryTeethApproval>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public virtual ICollection<Course> Courses { get; set; }
        public virtual ICollection<Criteria> Criterias { get; set; }
        public virtual ICollection<CriteriaType> CriteriaTypes { get; set; }
        public virtual ICollection<Critical> Criticals { get; set; }
        public virtual ICollection<Experience> Experiences { get; set; }
        public virtual ICollection<Instructor> Instructors { get; set; }
        public virtual ICollection<Laboratory> Laboratories { get; set; }
        public virtual ICollection<LaboratoryImage> LaboratoryImages { get; set; }
        public virtual ICollection<MinusPoint> MinusPoints { get; set; }
        public virtual ICollection<Student> Students { get; set; }
        public virtual ICollection<StudentLaboratory> StudentLaboratories { get; set; }
        public virtual ICollection<StudentLaboratoryImage> StudentLaboratoryImages { get; set; }
        public virtual ICollection<StudentMinusPoint> StudentMinusPoints { get; set; }
        public virtual ICollection<StudentScore> StudentScores { get; set; }
        public virtual ICollection<StudentSubTopic> StudentSubTopics { get; set; }
        public virtual ICollection<SubTopic> SubTopics { get; set; }
        public virtual ICollection<Topic> Topics { get; set; }
        public virtual ICollection<StudentLaboratoryTeethConfirm> StudentLaboratoryTeethConfirms { get; set; }
        public virtual ICollection<StudentLaboratoryTeethApproval> StudentLaboratoryTeethApprovals { get; set; }
    }
}
