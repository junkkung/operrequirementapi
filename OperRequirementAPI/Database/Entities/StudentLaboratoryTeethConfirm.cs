﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Database.Entities
{
    public partial class StudentLaboratoryTeethConfirm
    {
        public int Id { get; set; }
        public int StudentId { get; set; }
        public string TeethNo { get; set; }
        public bool CanUse { get; set; }
        public bool CanNotUse { get; set; }
        public bool UseInLab { get; set; }
        public bool UseInControl { get; set; }
        public string Comment { get; set; }
        public int StatusId { get; set; }
        public int? InstructorId { get; set; }
        public int LaboratoryId { get; set; }
        public DateTimeOffset CreateDate { get; set; }
        public DateTimeOffset UpdateDate { get; set; }

        public Status Status { get; set; }
        public Student Student { get; set; }
        public Instructor Instructor { get; set; }
        public Laboratory Laboratory { get; set; }
    }
}
