﻿using System;
using Database.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Database
{
    public partial class OperRequirementContext : DbContext
    {
        public OperRequirementContext()
        {
        }

        public OperRequirementContext(DbContextOptions<OperRequirementContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Config> Configs { get; set; }
        public virtual DbSet<Course> Courses { get; set; }
        public virtual DbSet<Criteria> Criterias { get; set; }
        public virtual DbSet<CriteriaType> CriteriaTypes { get; set; }
        public virtual DbSet<Critical> Criticals { get; set; }
        public virtual DbSet<Experience> Experiences { get; set; }
        public virtual DbSet<Instructor> Instructors { get; set; }
        public virtual DbSet<InstructorConfirmCode> InstructorConfirmCodes { get; set; }
        public virtual DbSet<Laboratory> Laboratories { get; set; }
        public virtual DbSet<LaboratoryImage> LaboratoryImages { get; set; }
        public virtual DbSet<MinusPoint> MinusPoints { get; set; }
        public virtual DbSet<Status> Statuses { get; set; }
        public virtual DbSet<Student> Students { get; set; }
        public virtual DbSet<StudentConfrimCode> StudentConfrimCodes { get; set; }
        public virtual DbSet<StudentLaboratory> StudentLaboratories { get; set; }
        public virtual DbSet<StudentLaboratoryTeethConfirm> StudentLaboratoryTeethConfirms { get; set; }
        public virtual DbSet<StudentLaboratoryTeethApproval> StudentLaboratoryTeethApprovals { get; set; }
        public virtual DbSet<StudentLaboratoryImage> StudentLaboratoryImages { get; set; }
        public virtual DbSet<StudentMinusPoint> StudentMinusPoints { get; set; }
        public virtual DbSet<StudentScore> StudentScores { get; set; }
        public virtual DbSet<StudentSubTopic> StudentSubTopics { get; set; }
        public virtual DbSet<SubTopic> SubTopics { get; set; }
        public virtual DbSet<Topic> Topics { get; set; }

        //        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        //        {
        //            if (!optionsBuilder.IsConfigured)
        //            {
        //#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
        //                optionsBuilder.UseMySQL("Server=localhost; Database=operrequirement; Uid=root; pwd=root;");
        //            }
        //        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("ProductVersion", "2.2.6-servicing-10079");

            modelBuilder.Entity<Config>(entity =>
            {
                entity.ToTable("config");

                entity.HasIndex(e => e.Id)
                    .HasName("IX__config__id");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("int(10)")
                    .ValueGeneratedNever();

                entity.Property(e => e.Name)
                    .HasColumnName("name")
                    .HasMaxLength(255)
                    .IsRequired();

                entity.Property(e => e.Value)
                    .HasColumnName("value")
                    .HasMaxLength(255)
                    .IsRequired();

                entity.Property(e => e.Unit)
                    .HasColumnName("unit")
                    .HasMaxLength(255);

                entity.Property(e => e.Description)
                    .HasColumnName("description")
                    .HasMaxLength(255);
            });

            modelBuilder.Entity<Course>(entity =>
            {
                entity.ToTable("course");

                entity.HasIndex(e => e.Id)
                    .HasName("ID__course__id");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("int(10)");

                entity.Property(e => e.Name)
                    .HasColumnName("name")
                    .HasMaxLength(255)
                    .IsRequired();

                entity.Property(e => e.SemesterYear)
                    .HasColumnName("semesterYear")
                    .HasColumnType("int(10)");

                entity.Property(e => e.StartDate)
                    .HasColumnName("startDate");

                entity.Property(e => e.EndDate)
                    .HasColumnName("endDate");

                entity.Property(e => e.CreateDate)
                    .HasColumnName("createDate")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("updateDate")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP");

                entity.Property(e => e.StatusId)
                    .HasColumnName("statusId")
                    .HasColumnType("int(10)");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.Courses)
                    .HasForeignKey(d => d.StatusId)
                    .HasConstraintName("FK__course_statusId__status_id");
            });          
            
            modelBuilder.Entity<Criteria>(entity =>
            {
                entity.ToTable("criteria");

                entity.HasIndex(e => e.Id)
                    .HasName("ID__criteria__id");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("int(10)");

                entity.Property(e => e.Name)
                    .HasColumnName("name")
                    .HasColumnType("text")
                    .IsRequired();

                entity.Property(e => e.CriteriaTypeId)
                    .HasColumnName("criteriaTypeId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.Score)
                    .HasColumnName("score")
                    .HasColumnType("int(10)");

                entity.Property(e => e.CreateDate)
                    .HasColumnName("createDate")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("updateDate")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP");

                entity.Property(e => e.StatusId)
                    .HasColumnName("statusId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.SubTopicId)
                    .HasColumnName("subTopicId")
                    .HasColumnType("int(10)");

                entity.HasOne(d => d.CriteriaType)
                    .WithMany(p => p.Criterias)
                    .HasForeignKey(d => d.CriteriaTypeId)
                    .HasConstraintName("FK__criteria_criteriaTypeId__criteria_type_id");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.Criterias)
                    .HasForeignKey(d => d.StatusId)
                    .HasConstraintName("FK__criteria_statusId__status_id");

                entity.HasOne(d => d.SubTopic)
                    .WithMany(p => p.Criterias)
                    .HasForeignKey(d => d.SubTopicId)
                    .HasConstraintName("FK__criteria_subTopicId__sub_topic_id");
            });

            modelBuilder.Entity<CriteriaType>(entity =>
            {
                entity.ToTable("criteria_type");

                entity.HasIndex(e => e.Id)
                    .HasName("ID__criteria_type__id");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("int(10)");

                entity.Property(e => e.Name)
                    .HasColumnName("name")
                    .HasMaxLength(255)
                    .IsRequired();

                entity.Property(e => e.Description)
                    .HasColumnName("description")
                    .HasMaxLength(255);

                entity.Property(e => e.CreateDate)
                    .HasColumnName("createDate")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("updateDate")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP");

                entity.Property(e => e.StatusId)
                    .HasColumnName("statusId")
                    .HasColumnType("int(10)");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.CriteriaTypes)
                    .HasForeignKey(d => d.StatusId)
                    .HasConstraintName("FK__criteria_type_statusId__status_id");
            });

            modelBuilder.Entity<Critical>(entity =>
            {
                entity.ToTable("critical");

                entity.HasIndex(e => e.Id)
                    .HasName("ID__critical__id");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("int(10)");

                entity.Property(e => e.Name)
                    .HasColumnName("name")
                    .HasColumnType("text")
                    .IsRequired();

                entity.Property(e => e.CreateDate)
                    .HasColumnName("createDate")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("updateDate")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP");

                entity.Property(e => e.StatusId)
                    .HasColumnName("statusId")
                    .HasColumnType("int(10)");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.Criticals)
                    .HasForeignKey(d => d.StatusId)
                    .HasConstraintName("FK__critical_statusId__status_id");
            });           

            modelBuilder.Entity<Experience>(entity =>
            {
                entity.ToTable("experience");

                entity.HasIndex(e => e.Id)
                    .HasName("ID__experience__id");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("int(10)");

                entity.Property(e => e.Name)
                    .HasColumnName("name")
                    .HasMaxLength(255)
                    .IsRequired();

                entity.Property(e => e.Description)
                    .HasColumnName("description")
                    .HasMaxLength(255);

                entity.Property(e => e.CreateDate)
                    .HasColumnName("createDate")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("updateDate")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP");

                entity.Property(e => e.StatusId)
                    .HasColumnName("statusId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.Sequence)
                    .HasColumnName("sequence")
                    .HasColumnType("int(5)");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.Experiences)
                    .HasForeignKey(d => d.StatusId)
                    .HasConstraintName("FK__experience_statusId__status_id");
            });            

            modelBuilder.Entity<Instructor>(entity =>
            {
                entity.ToTable("instructor");

                entity.HasIndex(e => e.Id)
                    .HasName("ID__instructor__id");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("int(10)");

                entity.Property(e => e.Firstname)
                    .HasColumnName("firstname")
                    .HasMaxLength(255)
                    .IsRequired();

                entity.Property(e => e.Lastname)
                    .HasColumnName("lastname")
                    .HasMaxLength(255)
                    .IsRequired();

                entity.Property(e => e.Username)
                    .HasColumnName("username")
                    .HasMaxLength(255)
                    .IsRequired();

                entity.Property(e => e.Password)
                    .HasColumnName("password")
                    .HasMaxLength(255)
                    .IsRequired();

                entity.Property(e => e.Image)
                    .HasColumnName("image")
                    .HasMaxLength(255);

                entity.Property(e => e.Email)
                    .HasColumnName("email")
                    .HasMaxLength(255)
                    .IsRequired();

                entity.Property(e => e.Telephone)
                    .HasColumnName("telephone")
                    .HasMaxLength(255);

                entity.Property(e => e.CreateDate)
                    .HasColumnName("createDate")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("updateDate")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP");

                entity.Property(e => e.StatusId)
                    .HasColumnName("statusId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.IsAdmin)
                    .HasColumnName("isAdmin")
                    .HasColumnType("bit(1)");

                entity.Property(e => e.RefreshToken)
                    .HasColumnName("refreshToken")
                    .HasColumnType("text")
                    .IsRequired(false);

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.Instructors)
                    .HasForeignKey(d => d.StatusId)
                    .HasConstraintName("FK__instructor_statusId__status_id");
            });

            modelBuilder.Entity<InstructorConfirmCode>(entity =>
            {
                entity.ToTable("instructor_confirm_code");

                entity.HasIndex(e => e.Id)
                    .HasName("ID__instructor_confirm_code__id");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("int(10)");

                entity.Property(e => e.Code)
                    .HasColumnName("code")
                    .HasMaxLength(255)
                    .IsRequired();
            });

            modelBuilder.Entity<Laboratory>(entity =>
            {
                entity.ToTable("laboratory");

                entity.HasIndex(e => e.Id)
                    .HasName("ID__laboratory__id");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("int(10)");

                entity.Property(e => e.Name)
                    .HasColumnName("name")
                    .HasMaxLength(255)
                    .IsRequired();

                entity.Property(e => e.Description)
                    .HasColumnName("description")
                    .HasMaxLength(255);

                entity.Property(e => e.CreateDate)
                    .HasColumnName("createDate")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("updateDate")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP");

                entity.Property(e => e.StatusId)
                    .HasColumnName("statusId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.Sequence)
                    .HasColumnName("sequence")
                    .HasColumnType("int(5)");

                entity.Property(e => e.EndDate)
                    .HasColumnName("endDate");

                entity.Property(e => e.CourseId)
                    .HasColumnName("courseId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.HasToothConfirm)
                    .HasColumnName("hasToothConfirm")
                    .HasColumnType("bit(1)")
                    .HasDefaultValue(0);

                entity.HasOne(d => d.Course)
                    .WithMany(p => p.Laboratories)
                    .HasForeignKey(d => d.CourseId)
                    .HasConstraintName("FK__laboratory_courseId__course_id");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.Laboratories)
                    .HasForeignKey(d => d.StatusId)
                    .HasConstraintName("FK__laboratory_statusId__status_id");
            });

            modelBuilder.Entity<LaboratoryImage>(entity =>
            {
                entity.ToTable("laboratory_image");

                entity.HasIndex(e => e.Id)
                    .HasName("ID__laboratory_image__id");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("int(10)");

                entity.Property(e => e.LaboratoryId)
                    .HasColumnName("laboratoryId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.Name)
                    .HasColumnName("name")
                    .HasMaxLength(255)
                    .IsRequired();

                entity.Property(e => e.Description)
                    .HasColumnName("description")
                    .HasMaxLength(255);

                entity.Property(e => e.CreateDate)
                    .HasColumnName("createDate")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("updateDate")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP");

                entity.Property(e => e.StatusId)
                    .HasColumnName("statusId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.Sequence)
                    .HasColumnName("sequence")
                    .HasColumnType("int(5)");

                entity.HasOne(d => d.Laboratory)
                    .WithMany(p => p.LaboratoryImages)
                    .HasForeignKey(d => d.LaboratoryId)
                    .HasConstraintName("FK__lab_image_laboratoryId__laboratory_id");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.LaboratoryImages)
                    .HasForeignKey(d => d.StatusId)
                    .HasConstraintName("FK__lab_image_statusId__status_id");
            });

            modelBuilder.Entity<MinusPoint>(entity =>
            {
                entity.ToTable("minus_point");

                entity.HasIndex(e => e.Id)
                    .HasName("ID__experience__id");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("int(10)");

                entity.Property(e => e.Name)
                    .HasColumnName("name")
                    .HasMaxLength(255)
                    .IsRequired();

                entity.Property(e => e.Description)
                    .HasColumnName("description")
                    .HasMaxLength(255);

                entity.Property(e => e.CreateDate)
                    .HasColumnName("createDate")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("updateDate")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP");

                entity.Property(e => e.StatusId)
                    .HasColumnName("statusId")
                    .HasColumnType("int(10)");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.MinusPoints)
                    .HasForeignKey(d => d.StatusId)
                    .HasConstraintName("FK__minus_point_statusId__status_id");
            });

            modelBuilder.Entity<Status>(entity =>
            {
                entity.ToTable("status");

                entity.HasIndex(e => e.Id)
                    .HasName("ID__status__id");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("int(10)");

                entity.Property(e => e.Name)
                    .HasColumnName("name")
                    .HasMaxLength(255)
                    .IsRequired();

                entity.Property(e => e.Description)
                    .HasColumnName("description")
                    .HasMaxLength(255);
            });

            modelBuilder.Entity<Student>(entity =>
            {
                entity.ToTable("student");

                entity.HasIndex(e => e.Id)
                    .HasName("ID__student__id");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("int(10)");

                entity.Property(e => e.Firstname)
                    .HasColumnName("firstname")
                    .HasMaxLength(255)
                    .IsRequired();

                entity.Property(e => e.Lastname)
                    .HasColumnName("lastname")
                    .HasMaxLength(255)
                    .IsRequired();

                entity.Property(e => e.Username)
                    .HasColumnName("username")
                    .HasMaxLength(255)
                    .IsRequired();

                entity.Property(e => e.Password)
                    .HasColumnName("password")
                    .HasMaxLength(255)
                    .IsRequired();

                entity.Property(e => e.Image)
                    .HasColumnName("image")
                    .HasMaxLength(255);

                entity.Property(e => e.Email)
                    .HasColumnName("email")
                    .HasMaxLength(255)
                    .IsRequired();

                entity.Property(e => e.Telephone)
                    .HasColumnName("telephone")
                    .HasMaxLength(255);

                entity.Property(e => e.CourseId)
                    .HasColumnName("courseId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.UniversityIdNumber)
                    .HasColumnName("universityIdNumber")
                    .HasMaxLength(255)
                    .IsRequired();

                entity.Property(e => e.Number)
                    .HasColumnName("number")
                    .HasColumnType("int(10)");

                entity.Property(e => e.Group)
                    .HasColumnName("group")
                    .HasColumnType("int(2)");

                entity.Property(e => e.CreateDate)
                    .HasColumnName("createDate")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("updateDate")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP");

                entity.Property(e => e.StatusId)
                    .HasColumnName("statusId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.RefreshToken)
                    .HasColumnName("refreshToken")
                    .HasColumnType("text")
                    .IsRequired(false);

                entity.HasOne(d => d.Course)
                    .WithMany(p => p.Students)
                    .HasForeignKey(d => d.CourseId)
                    .HasConstraintName("FK__student_courseId__course_id");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.Students)
                    .HasForeignKey(d => d.StatusId)
                    .HasConstraintName("FK__student_statusId__status_id");
            });

            modelBuilder.Entity<StudentConfrimCode>(entity =>
            {
                entity.ToTable("student_confirm_code");

                entity.HasIndex(e => e.Id)
                    .HasName("ID__student_confirm_code__id");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("int(10)");

                entity.Property(e => e.Code)
                    .HasColumnName("code")
                    .HasMaxLength(255)
                    .IsRequired();

                entity.Property(e => e.CourseId)
                    .HasColumnName("courseId")
                    .HasColumnType("int(10)");

                entity.HasOne(d => d.Course)
                    .WithMany(p => p.StudentConfrimCodes)
                    .HasForeignKey(d => d.CourseId)
                    .HasConstraintName("FK__student_confirm_code_courseId__course_id");
            });

            modelBuilder.Entity<StudentLaboratory>(entity =>
            {
                entity.ToTable("student_laboratory");

                entity.HasIndex(e => e.Id)
                    .HasName("ID__sub_topic__id");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("int(10)");

                entity.Property(e => e.StudentId)
                    .HasColumnName("studentId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.LaboratoryId)
                    .HasColumnName("laboratoryId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.Tooth)
                    .HasColumnName("tooth")
                    .HasMaxLength(255);

                entity.Property(e => e.KindOfWork)
                    .HasColumnName("kindOfWork")
                    .HasMaxLength(255);

                entity.Property(e => e.Material)
                    .HasColumnName("material")
                    .HasMaxLength(255);

                entity.Property(e => e.CreateDate)
                    .HasColumnName("createDate")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("updateDate")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP");

                entity.Property(e => e.StatusId)
                    .HasColumnName("statusId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.Image)
                    .HasColumnName("image")
                    .HasMaxLength(255)
                    .IsRequired(false);

                entity.Property(e => e.InstructorConfirmId)
                    .HasColumnName("InstuctorConfirmId")
                    .HasColumnType("int(10)")
                    .IsRequired(false);

                entity.HasOne(d => d.Laboratory)
                    .WithMany(p => p.StudentLaboratories)
                    .HasForeignKey(d => d.LaboratoryId)
                    .HasConstraintName("FK__student_laboratory_labId__laboratory_id");

                entity.HasOne(d => d.Student)
                    .WithMany(p => p.StudentLaboratories)
                    .HasForeignKey(d => d.StudentId)
                    .HasConstraintName("FK__student_laboratory_studentId__student_id");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.StudentLaboratories)
                    .HasForeignKey(d => d.StatusId)
                    .HasConstraintName("FK__student_laboratory_statusId__status_id");

                entity.HasOne(d => d.Instructor)
                    .WithMany(p => p.StudentLaboratories)
                    .HasForeignKey(d => d.InstructorConfirmId)
                    .HasConstraintName("FK__student_laboratory_instConfirmId__instructor_id");
            });

            modelBuilder.Entity<StudentLaboratoryTeethConfirm>(entity =>
            {
                entity.ToTable("student_laboratory_teeth_confirm");

                entity.HasIndex(e => e.Id)
                    .HasName("ID__sub_topic__id");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("int(10)");

                entity.Property(e => e.StudentId)
                    .HasColumnName("studentId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.TeethNo)
                    .HasColumnName("teethNo")
                    .HasMaxLength(255);

                entity.Property(e => e.CanUse)
                    .HasColumnName("canUse")
                    .HasColumnType("bit(1)")
                    .HasDefaultValue(0);

                entity.Property(e => e.CanNotUse)
                    .HasColumnName("canNotUse")
                    .HasColumnType("bit(1)")
                    .HasDefaultValue(0);

                entity.Property(e => e.UseInLab)
                    .HasColumnName("useInLab")
                    .HasColumnType("bit(1)")
                    .HasDefaultValue(0);

                entity.Property(e => e.UseInControl)
                    .HasColumnName("useInControl")
                    .HasColumnType("bit(1)")
                    .HasDefaultValue(0);

                entity.Property(e => e.Comment)
                    .HasColumnName("comment")
                    .HasMaxLength(255);

                entity.Property(e => e.CreateDate)
                    .HasColumnName("createDate")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("updateDate")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP");

                entity.Property(e => e.StatusId)
                    .HasColumnName("statusId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.LaboratoryId)
                    .HasColumnName("laboratoryId")
                    .HasColumnType("int(10)");

                entity.HasOne(d => d.Student)
                    .WithMany(p => p.StudentLaboratoryTeethConfirms)
                    .HasForeignKey(d => d.StudentId)
                    .HasConstraintName("FK__student_lab_con__studentId__student_id");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.StudentLaboratoryTeethConfirms)
                    .HasForeignKey(d => d.StatusId)
                    .HasConstraintName("FK__student_lab_con__statusId__status_id");

                entity.HasOne(d => d.Instructor)
                    .WithMany(p => p.StudentLaboratoryTeethConfirms)
                    .HasForeignKey(d => d.InstructorId)
                    .HasConstraintName("FK__student_lab_con__instId__instructor_id");

                entity.HasOne(d => d.Laboratory)
                    .WithMany(p => p.StudentLaboratoryTeethConfirms)
                    .HasForeignKey(d => d.LaboratoryId)
                    .HasConstraintName("FK__student_lab_con__labId__lab_id");

            });

            modelBuilder.Entity<StudentLaboratoryTeethApproval>(entity =>
            {
                entity.ToTable("student_laboratory_teeth_approval");

                entity.HasIndex(e => e.Id)
                    .HasName("ID__sub_topic__id");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("int(10)");

                entity.Property(e => e.StudentId)
                    .HasColumnName("studentId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.IsApproved)
                    .HasColumnName("isApproved")
                    .HasColumnType("bit(1)")
                    .IsRequired(false);

                entity.Property(e => e.InstructorId)
                    .HasColumnName("instructorId")
                    .HasColumnType("int(10)")
                    .IsRequired(false);

                entity.Property(e => e.Comment)
                    .HasColumnName("comment")
                    .HasMaxLength(255);

                entity.Property(e => e.CreateDate)
                    .HasColumnName("createDate")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("updateDate")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP");

                entity.Property(e => e.StatusId)
                    .HasColumnName("statusId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.LaboratoryId)
                    .HasColumnName("laboratoryId")
                    .HasColumnType("int(10)");

                entity.HasOne(d => d.Student)
                    .WithMany(p => p.StudentLaboratoryTeethApprovals)
                    .HasForeignKey(d => d.StudentId)
                    .HasConstraintName("FK__student_lab_app__studentId__student_id");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.StudentLaboratoryTeethApprovals)
                    .HasForeignKey(d => d.StatusId)
                    .HasConstraintName("FK__student_lab_app__statusId__status_id");

                entity.HasOne(d => d.Instructor)
                    .WithMany(p => p.StudentLaboratoryTeethApprovals)
                    .HasForeignKey(d => d.InstructorId)
                    .HasConstraintName("FK__student_lab_app__instId__instructor_id");

                entity.HasOne(d => d.Laboratory)
                    .WithMany(p => p.StudentLaboratoryTeethApprovals)
                    .HasForeignKey(d => d.LaboratoryId)
                    .HasConstraintName("FK__student_lab_app__labId__lab_id");

            });

            modelBuilder.Entity<StudentLaboratoryImage>(entity =>
            {
                entity.ToTable("student_laboratory_image");

                entity.HasIndex(e => e.Id)
                    .HasName("ID__sub_topic__id");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("int(10)");

                entity.Property(e => e.StudentId)
                    .HasColumnName("studentId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.LaboratoryImageId)
                    .HasColumnName("laboratoryImageId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.Image)
                    .HasColumnName("image")
                    .HasMaxLength(255)
                    .IsRequired(); 

                entity.Property(e => e.Remark)
                    .HasColumnName("remark")
                    .HasMaxLength(255);

                entity.Property(e => e.CreateDate)
                    .HasColumnName("createDate")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("updateDate")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP");

                entity.Property(e => e.StatusId)
                    .HasColumnName("statusId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.InstructorConfirmId)
                    .HasColumnName("InstuctorConfirmId")
                    .HasColumnType("int(10)")
                    .IsRequired(false);

                entity.HasOne(d => d.Student)
                    .WithMany(p => p.StudentLaboratoryImages)
                    .HasForeignKey(d => d.StudentId)
                    .HasConstraintName("FK__student_lab_Image_studentId__student_id");

                entity.HasOne(d => d.LaboratoryImage)
                    .WithMany(p => p.StudentLaboratoryImages)
                    .HasForeignKey(d => d.LaboratoryImageId)
                    .HasConstraintName("FK__student_lab_Image_labImgId__lab_img_id");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.StudentLaboratoryImages)
                    .HasForeignKey(d => d.StatusId)
                    .HasConstraintName("FK__student_lab_Image_statusId__status_id");

                entity.HasOne(d => d.Instructor)
                    .WithMany(p => p.StudentLaboratoryImages)
                    .HasForeignKey(d => d.InstructorConfirmId)
                    .HasConstraintName("FK__student_lab_Image_instConfirmId__instructor_id");
            });

            modelBuilder.Entity<StudentMinusPoint>(entity =>
            {
                entity.ToTable("student_minus_point");

                entity.HasIndex(e => e.Id)
                    .HasName("ID__sub_topic__id");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("int(10)");

                entity.Property(e => e.MinusPointId)
                    .HasColumnName("minusPointId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.InstructorId)
                    .HasColumnName("instructorId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.StudentLaboratoryId)
                    .HasColumnName("studentLaboratoryId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.Score)
                    .HasColumnName("score")
                    .HasColumnType("int(10)");

                entity.Property(e => e.Remark)
                    .HasColumnName("remark")
                    .HasMaxLength(255);

                entity.Property(e => e.CreateDate)
                    .HasColumnName("createDate")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("updateDate")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP");

                entity.Property(e => e.StatusId)
                    .HasColumnName("statusId")
                    .HasColumnType("int(10)");

                entity.HasOne(d => d.Instructor)
                    .WithMany(p => p.StudentMinusPoints)
                    .HasForeignKey(d => d.InstructorId)
                    .HasConstraintName("FK__student_minus_p_instructorId__instructor_id");

                entity.HasOne(d => d.MinusPoint)
                    .WithMany(p => p.StudentMinusPoints)
                    .HasForeignKey(d => d.MinusPointId)
                    .HasConstraintName("FK__student_minus_p_minusPointId__minus_point_id");

                entity.HasOne(d => d.StudentLaboratory)
                    .WithMany(p => p.StudentMinusPoints)
                    .HasForeignKey(d => d.StudentLaboratoryId)
                    .HasConstraintName("FK__student_minus_p_stuLabId__stu_lab_id");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.StudentMinusPoints)
                    .HasForeignKey(d => d.StatusId)
                    .HasConstraintName("FK__student_minus_p_statusId__status_id");
            });

            modelBuilder.Entity<StudentScore>(entity =>
            {
                entity.ToTable("student_score");

                entity.HasIndex(e => e.Id)
                    .HasName("ID__student_score__id");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("int(10)");

                entity.Property(e => e.StudentSubTopicId)
                    .HasColumnName("studentSubTopicId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.InstructorId)
                    .HasColumnName("instructorId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.CriteriaId)
                    .HasColumnName("criteriaId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.IsCritical)
                    .HasColumnName("isCritical")
                    .HasColumnType("bit(1)")
                    .HasDefaultValue(0);

                entity.Property(e => e.Score)
                    .HasColumnName("score")
                    .HasColumnType("int(10)");

                entity.Property(e => e.Remark)
                    .HasColumnName("remark")
                    .HasMaxLength(255);

                entity.Property(e => e.CreateDate)
                    .HasColumnName("createDate")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("updateDate")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP");

                entity.Property(e => e.StatusId)
                    .HasColumnName("statusId")
                    .HasColumnType("int(10)");

                entity.HasOne(d => d.Criteria)
                    .WithMany(p => p.StudentScores)
                    .HasForeignKey(d => d.CriteriaId)
                    .HasConstraintName("FK__student_score_criteriaId__criteria_id");

                entity.HasOne(d => d.Instructor)
                    .WithMany(p => p.StudentScores)
                    .HasForeignKey(d => d.InstructorId)
                    .HasConstraintName("FK__student_score_instructorId__instructor_id");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.StudentScores)
                    .HasForeignKey(d => d.StatusId)
                    .HasConstraintName("FK__student_score_statusId__status_id");

                entity.HasOne(d => d.StudentSubTopic)
                    .WithMany(p => p.StudentScores)
                    .HasForeignKey(d => d.StudentSubTopicId)
                    .HasConstraintName("FK__student_score_stuSubTopicId__stu_sub_topic_id");
            });

            modelBuilder.Entity<StudentSubTopic>(entity =>
            {
                entity.ToTable("student_sub_topic");

                entity.HasIndex(e => e.Id)
                    .HasName("ID__student_sub_topic__id");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("int(10)");

                entity.Property(e => e.StudentLaboratoryId)
                    .HasColumnName("studentLaboratoryId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.SubTopicId)
                    .HasColumnName("subTopicId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.SelfAssessment)
                    .HasColumnName("selfAssessment")
                    .HasColumnType("int(10)");

                entity.Property(e => e.CreateDate)
                    .HasColumnName("createDate")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("updateDate")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP");

                entity.Property(e => e.StatusId)
                    .HasColumnName("statusId")
                    .HasColumnType("int(10)");

                entity.HasOne(d => d.StudentLaboratory)
                    .WithMany(p => p.StudentSubTopics)
                    .HasForeignKey(d => d.StudentLaboratoryId)
                    .HasConstraintName("FK__student_sub_topic_stulabId__stu_laboratory_id");

                entity.HasOne(d => d.SubTopic)
                    .WithMany(p => p.StudentSubTopics)
                    .HasForeignKey(d => d.SubTopicId)
                    .HasConstraintName("FK__student_sub_topic_subTopicId__sub_topic_id");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.StudentSubTopics)
                    .HasForeignKey(d => d.StatusId)
                    .HasConstraintName("FK__student_sub_topic_statusId__status_id");
            });

            modelBuilder.Entity<SubTopic>(entity =>
            {
                entity.ToTable("sub_topic");

                entity.HasIndex(e => e.Id)
                    .HasName("ID__sub_topic__id");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("int(10)");

                entity.Property(e => e.Name)
                    .HasColumnName("name")
                    .HasMaxLength(255)
                    .IsRequired();

                entity.Property(e => e.Description)
                    .HasColumnName("description")
                    .HasMaxLength(255);

                entity.Property(e => e.CreateDate)
                    .HasColumnName("createDate")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("updateDate")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP");

                entity.Property(e => e.StatusId)
                    .HasColumnName("statusId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.TopicId)
                    .HasColumnName("topicId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.Sequence)
                    .HasColumnName("sequence")
                    .HasColumnType("int(5)");

                entity.Property(e => e.CriticalId)
                    .HasColumnName("criticalId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.IsLabel)
                    .HasColumnName("isLabel")
                    .HasColumnType("bit(1)")
                    .HasDefaultValue(0);

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.SubTopics)
                    .HasForeignKey(d => d.StatusId)
                    .HasConstraintName("FK__sub_topic_statusId__status_id");

                entity.HasOne(d => d.Topic)
                    .WithMany(p => p.SubTopics)
                    .HasForeignKey(d => d.TopicId)
                    .HasConstraintName("FK__sub_topic_topicId__topic_id");
                
                entity.HasOne(d => d.Critical)
                     .WithMany(p => p.SubTopics)
                     .HasForeignKey(d => d.CriticalId)
                     .HasConstraintName("FK__sub_topic_criticalId__critical_id");
            });

            modelBuilder.Entity<Topic>(entity =>
            {
                entity.ToTable("topic");

                entity.HasIndex(e => e.Id)
                    .HasName("ID__topic__id");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("int(10)");

                entity.Property(e => e.Name)
                    .HasColumnName("name")
                    .HasMaxLength(255)
                    .IsRequired();

                entity.Property(e => e.Description)
                    .HasColumnName("description")
                    .HasMaxLength(255);

                entity.Property(e => e.CreateDate)
                    .HasColumnName("createDate")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("updateDate")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP");

                entity.Property(e => e.StatusId)
                    .HasColumnName("statusId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.LaboratoryId)
                    .HasColumnName("laboratoryId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.ExperienceId)
                    .HasColumnName("experienceId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.Sequence)
                    .HasColumnName("sequence")
                    .HasColumnType("int(5)");

                entity.Property(e => e.EndDate)
                    .HasColumnName("endDate");

                entity.HasOne(d => d.Laboratory)
                    .WithMany(p => p.Topics)
                    .HasForeignKey(d => d.LaboratoryId)
                    .HasConstraintName("FK__topic_laboratoryId__laboratory_id");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.Topics)
                    .HasForeignKey(d => d.StatusId)
                    .HasConstraintName("FK__topic_statusId__status_id");

                entity.HasOne(d => d.Experience)
                    .WithMany(p => p.Topics)
                    .HasForeignKey(d => d.ExperienceId)
                    .HasConstraintName("FK__topic_experienceId__experience_id");
            });        
        }
    }
}
