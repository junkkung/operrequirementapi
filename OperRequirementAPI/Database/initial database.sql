-- ----------
-- status --
-- ----------
insert into status
    (name)
values
    ('active');
insert into status
    (name)
values
    ('inactive');


-- ----------
-- course --
-- ----------
insert into course
    (name, semesterYear, startDate, endDate, statusId)
values
    ('OperII(DRD 412)', '2020', '2020-08-01 00:00:00', '2020-05-30 00:00:00', 1);

-- ------------
-- minus point --
-- ------------
insert into minus_point
    (name, statusId)
values
    ('Knowledge for conducting labs exercise (-3)', 1);
insert into minus_point
    (name, statusId)
values
    ('Correct and well prepare instruments (-2)', 1);
insert into minus_point
    (name, statusId)
values
    ('Cleanliness, approriate manner (-2)', 1);
insert into minus_point
    (name, statusId)
values
    ('Time punctuality (-3)', 1);

-- --------------
-- laboratory --
-- --------------
INSERT INTO laboratory
    (name, statusId, sequence, courseId, endDate)
values
    ('Class II amalgam 46(MOD)', 1, 1, 1, '2021-05-30 23:59:59');
INSERT INTO laboratory
    (name, statusId, sequence, courseId, endDate)
values
    ('Caries removal', 1, 2, 1, '2021-05-30 23:59:59');
INSERT INTO laboratory
    (name, statusId, sequence, courseId, endDate)
values
    ('Pulp protection', 1, 3, 1, '2021-05-30 23:59:59');
INSERT INTO laboratory
    (name, statusId, sequence, courseId, endDate)
values
    ('Class I composite preparation 36(O)', 1, 4, 1, '2021-05-30 23:59:59');
INSERT INTO laboratory
    (name, statusId, sequence, courseId, endDate)
values
    ('Class II composite 36(MOD)', 1, 5, 1, '2021-05-30 23:59:59');
INSERT INTO laboratory
    (name, statusId, sequence, courseId, endDate)
values
    ('Cl.V composite', 1, 6, 1, '2021-05-30 23:59:59');
INSERT INTO laboratory
    (name, statusId, sequence, courseId, endDate)
values
    ('Cl.V GICs', 1, 7, 1, '2021-05-30 23:59:59');

-- -----------------
-- laboratory image--
-- -----------------
insert into laboratory_image
    (name, laboratoryId, statusId, sequence)
values
    ('Picture of class II amalgam cavity preparation Occlusal View', 1 , 1, 1);
insert into laboratory_image
    (name, laboratoryId, statusId, sequence)
values
    ('Picture of class II amalgam restoration polishing Occlusal View', 1 , 1, 2);
insert into laboratory_image
    (name, laboratoryId, statusId, sequence)
values
    ('Picture of class I composite cavity preparation Occlusal View', 4 , 1, 1);
insert into laboratory_image
    (name, laboratoryId, statusId, sequence)
values
    ('Picture of class II composite cavity preparation Occlusal View', 5 , 1, 1);
insert into laboratory_image
    (name, laboratoryId, statusId, sequence)
values
    ('Picture of class II composite restoration polishing Occlusal View', 5 , 1, 2);
insert into laboratory_image
    (name, laboratoryId, statusId, sequence)
values
    ('Picture of cavity preparation Class V composite Buccal View', 6 , 1, 1);
insert into laboratory_image
    (name, laboratoryId, statusId, sequence)
values
    ('Picture of restoration polishing Class V composite Buccal View', 6 , 1, 2);
insert into laboratory_image
    (name, laboratoryId, statusId, sequence)
values
    ('Picture of cavity preparation Class V GICs Buccal View', 7 , 1, 1);
insert into laboratory_image
    (name, laboratoryId, statusId, sequence)
values
    ('Picture of restoration polishing Class V GICs Buccal View', 7 , 1, 2);
-- --------------
-- experience --
-- --------------
insert into experience
    (name, statusId, sequence)
values
    ('Amalgampreparation', 1, 1);
insert into experience
    (name, statusId, sequence)
values
    ('Amalgam restoration', 1, 2);
insert into experience
    (name, statusId, sequence)
values
    ('Composite preparation', 1, 3);
insert into experience
    (name, statusId, sequence)
values
    ('Rubber dam', 1, 4);
insert into experience
    (name, statusId, sequence)
values
    ('Composite restoration', 1, 5);
insert into experience
    (name, statusId, sequence)
values
    ('Amalgam polishing', 1, 6);
insert into experience
    (name, statusId, sequence)
values
    ('Composite polishing', 1, 7);


-- ----------
-- topic --
-- ----------
insert into topic
    (name, statusId, laboratoryId, experienceId, sequence)
values
    ('Amalgam Beginning check', 1, 1, 1, 1);
insert into topic
    (name, statusId, laboratoryId, experienceId, sequence)
values
    ('Amalgam Occlusal cavity preparation', 1, 1, 1, 2);
insert into topic
    (name, statusId, laboratoryId, experienceId, sequence)
values
    ('Amalgam Proximal cavity preparation', 1, 1, 1, 3);
insert into topic
    (name, statusId, laboratoryId, experienceId, sequence)
values
    ('Amalgam Matrix & wedge', 1, 1, 2, 4);
insert into topic
    (name, statusId, laboratoryId, experienceId, sequence)
values
    ('Amalgam Restoration', 1, 1, 2, 5);
insert into topic
    (name, statusId, laboratoryId, experienceId, sequence)
values
    ('Amalgam Polishing', 1, 1, 6, 6);

insert into topic
    (name, statusId, laboratoryId, experienceId, sequence)
values
    ('Initial preparation', 1, 2, null, 1);
insert into topic
    (name, statusId, laboratoryId, experienceId, sequence)
values
    ('Superficial caries removal', 1, 2, null, 2);
insert into topic
    (name, statusId, laboratoryId, experienceId, sequence)
values
    ('Deep caries removal', 1, 2, null, 3);
insert into topic
    (name, statusId, laboratoryId, experienceId, sequence)
values
    ('Cleanliness', 1, 2, null, 4);

insert into topic
    (name, statusId, laboratoryId, experienceId, sequence)
values
    ('Subbase', 1, 3, null, 1);
insert into topic
    (name, statusId, laboratoryId, experienceId, sequence)
values
    ('Base', 1, 3, null, 2);
insert into topic
    (name, statusId, laboratoryId, experienceId, sequence)
values
    ('Cavity preparation', 1, 3, null, 3);

insert into topic
    (name, statusId, laboratoryId, experienceId, sequence)
values
    ('Class I composite Beginning check', 1, 4, null, 1);
insert into topic
    (name, statusId, laboratoryId, experienceId, sequence)
values
    ('Class I composite Cavity preparation', 1, 4, null, 2);

insert into topic
    (name, statusId, laboratoryId, experienceId, sequence)
values
    ('Class II composite Beginning check', 1, 5, 3, 1);
insert into topic
    (name, statusId, laboratoryId, experienceId, sequence)
values
    ('Class II composite Occlusal cavity preparation', 1, 5, 3, 2);
insert into topic
    (name, statusId, laboratoryId, experienceId, sequence)
values
    ('Class II composite Proximal cavity preparation', 1, 5, 3, 3);
insert into topic
    (name, statusId, laboratoryId, experienceId, sequence)
values
    ('Rubber dam application', 1, 5, 4, 4);
insert into topic
    (name, statusId, laboratoryId, experienceId, sequence)
values
    ('Rubber dam removal', 1, 5, 4, 5);
insert into topic
    (name, statusId, laboratoryId, experienceId, sequence)
values
    ('Class II composite Matrix and wedge', 1, 5, 5, 6);
insert into topic
    (name, statusId, laboratoryId, experienceId, sequence)
values
    ('Class II composite Restoration', 1, 5, 5, 7);
insert into topic
    (name, statusId, laboratoryId, experienceId, sequence)
values
    ('Class II composite Polishing', 1, 5, 7, 8);

insert into topic
    (name, statusId, laboratoryId, experienceId, sequence)
values
    ('Class V composite Beginning check', 1, 6, null, 1);
insert into topic
    (name, statusId, laboratoryId, experienceId, sequence)
values
    ('Class V composite Cavity preparation', 1, 6, null, 2);
insert into topic
    (name, statusId, laboratoryId, experienceId, sequence)
values
    ('Class V composite Restoration', 1, 6, null, 3);
insert into topic
    (name, statusId, laboratoryId, experienceId, sequence)
values
    ('Class V composite Polishing', 1, 6, null, 4);

insert into topic
    (name, statusId, laboratoryId, experienceId, sequence)
values
    ('Class V GICs Beginning check', 1, 7, null, 1);
insert into topic
    (name, statusId, laboratoryId, experienceId, sequence)
values
    ('Class V GICs Cavity preparation', 1, 7, null, 2);
insert into topic
    (name, statusId, laboratoryId, experienceId, sequence)
values
    ('Class V GICs Restoration', 1, 7, null, 3);
insert into topic
    (name, statusId, laboratoryId, experienceId, sequence)
values
    ('Class V GICs Polishing', 1, 7, null, 4);

-- ------------
-- critical --
-- ------------

insert into critical
    (name, statusId)
values
    ('- Incorrect tooth or surface', 1);

insert into critical
    (name, statusId)
values
    ('- Iatrogenic damage tooth structure on occlusal / buccal / lingual tooth surface which can be polished
- Grossly damage to tooth structure on occlusal / buccal / lingual surface which requires a restoration
- Gingival wall width ≥ 3 mm
- Pulpal wall depth ≥ 4 mm', 1);

insert into critical
    (name, statusId)
values
    ('- Grossly damage to tooth structure on occlusal / buccal / lingual surface which requires a restoration
- Gingival wall width ≥ 3 mm
- Pulpal wall depth ≥ 4 mm', 1);

insert into critical
    (name, statusId)
values
    ('- Iatrogenic tooth preparation on occlusal / buccal / lingual tooth surface which is needed for restoration
- Gingival wall width ≥ 3 mm
- Pulpal wall depth ≥ 4 mm', 1);

insert into critical
    (name, statusId)
values
    ('- Damage to adjacent tooth (tooth 45) which is polishable
- Damage to adjacent tooth (tooth 45) which restoration is needed 
- Gingival wall width ≥ 3 mm
- Pulpal wall depth ≥ 4 mm', 1);

insert into critical
    (name, statusId)
values
    ('- Damage to adjacent tooth (tooth 45) which restoration is needed 
- Gingival wall width ≥ 3 mm
- Pulpal wall depth ≥ 4 mm', 1);

insert into critical
    (name, statusId)
values
    ('- Damage to adjacent tooth (tooth 45) which is polishable
- Damage to adjacent tooth (tooth 45) which restoration is needed 
- Gingival wall width ≥ 3 mm
- Pulpal wall depth ≥ 4 mm', 1);

insert into critical
    (name, statusId)
values
    ('- Damage to adjacent tooth (tooth 45) which restoration is needed 
- Gingival wall width ≥ 3 mm
- Pulpal wall depth ≥ 4 mm', 1);

insert into critical
    (name, statusId)
values
    ('- Incorrect tooth number or tooth surface
- Use improper matrix system 
- Use improper wedge 
- Filling without inspection of matrix and wedge', 1);

insert into critical
    (name, statusId)
values
    ('- Filling without inspection of matrix and wedge 
- Polish without inspection of amalgam filling', 1);

insert into critical
    (name, statusId)
values
    ('- Polish without inspection of amalgam filling 
- Grossly damage tooth structure 
- Dry polishing', 1);

insert into critical
    (name, statusId)
values
    ('- Initial preparation without instructor’s permission', 1);

insert into critical
    (name, statusId)
values
    ('- Remove caries without instructor’s permission', 1);

insert into critical
    (name, statusId)
values
    ('- Remove caries without instructor’s permission
- Accidently exposed pulp with a “contaminate” field', 1);

insert into critical
    (name, statusId)
values
    ('- Use improper material', 1);

insert into critical
    (name, statusId)
values
    ('- Use improper material', 1);

insert into critical
    (name, statusId)
values
    ('- Grossly over extended', 1);

insert into critical
    (name, statusId)
values
    ('- Incorrect tooth or surface', 1);

insert into critical
    (name, statusId)
values
    ('- Iatrogenic damage tooth structure on occlusal / buccal / lingual tooth surface which can be polished 
- Grossly damage to tooth structure on occlusal / buccal / lingual surface which requires a restoration 
- Pulpal wall depth ≥ 4 mm', 1);

insert into critical
    (name, statusId)
values
    ('- Grossly damage to tooth structure on occlusal / buccal / lingual surface which requires a restoration 
- Pulpal wall depth ≥ 4 mm', 1);

insert into critical
    (name, statusId)
values
    ('- Incorrect tooth or surface', 1);

insert into critical
    (name, statusId)
values
    ('- Iatrogenic damage tooth structure on occlusal / buccal / lingual tooth surface which can be polished 
- Grossly damage to tooth structure on occlusal / buccal / lingual surface which requires a restoration 
- Pulpal wall depth ≥ 4 mm', 1);

insert into critical
    (name, statusId)
values
    ('- Damage to adjacent tooth (tooth 35) which is polishable- Damage to adjacent tooth (tooth 35) which restoration is needed 
- Gingival wall width ≥ 3 mm 
- Pulpal wall depth ≥ 4 mm', 1);

insert into critical
    (name, statusId)
values
    ('- Damage to adjacent tooth (tooth 35) which restoration is needed 
- Gingival wall width ≥ 3 mm 
- Pulpal wall depth ≥ 4 mm', 1);

insert into critical
    (name, statusId)
values
    ('- Inappropriate anchorage tooth 
- Inadequate extension of rubber dam', 1);

insert into critical
    (name, statusId)
values
    ('- Remove rubber dam without instructor’s inspection', 1);

insert into critical
    (name, statusId)
values
    ('- Incorrect tooth number or tooth surface 
- Use improper matrix system', 1);

insert into critical
    (name, statusId)
values
    ('- Bonding without instructor’s inspection', 1);

insert into critical
    (name, statusId)
values
    ('- Bonding without instructor’s inspection 
- Polish composite before restoration checked', 1);

insert into critical
    (name, statusId)
values
    ('- Polish composite before restoration checked 
- Grossly damage tooth structure', 1);

insert into critical
    (name, statusId)
values
    ('- Incorrect tooth or surface', 1);

insert into critical
    (name, statusId)
values
    ('- Iatrogenic damage tooth structure on occlusal / buccal / lingual tooth surface which can be polished 
- Grossly damage to tooth structure on occlusal / buccal / lingual surface which requires a restoration- Axial wall depth ≥ 3 mm', 1);

insert into critical
    (name, statusId)
values
    ('- Grossly damage to tooth structure on occlusal / buccal / lingual surface which requires a restoration 
- Axial wall depth ≥ 3 mm', 1);

insert into critical
    (name, statusId)
values
    ('- Iatrogenic tooth preparation on occlusal / buccal tooth surface which restoration is needed 
- Axial wall depth is deeper than 3 mm', 1);

insert into critical
    (name, statusId)
values
    ('- Bonding without instructor’s inspection', 1);

insert into critical
    (name, statusId)
values
    ('- Apply bonding without instructor’s inspection 
- Polish composite before restoration checked', 1);

insert into critical
    (name, statusId)
values
    ('- Polish composite before restoration checked 
- Grossly damage tooth structure', 1);

insert into critical
    (name, statusId)
values
    ('- Incorrect tooth or surface', 1);

insert into critical
    (name, statusId)
values
    ('- Iatrogenic damage tooth structure on occlusal / buccal / lingual tooth surface which can be polished 
- Grossly damage to tooth structure on occlusal / buccal / lingual surface which requires a restoration- Axial wall depth ≥ 3 mm', 1);

insert into critical
    (name, statusId)
values
    ('- Grossly damage to tooth structure on occlusal / buccal / lingual surface which requires a restoration 
- Axial wall depth ≥ 3 mm', 1);

insert into critical
    (name, statusId)
values
    ('- Iatrogenic tooth preparation on occlusal / buccal tooth surface which restoration is needed 
- Axial wall depth is deeper than 3 mm', 1);

insert into critical
    (name, statusId)
values
    ('- Apply Conditioner and mix GIC without instructor’s inspection 
- Polish GIC without restoration checked', 1);

insert into critical
    (name, statusId)
values
    ('- Polish GIC without restoration checked 
- Grossly damage tooth structure', 1);

-- ------------
-- subTopic --
-- ------------
insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Cavity design', 1, 1, 1, 1, 0);

insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Outline1', 1, 2, 1, 2, 0);
insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Outline2', 1, 2, 2, 2, 0);
insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Angle of departure1', 1, 2, 3, 3, 0);
insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Angle of departure2', 1, 2, 4, 3, 0);
insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Isthmus width', 1, 2, 5, 4, 0);
insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Remaining cusp thickness', 1, 2, 6, 4, 0);
insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Internal line angles', 1, 2, 7, 4, 0);
insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Wall dimension', 1, 2, 8, 4, 1);
insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Buccal wall', 1, 2, 9, 4, 0);
insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Lingual wall', 1, 2, 10, 4, 0);
insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Pulpal wall', 1, 2, 11, 4, 0);
insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Smoothness', 1, 2, 12, 4, 1);
insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Pulpal floor & surrounding walls', 1, 2, 13, 4, 0);

insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Mesial cavity', 1, 3, 1, 5, 1);
insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Buccal and lingual outline', 1, 3, 2, 5, 0);
insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Gingival outline', 1, 3, 3, 5, 0);
insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Buccal cavosurface margin', 1, 3, 4, 6, 0);
insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Lingual cavosurface margin', 1, 3, 5, 6, 0);
insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Gingival cavosurface margin', 1, 3, 6, 6, 0);
insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Buccal wall', 1, 3, 7, 6, 0);
insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Lingual wall', 1, 3, 8, 6, 0);
insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Gingival wall', 1, 3, 9, 6, 0);
insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Axial wall', 1, 3, 10, 6, 0);
insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Internal line angles', 1, 3, 11, 6, 0);
insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Axio-pulpal line angles', 1, 3, 12, 6, 0);
insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Smoothness', 1, 3, 13, 6, 0);
insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Distal cavity', 1, 3, 14, 7, 1);
insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Buccal and lingual outline', 1, 3, 15, 7, 0);
insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Gingival outline', 1, 3, 16, 7, 0);
insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Buccal cavosurface margin', 1, 3, 17, 8, 0);
insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Lingual cavosurface margin', 1, 3, 18, 8, 0);
insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Gingival cavosurface margin', 1, 3, 19, 8, 0);
insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Buccal wall', 1, 3, 20, 8, 0);
insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Lingual wall', 1, 3, 21, 8, 0);
insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Gingival wall', 1, 3, 22, 8, 0);
insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Axial wall', 1, 3, 23, 8, 0);
insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Internal line angles', 1, 3, 24, 8, 0);
insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Axio-pulpal line angles', 1, 3, 25, 8, 0);
insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Smoothness', 1, 3, 26, 8, 0);

insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Matrix retainer placement 1', 1, 4, 1, 9, 0);
insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Matrix retainer placement 2', 1, 4, 2, 9, 0);
insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Matrix retainer placement 3', 1, 4, 3, 9, 0);
insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Matrix Band placement 1', 1, 4, 4, 9, 0);
insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Matrix Band placement 2', 1, 4, 5, 9, 0);
insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Matrix Band placement 3', 1, 4, 6, 9, 0);
insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Matrix Band placement 4', 1, 4, 7, 9, 0);
insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Wedge Insertion 1', 1, 4, 8, 9, 0);
insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Wedge Insertion 2', 1, 4, 9, 9, 0);
insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Wedge Insertion 3', 1, 4, 10, 9, 0);

insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Occlusal margin', 1, 5, 1, 10, 0);
insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Gingival margin', 1, 5, 2, 10, 0);
insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Occlusal anatomy 1', 1, 5, 3, 10, 0);
insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Occlusal anatomy 2', 1, 5, 4, 10, 0);
insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Occlusal anatomy 3', 1, 5, 5, 10, 0);
insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Contact 1', 1, 5, 6, 10, 0);
insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Contact 2', 1, 5, 7, 10, 0);
insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Proximal 1', 1, 5, 8, 10, 0);
insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Proximal 2', 1, 5, 9, 10, 0);
insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Smoothness', 1, 5, 10, 10, 0);
insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Occlusion', 1, 5, 11, 10, 0);

insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Occlusal margin', 1, 6, 1, 11, 0);
insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Gingival margin', 1, 6, 2, 11, 0);
insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Occlusal anatomy 1', 1, 6, 3, 11, 0);
insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Occlusal anatomy 2', 1, 6, 4, 11, 0);
insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Occlusal anatomy 3', 1, 6, 5, 11, 0);
insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Contact 1', 1, 6, 6, 11, 0);
insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Contact 2', 1, 6, 7, 11, 0);
insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Proximal 1', 1, 6, 8, 11, 0);
insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Proximal 2', 1, 6, 9, 11, 0);
insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Smoothness', 1, 6, 10, 11, 0);
insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Surface polishing', 1, 6, 11, 11, 0);

insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Initial preparation 1', 1, 7, 1, 12, 0);
insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Initial preparation 2', 1, 7, 2, 12, 0);

insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Superficial caries removal 1', 1, 8, 1, 13, 0);
insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Superficial caries removal 2', 1, 8, 2, 13, 0);

insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Deep caries removal 1', 1, 9, 1, 14, 0);
insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Deep caries removal 2', 1, 9, 2, 14, 0);
insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Deep caries removal 3', 1, 9, 3, 14, 0);
insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Deep caries removal 4', 1, 9, 4, 14, 0);
insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Deep caries removal 5', 1, 9, 5, 14, 0);

insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Cleanliness', 1, 10, 1, 14, 0);

insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Subbase', 1, 11, 1, 15, 0);

insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Base', 1, 12, 1, 16, 0);

insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Cavity preparation 1', 1, 13, 1, 17, 0);
insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Cavity preparation 2', 1, 13, 2, 17, 0);
insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Cavity preparation 3', 1, 13, 3, 17, 0);
insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Cavity preparation 4', 1, 13, 4, 17, 0);

insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Cavity design', 1, 14, 1, 18, 0);

insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Outline', 1, 15, 1, 19, 0);
insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Cavosurface margin', 1, 15, 2, 20, 0);
insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Cavity depth', 1, 15, 3, 20, 0);
insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Smoothness', 1, 15, 4, 20, 0);
insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Line angles', 1, 15, 5, 20, 0);

insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Cavity design 1', 1, 16, 1, 21, 0);
insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Cavity design 2', 1, 16, 2, 21, 0);

insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Occlusal outline 1', 1, 17, 1, 22, 0);
insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Occlusal outline 2', 1, 17, 2, 22, 0);

insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Mesial cavity', 1, 18, 1, 23, 1);
insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Proximal outline 1', 1, 18, 2, 23, 0);
insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Proximal outline 2', 1, 18, 3, 23, 0);
insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Buccal wall', 1, 18, 4, 24, 0);
insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Lingual wall', 1, 18, 5, 24, 0);
insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Gingival wall', 1, 18, 6, 24, 0);
insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Axial wall', 1, 18, 7, 24, 0);
insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Internal line angles', 1, 18, 8, 24, 0);
insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Smoothness', 1, 18, 9, 24, 0);

insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Clamp position and stability', 1, 19, 1, 25, 0);
insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Prevention from the ingestion or aspiration of clamp', 1, 19, 2, 25, 0);
insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Frame position', 1, 19, 3, 25, 0);
insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Sheet position', 1, 19, 4, 25, 0);
insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Sealing quality 1', 1, 19, 5, 25, 0);
insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Sealing quality 2', 1, 19, 6, 25, 0);
insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Sealing quality 3', 1, 19, 7, 25, 0);
insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Sheet’s stability', 1, 19, 8, 25, 0);

insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Removal procedure', 1, 20, 1, 26, 0);

insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Matrix placement 1', 1, 21, 1, 27, 0);
insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Matrix placement 2', 1, 21, 2, 27, 0);
insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Matrix placement 3', 1, 21, 3, 27, 0);
insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Band position 1', 1, 21, 4, 27, 0);
insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Band position 2', 1, 21, 5, 27, 0);
insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Band position 3', 1, 21, 6, 27, 0);
insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Band position 4', 1, 21, 7, 27, 0);
insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Wedge Insertion 1', 1, 21, 8, 27, 0);
insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Wedge Insertion 2', 1, 21, 9, 27, 0);
insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Wedge Insertion 3', 1, 21, 10, 27, 0);

insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Bonding procedure', 1, 22, 1, 28, 0);
insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Occlusal margin', 1, 22, 2, 29, 0);
insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Gingival margin', 1, 22, 3, 29, 0);
insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Occlusal anatomy 1', 1, 22, 4, 29, 0);
insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Occlusal anatomy 2', 1, 22, 5, 29, 0);
insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Occlusal anatomy 3', 1, 22, 6, 29, 0);
insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Contact', 1, 22, 7, 29, 0);
insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Proximal 1', 1, 22, 8, 29, 0);
insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Proximal 2', 1, 22, 9, 29, 0);
insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Surface finish', 1, 22, 10, 29, 0);
insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Homogeneity 1', 1, 22, 11, 29, 0);
insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Homogeneity 2', 1, 22, 12, 29, 0);
insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Cleanliness', 1, 22, 13, 29, 0);

insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Occlusal margin', 1, 23, 1, 30, 0);
insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Gingival margin', 1, 23, 2, 30, 0);
insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Occlusal anatomy 1', 1, 23, 3, 30, 0);
insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Occlusal anatomy 2', 1, 23, 4, 30, 0);
insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Occlusal anatomy 3', 1, 23, 5, 30, 0);
insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Contact 1', 1, 23, 6, 30, 0);
insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Contact 2', 1, 23, 7, 30, 0);
insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Proximal 1', 1, 23, 8, 30, 0);
insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Proximal 2', 1, 23, 9, 30, 0);
insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Surface finish', 1, 23, 10, 30, 0);
insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Homogeneity 1', 1, 23, 11, 30, 0);
insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Homogeneity 2', 1, 23, 12, 30, 0);
insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Surface polishing', 1, 23, 13, 30, 0);
insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Occlusion', 1, 23, 14, 30, 0);
insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Cleanliness', 1, 23, 15, 30, 0);

insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Cavity design', 1, 24, 1, 31, 0);

insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Cavity outline 1', 1, 25, 1, 32, 0);
insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Cavity outline 2', 1, 25, 2, 32, 0);
insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Occlusal wall', 1, 25, 3, 33, 0);
insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Gingival wall', 1, 25, 4, 33, 0);
insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Mesial and distal walls', 1, 25, 5, 33, 0);
insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Line angles', 1, 25, 6, 33, 0);
insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Axial wall', 1, 25, 7, 34, 0);
insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Internal line angles', 1, 25, 8, 34, 0);
insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Occlusal cavosurface margin', 1, 25, 9, 34, 0);
insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Smoothness', 1, 25, 10, 34, 0);

insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Bonding procedure', 1, 26, 1, 35, 0);
insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Margin', 1, 26, 2, 36, 0);
insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Contour', 1, 26, 3, 36, 0);
insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Surface finish', 1, 26, 4, 36, 0);
insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Homogeneity 1', 1, 26, 5, 36, 0);
insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Homogeneity 2', 1, 26, 6, 36, 0);
insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Cleanliness', 1, 26, 7, 36, 0);

insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Margin', 1, 27, 1, 37, 0);
insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Contour', 1, 27, 2, 37, 0);
insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Surface finish', 1, 27, 3, 37, 0);
insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Homogeneity 1', 1, 27, 4, 37, 0);
insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Homogeneity 2', 1, 27, 5, 37, 0);
insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Surface polishing', 1, 27, 6, 37, 0);
insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Cleanliness', 1, 27, 7, 37, 0);

insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Cavity design', 1, 28, 1, 38, 0);

insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Cavity outline 1', 1, 29, 1, 39, 0);
insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Cavity outline 2', 1, 29, 2, 39, 0);
insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Occlusal wall', 1, 29, 3, 40, 0);
insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Gingival wall', 1, 29, 4, 40, 0);
insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Mesial and distal walls', 1, 29, 5, 40, 0);
insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Axial wall', 1, 29, 6, 41, 0);
insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Internal line angles', 1, 29, 7, 41, 0);
insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Cavosurface margin', 1, 29, 8, 41, 0);
insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Smoothness', 1, 29, 9, 41, 0);

insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Conditioning procedure', 1, 30, 1, 42, 0);
insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Placement of restorative material', 1, 30, 2, 42, 0);
insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Margin', 1, 30, 3, 42, 0);
insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Contour', 1, 30, 4, 42, 0);
insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Surface finish', 1, 30, 5, 42, 0);
insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Homogeneity 1', 1, 30, 6, 42, 0);
insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Homogeneity 2', 1, 30, 7, 42, 0);
insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Cleanliness', 1, 30, 8, 42, 0);

insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Margin', 1, 31, 1, 43, 0);
insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Contour', 1, 31, 2, 43, 0);
insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Surface finish', 1, 31, 3, 43, 0);
insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Homogeneity 1', 1, 31, 4, 43, 0);
insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Homogeneity 2', 1, 31, 5, 43, 0);
insert into sub_topic
    (name, statusId, topicId, sequence, criticalId, isLabel)
values
    ('Cleanliness', 1, 31, 6, 43, 0);


-- ---------------
-- criteria type--
-- ---------------
insert into criteria_type
    (name, statusId)
values
    ('Good', 1);

insert into criteria_type
    (name, statusId)
values
    ('Fair/Unacceptable', 1);

-- ---------------
-- criteria --
-- ---------------
insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Pit & grooves included
 - Smooth & free-flowing
 - Reverse curve
 - Angle of departure = 90°', 1, 1, 1, 1);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Pit & grooves included
 - Smooth curve
 - Free-flowing', 1, 1, 1, 2);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Reverse curve both mesial and distal aspects', 1, 1, 1, 3);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- 90° at mesial aspect both buccal and lingual margin', 1, 1, 1, 4);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- 90° at distal aspect both buccal and lingual margin', 1, 1, 1, 5);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- 1-1.5 mm', 1, 1, 1, 6);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('-  No area of the cavity wider than 1/3-1/2 from groove to cusp tip', 1, 1, 1, 7);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('-  Round internal line angle', 1, 1, 1, 8);


insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('-  Parallel to long axis of the tooth - Slightly convergent toward occlusal direction', 1, 1, 1, 10);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Parallel to long axis of the tooth - Slightly convergent toward occlusal direction', 1, 1, 1, 11);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- 1.5 - 2 mm', 1, 1, 1, 12);


insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('-  Smooth', 1, 1, 1, 14);


insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('-  Visually free of contact with an adjacent tooth -', 1, 1, 1, 16);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('-  Visually free of contact with an adjacent tooth', 1, 1, 1, 17);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('-  90° without unsupported enamel', 1, 1, 1, 18);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('-  90° without unsupported enamel', 1, 1, 1, 19);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('-  30° beveled without unsupported enamel', 1, 1, 1, 20);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Slightly convergent toward occlusal direction', 1, 1, 1, 21);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Slightly convergent toward occlusal direction', 1, 1, 1, 22);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- 1-1.5 mm width 
 - Equal width from buccal to lingual', 1, 1, 1, 23);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('-  Convex co-response to proximal contour', 1, 1, 1, 24);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('-  Well define internal line angles
-  Round internal line angles', 1, 1, 1, 25);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('-  Beveled axio-pulpal line angle 
 -  Well define axio-pulpal line angle', 1, 1, 1, 26);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('-  Smooth', 1, 1, 1, 27);


insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('-  Visually free of contact with an adjacent tooth -', 1, 1, 1, 29);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('-  Visually free of contact with an adjacent tooth', 1, 1, 1, 30);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('-  90° without unsupported enamel', 1, 1, 1, 31);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('-  90° without unsupported enamel', 1, 1, 1, 32);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('-  30° beveled without unsupported enamel', 1, 1, 1, 33);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('-  Slightly convergent toward occlusal direction', 1, 1, 1, 34);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('-  Slightly convergent toward occlusal direction', 1, 1, 1, 35);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- 1-1.5 mm width
 -  Equal width from buccal to lingual', 1, 1, 1, 36);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('-  Convex co-response to proximal contour', 1, 1, 1, 37);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('-  Well define internal line angles
 -  Round internal line angles', 1, 1, 1, 38);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('-  Beveled axio-pulpal line angle 
 -  Well define axio-pulpal line angle', 1, 1, 1, 39);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('-  Smooth', 1, 1, 1, 40);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('-  Proper direction of the boomerang-shaped band', 1, 1, 1, 41);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('-  Open end of the retainer head is placed toward the gingival', 1, 1, 1, 42);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Tofflemire retainer is placed at buccal aspect
 - Tofflemire retainer turns toward anterior teeth', 1, 1, 1, 43);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('-  Gingival portion of the band extends 0.5-1 mm below the gingival margin, both mesial and distal aspect', 1, 1, 1, 44);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('-  Occlusal portion of the band extends 1 – 2 mm above the marginal ridge of the adjacent teeth, both mesial and distal aspect', 1, 1, 1, 45);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('-  Band is adapted to proximal cavosuface margins without gap', 1, 1, 1, 46);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('-  Band is burnished to form the proximal contact areas - No gap between band and the adjacent teeth', 1, 1, 1, 47);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('-  Wedge is seated tightly in the gingival embrasure', 1, 1, 1, 48);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('-  Wedge is inserted from lingual embrasure', 1, 1, 1, 49);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('-  Tip of the wedge is placed toward occlusal aspect', 1, 1, 1, 50);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('-  Proper margin', 1, 1, 1, 51);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('-  No overhanging margin', 1, 1, 1, 52);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('-  Well defined anatomy - Proper groove position', 1, 1, 1, 53);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('-  Proper groove depth', 1, 1, 1, 54);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('-  Proper incline plane', 1, 1, 1, 55);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('-  Tight contact 
 -  Resistance to floss', 1, 1, 1, 56);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('-  Proper position both mesial and distal contact', 1, 1, 1, 57);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('-  Proper contour', 1, 1, 1, 58);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('-  Proper marginal ridge height', 1, 1, 1, 59);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('-  Smooth
 -  No void', 1, 1, 1, 60);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('-  Correct occlusion', 1, 1, 1, 61);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('-  Good margin', 1, 1, 1, 62);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('-  No overhanging margin', 1, 1, 1, 63);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('-  Well defined anatomy
 -  Proper groove position', 1, 1, 1, 64);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('-  Proper groove depth', 1, 1, 1, 65);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('-  Proper incline plane', 1, 1, 1, 66);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('-  Tight contact
 -  Resistance to floss', 1, 1, 1, 67);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('-  Proper position both mesial and distal contact', 1, 1, 1, 68);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('-  Proper contour', 1, 1, 1, 69);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('-  Proper marginal ridge height', 1, 1, 1, 70);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('-  Smooth 
 -  No void', 1, 1, 1, 71);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('-  Smooth and shiny', 1, 1, 1, 72);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Establish an ideal internal & external outline form and depth', 1, 1, 1, 73);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('Convenience, adequate access and visibility for removal of remaining carious lesion', 1, 1, 1, 74);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Use appropriate burs and instruments', 1, 1, 1, 75);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Caries at DEJ is completely removed', 1, 1, 1, 76);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Complete removal of caries infected dentin', 1, 1, 1, 77);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Caries-affected dentin is remained at the area closed to pulp', 1, 1, 1, 78);


insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Use appropriate burs and instruments to remove deep carious lesion', 1, 1, 1, 80);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Use appropriate instrument to check the remaining of carious lesion', 1, 1, 1, 81);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Clean operation field', 1, 1, 1, 82);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Properly placed with appropriate form & thickness', 1, 1, 1, 83);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Properly placed with appropriate form & thickness', 1, 1, 1, 84);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Establish an ideal cavity for amalgam restoration', 1, 1, 1, 85);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Axial wall is parallel to proximal contour of the tooth', 1, 1, 1, 86);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Cavosurface angle 90°', 1, 1, 1, 87);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- 45° axio-pulpal line angle beveled', 1, 1, 1, 88);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Pit & grooves included 
 - Distal margin parallel to oblique ridge 
 - Smooth & free-flowing', 1, 1, 1, 89);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Pit & grooves included 
 - Smooth curve 
 - Free-flowing', 1, 1, 1, 90);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- 80° - 100°', 1, 1, 1, 91);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- 1-2 mm', 1, 1, 1, 92);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Smooth', 1, 1, 1, 93);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Round and well define', 1, 1, 1, 94);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Smooth & free-flowing
 - Reverse curve', 1, 1, 1, 95);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Angle of departure = 80-100°', 1, 1, 1, 96);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Pit & grooves included - Smooth curve 
 - Free-flowing', 1, 1, 1, 97);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Angle of departure = 80-100° 
 - Tooth structure preservation', 1, 1, 1, 98);


insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Visually free of contact with an adjacent tooth', 1, 1, 1, 100);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Tooth structure preservation', 1, 1, 1, 101);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Slightly convergent toward occlusal direction or 
 - Parallel to long axis of the tooth', 1, 1, 1, 102);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('-Slightly convergent toward occlusal direction or 
 - Parallel to long axis of the tooth', 1, 1, 1, 103);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- 1-1.5 mm width 
 - Equal width from buccal to lingual', 1, 1, 1, 104);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Convex corresponds to proximal contour', 1, 1, 1, 105);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Well define internal line angles 
 - Round internal line angles', 1, 1, 1, 106);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Smooth', 1, 1, 1, 107);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- 4-point contact and 
 - Secured in place and
 - Stable', 1, 1, 1, 108);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Dental floss has been tied to the clamp', 1, 1, 1, 109);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Curvature of the frame concentric with the pt.’s face and 
 - Tie the floss to the frame', 1, 1, 1, 110);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- No excess dam material occlude pt.’s nasal airway and 
 - Adequate shield the pt.’s oral cavity', 1, 1, 1, 111);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- No shredded or torn dam and 
 - No “Pink” color in isolation area', 1, 1, 1, 112);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Dam intimately adapts to the teeth, cover and slightly retract the interdental tissue and 
 - Dam invert(seal) into gingival sulcus', 1, 1, 1, 113);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- No dam hanging at marginal ridges or distal of posterior anchor tooth', 1, 1, 1, 114);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- The sheet is kept in place by a fragment of the dam', 1, 1, 1, 115);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Correct methods 
 - Not damage tissue 
 - Complete rubber dam sheet, no portion of dam remain around teeth', 1, 1, 1, 116);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Proper direction and position of the band', 1, 1, 1, 117);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Place ring over the wedge', 1, 1, 1, 118);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Ring turns toward anterior teeth', 1, 1, 1, 119);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Gingival portion of the band extends 0.5-1 mm below the gingival margin', 1, 1, 1, 120);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Occlusal portion of the band extends 1 – 2 mm above the marginal ridge of the adjacent tooth', 1, 1, 1, 121);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Band is adapted to proximal cavosuface margins without gap', 1, 1, 1, 122);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Band is adapted to the adjacent tooth 
- No gap between band and the adjacent tooth', 1, 1, 1, 123);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Wedge is seated tightly in the gingival embrasure', 1, 1, 1, 124);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Proper size of wedge', 1, 1, 1, 125);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Tip of the wedge is placed toward occlusal aspect', 1, 1, 1, 126);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Properly performed 
 - No contamination', 1, 1, 1, 127);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Proper margin', 1, 1, 1, 128);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- No overhanging margin', 1, 1, 1, 129);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Well defined anatomy 
 - Proper groove position', 1, 1, 1, 130);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Proper groove depth', 1, 1, 1, 131);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Proper incline plane', 1, 1, 1, 132);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Tight contact 
- Resistance to floss
- Proper position', 1, 1, 1, 133);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Proper contour', 1, 1, 1, 134);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Proper marginal ridge height', 1, 1, 1, 135);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Smooth 
 - Free of irregularities', 1, 1, 1, 136);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Free of void', 1, 1, 1, 137);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- No premature polymerization', 1, 1, 1, 138);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- No contamination 
 - Clean', 1, 1, 1, 139);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Good marginal adaptation', 1, 1, 1, 140);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Good marginal adaptation 
 - No overhanging margin', 1, 1, 1, 141);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Well defined anatomy 
 - Proper groove position', 1, 1, 1, 142);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Proper groove depth', 1, 1, 1, 143);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Proper incline plane', 1, 1, 1, 144);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Tight contact 
 - Resistance to floss', 1, 1, 1, 145);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Proper position both mesial and distal contact', 1, 1, 1, 146);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Proper contour', 1, 1, 1, 147);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Proper marginal ridge height', 1, 1, 1, 148);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Smooth 
 - Free of irregularities', 1, 1, 1, 149);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Free of void', 1, 1, 1, 150);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- No premature polymerization', 1, 1, 1, 151);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Smooth and shiny', 1, 1, 1, 152);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Correct occlusion', 1, 1, 1, 153);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Clean', 1, 1, 1, 154);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Lesion included 
 - Smooth & free-flowing', 1, 1, 1, 155);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Extend from mesial to distal line angle 
 - Proper shape 
 - No sharp angle', 1, 1, 1, 156);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Tooth structure preservation', 1, 1, 1, 157);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- 1.5 mm depth and 
 - Perpendicular to external tooth surface', 1, 1, 1, 158);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- 1 mm width and 
 - Perpendicular to external tooth surface', 1, 1, 1, 159);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- 1 mm depth and 
 - Parallel to mesial and distal line angles', 1, 1, 1, 160);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Round and well define', 1, 1, 1, 161);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Convex co-response to buccal contour', 1, 1, 1, 162);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Well define internal line angles 
 - Round internal line angles', 1, 1, 1, 163);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- 45° beveled, 0.5-1 mm width', 1, 1, 1, 164);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Smooth', 1, 1, 1, 165);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Properly performed 
 - No contamination', 1, 1, 1, 166);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Proper margin', 1, 1, 1, 167);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Continuous with tooth contour', 1, 1, 1, 168);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Smooth 
 - Free of irregularities', 1, 1, 1, 169);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Free of void', 1, 1, 1, 170);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- No premature polymerization', 1, 1, 1, 171);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- No contamination 
 - Clean', 1, 1, 1, 172);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Good marginal adaptation', 1, 1, 1, 173);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Continuous with tooth contour', 1, 1, 1, 174);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Smooth 
 - Free of irregularities', 1, 1, 1, 175);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Free of void', 1, 1, 1, 176);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- No premature polymerization', 1, 1, 1, 177);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Smooth and shiny', 1, 1, 1, 178);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- No contamination 
 - Clean', 1, 1, 1, 179);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Lesion included 
 - Smooth & free-flowing', 1, 1, 1, 180);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Extend from mesial to distal line angle 
 - Proper shape 
 - No sharp angle', 1, 1, 1, 181);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Tooth structure preservation', 1, 1, 1, 182);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- 1.5 mm depth and 
 - Perpendicular to external tooth surface', 1, 1, 1, 183);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- 1 mm width and 
 - Perpendicular to external tooth surface', 1, 1, 1, 184);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- 1 mm depth and 
 - Parallel to mesial and distal line angles', 1, 1, 1, 185);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Convex co-response to buccal contour', 1, 1, 1, 186);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Well define internal line angles 
 - Round internal line angles', 1, 1, 1, 187);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Butt-joint margin', 1, 1, 1, 188);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Smooth', 1, 1, 1, 189);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Properly performed 
 - No contamination', 1, 1, 1, 190);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Material properly mixed and placed', 1, 1, 1, 191);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Proper margin', 1, 1, 1, 192);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Continuous with tooth contour', 1, 1, 1, 193);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Smooth 
 - Free of irregularities', 1, 1, 1, 194);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Free of void', 1, 1, 1, 195);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- No premature polymerization', 1, 1, 1, 196);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- No contamination 
 - Clean', 1, 1, 1, 197);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Good marginal adaptation', 1, 1, 1, 198);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Continuous with tooth contour
 - Proper anatomical form', 1, 1, 1, 199);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Smooth 
 - Free of irregularities', 1, 1, 1, 200);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Free of void', 1, 1, 1, 201);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- No premature polymerization', 1, 1, 1, 202);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- No contamination 
 - Clean', 1, 1, 1, 203);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Grossly over extended
 - Over extended
 - Under extended
 - No reverse curve
 - Sharp angle
 - Angle of departure < or >90°', 2, 0, 1, 1);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Grossly over extended
 - Over extended
 - Under extended
 - Sharp angle', 2, 0, 1, 2);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- No reverse curve
 - Reverse curve only at mesial or distal aspect', 2, 0, 1, 3);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- 90° at mesial aspect only at buccal or lingual margin
 - Grossly over extended
 - Slightly over extended / under extended
 - Angle of departure < or >90°', 2, 0, 1, 4);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- 90° at distal aspect only at buccal or lingual margin
 - Grossly over extended
 - Slightly over extended / under extended 
 - Angle of departure < or >90°', 2, 0, 1, 5);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Too wide (more than ½ intercuspal width) 
 - Extended (> ½ way to cuspal ridge) 
 - Wide (> 1/3 but no more than ½ intercuspal width) 
 - Too narrow', 2, 0, 1, 6);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Some area of the cavity wider than 1/3-1/2 from groove to cusp tip 
 - Extended >1/2 way to cusp tip', 2, 0, 1, 7);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Sharp internal line angle
 - No internal line angle', 2, 0, 1, 8);


insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Divergent toward occlusal direction 
 - Not parallel to long axis of the tooth 
 - Grossly convergent toward occlusal direction', 2, 0, 1, 10);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Divergent toward occlusal direction 
- Not parallel to long axis of the tooth 
- Grossly convergent toward occlusal direction
', 2, 0, 1, 11);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Deeper than 2 mm
 - Shallower than 1.5 mm', 2, 0, 1, 12);


insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Not smooth', 2, 0, 1, 14);


insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Free of contact only buccal or lingual margin 
 - Contact with an adjacent tooth 
 - More than 0.5 mm free of contact 
 - Grossly over extended', 2, 0, 1, 16);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Free of contact only buccal or lingual margin 
 - Contact with an adjacent tooth 
 - More than 0.5 mm free of contact 
 - Grossly over extended', 2, 0, 1, 17);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- < or >90° 
 - Unsupported enamel is presented', 2, 0, 1, 18);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- < or >90° 
 - Unsupported enamel is presented', 2, 0, 1, 19);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- < or >90° 
 - Unsupported enamel is presented', 2, 0, 1, 20);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Divergent toward occlusal direction 
 - Parallel to long axis of the tooth 
 - Grossly convergent toward occlusal direction', 2, 0, 1, 21);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Divergent toward occlusal direction 
 - Parallel to long axis of the tooth 
 - Grossly convergent toward occlusal direction', 2, 0, 1, 22);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Narrower than 1 mm width 
 - Wider than 1.5 mm width
 - Unequal width from buccal to lingual', 2, 0, 1, 23);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Too convex 
 - Flat or concave 
 - Too concave or too flat', 2, 0, 1, 24);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Poor define internal line angles', 2, 0, 1, 25);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Poor define axio-pulpal line angle 
 - Sharp axio-pulpal line angle', 2, 0, 1, 26);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Not smooth', 2, 0, 1, 27);


insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Free of contact only buccal or lingual margin 
 -  Contact with an adjacent tooth 
 - More than 0.5 mm free of contact 
 - Grossly over extended', 2, 0, 1, 29);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Free of contact only buccal or lingual margin 
 - Contact with an adjacent tooth 
 - More than 0.5 mm free of contact 
 - Grossly over extended', 2, 0, 1, 30);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- < or >90° 
 - Unsupported enamel is presented', 2, 0, 1, 31);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- < or >90° 
 - Unsupported enamel is presented', 2, 0, 1, 32);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- < or >90° 
 - Unsupported enamel is presented', 2, 0, 1, 33);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Divergent toward occlusal direction 
 - Parallel to long axis of the tooth 
 - Grossly convergent toward occlusal direction', 2, 0, 1, 34);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Divergent toward occlusal direction 
 - Parallel to long axis of the tooth 
 - Grossly convergent toward occlusal direction', 2, 0, 1, 35);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Narrower than 1 mm width 
 - Wider than 1.5 mm width 
 - Unequal width from buccal to lingual', 2, 0, 1, 36);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Too convex 
 - Flat or concave 
 - Too concave or too flat', 2, 0, 1, 37);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Poor define internal line angles', 2, 0, 1, 38);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Poor define axio-pulpal line angle 
 - Sharp axio-pulpal line angle', 2, 0, 1, 39);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Not smooth', 2, 0, 1, 40);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Improper direction of the boomerang-shaped band', 2, 0, 1, 41);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Open end of the retainer head is placed toward the occlusal', 2, 0, 1, 42);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Tofflemire retainer is placed at lingual aspect 
 - Tofflemire retainer turns toward posterior teeth', 2, 0, 1, 43);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Gingival portion of the band does not extend below the gingival margin at mesial or distal aspect
  - Gingival portion of the band does not extends below the gingival margin, both mesial and distal aspects', 2, 0, 1, 44);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Occlusal portion of the band extends less than 1 mm above the marginal ridge of the adjacent tooth, at mesial and/or distal aspect
  - Occlusal portion of the band extends more than 2 mm above the marginal ridge of the adjacent tooth, at mesial and/or distal aspect', 2, 0, 1, 45);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Gap is presented between band and proximal cavosuface margin', 2, 0, 1, 46);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Gap is presented between band and the adjacent teeth', 2, 0, 1, 47);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Wedge is seated loosely in the gingival embrasure', 2, 0, 1, 48);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Wedge is inserted from buccal embrasure', 2, 0, 1, 49);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Tip of the wedge is placed toward gingival aspect at messiah and/or distal aspect', 2, 0, 1, 50);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Under margin 
 - Over margin', 2, 0, 1, 51);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Overhanging margin', 2, 0, 1, 52);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Poor defined anatomy
 - Improper groove position', 2, 0, 1, 53);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Shallow / deep groove', 2, 0, 1, 54);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Over inclination 
 - Under inclination', 2, 0, 1, 55);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Loose or loss of contact, no resistance to floss 
 - Too tight contact, impossible to carve', 2, 0, 1, 56);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Wide / narrow contact area', 2, 0, 1, 57);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Flat / concave 
 - Too convex', 2, 0, 1, 58);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Improper marginal ridge height', 2, 0, 1, 59);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Not smooth 
 - Void', 2, 0, 1, 60);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- High in occlusion
 - Under occlusion', 2, 0, 1, 61);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Under margin 
 - Over margin', 2, 0, 1, 62);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Overhanging margin', 2, 0, 1, 63);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Poor defined anatomy
 - Improper groove position', 2, 0, 1, 64);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Shallow / deep groove', 2, 0, 1, 65);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Under / over inclination', 2, 0, 1, 66);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Loose or loss of contact, no resistance to floss 
 - Too tight contact, impossible to polish', 2, 0, 1, 67);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Wide / narrow contact area', 2, 0, 1, 68);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Flat / concave 
 - Too convex', 2, 0, 1, 69);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Improper marginal ridge height', 2, 0, 1, 70);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Rough or scratched
 - Void', 2, 0, 1, 71);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Not shiny surface polishing', 2, 0, 1, 72);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Over extended 
 - Under extended 
 - Grossly over / under extended', 2, 0, 1, 73);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Over extended 
 - Under extended 
 - Grossly over / under extended', 2, 0, 1, 74);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('Improper use of burs and/or instruments', 2, 0, 1, 75);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Caries at DEJ is remained 
 - Gross caries at DEJ is remained', 2, 0, 1, 76);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Caries-infected dentin is remained', 2, 0, 1, 77);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Unnecessary removal of sound tooth structure', 2, 0, 1, 78);


insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Improper use of burs and/or instruments to remove deep carious lesion', 2, 0, 1, 80);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Improper use of burs and/or instruments to remove deep carious lesion', 2, 0, 1, 81);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Not clean', 2, 0, 1, 82);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Improper form & thickness 
 - Fail to protect pulp 
 - Poorly handling & placement technique', 2, 0, 1, 83);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Improper form & thickness 
 - Fail to protect pulp 
 - Poorly handling & placement technique', 2, 0, 1, 84);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Improper cavity preparation for amalgam 
 - Slightly deeper or shallower than the ideal depth', 2, 0, 1, 85);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Not parallel to proximal contour', 2, 0, 1, 86);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Cavosurface angles are more / less than 90°', 2, 0, 1, 87);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- No beveled axio-pulpal line angle
 - Not 45° axio-pulpal line angle beveled', 2, 0, 1, 88);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Grossly over extended 
 - Over / under extended 
 - Sharp angle', 2, 0, 1, 89);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Over extended 
 - Under extended 
 - Sharp angle', 2, 0, 1, 90);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Less than 80° 
 - More than 100°', 2, 0, 1, 91);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Less than 1 mm 
 - More than 2 mm', 2, 0, 1, 92);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Not smooth', 2, 0, 1, 93);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Sharp and/or poor define', 2, 0, 1, 94);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Grossly over extended 
 - Over extended 
 - Under extended 
 - No reverse curve
 - Sharp angle', 2, 0, 1, 95);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Angle of departure <80°or >100° at buccal and/or lingual margin', 2, 0, 1, 96);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Over extended 
 - Under extended 
 - Sharp angle', 2, 0, 1, 97);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Angle of departure <80°or >100°and/or 
 - Over extended (proximal clearance more than 0.5 mm)', 2, 0, 1, 98);


insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Free of contact only buccal or lingual margin or gingival 
 - Contact with an adjacent tooth', 2, 0, 1, 100);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Over extended 
 - More than 0.5 mm free of contact', 2, 0, 1, 101);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Divergent toward occlusal direction 
 - Grossly convergent toward occlusal direction', 2, 0, 1, 102);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Divergent toward occlusal direction 
 - Grossly convergent toward occlusal direction', 2, 0, 1, 103);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Narrower than 1 mm width
  - Wider than 1.5 mm width 
 - Unequal width from buccal to lingual', 2, 0, 1, 104);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Too convex 
 - Concave 
 - Flat', 2, 0, 1, 105);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Poor define internal line angles', 2, 0, 1, 106);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Not smooth', 2, 0, 1, 107);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- No 4-point contact and/or 
 - Not secured in place and/ or 
 - Clamp pinch tissue and/or 
 - Not below height of contour and/or 
 - Unstable', 2, 0, 1, 108);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Dental floss has not been tied to the clamp', 2, 0, 1, 109);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Frame is not concentric but can access to operating area and/or 
 - Inconvenient to access operating area and/or  
 - Frame position near pt.’s eyes', 2, 0, 1, 110);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Dam occlude pt.’s nasal airway and/or 
 - Inadequate shield the pt.’s oral cavity and/or 
 - Inappropriate distance between hole', 2, 0, 1, 111);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Shredded or torn dam and/or 
 - “Pink” color in isolation area', 2, 0, 1, 112);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Dam does not adapt to the teeth and tissue and/or 
 - Dam doesn’t invert(seal) into gingival sulcus', 2, 0, 1, 113);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Dam hanging at marginal ridges or distal of posterior anchor tooth', 2, 0, 1, 114);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- The sheet is not kept in place by a fragment of the dam', 2, 0, 1, 115);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Incorrect methods and/or - Tissues damage and/or 
 - Remnant of rubber dam sheet remains around teeth', 2, 0, 1, 116);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Improper direction and/or position of the band', 2, 0, 1, 117);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Improper position of ring over the wedge', 2, 0, 1, 118);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Ring turns toward posterior teeth', 2, 0, 1, 119);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Gingival portion of the band does not extends below the gingival margin', 2, 0, 1, 120);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Occlusal portion of the band extends less than 1 mm above the marginal ridge of the adjacent tooth
- Occlusal portion of the band extends more than 2 mm above the marginal ridge of the adjacent tooth', 2, 0, 1, 121);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Gap is presented between band and proximal cavosuface margin', 2, 0, 1, 122);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Gap is presented between band and the adjacent tooth', 2, 0, 1, 123);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Wedge is seated loosely in the gingival embrasure', 2, 0, 1, 124);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Too large / too small wedge', 2, 0, 1, 125);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Tip of the wedge is placed toward gingival aspect', 2, 0, 1, 126);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Poorly handling materials - Contamination', 2, 0, 1, 127);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Under margin 
 - Over margin', 2, 0, 1, 128);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Overhanging margin', 2, 0, 1, 129);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Poor defined anatomy and/or 
 - Improper groove position', 2, 0, 1, 130);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Shallow / deep groove', 2, 0, 1, 131);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Over inclination 
 - Under inclination', 2, 0, 1, 132);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Loose or loss of contact, no resistance to floss and/or 
- Too tight contact, difficult to polish
- Wide / narrow contact area', 2, 0, 1, 133);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Flat / concave 
 - Too convex', 2, 0, 1, 134);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Improper marginal ridge height', 2, 0, 1, 135);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Rough and/or 
 - Scratched, irregularities', 2, 0, 1, 136);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Void', 2, 0, 1, 137);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Premature polymerization', 2, 0, 1, 138);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Contamination 
 - Not clean', 2, 0, 1, 139);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Under margin 
 - Over margin', 2, 0, 1, 140);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Overhanging margin 
 - Under margin', 2, 0, 1, 141);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Poor defined anatomy and/or 
 - Improper groove position', 2, 0, 1, 142);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Shallow / deep groove', 2, 0, 1, 143);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Over inclination 
 - Under inclination', 2, 0, 1, 144);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Loose or loss of contact, no resistance to floss and/or 
 - Too tight contact, difficult to polish', 2, 0, 1, 145);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Wide / narrow contact area', 2, 0, 1, 146);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Flat / concave 
 - Too convex', 2, 0, 1, 147);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Improper marginal ridge height', 2, 0, 1, 148);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Rough and/or 
 - Scratched, irregularities', 2, 0, 1, 149);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Void', 2, 0, 1, 150);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Premature polymerization', 2, 0, 1, 151);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Dull, rough surface 
 - Not shiny surface polishing', 2, 0, 1, 152);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- High in occlusion 
 - Under occlusion', 2, 0, 1, 153);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Not clean', 2, 0, 1, 154);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Over extended 
 - Under extended 
 - Sharp angle', 2, 0, 1, 155);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Incorrect shape 
 - Under extended 
 - Sharp angle', 2, 0, 1, 156);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Over extended', 2, 0, 1, 157);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Shallower or deeper than 1.5 mm depth and/or 
 - Not perpendicular to tooth surface', 2, 0, 1, 158);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- More or less than 1 mm width and/or 
 - Not perpendicular to external tooth surface', 2, 0, 1, 159);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('-  More or less than 1 mm depth and/or 
 - Not parallel to mesial and/or distal line angles', 2, 0, 1, 160);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Sharp and/or poor define', 2, 0, 1, 161);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Flat 
 - Concave 
 - Too convex', 2, 0, 1, 162);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Poor define internal line angles', 2, 0, 1, 163);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Inadequate beveled 
 - Overpreparation', 2, 0, 1, 164);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Not smooth', 2, 0, 1, 165);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Poorly handling materials 
 - Contamination', 2, 0, 1, 166);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Under margin 
 - Over margin', 2, 0, 1, 167);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Over contour 
 - Under contour', 2, 0, 1, 168);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Rough and/or 
 - Scratched, irregularities', 2, 0, 1, 169);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Void', 2, 0, 1, 170);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Premature polymerization', 2, 0, 1, 171);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Contamination 
 - Not clean', 2, 0, 1, 172);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Under margin 
 - Over margin', 2, 0, 1, 173);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Over contour 
 - Under contour', 2, 0, 1, 174);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Rough and/or 
 - Scratched, irregularities', 2, 0, 1, 175);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Void', 2, 0, 1, 176);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Premature polymerization', 2, 0, 1, 177);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Dull, rough surface 
 - Not shiny surface', 2, 0, 1, 178);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Contamination 
 - Not clean', 2, 0, 1, 179);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Over extended 
 - Under extended 
 - Sharp angle', 2, 0, 1, 180);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Incorrect shape 
 - Under extended 
 - Sharp angle', 2, 0, 1, 181);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Over extended', 2, 0, 1, 182);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Shallower or deeper than 1.5 mm depth and/or 
 - Not perpendicular to tooth surface', 2, 0, 1, 183);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- More or less than 1 mm width and/or 
 - Not perpendicular to external tooth surface', 2, 0, 1, 184);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('-  More or less than 1 mm depth and/or 
 - Not parallel to mesial and/or distal line angles', 2, 0, 1, 185);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Flat 
 - Concave 
 - Too convex', 2, 0, 1, 186);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Poor define internal line angles', 2, 0, 1, 187);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Beveled margin', 2, 0, 1, 188);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Not smooth', 2, 0, 1, 189);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Poorly handling materials 
 - Contamination', 2, 0, 1, 190);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Poorly handling the materials during placement procedures', 2, 0, 1, 191);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Under margin 
 - Over margin', 2, 0, 1, 192);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Over contour 
 - Under contour', 2, 0, 1, 193);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Rough and/or 
 - Scratched, irregularities', 2, 0, 1, 194);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Void', 2, 0, 1, 195);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Premature polymerization', 2, 0, 1, 196);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Contamination 
 - Not clean', 2, 0, 1, 197);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Under margin 
 - Over margin', 2, 0, 1, 198);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Over contour 
 - Under contour
 - No anatomical features/ loss of proper contour', 2, 0, 1, 199);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Rough and/or 
 - Scratched, irregularities', 2, 0, 1, 200);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Void', 2, 0, 1, 201);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Premature polymerization', 2, 0, 1, 202);

insert into criteria
    (name, criteriaTypeId, score, statusId, subTopicId)
values
    ('- Contamination 
 - Not clean', 2, 0, 1, 203);
